package com.ideas.ngi.functionspace.config;

import com.ideas.ngi.nucleus.config.mongo.repository.NucleusMongoRepositoryImpl;
import com.ideas.ngi.nucleus.config.threading.ThreadingProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.metrics.export.datadog.DatadogMetricsExportAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.metrics.web.servlet.WebMvcMetricsAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.config.EnableMessageHistory;
import org.springframework.jmx.export.MBeanExporter;
import org.springframework.jmx.support.RegistrationPolicy;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.StringValueResolver;

import java.time.Clock;
import java.util.HashMap;
import java.util.Map;

/**
 * Defines the Spring runtime configuration for Spring-Boot and the Spring container.
 */
@Configuration
@SpringBootApplication(scanBasePackages = "com.ideas", exclude = { MongoAutoConfiguration.class, DatadogMetricsExportAutoConfiguration.class, WebMvcMetricsAutoConfiguration.class })
@EnableIntegration
@IntegrationComponentScan("com.ideas")
@EnableMessageHistory
@EnableTransactionManagement
@EnableMongoRepositories(basePackages = {"com.ideas"}, repositoryBaseClass = NucleusMongoRepositoryImpl.class)
@EnableRetry
public class FunctionSpaceApplicationConfig {

    @Autowired
    private ThreadingProperties threadingProperties;

    protected FunctionSpaceApplicationConfig() {
    }

    public static void main(String[] args) {
        SpringApplication.run(FunctionSpaceApplicationConfig.class, args);
    }

    /**
     * Defines what the current time is.
     * Should be injected into, and used by any class that needs the current time.
     * Tests can then set the field value to a non-ticking clock (FixedClock) when time-based testing is required.
     */
    @Bean
    public Clock clock() {
        return Clock.systemDefaultZone();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer() {
            @Override
            protected void doProcessProperties(ConfigurableListableBeanFactory beanFactoryToProcess, StringValueResolver valueResolver) {
                super.doProcessProperties(beanFactoryToProcess, strVal -> {
                    String value = valueResolver.resolveStringValue(strVal);
                    return trimValue(value);
                });
            }
        };
    }

    private static String trimValue(String value) {
        return value == null ? null : value.trim().replaceAll("\\t", "");
    }

    @Bean(name = "nucleusThreadPoolTaskExecutor")
    @Primary
    public ThreadPoolTaskExecutor nucleusThreadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setThreadNamePrefix("NucleusThreadExecutor-");
        executor.setCorePoolSize(threadingProperties.getCorePoolSize());
        executor.setMaxPoolSize(threadingProperties.getMaxPoolSize());
        executor.setQueueCapacity(threadingProperties.getQueueCapacity());
        executor.setKeepAliveSeconds(threadingProperties.getKeepAliveSeconds());

        return executor;
    }

    @Bean
    @Primary
    protected MBeanExporter nucleusThreadPoolExporter(@Qualifier("nucleusThreadPoolTaskExecutor") ThreadPoolTaskExecutor nucleusThreadPoolTaskExecutor) {
        MBeanExporter exporter = new MBeanExporter();
        exporter.setRegistrationPolicy(RegistrationPolicy.IGNORE_EXISTING);
        Map<String, Object> beans = new HashMap<>();

        beans.put("com.ideas.ngi.nucleus:type=taskExecutor,name=nucleusThreadPoolTaskExecutor", nucleusThreadPoolTaskExecutor);
        beans.put("com.ideas.ngi.nucleus:type=executor,name=nucleusThreadPoolExecutor", nucleusThreadPoolTaskExecutor.getThreadPoolExecutor());
        beans.put("com.ideas.ngi.nucleus:type=queue,name=nucleusLinkedBlockingQueue", nucleusThreadPoolTaskExecutor.getThreadPoolExecutor().getQueue());

        exporter.setBeans(beans);
        return exporter;
    }
}
