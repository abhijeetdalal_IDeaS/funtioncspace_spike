package com.ideas.ngi.functionspace.config;

import com.ideas.ngi.nucleus.data.correlation.endpoint.CorrelationMetadataEndpoint;
import com.ideas.ngi.nucleus.data.functionspace.entity.FSNotificationTransformer;
import com.ideas.ngi.nucleus.integration.NucleusIntegrationComponent;
import com.ideas.ngi.nucleus.integration.router.NucleusReturnChannelHeaderRouter;
import com.ideas.ngi.nucleus.integration.util.MessageHeaderUtil;
import com.ideas.ngi.nucleus.util.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.router.RecipientListRouter;
import org.springframework.messaging.MessageChannel;

@Configuration
public class FSNotificationConfig {

    public static final String FS_NOTIFICATION_SEND_CHANNEL = "fsNotificationSendChannel";
    public static final String FS_NOTIFICATION_SEND_ROUTER_CHANNEL = "fsNotificationSendRouterChannel";

    @Autowired
    private NucleusIntegrationComponent nucleusIntegrationComponent;

    @Autowired
    private NucleusReturnChannelHeaderRouter nucleusReturnChannelHeaderRouter;

    @Autowired
    @Qualifier("allInternalClientsRestClientRouter")
    private RecipientListRouter allInternalClientsRestClientRouter;

    @Bean(name = FS_NOTIFICATION_SEND_CHANNEL)
    public MessageChannel fsSendEventChannel() {
        return nucleusIntegrationComponent.amqpChannel(FS_NOTIFICATION_SEND_CHANNEL);
    }

    @Bean(name = FS_NOTIFICATION_SEND_ROUTER_CHANNEL)
    public MessageChannel fsSendEventRouterChannel() {
        return nucleusIntegrationComponent.amqpPublishSubscribeChannel(FS_NOTIFICATION_SEND_ROUTER_CHANNEL);
    }

    @Bean
    public IntegrationFlow fsSendEventFlow() {
        return nucleusIntegrationComponent
                .from(FS_NOTIFICATION_SEND_CHANNEL)
                .channel(FS_NOTIFICATION_SEND_ROUTER_CHANNEL)
                .route(nucleusReturnChannelHeaderRouter)
                .get();
    }

    @Bean
    public IntegrationFlow fsSendEventRouterFlow(FSNotificationTransformer fsNotificationTransformer) {
        return nucleusIntegrationComponent
                .from(FS_NOTIFICATION_SEND_ROUTER_CHANNEL)
                .enrichHeaders(nucleusIntegrationComponent.buildHeaders(MapBuilder.with(MessageHeaderUtil.REST_ENDPOINT, CorrelationMetadataEndpoint.POST_EVENT).get()))
                .transform(fsNotificationTransformer)
                .route(allInternalClientsRestClientRouter)
                .get();

    }
}
