package com.ideas.ngi.functionspace.config;

import com.ideas.ngi.functionspace.dto.FunctionSpaceDeletionDto;
import com.ideas.ngi.functionspace.service.*;
import com.ideas.ngi.nucleus.config.mongo.repository.NucleusMongoRepositoryImpl;
import com.ideas.ngi.nucleus.data.correlation.repository.NucleusCorrelationMetadataRepository;
import com.ideas.ngi.nucleus.integration.NucleusIntegrationComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.metrics.export.datadog.DatadogMetricsExportAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.metrics.web.servlet.WebMvcMetricsAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.config.EnableMessageHistory;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.messaging.MessageChannel;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
public class FunctionSpaceConfig {

    public static final String FUNCTION_SPACE_DELETE_CHANNEL = "functionSpaceDeleteChannel";

    @Autowired
    private FunctionSpaceEventTypeService eventTypeService;
    @Autowired
    private FunctionSpaceMarketSegmentService marketSegmentService;
    @Autowired
    private FunctionSpaceFunctionRoomService functionRoomService;
    @Autowired
    private FunctionSpaceBookingService bookingService;
    @Autowired
    private FunctionSpaceBookingGuestRoomService guestRoomService;
    @Autowired
    private FunctionSpaceBookingPaceService bookingPaceService;
    @Autowired
    private FunctionSpaceGuestRoomPaceService guestRoomPaceService;
    @Autowired
    private FunctionSpaceEventService eventService;
    @Autowired
    private NucleusCorrelationMetadataRepository correlationMetadataRepository;

    @Autowired
    private NucleusIntegrationComponent nucleusIntegrationComponent;
    
    @Bean(name = FUNCTION_SPACE_DELETE_CHANNEL)
    public MessageChannel functionSpaceDeleteChannel() {
        return nucleusIntegrationComponent.direct(FUNCTION_SPACE_DELETE_CHANNEL);
    }
    
    @Bean
    public IntegrationFlow functionSpaceDeleteFlow() {
        return nucleusIntegrationComponent 
                .from(FUNCTION_SPACE_DELETE_CHANNEL)
                .<FunctionSpaceDeletionDto>handle((p, h) -> p.addNumberOfEventsDeleted(eventService.delete(p.getClientCode(), p.getPropertyCode())))
                .<FunctionSpaceDeletionDto>handle((p, h) -> p.addNumberOfBookingPaceGuestRoomsDeleted(guestRoomPaceService.delete(p.getClientCode(), p.getPropertyCode())))
                .<FunctionSpaceDeletionDto>handle((p, h) -> p.addNumberOfBookingPacesDeleted(bookingPaceService.delete(p.getClientCode(), p.getPropertyCode())))
                .<FunctionSpaceDeletionDto>handle((p, h) -> p.addNumberOfBookingGuestRoomsDeleted(guestRoomService.delete(p.getClientCode(), p.getPropertyCode())))
                .<FunctionSpaceDeletionDto>handle((p, h) -> p.addNumberOfBookingsDeleted(bookingService.delete(p.getClientCode(), p.getPropertyCode())))
                .<FunctionSpaceDeletionDto>handle((p, h) -> p.addNumberOfFunctionRoomsDeleted(functionRoomService.delete(p.getClientCode(), p.getPropertyCode())))
                .<FunctionSpaceDeletionDto>handle((p, h) -> p.addNumberOfMarketSegmentsDeleted(marketSegmentService.delete(p.getClientCode(), p.getPropertyCode())))
                .<FunctionSpaceDeletionDto>handle((p, h) -> p.addNumberOfEventTypesDeleted(eventTypeService.delete(p.getClientCode(), p.getPropertyCode())))
                .<FunctionSpaceDeletionDto>handle((p, h) -> p.addNumberOfCorrelationMetadataDeleted(correlationMetadataRepository.deleteByIntegrationTypeAndSendingSystemPropertyId(p.getIntegrationType(), p.getPropertyId())))
                .get();
    }
}
