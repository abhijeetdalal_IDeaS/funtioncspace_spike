package com.ideas.ngi.functionspace.repository;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEventType;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

/**
 * FunctionSpaceEventTypeRepository is used for CRUD operations on the nucleus FunctionSpaceEventType
 * entity and is exposes the collection via REST.
 */
@RepositoryRestResource
public interface NucleusFunctionSpaceEventTypeRepository extends NucleusFunctionSpaceRepository<NucleusFunctionSpaceEventType> {

    @RestResource(exported = false, path = "findOneByClientCodeAndPropertyCodeAndAbbreviation", rel = "findOneByClientCodeAndPropertyCodeAndAbbreviation")
    NucleusFunctionSpaceEventType findFirstByClientCodeAndPropertyCodeAndAbbreviation(
            String clientCode,
            String propertyCode,
            String abbreviation);

    @RestResource(exported = false)
    List<NucleusFunctionSpaceEventType> findByClientCodeAndPropertyCodeOrderByAbbreviationAsc(
            String clientCode,
            String propertyCode);
}
