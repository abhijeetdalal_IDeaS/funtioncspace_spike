package com.ideas.ngi.functionspace.repository;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceGuestRoomPace;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * FunctionSpaceGuestRoomPaceRepository is used for CRUD operations on the nucleus FunctionSpaceGuestRoomPace
 * entity and is exposes the collection via REST.
 */
@RepositoryRestResource
public interface NucleusFunctionSpaceGuestRoomPaceRepository extends NucleusFunctionSpaceRepository<NucleusFunctionSpaceGuestRoomPace> {

    @RestResource(exported = false, path = "findOneByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateAndBookingPaceId", rel = "findOneByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateAndBookingPaceId")
    NucleusFunctionSpaceGuestRoomPace findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateAndBookingPaceId(
            String clientCode,
            String propertyCode,
            String bookingId,
            String roomType,
            LocalDate stayDate,
            String bookingPaceId);

    @RestResource(exported = false)
    List<NucleusFunctionSpaceGuestRoomPace> findByClientCodeAndPropertyCodeAndBookingId(
            String clientCode,
            String propertyCode,
            String bookingId);

    @RestResource(exported = false)
    List<NucleusFunctionSpaceGuestRoomPace> findByClientCodeAndPropertyCodeOrderByBookingIdAscBookingPaceIdAsc(
            String clientCode,
            String propertyCode);

    @RestResource(exported = false)
    NucleusFunctionSpaceGuestRoomPace findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateOrderByChangeDateDesc(
            String clientCode,
            String propertyCode,
            String bookingId,
            String roomType,
            LocalDate stayDate);

    @RestResource(exported = false)
    NucleusFunctionSpaceGuestRoomPace findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateAndChangeDateLessThanOrderByChangeDateDesc(
            String clientCode,
            String propertyId,
            String bookingId,
            String roomType,
            Date stayDate,
            Date changeDate);

    @RestResource(exported = false)
    void deleteByCorrelationId(String correlationId);

    @RestResource(exported = false)
    NucleusFunctionSpaceGuestRoomPace findFirstByClientCodeAndPropertyCodeOrderByChangeDateDesc(String clientCode, String propertyCode);
}
