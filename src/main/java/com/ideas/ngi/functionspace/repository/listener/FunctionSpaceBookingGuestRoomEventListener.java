package com.ideas.ngi.functionspace.repository.listener;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBookingGuestRoom;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

@Component
public class FunctionSpaceBookingGuestRoomEventListener extends AbstractMongoEventListener<NucleusFunctionSpaceBookingGuestRoom> {

	@Override
	public void onBeforeConvert(BeforeConvertEvent<NucleusFunctionSpaceBookingGuestRoom> event) {
		event.getSource().hasBookedRooms();
	}
}
