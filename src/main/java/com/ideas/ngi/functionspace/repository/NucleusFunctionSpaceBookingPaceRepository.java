package com.ideas.ngi.functionspace.repository;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBookingPace;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Date;
import java.util.List;

/**
 * FunctionSpaceBookingPaceRepository is used for CRUD operations on the nucleus FunctionSpaceBookingPace
 * entity and is exposes the collection via REST.
 */
@RepositoryRestResource
public interface NucleusFunctionSpaceBookingPaceRepository extends NucleusFunctionSpaceRepository<NucleusFunctionSpaceBookingPace> {

    @RestResource(exported = false, path = "findOneByClientCodeAndPropertyCodeAndBookingIdAndBookingPaceId", rel = "findOneByClientCodeAndPropertyCodeAndBookingIdAndBookingPaceId")
    NucleusFunctionSpaceBookingPace findFirstByClientCodeAndPropertyCodeAndBookingIdAndBookingPaceId(
            String clientCode,
            String propertyCode,
            String bookingId,
            String bookingPaceId);

    @RestResource(exported = false)
    List<NucleusFunctionSpaceBookingPace> findByClientCodeAndPropertyCodeAndBookingId(
            String clientCode,
            String propertyCode,
            String bookingId);

    @RestResource(exported = false)
    List<NucleusFunctionSpaceBookingPace> findByClientCodeAndPropertyCodeOrderByBookingIdAscBookingPaceIdAsc(
            String clientCode,
            String propertyCode);

    @RestResource(path = "findOneByClientCodeAndPropertyCodeAndBookingIdOrderByChangeDateDesc", rel = "findOneByClientCodeAndPropertyCodeAndBookingIdOrderByChangeDateDesc")
    NucleusFunctionSpaceBookingPace findFirstByClientCodeAndPropertyCodeAndBookingIdOrderByChangeDateDesc(
            String clientCode,
            String propertyCode,
            String bookingId);

    @RestResource(path = "findOneByClientCodeAndPropertyCodeAndBookingIdAndChangeDateLessThanEqualOrderByChangeDateDesc", rel = "findOneByClientCodeAndPropertyCodeAndBookingIdAndChangeDateLessThanEqualOrderByChangeDateDesc")
    NucleusFunctionSpaceBookingPace findFirstByClientCodeAndPropertyCodeAndBookingIdAndChangeDateLessThanEqualOrderByChangeDateDesc(
            String clientCode,
            String propertyCode,
            String bookingId,
            Date changeDate);
}
