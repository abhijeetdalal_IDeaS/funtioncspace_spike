package com.ideas.ngi.functionspace.repository;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEventPace;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * FunctionSpaceEventPaceRepository is used for CRUD operations on the nucleus FunctionSpaceEventPace
 * entity and is exposes the collection via REST.
 */
@RepositoryRestResource
public interface NucleusFunctionSpaceEventPaceRepository extends NucleusFunctionSpaceRepository<NucleusFunctionSpaceEventPace> {

    @RestResource
    NucleusFunctionSpaceEventPace findFirstByClientCodeAndPropertyCodeAndBookingIdAndEventIdOrderByLastModifiedDateDesc(
            @Param("clientCode") String clientCode,
            @Param("propertyCode") String propertyCode,
            @Param("bookingId") String bookingId,
            @Param("eventId") String eventId);
}