package com.ideas.ngi.functionspace.repository;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceFunctionRoom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.time.LocalDateTime;
import java.util.List;

/**
 * FunctionSpaceFunctionRoomRepository is used for CRUD operations on the nucleus FunctionSpaceFunctionRoom
 * entity and is exposes the collection via REST.
 */
@RepositoryRestResource
public interface NucleusFunctionSpaceFunctionRoomRepository extends NucleusFunctionSpaceRepository<NucleusFunctionSpaceFunctionRoom> {

    @RestResource(exported = false)
    NucleusFunctionSpaceFunctionRoom findFirstByClientCodeAndPropertyCodeAndFunctionRoomId(
            String clientCode,
            String propertyCode,
            String functionRoomId);

    @RestResource(exported = false)
    List<NucleusFunctionSpaceFunctionRoom> findByClientCodeAndPropertyCodeOrderByFunctionRoomIdAsc(
            String clientCode,
            String propertyCode);

    @RestResource(exported = false)
    Page<NucleusFunctionSpaceFunctionRoom> findByClientCodeAndPropertyCodeAndComboIsTrueAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
            String clientCode,
            String propertyCode,
            LocalDateTime startDate,
            LocalDateTime endDate,
            Pageable pageable);

    @RestResource(exported = false)
    Page<NucleusFunctionSpaceFunctionRoom> findByIntegrationTypeAndPropertyIdAndComboIsTrueAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
            IntegrationType integrationType,
            String propertyId,
            LocalDateTime startDate,
            LocalDateTime endDate,
            Pageable page);

}
