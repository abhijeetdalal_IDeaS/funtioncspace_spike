package com.ideas.ngi.functionspace.repository;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBookingGuestRoom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * FunctionSpaceBookingGuestRoomRepository is used for CRUD operations on the nucleus FunctionSpaceBookingGuestRoom
 * entity and is exposes the collection via REST.
 */
@RepositoryRestResource
public interface NucleusFunctionSpaceBookingGuestRoomRepository extends NucleusFunctionSpaceRepository<NucleusFunctionSpaceBookingGuestRoom> {

    @RestResource(exported = false, path = "findOneByClientCodeAndPropertyCodeAndBookingIdAndOccupancyDateAndRoomCategory", rel = "findOneByClientCodeAndPropertyCodeAndBookingIdAndOccupancyDateAndRoomCategory")
    NucleusFunctionSpaceBookingGuestRoom findFirstByClientCodeAndPropertyCodeAndBookingIdAndOccupancyDateAndRoomCategory(
            String clientCode,
            String propertyCode,
            String bookingId,
            LocalDate occupancyDate,
            String roomCategory);

    @RestResource(exported = false)
    List<NucleusFunctionSpaceBookingGuestRoom> findByClientCodeAndPropertyCodeAndBookingId(
            String clientCode,
            String propertyCode,
            String bookingId);

    @RestResource(exported = false)
    List<NucleusFunctionSpaceBookingGuestRoom> findByClientCodeAndPropertyCodeOrderByBookingIdAscOccupancyDateAscRoomCategoryAsc(
            String clientCode,
            String propertyCode);

    @RestResource(exported = false)
    Page<NucleusFunctionSpaceBookingGuestRoom> findByClientCodeAndPropertyCodeAndHasBookedRoomsIsTrueAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
            String clientCode,
            String propertyCode,
            LocalDateTime startDate,
            LocalDateTime endDate,
            Pageable pageable);

    @RestResource(exported = false)
    Page<NucleusFunctionSpaceBookingGuestRoom> findByIntegrationTypeAndPropertyIdAndHasBookedRoomsIsTrueAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
            IntegrationType integrationType,
            String propertyId,
            LocalDateTime startDate,
            LocalDateTime endDate,
            Pageable pageable);

    @RestResource(exported = false)
    Integer countByClientCodeAndPropertyCodeAndBookingIdAndStatusIsNot(
            String clientCode,
            String propertyCode,
            String bookingId,
            String status);
}