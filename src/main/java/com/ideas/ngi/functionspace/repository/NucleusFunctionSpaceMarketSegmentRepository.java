package com.ideas.ngi.functionspace.repository;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceMarketSegment;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

/**
 * MarketSegmentRepository is used for CRUD operations on the nucleus FunctionSpaceMarketSegment
 * entity and is exposes the collection via REST.
 */
@RepositoryRestResource
public interface NucleusFunctionSpaceMarketSegmentRepository extends NucleusFunctionSpaceRepository<NucleusFunctionSpaceMarketSegment> {

    @RestResource(exported = false, path = "findOneByClientCodeAndPropertyCodeAndAbbreviation", rel = "findOneByClientCodeAndPropertyCodeAndAbbreviation")
    NucleusFunctionSpaceMarketSegment findFirstByClientCodeAndPropertyCodeAndAbbreviation(
            String clientCode,
            String propertyCode,
            String abbreviation);

    @RestResource(exported = false)
    List<NucleusFunctionSpaceMarketSegment> findByClientCodeAndPropertyCodeOrderByAbbreviationAsc(
            String clientCode,
            String propertyCode);

    @RestResource(exported = false, path = "findOneByClientCodeAndPropertyCodeAndMarketSegmentId", rel = "findOneByClientCodeAndPropertyCodeAndMarketSegmentId")
    NucleusFunctionSpaceMarketSegment findFirstByClientCodeAndPropertyCodeAndMarketSegmentId(
            String clientCode,
            String propertyCode,
            String marketSegmentId);

}
