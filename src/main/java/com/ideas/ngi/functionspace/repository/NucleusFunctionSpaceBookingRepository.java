package com.ideas.ngi.functionspace.repository;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.functionspace.entity.FunctionSpaceBookingCategory;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBooking;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.time.LocalDateTime;
import java.util.List;

/**
 * FunctionSpaceBookingRepository is used for CRUD operations on the nucleus FunctionSpaceBooking
 * entity and is exposes the collection via REST.
 */
@RepositoryRestResource
public interface NucleusFunctionSpaceBookingRepository extends NucleusFunctionSpaceRepository<NucleusFunctionSpaceBooking> {

    @RestResource(exported = false, path = "findOneByClientCodeAndPropertyCodeAndBookingId", rel = "findOneByClientCodeAndPropertyCodeAndBookingId")
    NucleusFunctionSpaceBooking findFirstByClientCodeAndPropertyCodeAndBookingId(
            String clientCode,
            String propertyCode,
            String bookingId);

    @RestResource(exported = false)
    List<NucleusFunctionSpaceBooking> findByClientCodeAndPropertyCodeOrderByBookingIdAsc(
            String clientCode,
            String propertyCode);

    @RestResource(exported = false)
    List<NucleusFunctionSpaceBooking> findByClientCodeAndPropertyCodeAndBookingIdIn(
            String clientCode,
            String propertyCode,
            List<String> bookingIds);

    @RestResource(exported = false)
    Page<NucleusFunctionSpaceBooking> findByIntegrationTypeAndPropertyIdAndFunctionSpaceBookingCategoryNotAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
            IntegrationType integrationType,
            String propertyId,
            FunctionSpaceBookingCategory functionSpaceBookingCategory,
            LocalDateTime startDate,
            LocalDateTime endDate,
            Pageable page);

    @RestResource(exported = false)
    Page<NucleusFunctionSpaceBooking> findByClientCodeAndPropertyCodeAndFunctionSpaceBookingCategoryNotAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
            String clientCode,
            String propertyCode,
            FunctionSpaceBookingCategory functionSpaceBookingCategory,
            LocalDateTime startDate,
            LocalDateTime endDate,
            Pageable page);
}