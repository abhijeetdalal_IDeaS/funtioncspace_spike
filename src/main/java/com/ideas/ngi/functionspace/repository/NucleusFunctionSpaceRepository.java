package com.ideas.ngi.functionspace.repository;

import com.ideas.ngi.nucleus.config.mongo.repository.NucleusMongoRepository;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.time.LocalDateTime;

import static com.ideas.ngi.functionspace.constant.Constants.PARAM_CLIENT_CODE;
import static com.ideas.ngi.functionspace.constant.Constants.PARAM_PROPERTY_CODE;

@NoRepositoryBean
public interface NucleusFunctionSpaceRepository<T> extends NucleusMongoRepository<T, String> {

    @RestResource(exported = false)
    Page<T> findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
            String clientCode,
            String propertyCode,
            LocalDateTime startDate,
            LocalDateTime endDate,
            Pageable page);

    @RestResource(exported = false)
    Page<T> findByIntegrationTypeAndPropertyIdAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
            IntegrationType integrationType,
            String propertyId,
            LocalDateTime startDate,
            LocalDateTime endDate,
            Pageable page);

    long deleteByClientCodeAndPropertyCode(
            @Param(PARAM_CLIENT_CODE) String clientCode,
            @Param(PARAM_PROPERTY_CODE) String propertyCode);

}
