package com.ideas.ngi.functionspace.repository;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceGuestRoomType;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

/**
 * NucleusFunctionSpaceGuestRoomTypeRepository is used for CRUD operations on the nucleus FunctionSpaceGuestRoomType
 * entity and is exposes the collection via REST.
 */
@RepositoryRestResource
public interface NucleusFunctionSpaceGuestRoomTypeRepository extends NucleusFunctionSpaceRepository<NucleusFunctionSpaceGuestRoomType> {

    @RestResource(exported = false, path = "findOneByClientCodeAndPropertyCodeAndFunctionRoomType", rel = "findOneByClientCodeAndPropertyCodeAndFunctionRoomType")
    NucleusFunctionSpaceGuestRoomType findFirstByClientCodeAndPropertyCodeAndFunctionRoomType(
            String clientCode,
            String propertyCode,
            String functionRoomType);

    @RestResource(exported = false)
    @Query("{clientCode: ?0, propertyCode: ?1, inactive: false}")
    List<NucleusFunctionSpaceGuestRoomType> findByClientCodeAndPropertyCode(String clientCode, String propertyCode, Sort sort);

}

