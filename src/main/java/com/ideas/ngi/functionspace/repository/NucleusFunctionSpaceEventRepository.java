package com.ideas.ngi.functionspace.repository;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEvent;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

/**
 * FunctionSpaceEventRepository is used for CRUD operations on the nucleus FunctionSpaceEvent
 * entity and is exposes the collection via REST.
 */
@RepositoryRestResource
public interface NucleusFunctionSpaceEventRepository extends NucleusFunctionSpaceRepository<NucleusFunctionSpaceEvent> {

    @RestResource(exported = false, path = "findOneByClientCodeAndPropertyCodeAndBookingIdAndEventId", rel = "findOneByClientCodeAndPropertyCodeAndBookingIdAndEventId")
    NucleusFunctionSpaceEvent findFirstByClientCodeAndPropertyCodeAndBookingIdAndEventId(
            String clientCode,
            String propertyCode,
            String bookingId,
            String eventId);

    @RestResource(exported = false)
    List<NucleusFunctionSpaceEvent> findByClientCodeAndPropertyCodeAndEventId(
            String clientCode,
            String propertyCode,
            String eventId);

    @RestResource(exported = false)
    List<NucleusFunctionSpaceEvent> findByClientCodeAndPropertyCodeOrderByBookingIdAscEventIdAsc(
            String clientCode,
            String propertyCode);

    @RestResource(exported = false)
    Integer countByClientCodeAndPropertyCodeAndBookingIdAndStatusIsNot(
            String clientCode,
            String propertyCode,
            String bookingId, String status);

    @RestResource(exported = false)
    List<NucleusFunctionSpaceEvent> findByClientCodeAndPropertyCodeAndBookingId(
            String clientCode,
            String propertyCode,
            String bookingId);

}
