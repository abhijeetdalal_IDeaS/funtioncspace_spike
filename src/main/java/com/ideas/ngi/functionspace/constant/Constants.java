package com.ideas.ngi.functionspace.constant;

public final class Constants {

    public static final String BOOKING_ID = "bookingId";
    public static final String STAY_DATE = "stayDate";
    public static final String BOOKING_PACE_ID = "bookingPaceId";
    public static final String PICKUP_ROOM_NIGHTS = "roomNights";
    public static final String PICKUP_ROOM_REVENUE = "roomRevenue";
    public static final String PICKUP_ROOM_NIGHTS_CHANGED = "roomNightsChanged";
    public static final String PICKUP_ROOM_REVENUE_CHANGED = "roomRevenueChanged";
    public static final String BLOCKED_ROOM_NIGHTS_CHANGED = "blockedRoomNightsChanged";
    public static final String BLOCKED_ROOM_REVENUE_CHANGED = "blockedRoomRevenueChanged";
    public static final String CONTRACTED_ROOM_NIGHTS_CHANGED = "contractedRoomNightsChanged";
    public static final String CONTRACTED_ROOM_REVENUE_CHANGED = "contractedRoomRevenueChanged";
    public static final String GUEST_ROOM_STATUS = "guestRoomStatus";
    public static final String CHANGE_DATE = "changeDate";
    public static final String CHANGE_DATE_WITHOUT_TIME = "changeDateWithoutTime";
    public static final String OFFSET_ADJUSTED_TIME_HOUR = "OffsetAdjustedTimeInHour";
    public static final String OFFSET_ADJUSTED_TIME_MINUTE = "OffsetAdjustedTimeInMinute";
    public static final String ROOM_TYPE = "roomType";
    public static final String LAST_MODIFIED_DATE = "lastModifiedDate";
    public static final String CLIENT_CODE = "clientCode";
    public static final String PROPERTY_CODE = "propertyCode";
    public static final String INTEGRATION_TYPE = "integrationType";
    public static final String PROPERTY_ID = "propertyId";
    public static final String ROOM_ID = "roomId";


    public static final String ABBREVIATION = "abbreviation";
    public static final String NAME = "name";
    public static final String MARKET_SEGMENT_ID = "marketSegmentId";
    public static final String DESCRIPTION = "description";
    public static final String CREATED_DATE = "createDate";

    public static final String PARAM_CLIENT_CODE = "clientCode";
    public static final String PARAM_PROPERTY_CODE = "propertyCode";
    public static final String PARAM_PROPERTY_ID = "propertyId";
    public static final String PARAM_INTEGRATION_TYPE = "integrationType";

    private Constants() {
    }
}
