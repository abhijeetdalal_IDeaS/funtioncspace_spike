package com.ideas.ngi.functionspace.file.sftp.exception;

import com.ideas.ngi.nucleus.exception.NucleusException;
import com.ideas.ngi.functionspace.file.ftp.FTPConfigProperties;

public class NucleusSftpException extends NucleusException {
    private final FTPConfigProperties ftpConfigProperties;

    public NucleusSftpException(String message, FTPConfigProperties ftpConfigProperties) {
        super(message);
        this.ftpConfigProperties = ftpConfigProperties;
    }

    public FTPConfigProperties getFtpConfigProperties() {
         return ftpConfigProperties;
    }

    @Override
    public String getMessage() {
        FTPConfigProperties ftpConfigProperties = getFtpConfigProperties();
        return "Could not connect to: " + ftpConfigProperties.getHost() + ":" + ftpConfigProperties.getPort() +
                " with user " + ftpConfigProperties.getUser() + ". Error: " + super.getMessage();
    }

}
