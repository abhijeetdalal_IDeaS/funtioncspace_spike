package com.ideas.ngi.functionspace.file.gridfs;

import com.mongodb.client.gridfs.model.GridFSFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class NucleusGridFSService {

    private final GridFsOperations gridFsOperations;

    @Autowired
    public NucleusGridFSService(@Qualifier("GridFsTemplate") GridFsOperations gridFsOperations) {
        this.gridFsOperations = gridFsOperations;
    }

    public List<GridFSFile> findFiles(List<String> ids) {
        return findFiles(Query.query(Criteria.where("_id").in(ids)));
    }

    public List<GridFSFile> findFiles(Query query) {
        return gridFsOperations.find(query).into(new ArrayList<>());
    }

    public GridFSFile findFile(Object id) {
        return gridFsOperations.findOne(Query.query(Criteria.where("_id").is(id)));
    }

    public GridFSFile findFile(Query query) {
        return gridFsOperations.findOne(query);
    }

    public GridFsResource findResource(Object payloadReferenceId) {
        GridFSFile file = findFile(payloadReferenceId);
        return file != null ? gridFsOperations.getResource(file.getFilename()) : null;
    }
}
