package com.ideas.ngi.functionspace.file.sftp;

import com.ideas.ngi.functionspace.file.ftp.FTPConfigProperties;
import com.ideas.ngi.functionspace.file.sftp.exception.NucleusSftpException;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.sftp.session.SftpSession;

import java.util.List;

/**
 * Exceptions in JSCH, when connecting, usually get wrapped
 * in an IllegalArgumentException, and are later unwrapped to the original
 * jsch exception causing some issues with exception translation and handling.
 *
 * This class wraps the session creation logic and discards the original
 * exception, trying to keep as much of the original exception detail as possible,
 * so when the exception is reported, the details about the sftp connection
 * are preserved.
 */
public class NucleusSftpSessionFactory extends DefaultSftpSessionFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(NucleusSftpSessionFactory.class);
    private final FTPConfigProperties ftpConfigProperties;

    public NucleusSftpSessionFactory(FTPConfigProperties ftpConfigProperties) {
        this.ftpConfigProperties = ftpConfigProperties;
        setHost(ftpConfigProperties.getHost());
        setPort(ftpConfigProperties.getPort());
        setUser(ftpConfigProperties.getUser());
        setPassword(ftpConfigProperties.getPassword());
        setAllowUnknownKeys(true);
    }

    @SuppressWarnings("squid:S2221")
    @Override
    public SftpSession getSession() {
        try {
            return super.getSession();
        } catch (Exception e) {
            LOGGER.warn("Caught exception getting sftp session", e);
            Throwable cause = getMostSpecificCause(e);
            String rootCauseMessage = cause.getMessage();
            throw new NucleusSftpException(rootCauseMessage, ftpConfigProperties);
        }
    }

    private Throwable getMostSpecificCause(Throwable t) {
        List<Throwable> exceptionChain = ExceptionUtils.getThrowableList(t);
        for (Throwable cause : exceptionChain) {
            if (cause instanceof JSchException || cause instanceof SftpException) {
                return cause;
            }
        }

        if (exceptionChain.isEmpty()) {
            return t;
        } else {
            return exceptionChain.get(exceptionChain.size() - 1);
        }
    }
}
