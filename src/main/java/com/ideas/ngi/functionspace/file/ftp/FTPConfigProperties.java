package com.ideas.ngi.functionspace.file.ftp;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

public abstract class FTPConfigProperties implements Serializable {

    private static final long serialVersionUID = -5783999263116207331L;

    private static final Logger LOGGER = LoggerFactory.getLogger(FTPConfigProperties.class);

    public static final String DEFAULT_INCOMING_DIRECTORY = "incoming";
    public static final String DEFAULT_ARCHIVE_DIRECTORY = "archive";
    public static final String DEFAULT_PROCESSED_SUFFIX = "processed";
    public static final String DEFAULT_INPROCESS_SUFFIX = "inprocess";

    public abstract String getHost();

    public abstract Integer getPort();

    public abstract String getUser();

    public abstract String getPassword();

    public abstract String getRemoteDirectory();

    public abstract String getFilenamePatternFilter();

    public abstract File getLocalDataDirectory();

    public File getIncomingDirectory() {
        return new File(getLocalDataDirectory(), DEFAULT_INCOMING_DIRECTORY);
    }

    public File getInprocessDirectory() {
        return new File(getLocalDataDirectory(), DEFAULT_INPROCESS_SUFFIX);
    }

    public File getArchiveDirectory() {
        return new File(getLocalDataDirectory(), DEFAULT_ARCHIVE_DIRECTORY);
    }

    public String getProcessedFileSuffix() {
        return DEFAULT_PROCESSED_SUFFIX;
    }

    public String getInprocessFileSuffix() {
        return DEFAULT_INPROCESS_SUFFIX;
    }

    public boolean isSftpConfigured() {
        return StringUtils.isNotEmpty(getHost()) && StringUtils.isNotEmpty(getUser()) && StringUtils.isNotEmpty(getPassword());
    }

    @PostConstruct
    public void init() throws IOException {
        File incomingDirectory = getIncomingDirectory();
        if (!incomingDirectory.exists()) {
            FileUtils.forceMkdir(incomingDirectory);
        }

        File inprocessDirectory = getInprocessDirectory();
        if (!inprocessDirectory.exists()) {
            FileUtils.forceMkdir(inprocessDirectory);
        }

        File archiveDirectory = getArchiveDirectory();
        if (archiveDirectory != null && !archiveDirectory.exists()) {
            FileUtils.forceMkdir(archiveDirectory);
        }
        printConfiguration();
    }

    private void printConfiguration() {
        StringBuilder config = new StringBuilder("\nFTP Configuration:")
                .append("\n   Host: [").append(getHost()).append("]")
                .append("\n   Port: [").append(getPort()).append("]")
                .append("\n   User: [").append(getUser()).append("]")
                .append("\n   Remote Files: [").append(getRemoteDirectory()).append("/").append(getFilenamePatternFilter()).append("]")
                .append("\n   Local Incoming Directory: [").append(getIncomingDirectory()).append("]")
                .append("\n   Local Inprocess Directory: [").append(getInprocessDirectory()).append("]")
                .append("\n   Archive Directory: [").append(getArchiveDirectory()).append("]");

        if (isSftpConfigured()) {
            config.append("\n   FTP is enabled");
        } else {
            config.append("\n   FTP is disabled");
        }
        LOGGER.info(config.toString());
    }
}
