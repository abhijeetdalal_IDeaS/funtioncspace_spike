package com.ideas.ngi.functionspace.file;

import com.ideas.ngi.nucleus.exception.NucleusException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Component
public class FileService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public File moveFileToDirectory(File file, File directory) {
        Assert.notNull(file, "File cannot be null");
        Assert.notNull(directory, "Directory cannot be null");

        createDirectory(directory);

        File movedFile = new File(directory, file.getName());
        deleteFileIfExists(movedFile);

        logger.info("Moving File: " + file + " to Directory: " + directory);

        try {
            FileUtils.moveFileToDirectory(file, directory, true);
        } catch (IOException ioe) {
            throw new NucleusException("Unable to copy file: " + file + " to directory: " + directory, ioe);
        }

        return new File(directory, file.getName());
    }

    private void deleteFileIfExists(File fileToDelete) {
        if (fileToDelete.exists()) {
            fileToDelete.delete();
        }
    }

    public File copyFileToDirectory(File file, File directory) {
        Assert.notNull(file, "File cannot be null");
        Assert.notNull(directory, "Directory cannot be null");

        createDirectory(directory);

        File copyFile = new File(directory, file.getName());
        deleteFileIfExists(copyFile);

        logger.info("Copying File: " + file + " to Directory: " + directory);

        try {
            FileUtils.copyFileToDirectory(file, directory, true);
        } catch (IOException ioe) {
            throw new NucleusException("Unable to copy file: " + file + " to directory: " + directory, ioe);
        }

        return new File(directory, file.getName());
    }

    public boolean deleteQuietly(File directory) {
        try {
            deleteDirectory(directory);
            return true;
        } catch (NucleusException e) {
            logger.warn("Could not delete directory " + directory, e);
            return false;
        }
    }

    public boolean deleteDirectory(File directory) {
        logger.info("Deleting Directory: " + directory);

        try {
            FileUtils.deleteDirectory(directory);
            return true;
        } catch (IOException e) {
            throw new NucleusException("An error occurred while trying to delete the directory : " +directory.getAbsolutePath(), e);
        }
    }

    public void uploadFile(MultipartFile multipartFile, File directory) {
        File tempFileLocation = new File(FileUtils.getTempDirectory(), multipartFile.getOriginalFilename());
        try(BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(tempFileLocation))) {
            // Read the file to a temp location
            byte[] bytes = multipartFile.getBytes();
            stream.write(bytes);
            stream.close();
            // Once the file is uploaded, move it to the directory
            FileUtils.moveFileToDirectory(tempFileLocation, directory, true);
        } catch (IOException e) {
            throw new NucleusException("Unable to upload file: " + multipartFile.getOriginalFilename(), e);
        }
    }

    public boolean deleteFilesOlderThan(Long days, File element) {

        boolean isDirectory = element.isDirectory();
        if (isDirectory && element.listFiles() != null) {
            logger.info("Purging Activity: deleting files from " + element + " older than " + days + " days");
            for (File sub : element.listFiles()) {
                deleteFilesOlderThan(days, sub);
            }
        }

        Long eligibleForDeletion = System.currentTimeMillis() - (days * 24 * 60 * 60 * 1000L);
        if ((element.isDirectory() && element.listFiles() == null) || element.lastModified() < eligibleForDeletion) {
            logger.info("Purging Activity: deleting file/folder: " + element);
            if (!element.delete() && !isDirectory) {
                throw new NucleusException("Purging Activity: unable to delete file " + element);
            }
        }
        return true;
    }

    public boolean createDirectory(File directory) {
        return directory.exists() || directory.mkdirs();
    }

    public File[] getSubDirectories(File parentDirectory) {
        return parentDirectory.listFiles(File::isDirectory);
    }

    public void deleteFile(String fileToDelete) {
        File dir = new File(fileToDelete);
        try {
            FileUtils.forceDelete(dir);
        } catch (IOException e) {
            throw new NucleusException("Exception while deleting file" + fileToDelete, e);
        }

    }

    public int deleteFiles(File directory, String fileNameContainsText) throws IOException {
        int fileDeletedCount = 0;
        if (!directory.exists()) {
            String message = directory + " does not exist";
            throw new IllegalArgumentException(message);
        }

        if (!directory.isDirectory()) {
            String message = directory + " is not a directory";
            throw new IllegalArgumentException(message);
        }

        File[] files = directory.listFiles();
        if (files == null) {
            throw new IOException("Failed to list contents of " + directory);
        }

        IOException exception = null;
        for (File file : files) {
            if(StringUtils.isBlank(fileNameContainsText) || file.getName().contains(fileNameContainsText)) {
                try {
                    FileUtils.forceDelete(file);
                    ++fileDeletedCount;
                } catch (IOException ioe) {
                    exception = ioe;
                }
            }
        }

        if (null != exception) {
            throw exception;
        }
        return fileDeletedCount;
    }
}
