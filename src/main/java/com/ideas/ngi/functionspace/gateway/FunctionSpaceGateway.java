package com.ideas.ngi.functionspace.gateway;

import com.ideas.ngi.functionspace.config.FunctionSpaceConfig;
import com.ideas.ngi.functionspace.dto.FunctionSpaceDeletionDto;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.context.IntegrationContextUtils;

@MessagingGateway(errorChannel = IntegrationContextUtils.ERROR_CHANNEL_BEAN_NAME)
public interface FunctionSpaceGateway {
    
    @Gateway(requestChannel = FunctionSpaceConfig.FUNCTION_SPACE_DELETE_CHANNEL)
    FunctionSpaceDeletionDto deletePropertyData(FunctionSpaceDeletionDto functionSpaceDeletionDto);

}
