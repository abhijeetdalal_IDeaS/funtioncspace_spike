package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.functionspace.constant.Constants;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceMarketSegment;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEventType;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceMarketSegmentRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;


@Service
public class FunctionSpaceMarketSegmentService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    private final NucleusFunctionSpaceMarketSegmentRepository functionSpaceMarketSegmentRepository;

    @Autowired
    public FunctionSpaceMarketSegmentService(NucleusFunctionSpaceMarketSegmentRepository functionSpaceMarketSegmentRepository) {
        this.functionSpaceMarketSegmentRepository = functionSpaceMarketSegmentRepository;
    }

    public NucleusFunctionSpaceMarketSegment findOrCreateFunctionSpaceMarketSegment(IntegrationType integrationType, String propertyId, String clientCode, String propertyCode, String abbreviation) {
        logger.debug("Looking for FunctionSpaceMarketSegment: integrationType={}, propertyId={}, abbreviation={}",
                integrationType, propertyId, abbreviation);

        NucleusFunctionSpaceMarketSegment functionSpaceMarketSegment = functionSpaceMarketSegmentRepository.findFirstByClientCodeAndPropertyCodeAndAbbreviation(clientCode, propertyCode, abbreviation);
        if (functionSpaceMarketSegment == null) {
            functionSpaceMarketSegment = new NucleusFunctionSpaceMarketSegment();
            functionSpaceMarketSegment.setIntegrationType(integrationType);
            functionSpaceMarketSegment.setPropertyId(propertyId);
            functionSpaceMarketSegment.setAbbreviation(abbreviation);
            functionSpaceMarketSegment.setName(abbreviation);
            functionSpaceMarketSegment.setDescription(abbreviation);
            functionSpaceMarketSegment.setMarketSegmentId(abbreviation);
            functionSpaceMarketSegment.setClientCode(clientCode);
            functionSpaceMarketSegment.setPropertyCode(propertyCode);
            functionSpaceMarketSegment = functionSpaceMarketSegmentRepository.save(functionSpaceMarketSegment);
        }

        return functionSpaceMarketSegment;
    }

    public NucleusFunctionSpaceMarketSegment save(NucleusFunctionSpaceMarketSegment functionSpaceMarketSegment) {
        NucleusFunctionSpaceMarketSegment existingMarketSegment = functionSpaceMarketSegmentRepository.findFirstByClientCodeAndPropertyCodeAndMarketSegmentId(functionSpaceMarketSegment.getClientCode(), functionSpaceMarketSegment.getPropertyCode(), functionSpaceMarketSegment.getMarketSegmentId());
        if (existingMarketSegment != null) {
            functionSpaceMarketSegment.setId(existingMarketSegment.getId());
            functionSpaceMarketSegment.setCreateDate(existingMarketSegment.getCreateDate());
        }
        return functionSpaceMarketSegmentRepository.save(functionSpaceMarketSegment);
    }

    public NucleusFunctionSpaceMarketSegment findOne(String clientCode, String propertyCode, String marketSegmentId) {
        return functionSpaceMarketSegmentRepository.findFirstByClientCodeAndPropertyCodeAndMarketSegmentId(clientCode, propertyCode, marketSegmentId);
    }

    public List<NucleusFunctionSpaceMarketSegment> findAll(String clientCode, String propertyCode) {
        return functionSpaceMarketSegmentRepository.findByClientCodeAndPropertyCodeOrderByAbbreviationAsc(clientCode, propertyCode);
    }

    public Page<NucleusFunctionSpaceMarketSegment> findByIntegrationTypeAndDateRange(IntegrationType integrationType, String propertyId, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionSpaceMarketSegmentRepository.findByIntegrationTypeAndPropertyIdAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(integrationType, propertyId, startDate, endDate, page);
    }

    public List<NucleusFunctionSpaceMarketSegment> save(Collection<NucleusFunctionSpaceMarketSegment> marketSegmentList) {
        return functionSpaceMarketSegmentRepository.saveAll(marketSegmentList);
    }

    public long delete(String clientCode, String propertyCode) {
        return functionSpaceMarketSegmentRepository.deleteByClientCodeAndPropertyCode(clientCode, propertyCode);
    }

    public Page<NucleusFunctionSpaceMarketSegment> findAllByDateRange(String clientCode, String propertyCode, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionSpaceMarketSegmentRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(clientCode, propertyCode, startDate, endDate, page);
    }

    public String getMarketSegmentCode(String clientCode, String propertyCode, String marketSegmentId, String defaultValue) {
        if (StringUtils.isEmpty(marketSegmentId)) {
            return defaultValue;
        }

        NucleusFunctionSpaceMarketSegment marketSegment = findOne(clientCode, propertyCode, marketSegmentId);
        return marketSegment == null ? defaultValue : marketSegment.getAbbreviation();
    }

    public void upsertAll(List<NucleusFunctionSpaceMarketSegment> nucleusFunctionSpaceMarketSegments) {
        if (CollectionUtils.isEmpty(nucleusFunctionSpaceMarketSegments)) {
            return;
        }
        logger.debug("Upserting {} documents in NucleusFunctionSpaceMarketSegment collection", nucleusFunctionSpaceMarketSegments.size());
        List<Pair<Query, Update>> updates = getPairsToUpdateOrSave(nucleusFunctionSpaceMarketSegments);
        functionSpaceMarketSegmentRepository.bulkOpsUpsertMulti(updates);
        logger.debug("Upsert Complete in NucleusFunctionSpaceMarketSegment collection");
    }

    private List<Pair<Query, Update>> getPairsToUpdateOrSave(List<NucleusFunctionSpaceMarketSegment> nucleusFunctionSpaceMarketSegments) {

        List<Pair<Query, Update>> updates = new ArrayList<>();
        nucleusFunctionSpaceMarketSegments.forEach(nucleusFunctionSpaceMarketSegment -> {
            Update update = getUpdate(nucleusFunctionSpaceMarketSegment);
            Pair<Query, Update> pair = Pair.of(createIsPresentQuery(nucleusFunctionSpaceMarketSegment.getAbbreviation(), nucleusFunctionSpaceMarketSegment.getClientCode(), nucleusFunctionSpaceMarketSegment.getPropertyCode()), update);
            updates.add(pair);
        });
        return updates;

    }

    private Query createIsPresentQuery(String abbreviation, String clientCode, String propertyCode) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Constants.ABBREVIATION).is(abbreviation));
        query.addCriteria(Criteria.where(Constants.CLIENT_CODE).is(clientCode));
        query.addCriteria(Criteria.where(Constants.PROPERTY_CODE).is(propertyCode));
        return query;
    }

    private Update getUpdate(NucleusFunctionSpaceMarketSegment nucleusFunctionSpaceMarketSegment) {

        Update update = new Update();

        update.set(NucleusFunctionSpaceEventType.LAST_MODIFIED_FIELD, new Date());
        update.set(Constants.CLIENT_CODE, nucleusFunctionSpaceMarketSegment.getClientCode());
        update.set(Constants.PROPERTY_CODE, nucleusFunctionSpaceMarketSegment.getPropertyCode());
        update.set(Constants.PROPERTY_ID, nucleusFunctionSpaceMarketSegment.getPropertyId());
        update.set(Constants.INTEGRATION_TYPE, nucleusFunctionSpaceMarketSegment.getIntegrationType());
        update.set(Constants.ABBREVIATION, nucleusFunctionSpaceMarketSegment.getAbbreviation());
        update.set(Constants.DESCRIPTION, nucleusFunctionSpaceMarketSegment.getDescription());
        update.set(Constants.MARKET_SEGMENT_ID, nucleusFunctionSpaceMarketSegment.getMarketSegmentId());
        update.set(Constants.NAME, nucleusFunctionSpaceMarketSegment.getName());
        update.setOnInsert(Constants.CREATED_DATE,new Date());

        return update;
    }
}
