package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEventPace;
import com.ideas.ngi.nucleus.data.functionspace.entity.FunctionSpaceEventRevenue;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceEventPaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.ideas.ngi.nucleus.data.AuditableEntity.CREATED_FIELD;
import static com.ideas.ngi.nucleus.data.AuditableEntity.LAST_MODIFIED_FIELD;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;

@Service
public class FunctionSpaceEventPaceService {

    private final NucleusFunctionSpaceEventPaceRepository functionSpaceEventPaceRepository;

    @Autowired
    public FunctionSpaceEventPaceService(NucleusFunctionSpaceEventPaceRepository functionSpaceEventPaceRepository) {
        this.functionSpaceEventPaceRepository = functionSpaceEventPaceRepository;
    }

    public List<NucleusFunctionSpaceEventPace> save(List<NucleusFunctionSpaceEventPace> functionSpaceEventsPaces) {
        return functionSpaceEventPaceRepository.saveAll(functionSpaceEventsPaces);
    }

    public long delete(String clientCode, String propertyCode) {
        return functionSpaceEventPaceRepository.deleteByClientCodeAndPropertyCode(clientCode, propertyCode);
    }

    public NucleusFunctionSpaceEventPace findLatest(String clientCode, String propertyCode, String bookingId, String eventId) {
        return functionSpaceEventPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndEventIdOrderByLastModifiedDateDesc(clientCode, propertyCode, bookingId, eventId);
    }

    public Page<NucleusFunctionSpaceEventPace> findAllByDateRange(String clientCode, String propertyCode, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionSpaceEventPaceRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(clientCode, propertyCode, startDate, endDate, page);
    }


    public boolean equalsWithPrev(NucleusFunctionSpaceEventPace obj, NucleusFunctionSpaceEventPace prevObj) {
        if (reflectionEquals(obj, prevObj, "id", "correlationMetadataId", "changeDate", "functionSpaceEventRevenues", LAST_MODIFIED_FIELD, CREATED_FIELD)) {
            if (prevObj == null) {
                return false;
            }
            return functionSpaceEventRevenuesEquals(obj.getFunctionSpaceEventRevenues(), prevObj.getFunctionSpaceEventRevenues());
        }
        return false;
    }

    private boolean functionSpaceEventRevenuesEquals(List<FunctionSpaceEventRevenue> obj, List<FunctionSpaceEventRevenue> prevObj) {
        if (obj == null && prevObj == null) {
            return true;
        } else if (obj != null && prevObj != null && obj.size() == prevObj.size()) {
            return obj.stream().map(functionSpaceEventRevenue -> prevObj.stream().filter(prevFunctionSpaceEventRevenue ->
                    reflectionEquals(functionSpaceEventRevenue, prevFunctionSpaceEventRevenue, "insertDate", "updateDate"))
                    .findFirst()).allMatch(Optional::isPresent);
        } else {
            return false;
        }
    }
}