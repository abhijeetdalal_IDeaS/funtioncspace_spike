package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.functionspace.constant.Constants;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEvent;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class FunctionSpaceEventService {
    private final NucleusFunctionSpaceEventRepository functionSpaceEventRepository;

    private MongoOperations mongoOperations;

    @Autowired
    public FunctionSpaceEventService(NucleusFunctionSpaceEventRepository functionSpaceEventRepository, MongoOperations mongoOperations) {
        this.functionSpaceEventRepository = functionSpaceEventRepository;
        this.mongoOperations = mongoOperations;
    }

    public List<NucleusFunctionSpaceEvent> findByClientCodeAndPropertyCodeAndBookingId(String clientCode, String propertyCode, String bookingId) {
        return functionSpaceEventRepository.findByClientCodeAndPropertyCodeAndBookingId(clientCode, propertyCode, bookingId);
    }

    public List<NucleusFunctionSpaceEvent> findAll(String clientCode, String propertyCode) {
        return functionSpaceEventRepository.findByClientCodeAndPropertyCodeOrderByBookingIdAscEventIdAsc(clientCode, propertyCode);
    }

    public Page<NucleusFunctionSpaceEvent> findAllByDateRange(String clientCode, String propertyCode, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionSpaceEventRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(clientCode, propertyCode, startDate, endDate, page);
    }

    public Page<NucleusFunctionSpaceEvent> findByIntegrationTypeAndDateRange(IntegrationType integrationType, String propertyId, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionSpaceEventRepository.findByIntegrationTypeAndPropertyIdAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(integrationType, propertyId, startDate, endDate, page);
    }

    public NucleusFunctionSpaceEvent save(NucleusFunctionSpaceEvent functionSpaceEvent) {
        return functionSpaceEventRepository.save(functionSpaceEvent);
    }

    public List<NucleusFunctionSpaceEvent> save(List<NucleusFunctionSpaceEvent> functionSpaceEvents) {
        return functionSpaceEventRepository.saveAll(functionSpaceEvents);
    }

    public long delete(String clientCode, String propertyCode) {
        return functionSpaceEventRepository.deleteByClientCodeAndPropertyCode(clientCode, propertyCode);
    }

    public Integer findCount(String clientCode, String propertyCode, String bookingId, String status) {
        return functionSpaceEventRepository.countByClientCodeAndPropertyCodeAndBookingIdAndStatusIsNot(clientCode, propertyCode, bookingId, status);
    }

    public NucleusFunctionSpaceEvent findOne(String clientCode, String propertyCode, String bookingId, String eventId) {
        return functionSpaceEventRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndEventId(clientCode, propertyCode, bookingId, eventId);
    }

    public List<NucleusFunctionSpaceEvent> findAll(String clientCode, String propertyCode, String eventId) {
        return functionSpaceEventRepository.findByClientCodeAndPropertyCodeAndEventId(clientCode, propertyCode, eventId);
    }

    public List<String> getDistinctRoomIdsByClientCodeAndPropertyCode(String clientCode, String propertyCode) {
        Criteria criteria = Criteria.where(Constants.CLIENT_CODE).is(clientCode).and(Constants.PROPERTY_CODE).is(propertyCode);
        Query query = Query.query(criteria);

        return mongoOperations.getCollection(mongoOperations.getCollectionName(NucleusFunctionSpaceEvent.class))
                .distinct(Constants.ROOM_ID, query.getQueryObject(), String.class)
                .into(new ArrayList<>());
    }
}
