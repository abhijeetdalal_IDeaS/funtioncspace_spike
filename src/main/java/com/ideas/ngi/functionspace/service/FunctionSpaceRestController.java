package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.functionspace.dto.FunctionSpaceGuestRoomPaceWithBookingInfoDto;
import com.ideas.ngi.nucleus.data.functionspace.entity.*;
import com.ideas.ngi.nucleus.util.DateTimeUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import static com.ideas.ngi.functionspace.constant.Constants.*;
import static com.ideas.ngi.nucleus.util.DateTimeUtil.getOrDefault;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@SuppressWarnings("squid:S00107")
@RestController
@RequestMapping(path = "/functionSpace", produces = {APPLICATION_JSON_VALUE})
public class FunctionSpaceRestController {

    private final FunctionSpaceBookingService functionSpaceBookingService;
    private final FunctionSpaceFunctionRoomService functionSpaceFunctionRoomService;
    private final FunctionSpaceEventService functionSpaceEventService;
    private final FunctionSpaceEventPaceService functionSpaceEventPaceService;
    private final FunctionSpaceEventTypeService functionSpaceEventTypeService;
    private final FunctionSpaceBookingGuestRoomService functionSpaceBookingGuestRoomService;
    private final FunctionSpaceBookingPaceService functionSpaceBookingPaceService;
    private final FunctionSpaceGuestRoomPaceService functionSpaceGuestRoomPaceService;
    private final FunctionSpaceGuestRoomTypeService functionSpaceGuestRoomTypeService;
    private final FunctionSpaceMarketSegmentService functionSpaceMarketSegmentService;

    @Autowired
    public FunctionSpaceRestController(FunctionSpaceBookingService functionSpaceBookingService,
                                       FunctionSpaceFunctionRoomService functionSpaceFunctionRoomService,
                                       FunctionSpaceEventService functionSpaceEventService,
                                       FunctionSpaceEventPaceService functionSpaceEventPaceService,
                                       FunctionSpaceEventTypeService functionSpaceEventTypeService,
                                       FunctionSpaceBookingGuestRoomService functionSpaceBookingGuestRoomService,
                                       FunctionSpaceBookingPaceService functionSpaceBookingPaceService,
                                       FunctionSpaceGuestRoomPaceService functionSpaceGuestRoomPaceService,
                                       FunctionSpaceGuestRoomTypeService functionSpaceGuestRoomTypeService,
                                       FunctionSpaceMarketSegmentService functionSpaceMarketSegmentService) {
        this.functionSpaceBookingService = functionSpaceBookingService;
        this.functionSpaceFunctionRoomService = functionSpaceFunctionRoomService;
        this.functionSpaceEventService = functionSpaceEventService;
        this.functionSpaceEventPaceService = functionSpaceEventPaceService;
        this.functionSpaceEventTypeService = functionSpaceEventTypeService;
        this.functionSpaceBookingGuestRoomService = functionSpaceBookingGuestRoomService;
        this.functionSpaceBookingPaceService = functionSpaceBookingPaceService;
        this.functionSpaceGuestRoomPaceService = functionSpaceGuestRoomPaceService;
        this.functionSpaceGuestRoomTypeService = functionSpaceGuestRoomTypeService;
        this.functionSpaceMarketSegmentService = functionSpaceMarketSegmentService;
    }

    @GetMapping("/bookings")
    @ResponseBody
    @ApiOperation(value = "Retrieves All Function Space Bookings updated within a specified date range", tags = "functionSpace")
    public Page<NucleusFunctionSpaceBooking> getAllBookingsByDateRange(
            @RequestParam(PARAM_CLIENT_CODE) String clientCode,
            @RequestParam(PARAM_PROPERTY_CODE) String propertyCode,
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
            Pageable page) {

        return functionSpaceBookingService.findAllByDateRange(clientCode, propertyCode, startDate, getOrDefault(endDate), page);
    }

    @GetMapping(value = "/bookings/search", produces = {MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE})
    @ResponseBody
    @ApiOperation(value = "Retrieves Function Space Bookings by integration type and updated within a specified date range", tags = "functionSpace")
    public PagedResources<Resource<NucleusFunctionSpaceBooking>> searchBookingsByIntegrationTypeAndDateRange(
            @RequestParam(PARAM_INTEGRATION_TYPE) IntegrationType integrationType,
            @RequestParam(PARAM_PROPERTY_ID) String propertyId,
            @RequestParam(value = "startDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime startDate,
            @RequestParam(value = "endDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime endDate,
            Pageable page, PagedResourcesAssembler<NucleusFunctionSpaceBooking> pagedResourcesAssembler) {

        return pagedResourcesAssembler.toResource(functionSpaceBookingService.findByIntegrationTypeAndNotCategoryAndDateRange(integrationType, propertyId, FunctionSpaceBookingCategory.BOOKING, startDate, endDate, page));
    }

    @GetMapping(value = "/bookings/search/optimized", produces = {MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE})
    @ResponseBody
    @ApiOperation(value = "Retrieves Function Space Bookings by client/property and updated within a specified date range", tags = "functionSpace")
    public PagedResources<Resource<NucleusFunctionSpaceBooking>> searchBookingsByClientAndPropertyAndDateRange(
            @RequestParam(PARAM_CLIENT_CODE) String clientCode,
            @RequestParam(PARAM_PROPERTY_CODE) String propertyCode,
            @RequestParam(value = "startDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime startDate,
            @RequestParam(value = "endDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime endDate,
            Pageable page, PagedResourcesAssembler<NucleusFunctionSpaceBooking> pagedResourcesAssembler) {

        return pagedResourcesAssembler.toResource(functionSpaceBookingService.findByClientAndPropertyAndNotCategoryAndDateRange(clientCode, propertyCode, FunctionSpaceBookingCategory.BOOKING, startDate, endDate, page));
    }

    @GetMapping(value = "/events", produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Retrieves All Function Space Events updated within a specified date range", tags = "functionSpace")
    public Page<NucleusFunctionSpaceEvent> getAllEventsByDateRange(
            @RequestParam(PARAM_CLIENT_CODE) String clientCode,
            @RequestParam(value = PARAM_PROPERTY_CODE) String propertyCode,
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
            Pageable page) {

        return functionSpaceEventService.findAllByDateRange(clientCode, propertyCode,
                startDate, getOrDefault(endDate), page);
    }

    @GetMapping(value = "/events/search", produces = {MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE})
    @ResponseBody
    @ApiOperation(value = "Retrieves Function Space Events by integration type updated within a specified date range", tags = "functionSpace")
    public PagedResources<Resource<NucleusFunctionSpaceEvent>> searchEventsByIntegrationTypeAndDateRange(
            @RequestParam(PARAM_INTEGRATION_TYPE) IntegrationType integrationType,
            @RequestParam(PARAM_PROPERTY_ID) String propertyId,
            @RequestParam(value = "startDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime startDate,
            @RequestParam(value = "endDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime endDate,
            Pageable page, PagedResourcesAssembler<NucleusFunctionSpaceEvent> pagedResourcesAssembler) {

        return pagedResourcesAssembler.toResource(functionSpaceEventService.findByIntegrationTypeAndDateRange(integrationType, propertyId, startDate, endDate, page));
    }

    @GetMapping(value = "/events/search/optimized", produces = {MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE})
    @ResponseBody
    @ApiOperation(value = "Retrieves All Function Space Events by client/property and updated within a specified date range", tags = "functionSpace")
    public PagedResources<Resource<NucleusFunctionSpaceEvent>> searchEventsByClientAndPropertyAndDateRange(
            @RequestParam(PARAM_CLIENT_CODE) String clientCode,
            @RequestParam(value = PARAM_PROPERTY_CODE) String propertyCode,
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
            Pageable page, PagedResourcesAssembler<NucleusFunctionSpaceEvent> pagedResourcesAssembler) {

        return pagedResourcesAssembler.toResource(functionSpaceEventService.findAllByDateRange(clientCode, propertyCode,
                startDate, getOrDefault(endDate), page));
    }

    @GetMapping(value = "/eventPace", produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Retrieves All Function Space Events pace updated within a specified date range", tags = "functionSpace")
    public Page<NucleusFunctionSpaceEventPace> getAllEventsPaceByDateRange(
            @RequestParam(PARAM_CLIENT_CODE) String clientCode,
            @RequestParam(value = PARAM_PROPERTY_CODE) String propertyCode,
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
            Pageable page) {

        return functionSpaceEventPaceService.findAllByDateRange(clientCode, propertyCode,
                startDate, getOrDefault(endDate), page);
    }

    @GetMapping("/eventTypes")
    @ResponseBody
    @ApiOperation(value = "Retrieves All Function Space Event Types updated within a specified date range", tags = "functionSpace")
    public Page<NucleusFunctionSpaceEventType> getAllEventTypesByDateRange(
            @RequestParam(PARAM_CLIENT_CODE) String clientCode,
            @RequestParam(PARAM_PROPERTY_CODE) String propertyCode,
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
            Pageable page) {

        return functionSpaceEventTypeService.findAllByDateRange(clientCode, propertyCode, startDate, getOrDefault(endDate), page);
    }

    @GetMapping(value = "/eventTypes/search", produces = {MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE})
    @ResponseBody
    @ApiOperation(value = "Retrieves Function Space Event Types by integration type updated within a specified date range", tags = "functionSpace")
    public PagedResources<Resource<NucleusFunctionSpaceEventType>> searchEventTypesByIntegrationTypeAndDateRange(
            @RequestParam(PARAM_INTEGRATION_TYPE) IntegrationType integrationType,
            @RequestParam(PARAM_PROPERTY_ID) String propertyId,
            @RequestParam(value = "startDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime startDate,
            @RequestParam(value = "endDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime endDate,
            Pageable page, PagedResourcesAssembler<NucleusFunctionSpaceEventType> pagedResourcesAssembler) {

        return pagedResourcesAssembler.toResource(functionSpaceEventTypeService.findByIntegrationTypeAndDateRange(integrationType, propertyId, startDate, endDate, page));
    }

    @GetMapping("/rooms")
    @ResponseBody
    @ApiOperation(value = "Retrieves All Function Space Function Rooms updated within a specified date range", tags = "functionSpace")
    public Page<NucleusFunctionSpaceFunctionRoom> getAllFunctionRoomsByDateRange(
            @RequestParam(PARAM_CLIENT_CODE) String clientCode,
            @RequestParam(PARAM_PROPERTY_CODE) String propertyCode,
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
            Pageable page) {

        return functionSpaceFunctionRoomService.findAllByDateRange(clientCode, propertyCode, startDate, getOrDefault(endDate), page);
    }

    @GetMapping(value = "/rooms/search", produces = {MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE})
    @ResponseBody
    @ApiOperation(value = "Retrieves Function Space Function Rooms by integration type updated within a specified date range", tags = "functionSpace")
    public  PagedResources<Resource<NucleusFunctionSpaceFunctionRoom>> searchFunctionRoomsByIntegrationTypeAndDateRange(
            @RequestParam(PARAM_INTEGRATION_TYPE) IntegrationType integrationType,
            @RequestParam(PARAM_PROPERTY_ID) String propertyId,
            @RequestParam(value = "startDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime startDate,
            @RequestParam(value = "endDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime endDate,
            Pageable page, PagedResourcesAssembler<NucleusFunctionSpaceFunctionRoom> pagedResourcesAssembler) {

        return pagedResourcesAssembler.toResource(functionSpaceFunctionRoomService.findByIntegrationTypeAndDateRange(integrationType, propertyId, startDate, endDate, page));
    }

    @GetMapping("/comboRooms")
    @ResponseBody
    @ApiOperation(value = "Retrieves All Function Space Combination Function Rooms updated within a specified date range", tags = "functionSpace")
    public Page<NucleusFunctionSpaceFunctionRoom> getAllComboRoomsByDateRange(
            @RequestParam(PARAM_CLIENT_CODE) String clientCode,
            @RequestParam(PARAM_PROPERTY_CODE) String propertyCode,
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
            Pageable page) {

        return functionSpaceFunctionRoomService.findAllComboRoomsByDateRange(clientCode, propertyCode,
                startDate, getOrDefault(endDate), page);
    }

    @GetMapping(value = "/comboRooms/search", produces = {MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE})
    @ResponseBody
    @ApiOperation(value = "Retrieves Function Space Combination Function Rooms by integration type updated within a specified date range", tags = "functionSpace")
    public PagedResources<Resource<NucleusFunctionSpaceFunctionRoom>> searchComboRoomsByIntegrationTypeAndDateRange(
            @RequestParam(PARAM_INTEGRATION_TYPE) IntegrationType integrationType,
            @RequestParam(PARAM_PROPERTY_ID) String propertyId,
            @RequestParam(value = "startDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime startDate,
            @RequestParam(value = "endDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime endDate,
            Pageable page, PagedResourcesAssembler<NucleusFunctionSpaceFunctionRoom> pagedResourcesAssembler) {

        return pagedResourcesAssembler.toResource(functionSpaceFunctionRoomService.findComboRoomsByIntegrationTypeAndDateRange(integrationType, propertyId,
                startDate, endDate, page));
    }

    @GetMapping("/guestRooms")
    @ResponseBody
    @ApiOperation(value = "Retrieves All Function Space Guest Rooms updated within a specified date range", tags = "functionSpace")
    public Page<NucleusFunctionSpaceBookingGuestRoom> getAllGuestRoomsByDateRange(
            @RequestParam(PARAM_CLIENT_CODE) String clientCode,
            @RequestParam(PARAM_PROPERTY_CODE) String propertyCode,
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
            Pageable page) {

        return functionSpaceBookingGuestRoomService.findAllByDateRange(clientCode, propertyCode, startDate, getOrDefault(endDate), page);
    }

    @GetMapping("/bookedGuestRooms")
    @ResponseBody
    @ApiOperation(value = "Retrieves All Function Space Booked Guest Rooms updated within a specified date range", tags = "functionSpace")
    public Page<NucleusFunctionSpaceBookingGuestRoom> getAllBookedGuestRoomsByDateRange(
            @RequestParam(PARAM_CLIENT_CODE) String clientCode,
            @RequestParam(PARAM_PROPERTY_CODE) String propertyCode,
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
            Pageable page) {

        return functionSpaceBookingGuestRoomService.findAllBookedRoomsByDateRange(clientCode, propertyCode, startDate, getOrDefault(endDate), page);
    }

    @GetMapping(value = "/bookedGuestRooms/search", produces = {MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE})
    @ResponseBody
    @ApiOperation(value = "Retrieves Function Space Booked Guest Rooms by integration type updated within a specified date range", tags = "functionSpace")
    public PagedResources<Resource<NucleusFunctionSpaceBookingGuestRoom>> searchBookedGuestRoomsByIntegrationTypeAndDateRange(
            @RequestParam(PARAM_INTEGRATION_TYPE) IntegrationType integrationType,
            @RequestParam(PARAM_PROPERTY_ID) String propertyId,
            @RequestParam(value = "startDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime startDate,
            @RequestParam(value = "endDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime endDate,
            Pageable page, PagedResourcesAssembler<NucleusFunctionSpaceBookingGuestRoom> pagedResourcesAssembler) {

        return pagedResourcesAssembler.toResource(functionSpaceBookingGuestRoomService.findBookedRoomsByIntegrationTypeAndDateRange(integrationType, propertyId, startDate, endDate, page));
    }

    @GetMapping("/guestRoomTypes")
    @ResponseBody
    @ApiOperation(value = "Retrieves All Function Space Guest Room Types updated within a specified date range", tags = "functionSpace")
    public Page<NucleusFunctionSpaceGuestRoomType> getAllGuestRoomTypesByDateRange(
            @RequestParam(PARAM_CLIENT_CODE) String clientCode,
            @RequestParam(PARAM_PROPERTY_CODE) String propertyCode,
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
            Pageable page) {

        return functionSpaceGuestRoomTypeService.findAllByDateRange(clientCode, propertyCode, startDate, getOrDefault(endDate), page);
    }

    @GetMapping("/bookings/pace")
    @ResponseBody
    @ApiOperation(value = "Retrieves All Function Space Booked Paces updated within a specified date range", tags = "functionSpace")
    public Page<NucleusFunctionSpaceBookingPace> getAllBookingPacesByDateRange(
            @RequestParam(PARAM_CLIENT_CODE) String clientCode,
            @RequestParam(PARAM_PROPERTY_CODE) String propertyCode,
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
            Pageable page) {

        return functionSpaceBookingPaceService.findAllByDateRange(clientCode, propertyCode, startDate, getOrDefault(endDate), page);
    }

    @GetMapping(value = "/bookings/pace/search", produces = {MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE})
    @ResponseBody
    @ApiOperation(value = "Retrieves Function Space Booked Paces by integration type updated within a specified date range", tags = "functionSpace")
    public PagedResources<Resource<NucleusFunctionSpaceBookingPace>> searchBookingPacesByIntegrationTypeAndDateRange(
            @RequestParam(PARAM_INTEGRATION_TYPE) IntegrationType integrationType,
            @RequestParam(PARAM_PROPERTY_ID) String propertyId,
            @RequestParam(value = "startDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime startDate,
            @RequestParam(value = "endDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime endDate,
            Pageable page, PagedResourcesAssembler<NucleusFunctionSpaceBookingPace> pagedResourcesAssembler) {

        return pagedResourcesAssembler.toResource(functionSpaceBookingPaceService.findByIntegrationTypeAndDateRange(integrationType, propertyId, startDate, endDate, page));
    }

    @GetMapping("/marketSegments")
    @ResponseBody
    @ApiOperation(value = "Retrieves All Function Space Market Segments updated within a specified date range", tags = "functionSpace")
    public Page<NucleusFunctionSpaceMarketSegment> getAllMarketSegmentsByDateRange(
            @RequestParam(PARAM_CLIENT_CODE) String clientCode,
            @RequestParam(PARAM_PROPERTY_CODE) String propertyCode,
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
            Pageable page) {

        return functionSpaceMarketSegmentService.findAllByDateRange(clientCode, propertyCode, startDate, getOrDefault(endDate), page);
    }

    @GetMapping(value = "/marketSegments/search", produces = {MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE})
    @ResponseBody
    @ApiOperation(value = "Retrieves Function Space Market Segments by integration type updated within a specified date range", tags = "functionSpace")
    public PagedResources<Resource<NucleusFunctionSpaceMarketSegment>> searchMarketSegmentsByIntegrationTypeAndDateRange(
            @RequestParam(PARAM_INTEGRATION_TYPE) IntegrationType integrationType,
            @RequestParam(PARAM_PROPERTY_ID) String propertyid,
            @RequestParam(value = "startDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime startDate,
            @RequestParam(value = "endDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) LocalDateTime endDate,
            Pageable page, PagedResourcesAssembler<NucleusFunctionSpaceMarketSegment> pagedResourcesAssembler) {

        return pagedResourcesAssembler.toResource(functionSpaceMarketSegmentService.findByIntegrationTypeAndDateRange(integrationType, propertyid, startDate, endDate, page));
    }

    @GetMapping("/guestRooms/pace")
    @ResponseBody
    @ApiOperation(value = "Retrieves All Function Space Guest Rooms Paces updated within a specified date range", tags = "functionSpace")
    public Page<NucleusFunctionSpaceGuestRoomPace> getAllGuestRoomPacesByDateRange(
            @RequestParam(PARAM_CLIENT_CODE) String clientCode,
            @RequestParam(PARAM_PROPERTY_CODE) String propertyCode,
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
            Pageable page) {

        return functionSpaceGuestRoomPaceService.findAllByDateRange(clientCode, propertyCode, startDate, getOrDefault(endDate), page);
    }

    @GetMapping(value = "/guestRooms/pace/byDay/withBookingInfo", produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Retrieves All Function Space Bookings Pace By Day data with booking information updated within a specified date range", tags = "functionSpace")
    public PagedResources<Resource<FunctionSpaceGuestRoomPaceWithBookingInfoDto>> getLatestBookingPerDayByGroup(
            @RequestParam(PARAM_CLIENT_CODE) String clientCode,
            @RequestParam(PARAM_PROPERTY_CODE) String propertyCode,
            @RequestParam(value = "startDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) Date startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) Date endDate
            , Pageable pageable
            , PagedResourcesAssembler<FunctionSpaceGuestRoomPaceWithBookingInfoDto> pagedResourcesAssembler
    ) {
        Link link = linkTo(methodOn(FunctionSpaceRestController.class)
                .getLatestBookingPerDayByGroup(clientCode, propertyCode, startDate, endDate, pageable, pagedResourcesAssembler)).withSelfRel();
        Page<FunctionSpaceGuestRoomPaceWithBookingInfoDto> bookingPage = functionSpaceGuestRoomPaceService.getLatestBookingPerDayByGroup(clientCode, propertyCode, startDate, endDate, pageable);
        return pagedResourcesAssembler.toResource(bookingPage, link);
    }

    @DeleteMapping("/property")
    @ResponseBody
    @ApiOperation(value = "Delete All Function Space Guest Rooms Paces based on property Id", tags = "functionSpace")
    public ResponseEntity delete(@RequestParam(PARAM_CLIENT_CODE) String clientCode, @RequestParam(PARAM_PROPERTY_CODE) String propertyCode) {
        functionSpaceEventService.delete(clientCode, propertyCode);
        functionSpaceEventPaceService.delete(clientCode, propertyCode);
        functionSpaceBookingService.delete(clientCode, propertyCode);
        functionSpaceBookingPaceService.delete(clientCode, propertyCode);
        functionSpaceFunctionRoomService.delete(clientCode, propertyCode);
        functionSpaceGuestRoomPaceService.delete(clientCode, propertyCode);
        functionSpaceMarketSegmentService.delete(clientCode, propertyCode);
        functionSpaceBookingGuestRoomService.delete(clientCode, propertyCode);
        functionSpaceEventTypeService.delete(clientCode, propertyCode);
        functionSpaceGuestRoomTypeService.delete(clientCode, propertyCode);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/missingFunctionRoomIdsReceivedInEvents")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @ApiOperation(value = "Retrieves all roomsIds that are received in events but are missing in function space function rooms for a client and property code", tags = "functionSpace")
    public List<String> getMissingFunctionRoomIdsReceivedInEvents(@RequestParam(PARAM_CLIENT_CODE) String clientCode, @RequestParam(PARAM_PROPERTY_CODE) String propertyCode) {
        List<String> distinctRoomIdsInEvent = functionSpaceEventService.getDistinctRoomIdsByClientCodeAndPropertyCode(clientCode, propertyCode);
        List<String> roomIdsInFunctionRoom = functionSpaceFunctionRoomService.fetchAllFunctionRoomIds(clientCode, propertyCode);
        distinctRoomIdsInEvent.removeAll(roomIdsInFunctionRoom);
        return distinctRoomIdsInEvent;
    }
}
