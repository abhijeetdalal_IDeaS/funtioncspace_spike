package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceGuestRoomType;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceGuestRoomTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Service
public class FunctionSpaceGuestRoomTypeService {

    private final NucleusFunctionSpaceGuestRoomTypeRepository functionSpaceBookingGuestRoomTypeRepository;

    @Autowired
    public FunctionSpaceGuestRoomTypeService(NucleusFunctionSpaceGuestRoomTypeRepository functionSpaceBookingGuestRoomTypeRepository) {
        this.functionSpaceBookingGuestRoomTypeRepository = functionSpaceBookingGuestRoomTypeRepository;
    }

    public NucleusFunctionSpaceGuestRoomType findOne(String clientCode, String propertyCode, String functionRoomType) {
        return functionSpaceBookingGuestRoomTypeRepository.findFirstByClientCodeAndPropertyCodeAndFunctionRoomType(clientCode, propertyCode, functionRoomType);
    }

    public List<NucleusFunctionSpaceGuestRoomType> findAll(String clientCode, String propertyCode) {
        return functionSpaceBookingGuestRoomTypeRepository.findByClientCodeAndPropertyCode(clientCode, propertyCode, new Sort(Sort.Direction.ASC, "functionRoomType"));
    }

    public long delete(String clientCode, String propertyCode) {
        return functionSpaceBookingGuestRoomTypeRepository.deleteByClientCodeAndPropertyCode(clientCode, propertyCode);
    }

    public Page<NucleusFunctionSpaceGuestRoomType> findAllByDateRange(String clientCode, String propertyCode, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionSpaceBookingGuestRoomTypeRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(clientCode, propertyCode, startDate, endDate, page);
    }

    public List<NucleusFunctionSpaceGuestRoomType> save(Collection<NucleusFunctionSpaceGuestRoomType> guestRoomTypes) {
        return functionSpaceBookingGuestRoomTypeRepository.saveAll(guestRoomTypes);
    }
}
