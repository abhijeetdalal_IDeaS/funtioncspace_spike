package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.functionspace.constant.Constants;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBookingGuestRoom;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceBookingGuestRoomRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FunctionSpaceBookingGuestRoomService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionSpaceBookingGuestRoomService.class);

    private final NucleusFunctionSpaceBookingGuestRoomRepository functionSpaceBookingGuestRoomRepository;

    private static final String CORRELATION_META_DATA_ID = "correlationMetadataId";
    public static final String CLIENT_CODE = "clientCode";
    public static final String PROPERTY_CODE = "propertyCode";
    public static final String PROPERTY_ID = "propertyId";
    public static final String INTEGRATION_TYPE = "integrationType";
    public static final String BOOKING_ID = "bookingId";
    public static final String OCCUPANCY_DATE = "occupancyDate";
    private static final String ROOM_CATEGORY = "roomCategory";
    public static final String STATUS = "status";
    private static final String GUESTROOM_RATE_SINGLE = "guestRoomRateSingle";
    private static final String BLOCKED_SINGLE_ROOMS = "blockedSingleRooms";
    private static final String PICKUP_SINGLE_ROOMS = "pickupSingleRooms";
    private static final String PICKUP_ROOMS_TOTAL = "pickupRoomsTotal";
    private static final String CONTRACTED_SINGLE_ROOMS = "contractedSingleRooms";
    private static final String CONTRACTED_ROOMS_TOTAL = "contractedRoomsTotal";


    @Autowired
    public FunctionSpaceBookingGuestRoomService(NucleusFunctionSpaceBookingGuestRoomRepository functionSpaceBookingGuestRoomRepository) {
        this.functionSpaceBookingGuestRoomRepository = functionSpaceBookingGuestRoomRepository;
    }

    public NucleusFunctionSpaceBookingGuestRoom findOne(String clientCode, String propertyCode, String bookingId, LocalDate occupancyDate, String roomCategory) {
        return functionSpaceBookingGuestRoomRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndOccupancyDateAndRoomCategory(clientCode, propertyCode, bookingId, occupancyDate, roomCategory);
    }

    public List<NucleusFunctionSpaceBookingGuestRoom> findAll(String clientCode, String propertyCode, String bookingId) {
        return functionSpaceBookingGuestRoomRepository.findByClientCodeAndPropertyCodeAndBookingId(clientCode, propertyCode, bookingId);
    }

    public List<NucleusFunctionSpaceBookingGuestRoom> findAll(String clientCode, String propertyCode) {
        return functionSpaceBookingGuestRoomRepository.findByClientCodeAndPropertyCodeOrderByBookingIdAscOccupancyDateAscRoomCategoryAsc(clientCode, propertyCode);
    }

    public List<NucleusFunctionSpaceBookingGuestRoom> save(List<NucleusFunctionSpaceBookingGuestRoom> bookingGuestRoomList) {
        return functionSpaceBookingGuestRoomRepository.saveAll(bookingGuestRoomList);
    }

    public long delete(String clientCode, String propertyCode) {
        return functionSpaceBookingGuestRoomRepository.deleteByClientCodeAndPropertyCode(clientCode, propertyCode);
    }

    public Page<NucleusFunctionSpaceBookingGuestRoom> findAllByDateRange(String clientCode, String propertyCode, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionSpaceBookingGuestRoomRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
                clientCode, propertyCode, startDate, endDate, page);
    }

    Page<NucleusFunctionSpaceBookingGuestRoom> findAllBookedRoomsByDateRange(String clientCode, String propertyCode, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionSpaceBookingGuestRoomRepository.findByClientCodeAndPropertyCodeAndHasBookedRoomsIsTrueAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
                clientCode, propertyCode, startDate, endDate, page);
    }

    Page<NucleusFunctionSpaceBookingGuestRoom> findBookedRoomsByIntegrationTypeAndDateRange(IntegrationType integrationType, String propertyId, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionSpaceBookingGuestRoomRepository.findByIntegrationTypeAndPropertyIdAndHasBookedRoomsIsTrueAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
                integrationType, propertyId, startDate, endDate, page);
    }

    public Integer findCount(String clientCode, String propertyCode, String bookingId, String status) {
        return functionSpaceBookingGuestRoomRepository.countByClientCodeAndPropertyCodeAndBookingIdAndStatusIsNot(clientCode, propertyCode, bookingId, status);
    }

    public void upsertAll(List<NucleusFunctionSpaceBookingGuestRoom> nucleusFunctionSpaceBookingGuestRooms) {
        if (nucleusFunctionSpaceBookingGuestRooms == null || nucleusFunctionSpaceBookingGuestRooms.isEmpty()) {
            return;
        }
        LOGGER.debug("Upserting {} documents in nucleusFunctionSpaceBookingGuestRoom collection", nucleusFunctionSpaceBookingGuestRooms.size());
        List<Pair<Query, Update>> updates = getPairstoUpdateOrSave(nucleusFunctionSpaceBookingGuestRooms);
        functionSpaceBookingGuestRoomRepository.bulkOpsUpsertMulti(updates);
        LOGGER.debug("Upsert Complete in nucleusFunctionSpaceBookingGuestRoom collection");
    }

    private List<Pair<Query, Update>> getPairstoUpdateOrSave(List<NucleusFunctionSpaceBookingGuestRoom> nucleusFunctionSpaceBookingGuestRooms) {
        List<Pair<Query, Update>> updates = new ArrayList<>();
        nucleusFunctionSpaceBookingGuestRooms.forEach(nucleusFunctionSpaceBookingGuestRoom -> {
            Update update = getUpdate(nucleusFunctionSpaceBookingGuestRoom);

            Pair<Query, Update> pair = Pair.of(createIsPresentQuery(nucleusFunctionSpaceBookingGuestRoom.getBookingId(),
                    nucleusFunctionSpaceBookingGuestRoom.getClientCode(),
                    nucleusFunctionSpaceBookingGuestRoom.getPropertyCode(),
                    nucleusFunctionSpaceBookingGuestRoom.getRoomCategory(),
                    nucleusFunctionSpaceBookingGuestRoom.getOccupancyDate()
            ), update);
            updates.add(pair);
        });
        return updates;
    }

    private Update getUpdate(NucleusFunctionSpaceBookingGuestRoom nucleusFunctionSpaceBookingGuestRoom) {
        Update update = new Update();
        update.set(NucleusFunctionSpaceBookingGuestRoom.LAST_MODIFIED_FIELD, new Date());
        update.set(CORRELATION_META_DATA_ID, nucleusFunctionSpaceBookingGuestRoom.getCorrelationMetadataId());
        update.set(CLIENT_CODE, nucleusFunctionSpaceBookingGuestRoom.getClientCode());
        update.set(PROPERTY_CODE, nucleusFunctionSpaceBookingGuestRoom.getPropertyCode());
        update.set(PROPERTY_ID, nucleusFunctionSpaceBookingGuestRoom.getPropertyId());
        update.set(INTEGRATION_TYPE, nucleusFunctionSpaceBookingGuestRoom.getIntegrationType());
        update.set(BOOKING_ID, nucleusFunctionSpaceBookingGuestRoom.getBookingId());
        update.set(OCCUPANCY_DATE, nucleusFunctionSpaceBookingGuestRoom.getOccupancyDate());
        update.set(ROOM_CATEGORY, nucleusFunctionSpaceBookingGuestRoom.getRoomCategory());
        update.set(STATUS, nucleusFunctionSpaceBookingGuestRoom.getStatus());
        update.set(GUESTROOM_RATE_SINGLE, nucleusFunctionSpaceBookingGuestRoom.getGuestRoomRateSingle());
        update.set(BLOCKED_SINGLE_ROOMS, nucleusFunctionSpaceBookingGuestRoom.getBlockedSingleRooms());
        update.set(PICKUP_SINGLE_ROOMS, nucleusFunctionSpaceBookingGuestRoom.getPickupSingleRooms());
        update.set(PICKUP_ROOMS_TOTAL, nucleusFunctionSpaceBookingGuestRoom.getPickupRoomsTotal());
        update.set(CONTRACTED_SINGLE_ROOMS, nucleusFunctionSpaceBookingGuestRoom.getContractedSingleRooms());
        update.set(CONTRACTED_ROOMS_TOTAL, nucleusFunctionSpaceBookingGuestRoom.getContractedRoomsTotal());
        update.setOnInsert(Constants.CREATED_DATE, new Date());
        return update;
    }

    private Query createIsPresentQuery(String bookingId, String clientCode, String propertyCode, String roomCategory, Date occupancyDate) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BOOKING_ID).is(bookingId));
        query.addCriteria(Criteria.where(CLIENT_CODE).is(clientCode));
        query.addCriteria(Criteria.where(PROPERTY_CODE).is(propertyCode));
        query.addCriteria(Criteria.where(ROOM_CATEGORY).is(roomCategory));
        query.addCriteria(Criteria.where(OCCUPANCY_DATE).is(occupancyDate));
        return query;
    }
}