package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.functionspace.constant.Constants;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.functionspace.entity.*;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceBookingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FunctionSpaceBookingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionSpaceBookingService.class);

    public static final String CORRELATION_META_DATA_ID = "correlationMetadataId";
    public static final String CLIENT_CODE = "clientCode";
    public static final String PROPERTY_CODE = "propertyCode";
    public static final String INTEGRATION_TYPE = "integrationType";
    public static final String BOOKING_ID = "bookingId";
    public static final String BLOCK_CODE = "blockCode";
    public static final String PROPERTY_ID = "propertyId";
    public static final String BLOCK_NAME = "blockName";
    public static final String OPPORTUNITY_CREATE_DATE = "opportunityCreateDate";
    public static final String MARKET_SEGMENT_CODE = "marketSegmentCode";
    public static final String ARRIVAL_DATE = "arrivalDate";
    public static final String DEPARTURE_DATE = "departureDate";
    public static final String STATUS = "status";
    public static final String INSERT_DATE = "insertDate";
    public static final String BOOKING_TYPE = "bookingType";
    public static final String DECISION_DATE = "decisionDate";
    public static final String CANCELLATION_DATE = "cancellationDate";
    public static final String CUTOFF_DATE = "cutoffDate";
    public static final String UPDATED_DATE = "updatedDate";
    public static final String BLOCK_OWNER_PRIMARY = "blockOwnerPrimary";
    public static final String CATERING_OWNER = "cateringOwner";
    public static final String BLOCK_STATUS_WHEN_LOST = "blockStatusWhenLost";
    public static final String LOST_BUSINESS_REASON = "lostBusinessReason";
    public static final String LOST_TO_DESTINATION = "lostToDestination";
    public static final String ATTENDEES = "attendees";
    private static final String FUNCTION_SPACE_BOOKING_CATEGORY = "functionSpaceBookingCategory";


    // Create an array of all the classes that need to be updated
    static final Class<?>[] FUNCTION_SPACE_BOOKING_CLASS_HIERARCHY = {NucleusFunctionSpaceBooking.class, NucleusFunctionSpaceBookingGuestRoom.class, NucleusFunctionSpaceGuestRoomPace.class, NucleusFunctionSpaceBookingPace.class, NucleusFunctionSpaceEvent.class};


    private MongoOperations mongoOperations;
    private final NucleusFunctionSpaceBookingRepository nucleusFunctionSpaceBookingRepository;

    @Autowired
    public FunctionSpaceBookingService(MongoOperations mongoOperations, NucleusFunctionSpaceBookingRepository nucleusFunctionSpaceBookingRepository) {
        this.mongoOperations = mongoOperations;
        this.nucleusFunctionSpaceBookingRepository = nucleusFunctionSpaceBookingRepository;
    }

    void updateEntireBookingForChange(IntegrationType integrationType, String propertyId, String bookingId, Date lastModifiedDate) {
        // Create findOne and update queries
        Query query = new Query(Criteria.where(INTEGRATION_TYPE).is(integrationType).and(PROPERTY_ID).is(propertyId).and(BOOKING_ID).is(bookingId));
        Update update = Update.update("lastModifiedDate", lastModifiedDate);

        // Iterate over the classes in the booking hierarchy
        for (Class<?> clazz : FUNCTION_SPACE_BOOKING_CLASS_HIERARCHY) {
            mongoOperations.findAndModify(query, update, clazz);
        }
    }

    public NucleusFunctionSpaceBooking findOne(String clientCode, String propertyCode, String bookingId) {
        return nucleusFunctionSpaceBookingRepository.findFirstByClientCodeAndPropertyCodeAndBookingId(clientCode, propertyCode, bookingId);
    }

    public List<NucleusFunctionSpaceBooking> findAll(String clientCode, String propertyCode) {
        return nucleusFunctionSpaceBookingRepository.findByClientCodeAndPropertyCodeOrderByBookingIdAsc(clientCode, propertyCode);
    }

    public long delete(String clientCode, String propertyCode) {
        return nucleusFunctionSpaceBookingRepository.deleteByClientCodeAndPropertyCode(clientCode, propertyCode);
    }

    public NucleusFunctionSpaceBooking save(NucleusFunctionSpaceBooking booking) {
        return nucleusFunctionSpaceBookingRepository.save(booking);
    }

    public Page<NucleusFunctionSpaceBooking> findAllByDateRange(String clientCode, String propertyCode, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return nucleusFunctionSpaceBookingRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(clientCode, propertyCode, startDate, endDate, page);
    }

    public List<NucleusFunctionSpaceBooking> findByClientCodeAndPropertyCodeAndBookingIdIn(String clientCode, String propertyCode, List<String> bookingId) {
        return nucleusFunctionSpaceBookingRepository.findByClientCodeAndPropertyCodeAndBookingIdIn(clientCode, propertyCode, bookingId);
    }

    public Page<NucleusFunctionSpaceBooking> findByIntegrationTypeAndNotCategoryAndDateRange(IntegrationType integrationType, String propertyId, FunctionSpaceBookingCategory functionSpaceBookingCategory, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return nucleusFunctionSpaceBookingRepository
                .findByIntegrationTypeAndPropertyIdAndFunctionSpaceBookingCategoryNotAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
                        integrationType, propertyId, functionSpaceBookingCategory, startDate, endDate, page);
    }

    public Page<NucleusFunctionSpaceBooking> findByClientAndPropertyAndNotCategoryAndDateRange(String clientCode, String propertyCode, FunctionSpaceBookingCategory functionSpaceBookingCategory, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return nucleusFunctionSpaceBookingRepository
                .findByClientCodeAndPropertyCodeAndFunctionSpaceBookingCategoryNotAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
                        clientCode, propertyCode, functionSpaceBookingCategory, startDate, endDate, page);
    }

    public void upsertAll(List<NucleusFunctionSpaceBooking> nucleusFunctionSpaceBookings) {
        if (nucleusFunctionSpaceBookings == null || nucleusFunctionSpaceBookings.isEmpty()) {
            return;
        }
        LOGGER.debug("Upserting {} documents in nucleusFunctionSpaceBooking collection", nucleusFunctionSpaceBookings.size());
        List<Pair<Query, Update>> updates = getPairstoUpdateOrSave(nucleusFunctionSpaceBookings);
        nucleusFunctionSpaceBookingRepository.bulkOpsUpsertMulti(updates);
        LOGGER.debug("Upsert Complete in nucleusFunctionSpaceBooking collection");
    }

    private List<Pair<Query, Update>> getPairstoUpdateOrSave(List<NucleusFunctionSpaceBooking> nucleusFunctionSpaceBookings) {
        List<Pair<Query, Update>> updates = new ArrayList<>();

        nucleusFunctionSpaceBookings.forEach(nucleusFunctionSpaceBooking -> {
            Update update = getUpdate(nucleusFunctionSpaceBooking);

            Pair<Query, Update> pair = Pair.of(createIsPresentQuery(nucleusFunctionSpaceBooking.getBookingId(), nucleusFunctionSpaceBooking.getClientCode(), nucleusFunctionSpaceBooking.getPropertyCode()), update);
            updates.add(pair);
        });

        return updates;
    }

    private Update getUpdate(NucleusFunctionSpaceBooking nucleusFunctionSpaceBooking) {
        Update update = new Update();
        update.set(NucleusFunctionSpaceBooking.LAST_MODIFIED_FIELD, new Date());
        update.set(CORRELATION_META_DATA_ID, nucleusFunctionSpaceBooking.getCorrelationMetadataId());
        update.set(CLIENT_CODE, nucleusFunctionSpaceBooking.getClientCode());
        update.set(PROPERTY_CODE, nucleusFunctionSpaceBooking.getPropertyCode());
        update.set(INTEGRATION_TYPE, nucleusFunctionSpaceBooking.getIntegrationType());
        update.set(BOOKING_ID, nucleusFunctionSpaceBooking.getBookingId());
        update.set(BLOCK_CODE, nucleusFunctionSpaceBooking.getBlockCode());
        update.set(PROPERTY_ID, nucleusFunctionSpaceBooking.getPropertyId());
        update.set(BLOCK_NAME, nucleusFunctionSpaceBooking.getBlockName());
        update.set(OPPORTUNITY_CREATE_DATE, nucleusFunctionSpaceBooking.getOpportunityCreateDate());
        update.set(MARKET_SEGMENT_CODE, nucleusFunctionSpaceBooking.getMarketSegmentCode());
        update.set(ARRIVAL_DATE, nucleusFunctionSpaceBooking.getArrivalDate());
        update.set(DEPARTURE_DATE, nucleusFunctionSpaceBooking.getDepartureDate());
        update.set(STATUS, nucleusFunctionSpaceBooking.getStatus());
        update.set(INSERT_DATE, nucleusFunctionSpaceBooking.getInsertDate());
        update.set(BOOKING_TYPE, nucleusFunctionSpaceBooking.getBookingType());
        update.set(DECISION_DATE, nucleusFunctionSpaceBooking.getDecisionDate());
        update.set(CANCELLATION_DATE, nucleusFunctionSpaceBooking.getCancellationDate());
        update.set(CUTOFF_DATE, nucleusFunctionSpaceBooking.getCutoffDate());
        update.set(UPDATED_DATE, nucleusFunctionSpaceBooking.getUpdateDate());
        update.set(BLOCK_OWNER_PRIMARY, nucleusFunctionSpaceBooking.getBlockOwnerPrimary());
        update.set(CATERING_OWNER, nucleusFunctionSpaceBooking.getCateringOwner());
        update.set(BLOCK_STATUS_WHEN_LOST, nucleusFunctionSpaceBooking.getBlockStatusWhenLost());
        update.set(LOST_BUSINESS_REASON, nucleusFunctionSpaceBooking.getLostBusinessReason());
        update.set(LOST_TO_DESTINATION, nucleusFunctionSpaceBooking.getLostToDestination());
        update.set(ATTENDEES, nucleusFunctionSpaceBooking.getAttendees());
        update.set(FUNCTION_SPACE_BOOKING_CATEGORY, nucleusFunctionSpaceBooking.getFunctionSpaceBookingCategory());
        update.setOnInsert(Constants.CREATED_DATE, new Date());
        return update;
    }

    private Query createIsPresentQuery(String bookingId, String clientCode, String propertyCode) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BOOKING_ID).is(bookingId));
        query.addCriteria(Criteria.where(CLIENT_CODE).is(clientCode));
        query.addCriteria(Criteria.where(PROPERTY_CODE).is(propertyCode));
        return query;
    }
}
