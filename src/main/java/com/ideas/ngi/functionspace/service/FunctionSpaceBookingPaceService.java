package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.nucleus.data.AuditableEntity;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBookingPace;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceBookingPaceRepository;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Service
public class FunctionSpaceBookingPaceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionSpaceBookingPaceService.class);

    private final NucleusFunctionSpaceBookingPaceRepository functionSpaceBookingPaceRepository;

    @Autowired
    public FunctionSpaceBookingPaceService(NucleusFunctionSpaceBookingPaceRepository functionSpaceBookingPaceRepository) {
        this.functionSpaceBookingPaceRepository = functionSpaceBookingPaceRepository;
    }

    public NucleusFunctionSpaceBookingPace findOne(String clientCode, String propertyCode, String bookingId) {
        return functionSpaceBookingPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdOrderByChangeDateDesc(clientCode, propertyCode, bookingId);
    }

    public NucleusFunctionSpaceBookingPace findOne(String clientCode, String propertyCode, String bookingId, Date changeDate) {
        return functionSpaceBookingPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndChangeDateLessThanEqualOrderByChangeDateDesc(clientCode, propertyCode, bookingId, changeDate);
    }

    public List<NucleusFunctionSpaceBookingPace> findAll(String clientCode, String propertyCode) {
        return functionSpaceBookingPaceRepository.findByClientCodeAndPropertyCodeOrderByBookingIdAscBookingPaceIdAsc(clientCode, propertyCode);
    }

    public NucleusFunctionSpaceBookingPace save(NucleusFunctionSpaceBookingPace functionSpaceBookingPace) {
        return functionSpaceBookingPaceRepository.save(functionSpaceBookingPace);
    }

    public long delete(String clientCode, String propertyCode) {
        return functionSpaceBookingPaceRepository.deleteByClientCodeAndPropertyCode(clientCode, propertyCode);
    }

    public Page<NucleusFunctionSpaceBookingPace> findAllByDateRange(String clientCode, String propertyCode, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionSpaceBookingPaceRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(clientCode, propertyCode, startDate, endDate, page);
    }

    public Page<NucleusFunctionSpaceBookingPace> findByIntegrationTypeAndDateRange(IntegrationType integrationType, String propertyId, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionSpaceBookingPaceRepository.findByIntegrationTypeAndPropertyIdAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(integrationType, propertyId, startDate, endDate, page);
    }

    public NucleusFunctionSpaceBookingPace findOne(String clientCode, String propertyCode, String bookingId, String bookingPaceId) {
        return functionSpaceBookingPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndBookingPaceId(clientCode, propertyCode, bookingId, bookingPaceId);
    }

    public List<NucleusFunctionSpaceBookingPace> findAll(String clientCode, String propertyCode, String bookingId) {
        return functionSpaceBookingPaceRepository.findByClientCodeAndPropertyCodeAndBookingId(clientCode, propertyCode, bookingId);
    }

    public void saveAll(List<NucleusFunctionSpaceBookingPace> nucleusFunctionSpaceBookingPaceList) {

        for (NucleusFunctionSpaceBookingPace nucleusFunctionSpaceBookingPace : nucleusFunctionSpaceBookingPaceList) {
            nucleusFunctionSpaceBookingPace.generateBookingPaceId();
            NucleusFunctionSpaceBookingPace existingFunctionSpaceBookingPace = findOne(nucleusFunctionSpaceBookingPace.getClientCode()
                    , nucleusFunctionSpaceBookingPace.getPropertyCode()
                    , nucleusFunctionSpaceBookingPace.getBookingId()
                    , nucleusFunctionSpaceBookingPace.getBookingPaceId());
            if (existingFunctionSpaceBookingPace != null) {
                nucleusFunctionSpaceBookingPace.setId(existingFunctionSpaceBookingPace.getId());
                nucleusFunctionSpaceBookingPace.setCreateDate(existingFunctionSpaceBookingPace.getCreateDate());
            }
            if (!equalsWithoutCorrelationMetadataId(existingFunctionSpaceBookingPace, nucleusFunctionSpaceBookingPace)) {
                LOGGER.debug("Inserting document with bookingPaceId : {} in nucleusFunctionSpaceBookingGuestRoom collection", nucleusFunctionSpaceBookingPace.getBookingPaceId());
                save(nucleusFunctionSpaceBookingPace);
                LOGGER.debug("Insert Complete for document with bookingPaceId : {} nucleusFunctionSpaceBookingGuestRoom collection", nucleusFunctionSpaceBookingPace.getBookingPaceId());
            }
        }
    }

    public boolean equalsWithoutCorrelationMetadataId(AuditableEntity ae1, AuditableEntity ae2) {
        return EqualsBuilder.reflectionEquals(ae1, ae2, "id", "correlationMetadataId", "hasBookedRooms", "createDate", "lastModifiedDate");
    }
}
