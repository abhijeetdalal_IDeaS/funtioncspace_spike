package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.functionspace.constant.Constants;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEvent;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEventType;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceEventTypeRepository;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FunctionSpaceEventTypeService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public static final String CLIENT_CODE = "clientCode";
    public static final String PROPERTY_CODE = "propertyCode";
    public static final String INTEGRATION_TYPE = "integrationType";
    public static final String PROPERTY_ID = "propertyId";
    public static final String NAME = "name";
    public static final String ABBREVIATION = "abbreviation";


    private final NucleusFunctionSpaceEventTypeRepository functionSpaceEventTypeRepository;

    @Autowired
    public FunctionSpaceEventTypeService(NucleusFunctionSpaceEventTypeRepository functionSpaceEventTypeRepository) {
        this.functionSpaceEventTypeRepository = functionSpaceEventTypeRepository;
    }

    public NucleusFunctionSpaceEventType findOrCreateEventType(IntegrationType integrationType, NucleusFunctionSpaceEvent functionSpaceEvent) {
        logger.debug("Looking for FunctionSpaceEventType: integrationType={}, propertyId={}, abbreviation={}",
                integrationType, functionSpaceEvent.getPropertyId(), functionSpaceEvent.getEventTypeCode());

        NucleusFunctionSpaceEventType eventType = functionSpaceEventTypeRepository.findFirstByClientCodeAndPropertyCodeAndAbbreviation(
                functionSpaceEvent.getClientCode(), functionSpaceEvent.getPropertyCode(), functionSpaceEvent.getEventTypeCode());
        if (eventType == null) {
            eventType = new NucleusFunctionSpaceEventType();
            eventType.setIntegrationType(integrationType);
            eventType.setPropertyId(functionSpaceEvent.getPropertyId());
            eventType.setClientCode(functionSpaceEvent.getClientCode());
            eventType.setPropertyCode(functionSpaceEvent.getPropertyCode());
            eventType.setAbbreviation(functionSpaceEvent.getEventTypeCode());
            eventType.setName(functionSpaceEvent.getEventTypeCode());

            try {
                eventType = functionSpaceEventTypeRepository.save(eventType);
            } catch (DuplicateKeyException e) {
                logger.warn("Encountered duplicate event type for property id " + functionSpaceEvent.getPropertyId() + " and abbreviation " + functionSpaceEvent.getEventTypeCode(), e);
                eventType = functionSpaceEventTypeRepository.findFirstByClientCodeAndPropertyCodeAndAbbreviation(
                        functionSpaceEvent.getClientCode(), functionSpaceEvent.getPropertyCode(), functionSpaceEvent.getEventTypeCode());
            }
        }

        return eventType;
    }

    public NucleusFunctionSpaceEventType save(NucleusFunctionSpaceEventType functionSpaceEventType) {
        return functionSpaceEventTypeRepository.save(functionSpaceEventType);
    }

    public NucleusFunctionSpaceEventType findOne(String clientCode, String propertyCode, String abbreviation) {
        return functionSpaceEventTypeRepository.findFirstByClientCodeAndPropertyCodeAndAbbreviation(clientCode, propertyCode, abbreviation);
    }

    public Page<NucleusFunctionSpaceEventType> findAllByDateRange(String clientCode, String propertyCode, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionSpaceEventTypeRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(clientCode,
                propertyCode, startDate, endDate, page);
    }

    public Page<NucleusFunctionSpaceEventType> findByIntegrationTypeAndDateRange(IntegrationType integrationType, String propertyId, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionSpaceEventTypeRepository.findByIntegrationTypeAndPropertyIdAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(integrationType, propertyId, startDate, endDate, page);
    }

    public long delete(String clientCode, String propertyCode) {
        return functionSpaceEventTypeRepository.deleteByClientCodeAndPropertyCode(clientCode, propertyCode);
    }

    public List<NucleusFunctionSpaceEventType> findAll(String clientCode, String propertyCode) {
        return functionSpaceEventTypeRepository.findByClientCodeAndPropertyCodeOrderByAbbreviationAsc(clientCode, propertyCode);
    }

    public void upsertAll(List<NucleusFunctionSpaceEventType> nucleusFunctionSpaceEventTypes) {

        if (CollectionUtils.isEmpty(nucleusFunctionSpaceEventTypes)) {
            return;
        }
        logger.debug("Upserting {} documents in nucleusFunctionSpaceEventType collection", nucleusFunctionSpaceEventTypes.size());
        List<Pair<Query, Update>> updates = getPairsToUpdateOrSave(nucleusFunctionSpaceEventTypes);
        functionSpaceEventTypeRepository.bulkOpsUpsertMulti(updates);
        logger.debug("Upsert Complete in nucleusFunctionSpaceEventType collection");

    }

    private List<Pair<Query, Update>> getPairsToUpdateOrSave(List<NucleusFunctionSpaceEventType> nucleusFunctionSpaceEventTypes) {

        List<Pair<Query, Update>> updates = new ArrayList<>();

        nucleusFunctionSpaceEventTypes.forEach(nucleusFunctionSpaceEventType -> {
            Update update = getUpdate(nucleusFunctionSpaceEventType);

            Pair<Query, Update> pair = Pair.of(createIsPresentQuery(nucleusFunctionSpaceEventType.getAbbreviation(), nucleusFunctionSpaceEventType.getClientCode(), nucleusFunctionSpaceEventType.getPropertyCode()), update);
            updates.add(pair);
        });

        return updates;
    }

    private Query createIsPresentQuery(String abbreviation, String clientCode, String propertyCode) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ABBREVIATION).is(abbreviation));
        query.addCriteria(Criteria.where(CLIENT_CODE).is(clientCode));
        query.addCriteria(Criteria.where(PROPERTY_CODE).is(propertyCode));
        return query;
    }

    private Update getUpdate(NucleusFunctionSpaceEventType nucleusFunctionSpaceEventType) {

        Update update = new Update();
        update.set(NucleusFunctionSpaceEventType.LAST_MODIFIED_FIELD, new Date());
        update.set(CLIENT_CODE, nucleusFunctionSpaceEventType.getClientCode());
        update.set(PROPERTY_CODE, nucleusFunctionSpaceEventType.getPropertyCode());
        update.set(PROPERTY_ID, nucleusFunctionSpaceEventType.getPropertyId());
        update.set(NAME, nucleusFunctionSpaceEventType.getName());
        update.set(INTEGRATION_TYPE, nucleusFunctionSpaceEventType.getIntegrationType());
        update.set(ABBREVIATION, nucleusFunctionSpaceEventType.getAbbreviation());
        update.setOnInsert(Constants.CREATED_DATE, new Date());

        return update;
    }
}