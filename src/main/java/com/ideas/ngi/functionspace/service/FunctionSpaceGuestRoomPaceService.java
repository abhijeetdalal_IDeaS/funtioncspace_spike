package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.functionspace.constant.Constants;
import com.ideas.ngi.functionspace.dto.FunctionSpaceGuestRoomPaceWithBookingInfoDto;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBooking;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBookingPace;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceGuestRoomPace;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceGuestRoomPaceRepository;
import com.ideas.ngi.nucleus.data.occupancy.service.CountHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregationOptions;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

@Service
public class FunctionSpaceGuestRoomPaceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionSpaceGuestRoomPaceService.class);

    private final NucleusFunctionSpaceGuestRoomPaceRepository functionSpaceGuestRoomPaceRepository;
    private final FunctionSpaceBookingService functionSpaceBookingService;
    private final FunctionSpaceBookingPaceService spaceBookingPaceService;
    private MongoOperations mongoOperations;

    private static final Integer DEFAULT_INTEGER_VALUE = 0;
    private static final BigDecimal DEFAULT_BIG_DECIMAL_VALUE = BigDecimal.ZERO;

    @Autowired
    public FunctionSpaceGuestRoomPaceService(NucleusFunctionSpaceGuestRoomPaceRepository functionSpaceGuestRoomPaceRepository, MongoOperations mongoOperations, FunctionSpaceBookingService functionSpaceBookingService, FunctionSpaceBookingPaceService spaceBookingPaceService) {
        this.functionSpaceBookingService = functionSpaceBookingService;
        this.functionSpaceGuestRoomPaceRepository = functionSpaceGuestRoomPaceRepository;
        this.mongoOperations = mongoOperations;
        this.spaceBookingPaceService = spaceBookingPaceService;
    }

    public NucleusFunctionSpaceGuestRoomPace findOne(String clientCode, String propertyCode, String bookingId, String roomType, LocalDate stayDate, String bookingPaceId) {
        return functionSpaceGuestRoomPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateAndBookingPaceId(clientCode, propertyCode, bookingId, roomType, stayDate, bookingPaceId);
    }

    public NucleusFunctionSpaceGuestRoomPace findOne(String clientCode, String propertyCode, String bookingId, String roomType, LocalDate stayDate) {
        return functionSpaceGuestRoomPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateOrderByChangeDateDesc(clientCode, propertyCode, bookingId, roomType, stayDate);
    }

    public List<NucleusFunctionSpaceGuestRoomPace> findAll(String clientCode, String propertyCode) {
        return functionSpaceGuestRoomPaceRepository.findByClientCodeAndPropertyCodeOrderByBookingIdAscBookingPaceIdAsc(clientCode, propertyCode);
    }

    public List<NucleusFunctionSpaceGuestRoomPace> findAll(String clientCode, String propertyCode, String bookingId) {
        return functionSpaceGuestRoomPaceRepository.findByClientCodeAndPropertyCodeAndBookingId(clientCode, propertyCode, bookingId);
    }

    public List<NucleusFunctionSpaceGuestRoomPace> save(List<NucleusFunctionSpaceGuestRoomPace> functionSpaceGuestRoomPaces) {
        return functionSpaceGuestRoomPaceRepository.saveAll(functionSpaceGuestRoomPaces);
    }

    public long delete(String clientCode, String propertyCode) {
        return functionSpaceGuestRoomPaceRepository.deleteByClientCodeAndPropertyCode(clientCode, propertyCode);
    }

    public Page<NucleusFunctionSpaceGuestRoomPace> findAllByDateRange(String clientCode, String propertyCode, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionSpaceGuestRoomPaceRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
                clientCode, propertyCode, startDate, endDate, page);
    }

    public Page<FunctionSpaceGuestRoomPaceWithBookingInfoDto> getLatestBookingPerDayByGroup(String clientCode,
                                                                                            String propertyCode,
                                                                                            Date startDate,
                                                                                            Date endDate,
                                                                                            Pageable pageable) {
        List<NucleusFunctionSpaceGuestRoomPace> nucleusFunctionSpaceGuestRoomPaces = getChangesPerBooking(clientCode, propertyCode, startDate, endDate, pageable.getPageSize(), pageable.getOffset());
        List<FunctionSpaceGuestRoomPaceWithBookingInfoDto> functionSpaceGuestRoomPaceWithBookingInfoDtos = getFunctionSpaceGuestRoomPaceWithBookingInfo(clientCode, propertyCode, nucleusFunctionSpaceGuestRoomPaces);
        return new PageImpl<>(functionSpaceGuestRoomPaceWithBookingInfoDtos, pageable, getChangesPerBookingCount(clientCode, propertyCode, startDate, endDate));
    }

    List<FunctionSpaceGuestRoomPaceWithBookingInfoDto> getFunctionSpaceGuestRoomPaceWithBookingInfo(String clientCode,
                                                                                                    String propertyCode,
                                                                                                    List<NucleusFunctionSpaceGuestRoomPace> pagedBookingsPace) {
        List<String> distinctBookingIds = pagedBookingsPace.stream().map(NucleusFunctionSpaceGuestRoomPace::getBookingId).distinct().collect(Collectors.toList());
        List<NucleusFunctionSpaceBooking> byClientCodeAndPropertyCodeAndBookingIdIn =
                functionSpaceBookingService.findByClientCodeAndPropertyCodeAndBookingIdIn(clientCode, propertyCode, distinctBookingIds);
        Map<String, NucleusFunctionSpaceBooking> bookingMetaDataMap = byClientCodeAndPropertyCodeAndBookingIdIn.stream().
                collect(Collectors.toMap(NucleusFunctionSpaceBooking::getBookingId, Function.identity()));

        Map<String, NucleusFunctionSpaceBookingPace> bookingPaceStore = new HashMap<>();
        return pagedBookingsPace.stream().map(obj ->
                getResponseDto(clientCode, propertyCode, bookingMetaDataMap, bookingPaceStore, obj)).collect(Collectors.toList());
    }

    protected FunctionSpaceGuestRoomPaceWithBookingInfoDto getResponseDto(String clientCode, String propertyCode, Map<String, NucleusFunctionSpaceBooking> bookingMetaDataMap, Map<String, NucleusFunctionSpaceBookingPace> bookingPaceStore, NucleusFunctionSpaceGuestRoomPace obj) {
        FunctionSpaceGuestRoomPaceWithBookingInfoDto dto = new FunctionSpaceGuestRoomPaceWithBookingInfoDto(obj, bookingMetaDataMap.get(obj.getBookingId()));
        NucleusFunctionSpaceBookingPace bookingPace;
        Date dtoChangeDate = dto.getChangeDate();
        String key = dto.getBookingId() +"-"+ dtoChangeDate.getTime();
        if (bookingPaceStore.get(key) == null) {
            bookingPace = spaceBookingPaceService.findOne(clientCode, propertyCode, dto.getBookingId(), dtoChangeDate);
            bookingPaceStore.put(key, bookingPace);
        } else {
            bookingPace = bookingPaceStore.get(key);
        }
        if (bookingPace != null && bookingPace.getArrivalDate() != null && bookingPace.getDepartureDate() != null) {
            dto.setArrivalDate(bookingPace.getArrivalDate());
            dto.setDepartureDate(bookingPace.getDepartureDate());
        }
        return dto;
    }

    List<NucleusFunctionSpaceGuestRoomPace> getChangesPerBooking(String clientCode,
                                                                 String propertyCode, Date startDate,
                                                                 Date endDate, int limit, long skip) {
        AggregationOperation limitAggregation = Aggregation.limit(limit);
        AggregationOperation skipAggregation = Aggregation.skip(skip);
        Aggregation aggregation = Aggregation.newAggregation(NucleusFunctionSpaceGuestRoomPace.class,
                Aggregation.match(Criteria.where(Constants.CLIENT_CODE).is(clientCode).and(Constants.PROPERTY_CODE).is(propertyCode).
                        andOperator(
                                Criteria.where(Constants.LAST_MODIFIED_DATE).lt(endDate),
                                Criteria.where(Constants.LAST_MODIFIED_DATE).gte(startDate)
                        ))
                , projectionByDateRemoveZoneOffset()
                , projectionByDate()
                , sortProjectedDateAscAndlastModifiedDateDesc()
                , groupByGroupBooking()
                , sortByStayDateAscChangeDateAsc()
                , skipAggregation
                , limitAggregation
        );
        AggregationResults<NucleusFunctionSpaceGuestRoomPace> aggregationResults =
                mongoOperations.aggregate(aggregation.withOptions(newAggregationOptions().allowDiskUse(true).build()),
                        NucleusFunctionSpaceGuestRoomPace.class, NucleusFunctionSpaceGuestRoomPace.class);
        return aggregationResults.getMappedResults();
    }

    long getChangesPerBookingCount(String clientCode,
                                   String propertyCode, Date startDate,
                                   Date endDate) {
        CountOperation count = Aggregation.count().as("count");
        Aggregation aggregation = Aggregation.newAggregation(NucleusFunctionSpaceGuestRoomPace.class,
                Aggregation.match(Criteria.where(Constants.CLIENT_CODE).is(clientCode).and(Constants.PROPERTY_CODE).is(propertyCode).
                        andOperator(
                                Criteria.where(Constants.LAST_MODIFIED_DATE).lt(endDate),
                                Criteria.where(Constants.LAST_MODIFIED_DATE).gte(startDate)
                        ))
                , projectionByDateRemoveZoneOffset()
                , projectionByDate()
                , groupByGroupBooking()
                , count
        );
        CountHolder countHolder = mongoOperations.aggregate(aggregation.withOptions(newAggregationOptions().allowDiskUse(true).build()),
                NucleusFunctionSpaceGuestRoomPace.class, CountHolder.class).getUniqueMappedResult();
        return countHolder != null ? countHolder.getCount() : 0;
    }

    private ProjectionOperation projectionByDateRemoveZoneOffset() {
        String zoneId = TimeZone.getDefault().getID();
        return Aggregation.project().and(Constants.CLIENT_CODE).as(Constants.CLIENT_CODE)
                .and(Constants.PROPERTY_CODE).as(Constants.PROPERTY_CODE)
                .and(Constants.BOOKING_ID).as(Constants.BOOKING_ID)
                .and(Constants.ROOM_TYPE).as(Constants.ROOM_TYPE)
                .and(Constants.STAY_DATE).as(Constants.STAY_DATE)
                .and(Constants.BOOKING_PACE_ID).as(Constants.BOOKING_PACE_ID)
                .and(Constants.PICKUP_ROOM_NIGHTS).as(Constants.PICKUP_ROOM_NIGHTS)
                .and(Constants.PICKUP_ROOM_REVENUE).as(Constants.PICKUP_ROOM_REVENUE)
                .and(Constants.PICKUP_ROOM_NIGHTS_CHANGED).as(Constants.PICKUP_ROOM_NIGHTS_CHANGED)
                .and(Constants.PICKUP_ROOM_REVENUE_CHANGED).as(Constants.PICKUP_ROOM_REVENUE_CHANGED)
                .and(Constants.BLOCKED_ROOM_NIGHTS_CHANGED).as(Constants.BLOCKED_ROOM_NIGHTS_CHANGED)
                .and(Constants.BLOCKED_ROOM_REVENUE_CHANGED).as(Constants.BLOCKED_ROOM_REVENUE_CHANGED)
                .and(Constants.CONTRACTED_ROOM_NIGHTS_CHANGED).as(Constants.CONTRACTED_ROOM_NIGHTS_CHANGED)
                .and(Constants.CONTRACTED_ROOM_REVENUE_CHANGED).as(Constants.CONTRACTED_ROOM_REVENUE_CHANGED)
                .and(Constants.STAY_DATE).as(Constants.STAY_DATE)
                .and(Constants.CHANGE_DATE).as(Constants.CHANGE_DATE)
                .and(Constants.GUEST_ROOM_STATUS).as(Constants.GUEST_ROOM_STATUS)
                .and(Constants.LAST_MODIFIED_DATE).as(Constants.LAST_MODIFIED_DATE)
                .and(DateOperators.Hour.hour("$" + Constants.CHANGE_DATE).withTimezone(DateOperators.Timezone.valueOf(zoneId))).as(Constants.OFFSET_ADJUSTED_TIME_HOUR)
                .and(DateOperators.Minute.minute("$" + Constants.CHANGE_DATE).withTimezone(DateOperators.Timezone.valueOf(zoneId))).as(Constants.OFFSET_ADJUSTED_TIME_MINUTE)
                ;
    }

    private ProjectionOperation projectionByDate() {
        return Aggregation.project().and(Constants.CLIENT_CODE).as(Constants.CLIENT_CODE)
                .and(Constants.PROPERTY_CODE).as(Constants.PROPERTY_CODE)
                .and(Constants.BOOKING_ID).as(Constants.BOOKING_ID)
                .and(Constants.ROOM_TYPE).as(Constants.ROOM_TYPE)
                .and(Constants.STAY_DATE).as(Constants.STAY_DATE)
                .and(Constants.BOOKING_PACE_ID).as(Constants.BOOKING_PACE_ID)
                .and(Constants.PICKUP_ROOM_NIGHTS).as(Constants.PICKUP_ROOM_NIGHTS)
                .and(Constants.PICKUP_ROOM_REVENUE).as(Constants.PICKUP_ROOM_REVENUE)
                .and(Constants.PICKUP_ROOM_NIGHTS_CHANGED).as(Constants.PICKUP_ROOM_NIGHTS_CHANGED)
                .and(Constants.PICKUP_ROOM_REVENUE_CHANGED).as(Constants.PICKUP_ROOM_REVENUE_CHANGED)
                .and(Constants.BLOCKED_ROOM_NIGHTS_CHANGED).as(Constants.BLOCKED_ROOM_NIGHTS_CHANGED)
                .and(Constants.BLOCKED_ROOM_REVENUE_CHANGED).as(Constants.BLOCKED_ROOM_REVENUE_CHANGED)
                .and(Constants.CONTRACTED_ROOM_NIGHTS_CHANGED).as(Constants.CONTRACTED_ROOM_NIGHTS_CHANGED)
                .and(Constants.CONTRACTED_ROOM_REVENUE_CHANGED).as(Constants.CONTRACTED_ROOM_REVENUE_CHANGED)
                .and(Constants.STAY_DATE).as(Constants.STAY_DATE)
                .and(Constants.CHANGE_DATE).as(Constants.CHANGE_DATE)
                .and(Constants.GUEST_ROOM_STATUS).as(Constants.GUEST_ROOM_STATUS)
                .and(Constants.LAST_MODIFIED_DATE).as(Constants.LAST_MODIFIED_DATE)
                .andExpression(Constants.CHANGE_DATE + " - ( ( " + Constants.OFFSET_ADJUSTED_TIME_HOUR + " *60*60*1000) + ( " + Constants.OFFSET_ADJUSTED_TIME_MINUTE + " *60*1000) +(second(" + Constants.CHANGE_DATE + ")*1000) + (millisecond(" + Constants.CHANGE_DATE + ")))")
                .as(Constants.CHANGE_DATE_WITHOUT_TIME);
    }

    private SortOperation sortProjectedDateAscAndlastModifiedDateDesc() {
        return sort(Sort.Direction.ASC, Constants.CLIENT_CODE)
                .and(Sort.Direction.ASC, Constants.PROPERTY_CODE)
                .and(Sort.Direction.ASC, Constants.BOOKING_ID)
                .and(Sort.Direction.ASC, Constants.STAY_DATE)
                .and(Sort.Direction.ASC, Constants.CHANGE_DATE_WITHOUT_TIME)
                .and(Sort.Direction.DESC, Constants.CHANGE_DATE);
    }

    private SortOperation sortByStayDateAscChangeDateAsc() {
        return sort(Sort.Direction.ASC, Constants.CLIENT_CODE)
                .and(Sort.Direction.ASC, Constants.PROPERTY_CODE)
                .and(Sort.Direction.ASC, Constants.CHANGE_DATE)
                .and(Sort.Direction.ASC, Constants.BOOKING_ID)
                .and(Sort.Direction.ASC, Constants.STAY_DATE);
    }

    private GroupOperation groupByGroupBooking() {
        return Aggregation.group(Constants.CLIENT_CODE, Constants.PROPERTY_CODE, Constants.BOOKING_ID, Constants.STAY_DATE, Constants.CHANGE_DATE_WITHOUT_TIME)
                .first(Constants.BOOKING_ID).as(Constants.BOOKING_ID)
                .first(Constants.BOOKING_PACE_ID).as(Constants.BOOKING_PACE_ID)
                .first(Constants.ROOM_TYPE).as(Constants.ROOM_TYPE)
                .sum(Constants.PICKUP_ROOM_NIGHTS_CHANGED).as(Constants.PICKUP_ROOM_NIGHTS_CHANGED)
                .sum(Constants.PICKUP_ROOM_REVENUE_CHANGED).as(Constants.PICKUP_ROOM_REVENUE_CHANGED)
                .sum(Constants.BLOCKED_ROOM_NIGHTS_CHANGED).as(Constants.BLOCKED_ROOM_NIGHTS_CHANGED)
                .sum(Constants.BLOCKED_ROOM_REVENUE_CHANGED).as(Constants.BLOCKED_ROOM_REVENUE_CHANGED)
                .sum(Constants.CONTRACTED_ROOM_NIGHTS_CHANGED).as(Constants.CONTRACTED_ROOM_NIGHTS_CHANGED)
                .sum(Constants.CONTRACTED_ROOM_REVENUE_CHANGED).as(Constants.CONTRACTED_ROOM_REVENUE_CHANGED)
                .first(Constants.GUEST_ROOM_STATUS).as(Constants.GUEST_ROOM_STATUS)
                .first(Constants.STAY_DATE).as(Constants.STAY_DATE)
                .first(Constants.CHANGE_DATE).as(Constants.CHANGE_DATE);
    }

    public void prepareAll(List<NucleusFunctionSpaceGuestRoomPace> nucleusFunctionSpaceGuestRoomPaces) {
        Set<NucleusFunctionSpaceGuestRoomPace> saveableNucleusFunctionSpaceGuestRoomPaces = new HashSet<>();
        nucleusFunctionSpaceGuestRoomPaces.forEach(nucleusFunctionSpaceGuestRoomPace -> {
            nucleusFunctionSpaceGuestRoomPace.setBookingPaceId(UUID.randomUUID().toString());
            NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPaceExisting = getExistingNucleusFunctionSpaceGuestRoomPace(nucleusFunctionSpaceGuestRoomPace);
            if (null == nucleusFunctionSpaceGuestRoomPaceExisting) {
                calculateNights(saveableNucleusFunctionSpaceGuestRoomPaces, nucleusFunctionSpaceGuestRoomPace);
            } else {
                calculatePace(saveableNucleusFunctionSpaceGuestRoomPaces, nucleusFunctionSpaceGuestRoomPace, nucleusFunctionSpaceGuestRoomPaceExisting);
            }
        });

        if (!saveableNucleusFunctionSpaceGuestRoomPaces.isEmpty()) {
            LOGGER.debug("Inserting {} documents in nucleusFunctionSpaceGuestRoomPace collection", saveableNucleusFunctionSpaceGuestRoomPaces.size());
            functionSpaceGuestRoomPaceRepository.saveAll(saveableNucleusFunctionSpaceGuestRoomPaces);
            LOGGER.debug("Insert Complete in nucleusFunctionSpaceGuestRoomPace collection");
        }

    }

    private void calculateNights(Set<NucleusFunctionSpaceGuestRoomPace> saveableNucleusFunctionSpaceGuestRoomPaces, NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace) {
        setRoomNights(nucleusFunctionSpaceGuestRoomPace);
        saveableNucleusFunctionSpaceGuestRoomPaces.add(nucleusFunctionSpaceGuestRoomPace);
    }

    private void setRoomNights(NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace) {
        if (nucleusFunctionSpaceGuestRoomPace.getRoomNights() != null && nucleusFunctionSpaceGuestRoomPace.getContractedRoomNights() != null) {
            nucleusFunctionSpaceGuestRoomPace.setRoomNights(nucleusFunctionSpaceGuestRoomPace.getRoomNights());
            nucleusFunctionSpaceGuestRoomPace.setContractedRoomNights(nucleusFunctionSpaceGuestRoomPace.getContractedRoomNights());
        }
        if (nucleusFunctionSpaceGuestRoomPace.getRoomNights() == null) {
            nucleusFunctionSpaceGuestRoomPace.setRoomNights(nucleusFunctionSpaceGuestRoomPace.getBlockedRoomNights());
        }
        if (nucleusFunctionSpaceGuestRoomPace.getContractedRoomNights() == null) {
            nucleusFunctionSpaceGuestRoomPace.setContractedRoomNights(nucleusFunctionSpaceGuestRoomPace.getBlockedRoomNights());
        }
    }

    private void calculatePace(Set<NucleusFunctionSpaceGuestRoomPace> saveableNucleusFunctionSpaceGuestRoomPaces, NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace, NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPaceExisting) {
        if (!nucleusFunctionSpaceGuestRoomPaceExisting.equals(nucleusFunctionSpaceGuestRoomPace)) {
            nucleusFunctionSpaceGuestRoomPace.setCreateDate(nucleusFunctionSpaceGuestRoomPaceExisting.getCreateDate());
            nucleusFunctionSpaceGuestRoomPace.setLastModifiedDate(new Date());
            if (nucleusFunctionSpaceGuestRoomPace.getContractedRoomNights() == null) {
                nucleusFunctionSpaceGuestRoomPace.setContractedRoomNights(coalesce(nucleusFunctionSpaceGuestRoomPaceExisting.getContractedRoomNights(), nucleusFunctionSpaceGuestRoomPace.getBlockedRoomNights()));
                nucleusFunctionSpaceGuestRoomPace.setContractedRoomRevenue(coalesce(nucleusFunctionSpaceGuestRoomPaceExisting.getContractedRoomRevenue(), DEFAULT_BIG_DECIMAL_VALUE));
            }
            if (nucleusFunctionSpaceGuestRoomPace.getRoomNights() == null) {
                nucleusFunctionSpaceGuestRoomPace.setRoomNights(coalesce(nucleusFunctionSpaceGuestRoomPaceExisting.getRoomNights(), nucleusFunctionSpaceGuestRoomPace.getBlockedRoomNights()));
                nucleusFunctionSpaceGuestRoomPace.setRoomRevenue(coalesce(nucleusFunctionSpaceGuestRoomPaceExisting.getRoomRevenue(), DEFAULT_BIG_DECIMAL_VALUE));
            }
            calculateNightsChanges(nucleusFunctionSpaceGuestRoomPace, nucleusFunctionSpaceGuestRoomPaceExisting);
            calculateRevenueChanges(nucleusFunctionSpaceGuestRoomPace, nucleusFunctionSpaceGuestRoomPaceExisting);
            saveableNucleusFunctionSpaceGuestRoomPaces.add(nucleusFunctionSpaceGuestRoomPace);
        }
    }

    private NucleusFunctionSpaceGuestRoomPace getExistingNucleusFunctionSpaceGuestRoomPace(NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace) {
        return functionSpaceGuestRoomPaceRepository.
                findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateAndChangeDateLessThanOrderByChangeDateDesc(
                        nucleusFunctionSpaceGuestRoomPace.getClientCode(),
                        nucleusFunctionSpaceGuestRoomPace.getPropertyCode(),
                        nucleusFunctionSpaceGuestRoomPace.getBookingId(),
                        nucleusFunctionSpaceGuestRoomPace.getRoomType(),
                        nucleusFunctionSpaceGuestRoomPace.getStayDate(),
                        nucleusFunctionSpaceGuestRoomPace.getChangeDate());
    }

    private void calculateNightsChanges(NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace, NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPaceExisting) {

        nucleusFunctionSpaceGuestRoomPace.setRoomNightsChanged(coalesce(nucleusFunctionSpaceGuestRoomPace.getRoomNights(), DEFAULT_INTEGER_VALUE) - (nucleusFunctionSpaceGuestRoomPaceExisting != null ? coalesce(nucleusFunctionSpaceGuestRoomPaceExisting.getRoomNights(), DEFAULT_INTEGER_VALUE) : DEFAULT_INTEGER_VALUE));
        nucleusFunctionSpaceGuestRoomPace.setBlockedRoomNightsChanged(coalesce(nucleusFunctionSpaceGuestRoomPace.getBlockedRoomNights(), DEFAULT_INTEGER_VALUE) - (nucleusFunctionSpaceGuestRoomPaceExisting != null ? coalesce(nucleusFunctionSpaceGuestRoomPaceExisting.getBlockedRoomNights(), DEFAULT_INTEGER_VALUE) : DEFAULT_INTEGER_VALUE));
        nucleusFunctionSpaceGuestRoomPace.setContractedRoomNightsChanged(coalesce(nucleusFunctionSpaceGuestRoomPace.getContractedRoomNights(), DEFAULT_INTEGER_VALUE) - (nucleusFunctionSpaceGuestRoomPaceExisting != null ? coalesce(nucleusFunctionSpaceGuestRoomPaceExisting.getContractedRoomNights(), DEFAULT_INTEGER_VALUE) : DEFAULT_INTEGER_VALUE));

    }


    private void calculateRevenueChanges(NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace, NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPaceExisting) {
        nucleusFunctionSpaceGuestRoomPace.setRoomRevenueChanged(nucleusFunctionSpaceGuestRoomPace.getRoomRevenue().subtract(nucleusFunctionSpaceGuestRoomPaceExisting != null ? coalesce(nucleusFunctionSpaceGuestRoomPaceExisting.getRoomRevenue(), DEFAULT_BIG_DECIMAL_VALUE) : DEFAULT_BIG_DECIMAL_VALUE));
        nucleusFunctionSpaceGuestRoomPace.setBlockedRoomRevenueChanged(nucleusFunctionSpaceGuestRoomPace.getBlockedRoomRevenue().subtract(nucleusFunctionSpaceGuestRoomPaceExisting != null ? coalesce(nucleusFunctionSpaceGuestRoomPaceExisting.getBlockedRoomRevenue(), DEFAULT_BIG_DECIMAL_VALUE) : DEFAULT_BIG_DECIMAL_VALUE));
        nucleusFunctionSpaceGuestRoomPace.setContractedRoomRevenueChanged(getContractedRoomRevenue(nucleusFunctionSpaceGuestRoomPace.getContractedRoomRevenue(), nucleusFunctionSpaceGuestRoomPaceExisting));
    }

    private BigDecimal getContractedRoomRevenue(BigDecimal contractedRoomRevenue, NucleusFunctionSpaceGuestRoomPace existingNucleusFunctionSpaceGuestRoom) {

        if (contractedRoomRevenue == null) {
            return coalesce(existingNucleusFunctionSpaceGuestRoom.getContractedRoomRevenue(), DEFAULT_BIG_DECIMAL_VALUE);
        } else {
            return contractedRoomRevenue.subtract(existingNucleusFunctionSpaceGuestRoom != null ? coalesce(existingNucleusFunctionSpaceGuestRoom.getContractedRoomRevenue(), DEFAULT_BIG_DECIMAL_VALUE) : DEFAULT_BIG_DECIMAL_VALUE);
        }
    }


    private <T> T coalesce(T value, T defaultValue) {
        return value == null ? defaultValue : value;
    }

    public NucleusFunctionSpaceGuestRoomPace findFirstByClientCodeAndPropertyCodeOrderByChangeDateDesc(String clientCode, String propertyCode) {
        return functionSpaceGuestRoomPaceRepository.findFirstByClientCodeAndPropertyCodeOrderByChangeDateDesc(clientCode, propertyCode);
    }
}
