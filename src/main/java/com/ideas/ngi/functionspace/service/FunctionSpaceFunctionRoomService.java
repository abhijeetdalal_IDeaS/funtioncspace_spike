package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.functionspace.constant.Constants;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceFunctionRoom;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceFunctionRoomRepository;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEventType;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FunctionSpaceFunctionRoomService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionSpaceFunctionRoomService.class);

    public static final String CLIENT_CODE = "clientCode";
    public static final String PROPERTY_CODE = "propertyCode";
    public static final String FUNCTION_ROOM_ID = "functionRoomId";
    public static final String PROPERTY_ID = "propertyId";
    public static final String INTEGRATION_TYPE = "integrationType";
    public static final String CORRELATIONMETADATA_ID = "correlationMetadataId";
    public static final String FUNCTION_ABBREVIATION = "functionRoomAbbreviation";
    public static final String ROOM_CATEGORY = "roomCategory";

    public static final String ALIAS = "alias";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String DIARY_NAME = "diaryName";
    public static final String AREA_SQ_FEET = "areaSqFeet";
    public static final String AREA_SQ_METERS = "areaSqMeters";
    public static final String LENGTH_IN_FEET = "lengthInFeet";
    public static final String LENGTH_IN_METERS = "lengthInMeters";
    public static final String WIDTH_IN_FEET = "widthInFeet";
    public static final String WIDTH_IN_METERS = "widthInMeters";

    private final NucleusFunctionSpaceFunctionRoomRepository functionRoomRepository;

    @Autowired
    public FunctionSpaceFunctionRoomService(NucleusFunctionSpaceFunctionRoomRepository functionRoomRepository) {
        this.functionRoomRepository = functionRoomRepository;
    }

    public NucleusFunctionSpaceFunctionRoom findOne(String clientCode, String propertyCode, String functionRoomId) {
        return functionRoomRepository.findFirstByClientCodeAndPropertyCodeAndFunctionRoomId(clientCode, propertyCode, functionRoomId);
    }

    public List<NucleusFunctionSpaceFunctionRoom> findAll(String clientCode, String propertyCode) {
        return functionRoomRepository.findByClientCodeAndPropertyCodeOrderByFunctionRoomIdAsc(clientCode, propertyCode);
    }

    public long delete(String clientCode, String propertyCode) {
        long deleted = functionRoomRepository.deleteByClientCodeAndPropertyCode(clientCode, propertyCode);
        LOGGER.debug("Deleted {} function rooms for client {} and property {}", deleted, clientCode, propertyCode);
        return deleted;
    }

    public Page<NucleusFunctionSpaceFunctionRoom> findAllComboRoomsByDateRange(String clientCode, String propertyCode, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionRoomRepository.findByClientCodeAndPropertyCodeAndComboIsTrueAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
                clientCode, propertyCode, startDate, endDate, page);
    }

    public Page<NucleusFunctionSpaceFunctionRoom> findComboRoomsByIntegrationTypeAndDateRange(IntegrationType integrationType, String propertyId, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionRoomRepository.findByIntegrationTypeAndPropertyIdAndComboIsTrueAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(integrationType, propertyId, startDate, endDate, page);
    }

    public NucleusFunctionSpaceFunctionRoom save(NucleusFunctionSpaceFunctionRoom functionSpaceFunctionRoom) {
        return functionRoomRepository.save(functionSpaceFunctionRoom);
    }

    public List<NucleusFunctionSpaceFunctionRoom> save(Collection<NucleusFunctionSpaceFunctionRoom> functionSpaceFunctionRooms) {
        return functionRoomRepository.saveAll(functionSpaceFunctionRooms);
    }

    public Page<NucleusFunctionSpaceFunctionRoom> findAllByDateRange(String clientCode, String propertyCode, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionRoomRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(
                clientCode, propertyCode, startDate, endDate, page);
    }

    public Page<NucleusFunctionSpaceFunctionRoom> findByIntegrationTypeAndDateRange(IntegrationType integrationType, String propertyId, LocalDateTime startDate, LocalDateTime endDate, Pageable page) {
        return functionRoomRepository.findByIntegrationTypeAndPropertyIdAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(integrationType, propertyId, startDate, endDate, page);
    }

    public List<String> fetchAllFunctionRoomIds(String clientCode, String propertyCode) {
        List<NucleusFunctionSpaceFunctionRoom> functionRooms = findAll(clientCode, propertyCode);
        return functionRooms.stream().map(NucleusFunctionSpaceFunctionRoom::getFunctionRoomId).collect(Collectors.toList());
    }

    public void upsertAll(List<NucleusFunctionSpaceFunctionRoom> nucleusFunctionSpaceFunctionRooms) {
        if (CollectionUtils.isEmpty(nucleusFunctionSpaceFunctionRooms)) {
            return;
        }
        LOGGER.debug("Upserting {} documents in nucleusFunctionSpaceEventType collection", nucleusFunctionSpaceFunctionRooms.size());
        List<Pair<Query, Update>> updates = getPairsToUpdateOrSave(nucleusFunctionSpaceFunctionRooms);
        functionRoomRepository.bulkOpsUpsertMulti(updates);
        LOGGER.debug("Upsert Complete in nucleusFunctionSpaceEventType collection");

    }

    private List<Pair<Query, Update>> getPairsToUpdateOrSave(List<NucleusFunctionSpaceFunctionRoom> nucleusFunctionSpaceFunctionRooms) {
        List<Pair<Query, Update>> updates = new ArrayList<>();
        nucleusFunctionSpaceFunctionRooms.forEach(nucleusFunctionSpaceFunctionRoom -> {
            Update update = getUpdate(nucleusFunctionSpaceFunctionRoom);
            Pair<Query, Update> pair = Pair.of(createIsPresentQuery(nucleusFunctionSpaceFunctionRoom.getFunctionRoomId(), nucleusFunctionSpaceFunctionRoom.getClientCode(), nucleusFunctionSpaceFunctionRoom.getPropertyCode()), update);
            updates.add(pair);
        });
        return updates;
    }

    private Query createIsPresentQuery(String functionRoomId, String clientCode, String propertyCode) {
        Query query = new Query();
        query.addCriteria(Criteria.where(FUNCTION_ROOM_ID).is(functionRoomId));
        query.addCriteria(Criteria.where(CLIENT_CODE).is(clientCode));
        query.addCriteria(Criteria.where(PROPERTY_CODE).is(propertyCode));
        return query;
    }

    private Update getUpdate(NucleusFunctionSpaceFunctionRoom nucleusFunctionSpaceFunctionRoom) {

        Update update = new Update();
        update.set(NucleusFunctionSpaceEventType.LAST_MODIFIED_FIELD, new Date());
        update.set(CLIENT_CODE, nucleusFunctionSpaceFunctionRoom.getClientCode());
        update.set(PROPERTY_CODE, nucleusFunctionSpaceFunctionRoom.getPropertyCode());
        update.set(PROPERTY_ID, nucleusFunctionSpaceFunctionRoom.getPropertyId());
        update.set(INTEGRATION_TYPE, nucleusFunctionSpaceFunctionRoom.getIntegrationType());
        update.set(FUNCTION_ROOM_ID, nucleusFunctionSpaceFunctionRoom.getFunctionRoomId());
        update.set(CORRELATIONMETADATA_ID, nucleusFunctionSpaceFunctionRoom.getCorrelationMetadataId());
        update.set(FUNCTION_ABBREVIATION, nucleusFunctionSpaceFunctionRoom.getFunctionRoomAbbreviation());
        update.set(ROOM_CATEGORY, nucleusFunctionSpaceFunctionRoom.getRoomCategory());
        update.set(ALIAS, nucleusFunctionSpaceFunctionRoom.getAlias());
        update.set(NAME, nucleusFunctionSpaceFunctionRoom.getName());
        update.set(DESCRIPTION, nucleusFunctionSpaceFunctionRoom.getDescription());
        update.set(DIARY_NAME, nucleusFunctionSpaceFunctionRoom.getDiaryName());
        update.set(AREA_SQ_FEET, nucleusFunctionSpaceFunctionRoom.getAreaSqFeet());
        update.set(AREA_SQ_METERS, nucleusFunctionSpaceFunctionRoom.getAreaSqMeters());
        update.set(LENGTH_IN_FEET, nucleusFunctionSpaceFunctionRoom.getLengthInFeet());
        update.set(LENGTH_IN_METERS, nucleusFunctionSpaceFunctionRoom.getAreaSqMeters());
        update.set(WIDTH_IN_FEET, nucleusFunctionSpaceFunctionRoom.getWidthInFeet());
        update.set(WIDTH_IN_METERS, nucleusFunctionSpaceFunctionRoom.getAreaSqMeters());
        update.setOnInsert(Constants.CREATED_DATE, new Date());
        return update;
    }

}
