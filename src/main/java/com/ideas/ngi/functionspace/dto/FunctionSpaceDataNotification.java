package com.ideas.ngi.functionspace.dto;

import org.springframework.hateoas.Link;

public class FunctionSpaceDataNotification extends FunctionSpaceData {
    private Link correlationLink;
    private Link eventTypeLink;
    private Link marketSegmentLink;
    private Link roomLink;
    private Link roomComboLink;
    private Link bookingLink;
    private Link bookingIndexedLink;
    private Link bookingPaceLink;
    private Link bookingGuestLink;
    private Link eventLink;
    private Link eventIndexedLink;

    public FunctionSpaceDataNotification() {
        //empty constructor
    }

    public FunctionSpaceDataNotification(FunctionSpaceData source) {
        this.setClientCode(source.getClientCode());
        this.setPropertyCode(source.getPropertyCode());
        this.setIntegrationType(source.getIntegrationType());
        this.setSendingSystemPropertyId(source.getSendingSystemPropertyId());
        this.setFullSync(source.isFullSync());
    }

    public Link getCorrelationLink() {
        return correlationLink;
    }

    public void setCorrelationLink(Link correlationLink) {
        this.correlationLink = correlationLink;
    }

    public Link getEventTypeLink() {
        return eventTypeLink;
    }

    public void setEventTypeLink(Link eventTypeLink) {
        this.eventTypeLink = eventTypeLink;
    }

    public Link getMarketSegmentLink() {
        return marketSegmentLink;
    }

    public void setMarketSegmentLink(Link marketSegmentLink) {
        this.marketSegmentLink = marketSegmentLink;
    }

    public Link getRoomLink() {
        return roomLink;
    }

    public void setRoomLink(Link roomLink) {
        this.roomLink = roomLink;
    }

    public Link getRoomComboLink() {
        return roomComboLink;
    }

    public void setRoomComboLink(Link roomComboLink) {
        this.roomComboLink = roomComboLink;
    }

    public Link getBookingLink() {
        return bookingLink;
    }

    public void setBookingLink(Link bookingLink) {
        this.bookingLink = bookingLink;
    }

    public Link getBookingIndexedLink() {
        return bookingIndexedLink;
    }

    public void setBookingIndexedLink(Link bookingIndexedLink) {
        this.bookingIndexedLink = bookingIndexedLink;
    }

    public Link getBookingPaceLink() {
        return bookingPaceLink;
    }

    public void setBookingPaceLink(Link bookingPaceLink) {
        this.bookingPaceLink = bookingPaceLink;
    }

    public Link getBookingGuestLink() {
        return bookingGuestLink;
    }

    public void setBookingGuestLink(Link bookingGuestLink) {
        this.bookingGuestLink = bookingGuestLink;
    }

    public Link getEventLink() {
        return eventLink;
    }

    public void setEventLink(Link eventLink) {
        this.eventLink = eventLink;
    }

    public Link getEventIndexedLink() {
        return eventIndexedLink;
    }

    public void setEventIndexedLink(Link eventIndexedLink) {
        this.eventIndexedLink = eventIndexedLink;
    }
}
