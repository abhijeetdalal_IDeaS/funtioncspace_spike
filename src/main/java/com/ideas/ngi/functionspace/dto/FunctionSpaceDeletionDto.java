package com.ideas.ngi.functionspace.dto;


import com.ideas.ngi.nucleus.data.correlation.dto.PropertyIdentifier;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;

public class FunctionSpaceDeletionDto {

    private IntegrationType integrationType;
    private String propertyId;
    private String clientCode;
    private String propertyCode;

    private long numberOfCorrelationMetadataDeleted = 0;
    private long numberOfEventTypesDeleted = 0;
    private long numberOfMarketSegmentsDeleted = 0;
    private long numberOfFunctionRoomsDeleted = 0;
    private long numberOfBookingsDeleted = 0;
    private long numberOfBookingGuestRoomsDeleted = 0;
    private long numberOfBookingPacesDeleted = 0;
    private long numberOfBookingPaceGuestRoomsDeleted = 0;
    private long numberOfEventsDeleted = 0;

    public FunctionSpaceDeletionDto(IntegrationType integrationType, PropertyIdentifier propertyIdentifier) {
        this.integrationType = integrationType;
        this.propertyId = propertyIdentifier.getSendingSystemPropertyId();
        this.clientCode = propertyIdentifier.getClientCode();
        this.propertyCode = propertyIdentifier.getPropertyCode();
    }

    public FunctionSpaceDeletionDto(IntegrationType integrationType, String propertyId, String clientCode, String propertyCode) {
        this.integrationType = integrationType;
        this.propertyId = propertyId;
        this.clientCode = clientCode;
        this.propertyCode = propertyCode;
    }
    
    public IntegrationType getIntegrationType() {
        return integrationType;
    }
    
    public String getPropertyId() {
        return propertyId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public long getNumberOfCorrelationMetadataDeleted() {
        return numberOfCorrelationMetadataDeleted;
    }
    
    public long getNumberOfEventTypesDeleted() {
        return numberOfEventTypesDeleted;
    }

    public long getNumberOfMarketSegmentsDeleted() {
        return numberOfMarketSegmentsDeleted;
    }

    public long getNumberOfFunctionRoomsDeleted() {
        return numberOfFunctionRoomsDeleted;
    }

    public long getNumberOfBookingsDeleted() {
        return numberOfBookingsDeleted;
    }

    public long getNumberOfBookingGuestRoomsDeleted() {
        return numberOfBookingGuestRoomsDeleted;
    }

    public long getNumberOfBookingPacesDeleted() {
        return numberOfBookingPacesDeleted;
    }

    public long getNumberOfBookingPaceGuestRoomsDeleted() {
        return numberOfBookingPaceGuestRoomsDeleted;
    }

    public long getNumberOfEventsDeleted() {
        return numberOfEventsDeleted;
    }
    
    public FunctionSpaceDeletionDto addNumberOfCorrelationMetadataDeleted(long numberOfCorrelationMetadataDeleted) {
        this.numberOfCorrelationMetadataDeleted += numberOfCorrelationMetadataDeleted;
        return this;
    }
    
    public FunctionSpaceDeletionDto addNumberOfEventTypesDeleted(long numberOfEventTypesDeleted) {
        this.numberOfEventTypesDeleted += numberOfEventTypesDeleted;
        return this;
    }

    public FunctionSpaceDeletionDto addNumberOfMarketSegmentsDeleted(long numberOfMarketSegmentsDeleted) {
        this.numberOfMarketSegmentsDeleted += numberOfMarketSegmentsDeleted;
        return this;
    }

    public FunctionSpaceDeletionDto addNumberOfFunctionRoomsDeleted(long numberOfFunctionRoomsDeleted) {
        this.numberOfFunctionRoomsDeleted += numberOfFunctionRoomsDeleted;
        return this;
    }
    
    public FunctionSpaceDeletionDto addNumberOfBookingsDeleted(long numberOfBookingsDeleted) {
        this.numberOfBookingsDeleted += numberOfBookingsDeleted;
        return this;
    }
    
    public FunctionSpaceDeletionDto addNumberOfBookingGuestRoomsDeleted(long numberOfBookingGuestRoomsDeleted) {
        this.numberOfBookingGuestRoomsDeleted += numberOfBookingGuestRoomsDeleted;
        return this;
    }
    
    public FunctionSpaceDeletionDto addNumberOfBookingPacesDeleted(long numberOfBookingPacesDeleted) {
        this.numberOfBookingPacesDeleted += numberOfBookingPacesDeleted;
        return this;
    }
    
    public FunctionSpaceDeletionDto addNumberOfBookingPaceGuestRoomsDeleted(long numberOfBookingPaceGuestRoomsDeleted) {
        this.numberOfBookingPaceGuestRoomsDeleted += numberOfBookingPaceGuestRoomsDeleted;
        return this;
    }

    public FunctionSpaceDeletionDto addNumberOfEventsDeleted(long numberOfEventsDeleted) {
        this.numberOfEventsDeleted += numberOfEventsDeleted;
        return this;
    }
}
