package com.ideas.ngi.functionspace.dto;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.correlation.entity.NucleusCorrelationMetadata;
import com.ideas.ngi.nucleus.integration.annotation.ClientCode;
import com.ideas.ngi.nucleus.integration.annotation.PropertyCode;

import java.io.Serializable;
import java.util.Objects;

public class FunctionSpaceData implements Serializable {
    private static final long serialVersionUID = -6721336314374578185L;


    @ClientCode
    private String clientCode;
    @PropertyCode
    private String propertyCode;
    private IntegrationType integrationType;
    private String sendingSystemPropertyId;
    private boolean fullSync;

    public FunctionSpaceData() {
        //empty constructor
    }

    public FunctionSpaceData(NucleusCorrelationMetadata source) {
        this.setClientCode(source.getClientCode());
        this.setPropertyCode(source.getPropertyCode());
        this.setIntegrationType(source.getIntegrationType());
        this.setSendingSystemPropertyId(source.getSendingSystemPropertyId());
    }

    public String getSendingSystemPropertyId() {
        return sendingSystemPropertyId;
    }

    public void setSendingSystemPropertyId(String sendingSystemPropertyId) {
        this.sendingSystemPropertyId = sendingSystemPropertyId;
    }

    public IntegrationType getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(IntegrationType integrationType) {
        this.integrationType = integrationType;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }

    public void setFullSync(boolean fullSync) {
        this.fullSync = fullSync;
    }

    public boolean isFullSync() {
        return fullSync;
    }

    @Override
    public String toString() {
        return "FunctionSpaceData{" +
                "clientCode='" + clientCode + '\'' +
                ", propertyCode='" + propertyCode + '\'' +
                ", integrationType=" + integrationType +
                ", sendingSystemPropertyId='" + sendingSystemPropertyId + '\'' +
                ", fullSync=" + fullSync +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FunctionSpaceData that = (FunctionSpaceData) o;
        return clientCode.equals(that.clientCode) &&
                propertyCode.equals(that.propertyCode) &&
                integrationType == that.integrationType &&
                Objects.equals(sendingSystemPropertyId, that.sendingSystemPropertyId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientCode, propertyCode, integrationType, sendingSystemPropertyId);
    }
}
