package com.ideas.ngi.functionspace.dto;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBooking;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceGuestRoomPace;

import java.math.BigDecimal;
import java.util.Date;

public class FunctionSpaceGuestRoomPaceWithBookingInfoDto {
    private String clientCode;
    private String propertyCode;
    private String bookingId;
    private String bookingPaceId;
    private Date stayDate;
    private Date changeDate;
    private String marketSegmentCode;
    private String blockName;
    private Date arrivalDate;
    private Date departureDate;
    private String status;

    private Date insertDate;
    private String bookingType;
    private Date opportunityCreateDate;
    private Date cancellationDate;
    private Date cutoffDate;
    private Integer roomNightsChanged;
    private BigDecimal roomRevenueChanged;

    private BigDecimal blockedRoomRevenueChanged;
    private Integer blockedRoomNightsChanged;
    private BigDecimal contractedRoomRevenueChanged;
    private Integer contractedRoomNightsChanged;


    public FunctionSpaceGuestRoomPaceWithBookingInfoDto(NucleusFunctionSpaceGuestRoomPace guestRoomPace, NucleusFunctionSpaceBooking booking) {
        this.clientCode = guestRoomPace.getClientCode();
        this.propertyCode = guestRoomPace.getPropertyCode();
        this.bookingId = guestRoomPace.getBookingId();
        this.bookingPaceId = guestRoomPace.getBookingPaceId();
        this.stayDate = guestRoomPace.getStayDate();
        this.changeDate = guestRoomPace.getChangeDate();
        this.roomNightsChanged = guestRoomPace.getRoomNightsChanged();
        this.roomRevenueChanged = guestRoomPace.getRoomRevenueChanged();
        this.blockedRoomNightsChanged = guestRoomPace.getBlockedRoomNightsChanged();
        this.blockedRoomRevenueChanged = guestRoomPace.getBlockedRoomRevenueChanged();
        this.contractedRoomNightsChanged = guestRoomPace.getContractedRoomNightsChanged();
        this.contractedRoomRevenueChanged = guestRoomPace.getContractedRoomRevenueChanged();
        this.marketSegmentCode = booking.getMarketSegmentCode();
        this.blockName = booking.getBlockName();
        this.arrivalDate = booking.getArrivalDate();
        this.departureDate = booking.getDepartureDate();
        this.status = guestRoomPace.getGuestRoomStatus();
        this.insertDate = booking.getInsertDate();
        this.bookingType = booking.getBookingType();
        this.opportunityCreateDate = booking.getOpportunityCreateDate();
        this.cancellationDate = booking.getCancellationDate();
        this.cutoffDate = booking.getCutoffDate();
    }

    public String getClientCode() {
        return clientCode;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public String getBookingId() {
        return bookingId;
    }

    public String getBookingPaceId() {
        return bookingPaceId;
    }

    public Date getStayDate() {
        return stayDate;
    }

    public Date getChangeDate() {
        return changeDate;
    }

    public String getMarketSegmentCode() {
        return marketSegmentCode;
    }

    public String getBlockName() {
        return blockName;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public String getStatus() {
        return status;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public String getBookingType() {
        return bookingType;
    }

    public Date getOpportunityCreateDate() {
        return opportunityCreateDate;
    }

    public Date getCancellationDate() {
        return cancellationDate;
    }

    public Date getCutoffDate() {
        return cutoffDate;
    }

    public Integer getRoomNightsChanged() {
        return roomNightsChanged;
    }

    public BigDecimal getRoomRevenueChanged() {
        return roomRevenueChanged;
    }

    public BigDecimal getBlockedRoomRevenueChanged() {
        return blockedRoomRevenueChanged;
    }

    public Integer getBlockedRoomNightsChanged() {
        return blockedRoomNightsChanged;
    }

    public BigDecimal getContractedRoomRevenueChanged() {
        return contractedRoomRevenueChanged;
    }

    public Integer getContractedRoomNightsChanged() {
        return contractedRoomNightsChanged;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "FunctionSpaceGuestRoomPaceWithBookingInfoDto{" +
                "clientCode='" + clientCode + '\'' +
                ", propertyCode='" + propertyCode + '\'' +
                ", bookingId='" + bookingId + '\'' +
                ", bookingPaceId='" + bookingPaceId + '\'' +
                ", stayDate=" + stayDate +
                ", changeDate=" + changeDate +
                ", marketSegmentCode='" + marketSegmentCode + '\'' +
                ", blockName='" + blockName + '\'' +
                ", arrivalDate=" + arrivalDate +
                ", departureDate=" + departureDate +
                ", status='" + status + '\'' +
                ", insertDate=" + insertDate +
                ", bookingType='" + bookingType + '\'' +
                ", opportunityCreateDate=" + opportunityCreateDate +
                ", cancellationDate=" + cancellationDate +
                ", cutoffDate=" + cutoffDate +
                ", roomNightsChanged=" + roomNightsChanged +
                ", roomRevenueChanged=" + roomRevenueChanged +
                ", blockedRoomRevenueChanged=" + blockedRoomRevenueChanged +
                ", blockedRoomNightsChanged=" + blockedRoomNightsChanged +
                ", contractedRoomRevenueChanged=" + contractedRoomRevenueChanged +
                ", contractedRoomNightsChanged=" + contractedRoomNightsChanged +
                '}';
    }
}
