package com.ideas.ngi.nucleus.util;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class MapBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(MapBuilder.class);

    private Map<String, Object> parameters = null;
    private boolean ignoreIfNull;

    public static final String KEY_DELIMITER = "-";

    private MapBuilder(String name, Object value, boolean ignoreIfNull) {
        this.parameters = new HashMap<>();
        this.ignoreIfNull = ignoreIfNull;
        and(name, value);
    }

    public static MapBuilder with(String name, Object value) {
        return with(name, value, false);
    }
    public static MapBuilder with(String name, Object value, boolean ignoreIfNull) {
        return new MapBuilder(name, value, ignoreIfNull);
    }

    public MapBuilder and(String name, Object value) {
        if (value != null || !ignoreIfNull) {
            this.parameters.put(name, value);
        }
        return this;
    }

    public Map<String, Object> get() {
        return this.parameters;
    }
    
    public static <T> String buildKey(T record, String... keyFields) {
        StringBuilder builder = new StringBuilder();
        
        for (String keyField : keyFields) {
            if (builder.length() != 0) {
                builder.append(KEY_DELIMITER);
            }
            
            builder.append(ReflectionUtil.getField(record, keyField));
        }
        
        return builder.toString();
    }

    public static Map<String, Object> convertBeanToMap(Object bean){
        Map<String, Object> beanProperties = new HashedMap();
        try {
            beanProperties = PropertyUtils.describe(bean);
        }catch (IllegalAccessException e){
            LOGGER.warn("Do not have access to this bean property", e);
        }catch (IllegalArgumentException e){
            LOGGER.warn("This bean is null", e);
        }catch (InvocationTargetException e){
            LOGGER.warn("Bean property accessor method threw an exception", e);
        }catch (NoSuchMethodException e){
            LOGGER.warn("Bean accessor method cannot be found", e);
        }
        return beanProperties;
    }
}
