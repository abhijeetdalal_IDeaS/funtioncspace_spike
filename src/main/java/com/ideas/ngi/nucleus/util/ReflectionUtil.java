package com.ideas.ngi.nucleus.util;

import com.ideas.ngi.nucleus.exception.NucleusException;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ReflectionUtil {
    private static Logger LOGGER = LoggerFactory.getLogger(ReflectionUtil.class);

    private ReflectionUtil() {
    }

    public static List<String> getFieldNames(Class<?> clazz) {
        List<Field> fields = findFields(clazz);
        return fields.stream().map(Field::getName).collect(Collectors.toList());
    }

    public static Object getField(Object obj, String fieldName) {
        try {
            List<Field> fields = findFields(obj.getClass());
            for (Field field : fields) {
                if (field.getName().equals(fieldName)) {
                    return field.get(obj);
                }
            }
            
            throw new NucleusException("Unable to get field: " + fieldName + " on Object: " + obj);
        } catch (IllegalAccessException e) {
            throw new NucleusException("Unable to get field: " + fieldName + " on Object: " + obj, e);
        }
    }

    @SuppressWarnings("squid:S2221")
    public static Object getValueWithAnnotation(Object obj, Class<? extends Annotation> annotation) {
        List<Field> fieldsWithAnnotation = getFieldsWithAnnotations(obj.getClass(), annotation);
        
        if (CollectionUtils.isNotEmpty(fieldsWithAnnotation)) {
            try {
                return fieldsWithAnnotation.get(0).get(obj);
            } catch (Exception e) {
                LOGGER.trace("Unable to get field value with Annotation:" + annotation, e);
                return null;
            }
        }

        return null;
    }

    @SafeVarargs
    public static List<Field> getFieldsWithAnnotations(Class<?> clazz, Class<? extends Annotation>... annotations) {
        List<Field> fieldsWithAnnotations = new ArrayList<>();

        List<Field> fields = findFields(clazz);
        for (Field field : fields) {
            for (Class<? extends Annotation> annotation : annotations) {
                if (field.isAnnotationPresent(annotation)) {
                    fieldsWithAnnotations.add(field);
                }
            }
        }

        return fieldsWithAnnotations;
    }

    public static Field getFieldWithAnnotation(Class<?> clazz, Class<? extends Annotation> annotation) {
        List<Field> fields = getFieldsWithAnnotations(clazz, annotation);
        if (fields.size() != 1) {
            throw new IllegalArgumentException("Expected one field annotated with " + annotation.getSimpleName() + ", but found " + fields.size());
        }
        return fields.iterator().next();
    }

    public static List<Field> findFields(Class<?> clazz) {
        final List<Field> fields = new ArrayList<>();
        
        List<Class<?>> classes = findClassHierarchy(clazz);
        for (Class<?> clazzz : classes) {
            ReflectionUtils.doWithFields(clazzz, field -> {
                field.setAccessible(true);
                if (!fields.contains(field)) {
                    fields.add(field);
                }
            }, ReflectionUtils.COPYABLE_FIELDS);
        }
        
        return fields;
    }

    public static boolean hasField(Class<?> clazz, String field) {
        return ReflectionUtils.findField(clazz, field) != null;
    }

    private static List<Class<?>> findClassHierarchy(Class<?> clazz) {
        // Determine the class hierarchy
        List<Class<?>> classHierarchy = new ArrayList<>();
        Class<?> currentClass = clazz;
        while (!currentClass.equals(Object.class)) {
            classHierarchy.add(currentClass);
            currentClass = currentClass.getSuperclass();
        }
        
        // Reverse the classes so that super class steps would come before their subclass
        Collections.reverse(classHierarchy);
        return classHierarchy;
    }
}
