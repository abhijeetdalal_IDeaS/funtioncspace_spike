package com.ideas.ngi.nucleus.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

public final class NullableMethodChainUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(NullableMethodChainUtil.class);

    private NullableMethodChainUtil() {
        // static
    }

    public static <T> Optional<T> resolveChainAsNullable(Supplier<T> resolver) {
        try {
            T result = resolver.get();
            return Optional.ofNullable(result);
        } catch (NullPointerException npe) {
            LOGGER.debug("NullPointer in evaluation", npe);
            return Optional.empty();
        } catch (IndexOutOfBoundsException ioob) {
            LOGGER.debug("IndexOutOfBounds in evaluation", ioob);
            return Optional.empty();
        }
    }

    public static <T> List<T> resolveChainAsList(Supplier<List<T>> resolver) {
        return resolveChainAsNullable(resolver).orElseGet(Collections::emptyList);
    }
}

