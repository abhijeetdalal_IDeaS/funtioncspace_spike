package com.ideas.ngi.nucleus.util;

import com.google.common.base.Strings;
import com.ideas.ngi.nucleus.exception.NucleusException;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.util.Asserts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("squid:S1200")
public class DateTimeUtil {
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String DATE_TIME_FORMAT_WITHOUT_MS_AND_TIMEZONE = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_TIME_FORMAT_WITHOUT_MS = "yyyy-MM-dd HH:mm:ss z";
    public static final String DATE_TIME_T_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    public static final String DATE_TIME_FORMAT_WITHOUT_SECONDS = "dd-MMM-yyyy HH:mm";
    public static final String UTC = "UTC";
    public static final String TIMESTAMP_FORMAT_WITHOUT_MS = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String TIMESTAMP_FORMAT_WITH_OFFSET = "yyyy-MM-dd'T'HH:mm:ssX";
    public static final String DATE_TIME_TIMEZONE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    public static final String DATE_TIME_TIMEZONE_FORMAT_NO_MILLIS = "yyyy-MM-dd'T'HH:mm:ssXXX";
    public static final String DATE_FORMAT_DD_MMM_YYYY = "dd-MMM-yyyy";
    public static final String DATE_FORMAT_DAY_OF_WEEK_DATE_TIME = "EEE dd-MMM-yyyy HH:mm";
    public static final String DATE_TIME_FORMAT_WITH_DAY_AND_TIMEZONE = "EEEE, dd-MMM-yyyy HH:mm:ss aaa z";
    public static final String DATE_TIME_FORMAT_WITH_TIMEZONE = "dd-MMM-yyyy HH:mm:ss z";
    public static final String DATE_FORMAT_YYYYMMDD = "yyyyMMdd";
    public static final DateTimeFormatter DATE_FORMATTER_YYYYMMDD = DateTimeFormatter.ofPattern(DATE_FORMAT_YYYYMMDD);
    private static final Logger LOGGER = LoggerFactory.getLogger(DateTimeUtil.class);
    private static final String DATE_CANNOT_BE_NULL = "Date can't be null";
    private static final String FORMAT_CANNOT_BE_NULL = "Format can't be null";
    private static final String CALENDAR_CANNOT_BE_NULL = "Calendar cannot be null";
    public static final String DATE_FORMAT_DD_MMM_YY = "dd-MMM-yy";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendPattern(DATE_FORMAT_DD_MMM_YY)
            .toFormatter(Locale.ENGLISH);

    private static DatatypeFactory datatypeFactory;

    static {
        try {
            datatypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            throw new IllegalStateException("Failed to get DatatypeFactory", e);
        }
    }

    private DateTimeUtil() {
    }

    public static Instant getInstantFromString(String timeString) {
        timeString = timeString.replace(" ", "T");
        timeString = timeString + ":00Z";

        return Instant.parse(timeString);
    }

    public static Instant parseInstantToUtcMidnight(final String dateString) {
        Asserts.notNull(dateString, "dateString");

        List<Pair<String, Function<String,Instant>>> attempts = Arrays.asList(
            Pair.of("ZonedDateTime", s -> ZonedDateTime.parse(s).toInstant()),
            Pair.of("LocalDateTime", s -> LocalDateTime.parse(s).toInstant(ZoneOffset.UTC)),
            Pair.of("LocalDate",     s -> LocalDate.parse(s).atStartOfDay().toInstant(ZoneOffset.UTC))
        );
        for (Pair<String, Function<String,Instant>> attempt : attempts) {
            try {
                return attempt.getValue().apply(dateString);

            } catch (Exception e) {
                LOGGER.debug("Could not parse date '{}' as a {}", dateString, attempt.getKey(), e);
            }
        }

        throw new NucleusException("There was an issue parsing the date from '" + dateString + "'");
    }

    public static Date parseDate(final String dateString, final String... formats) {
        assert dateString != null : DATE_CANNOT_BE_NULL;
        assert formats.length > 0 : FORMAT_CANNOT_BE_NULL;

        for (String format : formats) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
                return simpleDateFormat.parse(dateString);
            } catch (ParseException e) {
                LOGGER.debug("Could not parse date {}, with format {}", dateString, format);
            }
        }

        throw new NucleusException("There was an issue parsing the date from " + dateString + " with formats " + Arrays.toString(formats));

    }

    public static LocalDate parseLocalDate(String dateString) {
        return LocalDate.parse(dateString, DateTimeFormatter.ofPattern(DATE_FORMAT));
    }

    public static LocalDateTime parseLocalDateTime(String dateString, String... formats) {
        LocalDateTime localDateTime = null;
        for (String format : formats) {
            try {
                localDateTime = LocalDateTime.parse(dateString, DateTimeFormatter.ofPattern(format));
            } catch (DateTimeParseException dte) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Was not able to convert the string " + dateString + " with the format " + format, dte);
                }
            }
        }
        if (localDateTime == null) {
            throw new NucleusException(String.format("Was not able to convert the string %s with the formats %s", dateString, Arrays.toString(formats)));
        }
        return localDateTime;
    }

    public static LocalDateTime parseLocalDateTimeFromGregorian(XMLGregorianCalendar gregorianCalendar) {
        if (gregorianCalendar == null) {
            return null;
        }
        return parseLocalDateTime(gregorianCalendar.toString(), DATE_TIME_T_FORMAT, DATE_TIME_TIMEZONE_FORMAT);
    }

    public static LocalDateTime getLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    public static LocalDate parseLocalDateFromGregorian(XMLGregorianCalendar gregorianCalendar) {
        if (gregorianCalendar == null) {
            return null;
        }
        String stringTime = removeTime(gregorianCalendar);
        return parseLocalDate(stringTime);
    }

    public static LocalDate parseLocalDateFromDate(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static Date addMinutesToDate(Date date, int minutes) {
        return DateUtils.addMinutes(date, minutes);
    }

    public static Date addDaysToDate(Date date, int days) {
        assert date != null : DATE_CANNOT_BE_NULL;
        return DateUtils.addDays(date, days);
    }

    public static Date buildDateFromDateAndTime(String dateString, String timeString) {
        LocalDate localDate = LocalDate.parse(dateString);
        return buildDateFromDateAndTime(localDate, timeString);
    }

    public static Date buildDateFromDateAndTime(Date date, String timeString) {
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return buildDateFromDateAndTime(localDate, timeString);
    }

    public static Date buildDateFromDateAndTime(Date date, Date time) {
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalTime localTime = time.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
        return buildDateFromDateAndTime(localDate, localTime);
    }

    private static Date buildDateFromDateAndTime(LocalDate localDate, String timeString) {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
        ZonedDateTime time = ZonedDateTime.parse(timeString, formatter);
        return buildDateFromDateAndTime(localDate, time.toLocalTime());
    }

    public static Date toDate(LocalDateTime localDate) {
        return Date.from(localDate.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date toDate(LocalDate localDate) {
        return localDate != null ? Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()) : null;
    }

    public static LocalDate toLocalDate(Date date) {
        return date != null ? date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate() : null;
    }


    public static LocalDateTime toLocalDateTime(Date date) {
        return date != null ? Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime() : null;
    }

    public static LocalDate toLocalDate(XMLGregorianCalendar date) {
        return date == null ? null : toLocalDate(date.toGregorianCalendar().getTime());
    }

    private static Date buildDateFromDateAndTime(LocalDate localDate, LocalTime localTime) {
        LocalDateTime dateTime = localDate.atTime(localTime);
        return Date.from(dateTime.toInstant(ZoneOffset.UTC));
    }

    public static String formatDate(final Date date, final String format) {
        assert date != null : DATE_CANNOT_BE_NULL;
        assert format != null : FORMAT_CANNOT_BE_NULL;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    public static String formatDateLenient(final XMLGregorianCalendar calendar, String format) {
        if (calendar == null) {
            return null;
        }
        return formatDate(calendar, format);
    }

    public static String formatDate(final XMLGregorianCalendar calendar, String format) {
        assert calendar != null : CALENDAR_CANNOT_BE_NULL;
        assert format != null : FORMAT_CANNOT_BE_NULL;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return formatter.format(calendar.toGregorianCalendar().toZonedDateTime());
    }

    public static String formatDateTimeStringToDateString(String dateTimeString, String initialFormat, String outputFormat) {
        Date date = parseDate(dateTimeString, initialFormat);
        return formatDate(date, outputFormat);
    }

    public static String formatLocalDate(final LocalDate date, final String format) {
        assert date != null : DATE_CANNOT_BE_NULL;
        assert format != null : FORMAT_CANNOT_BE_NULL;

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return formatter.format(date);
    }

    public static Date buildDate(int year, int month, int dayOfMonth) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date buildDate(int year, int month, int dayOfMonth, int hour, int minute, int second) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getMinDateFromList(List<Date> dateList) {
        return dateList.stream()
                .min(Comparator.naturalOrder()).orElse(null);
    }

    public static Date getMaxDateFromList(List<Date> dateList) {
        return dateList.stream()
                .max(Comparator.naturalOrder()).orElse(null);
    }

    public static XMLGregorianCalendar getMinXMLGregorianCalendarFromList(List<XMLGregorianCalendar> dateList) {
        return dateList.stream()
                .min(XMLGregorianCalendar::compare).orElse(null);
    }

    public static XMLGregorianCalendar getMaxXMLGregorianCalendarFromList(List<XMLGregorianCalendar> dateList) {
        return dateList.stream()
                .max(XMLGregorianCalendar::compare).orElse(null);
    }

    public static String buildDateWithTimeZone(Date date, String timeZone, final String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        return dateFormat.format(date);
    }

    public static boolean isDateBetweenOrEqualToDates(LocalDate beginDate, LocalDate endDate, LocalDate dateInQuestion) {
        return isDateBetweenDates(beginDate, endDate, dateInQuestion) || dateInQuestion.equals(beginDate) ||
                dateInQuestion.equals(endDate);
    }

    public static boolean isDateBetweenDates(LocalDate beginDate, LocalDate endDate, LocalDate dateInQuestion) {
        return dateInQuestion.isAfter(beginDate) && (dateInQuestion.isBefore(endDate)) || dateInQuestion.equals(beginDate);
    }

    public static boolean isDateBetweenDatesAndNotEqualToBeginDate(LocalDate beginDate, LocalDate endDate, LocalDate dateInQuestion) {
        return dateInQuestion.isAfter(beginDate) && (dateInQuestion.isBefore(endDate));
    }

    public static boolean isDateBeforeDate(LocalDate dateInQuestion, LocalDate staticDate) {
        return dateInQuestion.isBefore(staticDate);
    }

    public static boolean isDateBeforeDate(LocalDate dateInQuestion, ZonedDateTime staticDate) {
        LocalDate staticLocalDate = staticDate.toLocalDate();
        return dateInQuestion.isBefore(staticLocalDate);
    }

    public static boolean isDateEqual(LocalDate dateInQuestion, LocalDate staticDate) {
        return dateInQuestion.isEqual(staticDate);
    }

    public static boolean isDateEqual(LocalDate dateInQuestion, XMLGregorianCalendar staticDate) {
        return isDateEqual(dateInQuestion, parseLocalDateFromGregorian(staticDate));
    }

    public static boolean isEqualOrBefore(LocalDate dateInQuestion, LocalDate staticDate) {
        return isDateBeforeDate(dateInQuestion, staticDate) || isDateEqual(dateInQuestion, staticDate);
    }

    public static boolean isEqualOrBefore(ZonedDateTime dateInQuestion, LocalDate staticDate) {
        LocalDate dateInQuestionLocalDate = dateInQuestion.toLocalDate();
        return isDateBeforeDate(dateInQuestionLocalDate, staticDate) || isDateEqual(dateInQuestionLocalDate, staticDate);
    }

    public static String removeTime(XMLGregorianCalendar timeStamp) {
        Calendar calendar = timeStamp.toGregorianCalendar();
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        format.setTimeZone(calendar.getTimeZone());
        return format.format(calendar.getTime());
    }

    public static XMLGregorianCalendar removeTimeFromXMLGregorianDate(XMLGregorianCalendar timeStamp) {
        Calendar calendar = timeStamp.toGregorianCalendar();
        return datatypeFactory.newXMLGregorianCalendarDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1,
                calendar.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);
    }

    public static XMLGregorianCalendar convertToXMLGregorianCalendar(LocalDate date) {
        if (date == null) {
            return null;
        }

        return buildXMLGregorianCalendar(date.getYear(), date.getMonthValue(), date.getDayOfMonth());
    }

    public static XMLGregorianCalendar convertZonedDateTimeToXMLGregorianCalendar(ZonedDateTime date) {
        if (date == null) {
            return null;
        }

        return buildXMLGregorianCalendar(date.getYear(), date.getMonthValue(), date.getDayOfMonth());
    }

    public static XMLGregorianCalendar buildXMLGregorianCalendar(int year, int month, int dayOfMonth) {
        // return date without timezone
        return datatypeFactory.newXMLGregorianCalendarDate(year, month, dayOfMonth, DatatypeConstants.FIELD_UNDEFINED);
    }

    public static Date convertToDate(XMLGregorianCalendar calendar) {
        assert calendar != null : CALENDAR_CANNOT_BE_NULL;
        return calendar.toGregorianCalendar().getTime();
    }

    /**
     * @deprecated Duplicate method use {@link #toLocalDate(XMLGregorianCalendar)}
     */
    @Deprecated
    public static LocalDate convertToLocalDate(XMLGregorianCalendar calendar) {
        assert calendar != null : CALENDAR_CANNOT_BE_NULL;
        return calendar.toGregorianCalendar().toZonedDateTime().toLocalDate();
    }

    public static XMLGregorianCalendar convertToXMLGregorianCalendar(Instant instant) {
        if (instant == null) {
            return null;
        }
        return datatypeFactory.newXMLGregorianCalendar(GregorianCalendar.from(instant.atZone(ZoneId.systemDefault())));
    }

    public static javax.xml.datatype.Duration getDayDuration(int numberOfDaysOffset) {
        return datatypeFactory.newDuration(true, BigInteger.ZERO, BigInteger.ZERO, BigInteger.valueOf(numberOfDaysOffset), BigInteger.ZERO, BigInteger.ZERO, BigDecimal.ZERO);
    }

    public static Date getDOWAdjustedLastYearDate(final Date date) {
        return DateUtils.addWeeks(date, -52);
    }

    public static Date getDOWAdjustedNextYearDate(Date date) {
        return DateUtils.addWeeks(date, 52);
    }

    public static LocalDate parseLocalDate(Date detailDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        return LocalDate.parse(simpleDateFormat.format(detailDate));
    }

    public static long daysBetween(Date d1, Date d2) {
        Instant instant1 = Instant.ofEpochMilli(d1.getTime());
        Instant instant2 = Instant.ofEpochMilli(d2.getTime());
        Duration between = Duration.between(instant1, instant2);
        return between.toDays();
    }

    public static List<Date> getAllDatesBetweenExcludingEndDate(LocalDate start, LocalDate end) {
        List<Date> datesBetween = new ArrayList<>();
        while (start.isBefore(end)) {
            datesBetween.add(DateTimeUtil.parseDate(start.toString(), DateTimeUtil.DATE_FORMAT));
            start = start.plusDays(1);
        }
        return datesBetween;
    }

    public static LocalDateTime buildLocalDateTime(int year, int month, int dayOfMonth, int hour, int minute, int second) {
        return LocalDateTime.of(year, month, dayOfMonth, hour, minute, second);
    }

    public static LocalDateTime makeLocalDateTimeFromDateString(String dateString, String timeString) {
        return DateTimeUtil.parseLocalDateTime(dateString + " " + timeString, DateTimeUtil.DATE_TIME_FORMAT);
    }

    public static Set<DayOfWeek> getDaysOfWeekForDateRange(LocalDate startDate, LocalDate endDate) {
        Set<DayOfWeek> daysOfWeek = new HashSet<>();
        int totalNumberOfDaysInAWeek = DayOfWeek.values().length;
        while (daysOfWeek.size() < totalNumberOfDaysInAWeek && !startDate.isAfter(endDate)) {
            daysOfWeek.add(startDate.getDayOfWeek());
            startDate = startDate.plusDays(1);
        }

        return daysOfWeek;
    }

    public static LocalDateTime getOrDefault(LocalDateTime date) {
        return date == null ? LocalDateTime.now() : date;
    }

    /**
     * Get all dates in range
     *
     * @param start start date of service
     * @param end   end date of service
     * @return all dates
     */
    public static List<LocalDate> dateRange(LocalDate start, LocalDate end) {
        return start.equals(end)
                ? Arrays.asList(start)
                : Stream.iterate(start, date -> date.plusDays(1)).limit(ChronoUnit.DAYS.between(start, end)).collect(Collectors.toList());
    }

    public static String parseDate(Date date, TimeZone fromTimeZone, TimeZone toTimeZone) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DateTimeUtil.DATE_TIME_FORMAT_WITHOUT_MS);
        final LocalDateTime localDateTime = parseLocalDateTime(formatDate(date, DATE_TIME_FORMAT_WITHOUT_MS_AND_TIMEZONE), DATE_TIME_FORMAT_WITHOUT_MS_AND_TIMEZONE);
        ZoneId propertyZoneId = fromTimeZone.toZoneId();
        ZonedDateTime zonedDateTime = localDateTime.atZone(propertyZoneId);
        final ZonedDateTime serverDateTime = zonedDateTime.withZoneSameInstant(toTimeZone.toZoneId());
        return formatter.format(serverDateTime);
    }

    /**
     * Get all dates in range
     *
     * @param inStart start date of service
     * @param inEnd   einStartnd date of service
     * @return all dates
     */
    public static List<LocalDate> dateRange(Date inStart, Date inEnd) {
        LocalDate start = toLocalDate(inStart);
        LocalDate end = toLocalDate(inEnd);
        if (start == null) {
            return new ArrayList<>();
        }
        return start.equals(end)
                ? Arrays.asList(start)
                : Stream.iterate(start, date -> date.plusDays(1)).limit(ChronoUnit.DAYS.between(start, end)).collect(Collectors.toList());
    }

    public static Date toLocalDate(String dateValue) {
        return !Strings.isNullOrEmpty(dateValue) ? toDate(LocalDate.parse(dateValue, DATE_FORMATTER_YYYYMMDD)) : null;
    }

    public static Date getDateFromString(String date) {
        LocalDate localDate = LocalDate.parse(date, DATE_TIME_FORMATTER);
        return Date.from(localDate.atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
    }

    public static Date getFrom(LocalDateTime dateTime) {
        if(dateTime != null) {
            return Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
        }
        return null;
    }

}
