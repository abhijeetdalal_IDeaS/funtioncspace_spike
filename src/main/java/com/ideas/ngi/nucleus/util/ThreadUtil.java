package com.ideas.ngi.nucleus.util;

import com.ideas.ngi.nucleus.exception.NucleusException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.function.Predicate;
import java.util.function.Supplier;

public final class ThreadUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(ThreadUtil.class);
    private static final int DEFAULT_MILLIS_BETWEEN_RETRIES = 200;
    private static final int DEFAULT_TIMEOUT_MILLIS = 30000;

    private ThreadUtil() {}

    public static <T> T waitUntil(Supplier<T> supplier) {
        return waitUntil(supplier, Objects::nonNull);
    }

    public static <T> T waitUntil(Supplier<T> supplier, Predicate<T> predicate) {
        return waitUntil(DEFAULT_MILLIS_BETWEEN_RETRIES, DEFAULT_TIMEOUT_MILLIS, supplier, predicate);
    }

    public static <T> T waitUntil(int millisBetweenRetries, int timeoutMillis, Supplier<T> supplier, Predicate<T> predicate) {
        // Keep track of the elapsed time
        long elapsedMillis;
        long startMillis = System.currentTimeMillis();

        // Check the captor values to see if we have the expected number of messages for the expected type
        while (true) {
            T result = supplier.get();

            if (predicate.test(result)) {
                return result;
            }

            // Calculate the number of milliseconds since the start of the wait
            elapsedMillis = System.currentTimeMillis() - startMillis;

            // If we have waited for longer than the expected amount of time, blow up so we don't hang out infinitely
            if (elapsedMillis > timeoutMillis) {
                throw new NucleusException("Exceeded timeout waiting for flow to complete for predicate");
            }

            // Sleep for some milliseconds before re-checking
            sleep(millisBetweenRetries);
        }
    }

    @SuppressWarnings("squid:S1166")
    public static void waitFor(Runnable task) {
        Supplier<Boolean> runnableSuccess = () -> {
            try {
                task.run();
                return true;
            } catch (AssertionError e) {
                LOGGER.debug("Encountered assertion error (will retry): {}", e.getMessage());
                return false;
            }
        };

        waitUntil(runnableSuccess, b -> b);
    }

    public static void sleep(int sleep) {
        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            throw new NucleusException("InterruptedException happened while attempting to wait to retry", e);
        }
    }
}


