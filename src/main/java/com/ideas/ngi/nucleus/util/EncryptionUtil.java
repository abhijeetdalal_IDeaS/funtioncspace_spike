package com.ideas.ngi.nucleus.util;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.salt.SaltGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class EncryptionUtil {
    private SaltGenerator saltGenerator;
    private String jasyptPassword = "c8KZPAOxr8tj";
    private StandardPBEStringEncryptor stringEncryptor;

    @Autowired
    public EncryptionUtil(SaltGenerator saltGenerator) {
        this.saltGenerator = saltGenerator;
    }

    @PostConstruct
    public void init(){
        stringEncryptor =  new StandardPBEStringEncryptor();
        stringEncryptor.setPassword(jasyptPassword);
        stringEncryptor.setSaltGenerator(saltGenerator );
        stringEncryptor.initialize();
    }

    public String encrypt(String encryptThis){
        return stringEncryptor.encrypt(encryptThis);
    }

    public String decrypt(String decryptThis){
        return stringEncryptor.decrypt(decryptThis);
    }
}
