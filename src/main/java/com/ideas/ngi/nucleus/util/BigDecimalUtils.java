package com.ideas.ngi.nucleus.util;


import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalUtils {

    public static final int DEFAULT_SCALE = 5;
    public static final RoundingMode DEFAULT_ROUNDING_MODE = RoundingMode.HALF_UP;

    private BigDecimalUtils() { /* Don't instantiate */}

    public static BigDecimal getBigDecimal(double value) {
        return BigDecimal.valueOf(value).setScale(DEFAULT_SCALE, DEFAULT_ROUNDING_MODE);
    }

    public static BigDecimal getBigDecimal(long value) {
        return BigDecimal.valueOf(value).setScale(DEFAULT_SCALE, DEFAULT_ROUNDING_MODE);
    }

    public static BigDecimal ensureScale(BigDecimal value) {
        return value.setScale(DEFAULT_SCALE, DEFAULT_ROUNDING_MODE);
    }

    public static BigDecimal getBigDecimal(Double value) {
        return value != null ? BigDecimal.valueOf(value).setScale(DEFAULT_SCALE, DEFAULT_ROUNDING_MODE) : BigDecimal.ZERO;
    }

    public static BigDecimal getBigDecimal(Float value) {
        return value != null ? BigDecimal.valueOf(value).setScale(DEFAULT_SCALE, DEFAULT_ROUNDING_MODE) : BigDecimal.ZERO;
    }
}
