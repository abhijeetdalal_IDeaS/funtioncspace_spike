package com.ideas.ngi.nucleus.config;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Component
@RefreshScope
public class NucleusConfigProperties {
    private static final Logger LOGGER = LoggerFactory.getLogger(NucleusConfigProperties.class);

    private String hostname = "unknown";

    @Value("${ngi.version:None}")
    private String ngiVersion;

    /**
     * returns whether or not to publish some information through rest. Used in lower environments to
     * display information like SFTP connection information for use by integration tests
     */
    @Value("${publish.private.info:true}")
    private Boolean publishPrivateInfo;

    @Value("${ngi.data.directory:/NGI/Data/SpringBoot}")
    private File dataDirectory;

    @Value("${nucleus.zero.occupancy.summary.groups.pastdates:true}")
    private boolean zeroOccupancySummaryForGroupsInPastDates;

    @Value("${nucleus.concurrent.queue.consumers:10}")
    private Integer concurrentQueueConsumers;

    @Value("${nucleus.resolve.undeclared.shares:true}")
    private boolean resolveUndeclaredShares;

    @Value("${nucleus.health.dependencyResolutionRequired:false}")
    private boolean dependencyResolutionRequired;

    @Value("${nucleus.carryforward.save.attempts:10}")
    private int carryForwardSaveAttempts;

    @Value("${nucleus.activity.bulk.ops.batch.size:1000}")
    private int activityBulkOpsBatchSize;

    @Value("${nucleus.activity.useInventoryForPastDays:false}")
    private boolean useInventoryForPastDays;

    @Value("${nucleus.toggle.vendorConfigEnabled:true}")
    private boolean vendorConfigEnabled;

    @Value("${nucleus.decision.delivery.monitor.delay.seconds:3600}")
    private Integer decisionDeliveryMonitorDelaySeconds;

    @Value("${nucleus.toggle.deleteRabbitQueues:true}")
    private boolean deleteRabbitQueues;

    @Value("${nucleus.monitor.external.enabled:true}")
    private boolean externalMonitorEnabled;

    @Value("${ngi.circuit.breaker.enabled:false}")
    private boolean circuitBreakerEnabled;

    @Value("${nucleus.customMetrics.enabled:true}")
    private boolean customMetricsEnabled;

    public String getNgiVersion() {
        return ngiVersion;
    }

    public void setNgiVersion(String ngiVersion) {
        this.ngiVersion = ngiVersion;
    }

    public Boolean getPublishPrivateInfo() {
        return publishPrivateInfo;
    }

    public void setPublishPrivateInfo(Boolean publishPrivateInfo) {
        this.publishPrivateInfo = publishPrivateInfo;
    }

    public File getDataDirectory() {
        return dataDirectory;
    }

    public void setDataDirectory(File dataDirectory) {
        this.dataDirectory = dataDirectory;
    }

    public boolean isZeroOccupancySummaryForGroupsInPastDates() {
        return zeroOccupancySummaryForGroupsInPastDates;
    }

    public void setZeroOccupancySummaryForGroupsInPastDates(boolean zeroOccupancySummaryForGroupsInPastDates) {
        this.zeroOccupancySummaryForGroupsInPastDates = zeroOccupancySummaryForGroupsInPastDates;
    }

    public Integer getConcurrentQueueConsumers() {
        return concurrentQueueConsumers;
    }

    public boolean isResolveUndeclaredShares() {
        return resolveUndeclaredShares;
    }

    public void setResolveUndeclaredShares(boolean resolveUndeclaredShares) {
        this.resolveUndeclaredShares = resolveUndeclaredShares;
    }

    public boolean isDependencyResolutionRequired() {
        return dependencyResolutionRequired;
    }

    public int getCarryForwardSaveAttempts() {
        return carryForwardSaveAttempts;
    }

    public void setCarryForwardSaveAttempts(int carryForwardSaveAttempts) {
        this.carryForwardSaveAttempts = carryForwardSaveAttempts;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getActivityBulkOpsBatchSize() {
        return activityBulkOpsBatchSize;
    }

    public void setActivityBulkOpsBatchSize(int activityBulkOpsBatchSize) {
        this.activityBulkOpsBatchSize = activityBulkOpsBatchSize;
    }

    public boolean isUseInventoryForPastDays() {
        return useInventoryForPastDays;
    }

    public void setUseInventoryForPastDays(boolean useInventoryForPastDays) {
        this.useInventoryForPastDays = useInventoryForPastDays;
    }

    public boolean isVendorConfigEnabled() {
        return vendorConfigEnabled;
    }

    public void setVendorConfigEnabled(boolean vendorConfigEnabled) {
        this.vendorConfigEnabled = vendorConfigEnabled;
    }

    public boolean shouldDeleteRabbitQueues() {
        return deleteRabbitQueues;
    }

    public void setDeleteRabbitQueues(boolean deleteRabbitQueues) {
        this.deleteRabbitQueues = deleteRabbitQueues;
    }

    @PostConstruct
    public void init() throws IOException {
        FileUtils.forceMkdir(dataDirectory);

        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            LOGGER.warn("Unable to determine app hostname", e);
        }
    }

    public Integer getDecisionDeliveryMonitorDelaySeconds() {
        return decisionDeliveryMonitorDelaySeconds;
    }

    public void setDecisionDeliveryMonitorDelaySeconds(Integer delaySeconds) {
        this.decisionDeliveryMonitorDelaySeconds = delaySeconds;
    }

    public boolean isExternalMonitorEnabled() {
        return externalMonitorEnabled;
    }

    public void setExternalMonitorEnabled(boolean externalMonitorEnabled) {
        this.externalMonitorEnabled = externalMonitorEnabled;
    }

    public boolean isCircuitBreakerEnabled() {
        return circuitBreakerEnabled;
    }

    public void setCircuitBreakerEnabled(boolean circuitBreakerEnabled) {
        this.circuitBreakerEnabled = circuitBreakerEnabled;
    }

    public boolean isCustomMetricsEnabled() {
        return customMetricsEnabled;
    }

    public void setCustomMetricsEnabled(boolean customMetricsEnabled) {
        this.customMetricsEnabled = customMetricsEnabled;
    }
}
