package com.ideas.ngi.nucleus.config.context;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ApplicationContextProvider implements ApplicationContextAware {

    private static ApplicationContext CONTEXT;

    public static ApplicationContext getApplicationContext() {
        return CONTEXT;
    }

    @SuppressWarnings("AccessStaticViaInstance")
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.CONTEXT = applicationContext;
    }

    public static <T> T getBeanOfType(Class<T> beanClass) {
        return CONTEXT.getBean(beanClass);
    }

    public static <T> T getFirstBeanOfType(Class<T> beanClass) {
        Map<String, T> beans = CONTEXT.getBeansOfType(beanClass);
        if (beans.isEmpty()) {
            throw new NoSuchBeanDefinitionException("No bean found for class " + beanClass);
        }
        return beans.values().iterator().next();
    }
}
