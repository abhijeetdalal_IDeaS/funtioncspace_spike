package com.ideas.ngi.nucleus.config.internalclient;

import com.google.common.annotations.VisibleForTesting;
import com.ideas.ngi.nucleus.config.NucleusConfigProperties;
import com.ideas.ngi.nucleus.config.circuitbreaker.CircuitBreaker;
import com.ideas.ngi.nucleus.config.mongo.repository.NucleusMongoRepository;
import com.ideas.ngi.nucleus.integration.handler.GenericHandler;
import com.ideas.ngi.nucleus.integration.util.MessageHeaderUtil;
import com.ideas.ngi.nucleus.monitor.entity.NucleusCompletedNotification;
import com.ideas.ngi.nucleus.monitor.entity.NucleusErrorNotification;
import com.ideas.ngi.nucleus.monitor.entity.NucleusMonitorMessage;
import com.ideas.ngi.nucleus.rest.client.RestClient;
import com.ideas.ngi.nucleus.rest.endpoint.RestEndpoint;
import com.ideas.ngi.nucleus.rest.handler.RestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.Link;
import org.springframework.integration.transformer.GenericTransformer;
import org.springframework.messaging.MessageChannel;

import java.util.Map;

public abstract class AbstractMonitorMessageHandler<S  extends NucleusMonitorMessage, T extends NucleusMonitorMessage> implements GenericHandler<S> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractMonitorMessageHandler.class);

    private NucleusConfigProperties configProperties;
    private CircuitBreaker circuitBreaker;
    private RestHandler restHandler;
    protected GenericTransformer<S,T> transformer;
    protected NucleusMongoRepository<S,String> repository;

    public AbstractMonitorMessageHandler(NucleusConfigProperties configProperties,
                                         CircuitBreaker circuitBreaker,
                                         RestHandler restHandler,
                                         GenericTransformer<S,T> transformer,
                                         NucleusMongoRepository<S,String> repository) {
        this.configProperties = configProperties;
        this.circuitBreaker = circuitBreaker;
        this.restHandler = restHandler;
        this.transformer = transformer;
        this.repository = repository;
    }

    @Override
    public Object handle(S monitorMessage, Map<String, Object> headers) {
        T payload = transformer.transform(monitorMessage);

        RestClient restClient = (RestClient) headers.get(MessageHeaderUtil.REST_CLIENT);
        RestEndpoint restEndpoint = (RestEndpoint) headers.get(MessageHeaderUtil.REST_ENDPOINT);
        MessageChannel fromChannel = (MessageChannel) headers.get(MessageHeaderUtil.FROM_CHANNEL);
        Long jobInstanceId;
        if (LOGGER.isDebugEnabled()) {
            logLinks(payload);
        }
        if (configProperties.isCircuitBreakerEnabled()) {
            if (fromChannel == null) {
                throw new RuntimeException("MessageChannel not found in header field '" + MessageHeaderUtil.FROM_CHANNEL + "', required for circuit breaker");
            }
            jobInstanceId = circuitBreaker.handle(restClient, restEndpoint, payload, headers, fromChannel);
        } else {
            jobInstanceId = restHandler.handle(restClient, restEndpoint, payload);
        }
        monitorMessage.setJobInstanceId(jobInstanceId);
        return repository.save(monitorMessage);
    }

    @VisibleForTesting
    public void setCircuitBreaker(CircuitBreaker circuitBreaker) {
        this.circuitBreaker = circuitBreaker;
    }

    private void logLinks(T payload) {
        if (payload instanceof NucleusErrorNotification) {
            NucleusErrorNotification errorNotification = ((NucleusErrorNotification) payload);
            LOGGER.debug("Monitor error notification to G3 @retry: {} @abandon: {} @payload: {}", getHref(errorNotification.getRetryLink()),
                    getHref(errorNotification.getAbandonLink()), getHref(errorNotification.getPayloadLink()));
        } else if (payload instanceof NucleusCompletedNotification) {
            NucleusCompletedNotification completedNotification = ((NucleusCompletedNotification) payload);
            LOGGER.debug("Monitor completed notification to G3 @payload {}", completedNotification.getPayloadLink().getHref());
        }
    }

    private String getHref(Link link) {
        return link != null ? link.getHref() : "null";
    }
}
