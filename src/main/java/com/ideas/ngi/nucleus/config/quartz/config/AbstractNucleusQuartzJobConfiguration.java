package com.ideas.ngi.nucleus.config.quartz.config;

import com.ideas.ngi.nucleus.exception.NucleusException;
import org.quartz.*;
import org.quartz.impl.JobDetailImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import javax.annotation.PostConstruct;

import static org.quartz.TriggerBuilder.newTrigger;

public abstract class AbstractNucleusQuartzJobConfiguration {

    public static final String TRIGGER = "Trigger";
    public static final String GROUP = "Group";

    private SchedulerFactoryBean schedulerFactoryBean;
    private SimpleScheduleBuilder simpleScheduleBuilder;

    protected abstract JobDetail getJobDetail();

    @Autowired
    public void setSchedulerFactoryBean(SchedulerFactoryBean schedulerFactoryBean) {
        this.schedulerFactoryBean = schedulerFactoryBean;
    }

    @Autowired
    public void setSimpleScheduleBuilder(SimpleScheduleBuilder simpleScheduleBuilder) {
        this.simpleScheduleBuilder = simpleScheduleBuilder;
    }

    @PostConstruct
    private void initialize() {
        addJobToScheduler();
    }

    protected void addJobToScheduler() {
        try {
            JobDetail jobDetail = getJobDetail();
            Trigger trigger = buildTrigger(jobDetail);

            schedulerFactoryBean.getScheduler().addJob(jobDetail, true, true);
            if (!schedulerFactoryBean.getScheduler().checkExists(trigger.getKey())) {
                schedulerFactoryBean.getScheduler().scheduleJob(trigger);
            }
        } catch (SchedulerException e) {
            throw new NucleusException("Failed while initializing the schedulerFactoryBean" + e);
        }
    }

    protected JobDetail buildJobDetail(Class<?> jobClass) {
        return buildJobDetail(jobClass.getSimpleName(), jobClass);
    }

    @SuppressWarnings("unchecked")
    protected JobDetail buildJobDetail(String name, Class<?> jobClass) {
        JobDetailImpl jobDetail = new JobDetailImpl();
        jobDetail.setKey(new JobKey(name, name + GROUP));
        jobDetail.setJobClass((Class<? extends Job>) jobClass);
        jobDetail.setDurability(true);
        return jobDetail;
    }

    protected Trigger buildTrigger(JobDetail jobDetail) {
        return newTrigger()
                .forJob(jobDetail)
                .withIdentity(jobDetail.getKey().getName() + TRIGGER, jobDetail.getKey().getName() + GROUP)
                .withSchedule(simpleScheduleBuilder.repeatForever())
                .build();
    }

}
