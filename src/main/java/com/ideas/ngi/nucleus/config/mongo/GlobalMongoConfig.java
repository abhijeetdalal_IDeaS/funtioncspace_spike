package com.ideas.ngi.nucleus.config.mongo;

import com.ideas.ngi.nucleus.config.mongo.annotation.GlobalCollection;
import com.ideas.ngi.nucleus.config.mongo.repository.NucleusMongoRepositoryImpl;
import com.ideas.ngi.nucleus.util.EncryptionUtil;
import com.mongodb.MongoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.actuate.mongo.MongoHealthIndicator;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Defines beans and configuration for using MongoDB.
 */
@Configuration
@RefreshScope
@EnableMongoRepositories(
        basePackages = {"com.ideas"},
        includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, value = GlobalCollection.class),
        repositoryBaseClass = NucleusMongoRepositoryImpl.class,
        mongoTemplateRef = GlobalMongoConfig.GLOBAL_MONGO_TEMPLATE)
public class GlobalMongoConfig extends AbstractMongoConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalMongoConfig.class);

    public static final String GLOBAL_MONGO_TEMPLATE = "globalMongoTemplate";
    public static final String GLOBAL_MONGO_CLIENT = "globalMongoClient";
    public static final String GLOBAL_MONGO_FACTORY = "globalMongoFactory";
    public static final String GLOBAL_MONGO_HEALTH = "globalMongoHealth";

    public GlobalMongoConfig(MongoConfigProperties mongoConfigProperties, EncryptionUtil encryptionUtil) {
        super(mongoConfigProperties, encryptionUtil);
    }

    @Override
    protected String getUri() {
        return mongoConfigProperties.getGlobalMongoUri();
    }

    @Override
    @Bean(name = GLOBAL_MONGO_CLIENT)
    public MongoClient mongoClient() {
        return createMongoClient(mongoConfigProperties.getSecureUri(getUri()), defaultClientOptions());
    }

    @Override
    protected String getDatabaseName() {
        return mongoConfigProperties.getGlobalDatabaseName();
    }

    @Bean(name = GLOBAL_MONGO_TEMPLATE)
    public MongoTemplate mongoTemplate(MongoConverter mappingMongoConverter) {
        LOGGER.info("Configuring MongoTemplate for database {}", mongoConfigProperties.getGlobalDatabaseName());
        return new MongoTemplate(mongoDbFactory(), mappingMongoConverter);
    }

    /*
     * Define custom error handling as the DuplicateKeyException for MongoDB is not fully serializable
     * and will prevent error messages from being placed on the errorChannel queue in Rabbit.
     * See https://jira.mongodb.org/browse/JAVA-2131
     */
    @Override
    @Bean(GLOBAL_MONGO_FACTORY)
    public MongoDbFactory mongoDbFactory() {
        return new NucleusMongoDbFactory(mongoClient(), getDatabaseName());
    }

    @Bean(GLOBAL_MONGO_HEALTH)
    public MongoHealthIndicator mongoHealthIndicator(@Qualifier(GLOBAL_MONGO_TEMPLATE) MongoTemplate mongoTemplate) {
        return new MongoHealthIndicator(mongoTemplate);
    }
}
