package com.ideas.ngi.nucleus.config.mongo.repository;

import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.FindIterable;
import org.bson.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.util.Pair;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Stream;

@NoRepositoryBean
public interface NucleusMongoRepository<T,ID extends Serializable> extends MongoRepository<T,ID> {

    Page<T> find(Criteria criteria, Pageable pageable);

    List<T> find(Criteria criteria, Sort sort);

    T findOne(String id);

    FindIterable<Document> find(Criteria criteria);

    /**
     * @deprecated Use {@link #saveAll(Iterable)} instead
     */
    @Deprecated
    default Iterable<T> save(Iterable<T> entities) {
        return saveAll(entities);
    }

    /**
     * @deprecated Use {@link #deleteAll(Iterable)} instead
     */
    @Deprecated
    default void delete(Iterable<? extends T> entities) {
        deleteAll(entities);
    }

    Slice<T> findSlice(Criteria criteria, Pageable pageable);

    Stream<T> stream(Criteria criteria, Sort sort);
    default Stream<T> stream(Criteria criteria) {
        return stream(criteria, Sort.unsorted());
    }

    Stream<T> parallelStream(Criteria criteria, Sort sort);
    default Stream<T> parallelStream(Criteria criteria) {
        return parallelStream(criteria, Sort.unsorted());
    }

    List<T> findLatest(Criteria criteria);

    Class<T> getEntityType();

    String getCollectionName();

    Page<T> aggregate(TypedAggregation aggregation, Class<T> tClass, Pageable pageable);

    int bulkOpsSave(BulkOperations.BulkMode bulkMode, List<T> entities);

    int bulkOpsSave(BulkOperations.BulkMode bulkMode, List<T> entities, int batchSize);

    int bulkOpsSave(BulkOperations.BulkMode bulkMode, List<T> entities, CarryForwardHandler<T> carryForwardHandler);

    int bulkOpsUpdateOne(BulkOperations.BulkMode bulkMode, List<T> entities, UpdatePerEntity<T> updatePerEntity);

    BulkWriteResult bulkOpsUpsertMulti(List<Pair<Query, Update>> updates);

    BulkWriteResult bulkOpsUpdateMulti(List<Pair<Query, Update>> updates);

    BulkOperations bulkOps(BulkOperations.BulkMode bulkMode);

    Update getUpdate(T entity, String... excludeKeys);

    String getIdAttribute();

    List<T> findAllAndRemove(Criteria criteria);

    long delete(Criteria criteria);

    interface UpdatePerEntity<T> {
        Update updatePerEntity(T entity);
    }

    interface CarryForwardHandler<T> {
        void carryForward(List<T> entitiesInError);
    }
}
