package com.ideas.ngi.nucleus.config.mongo.controller;

import com.github.rutledgepaulv.rqe.conversions.StringToTypeConverter;
import com.github.rutledgepaulv.rqe.pipes.DefaultArgumentConversionPipe;
import com.github.rutledgepaulv.rqe.pipes.QueryConversionPipeline;
import com.ideas.ngi.nucleus.config.mongo.converter.MongoSearchCriteriaConverter;


public final class QueryConversionPipelineIdeas {

    private static QueryConversionPipeline queryConversionPipeline = buildQueryConversionPipelineInstance();

    public static QueryConversionPipeline getInstance() {
        return queryConversionPipeline;
    }

    private QueryConversionPipelineIdeas() {}

    private static QueryConversionPipeline buildQueryConversionPipelineInstance() {

        // Get the QueryConversionPipelineBuilder so we can use our own ArgumentConversionPipe
        QueryConversionPipeline.QueryConversionPipelineBuilder queryConversionPipelineBuilder =
                QueryConversionPipeline.builder();

        // Get the DefaultArgumentConversionPipeBuilder so we can add our own StringToTypeConverter
        DefaultArgumentConversionPipe.DefaultArgumentConversionPipeBuilder argConversionBuilder =
                DefaultArgumentConversionPipe.builder();

        // Use custom resolver to allows for resolution of fields of Object type for searching sub-elements
        argConversionBuilder.useNonDefaultFieldResolver(new NucleusMongoPersistentEntityFieldTypeResolver());

        // Create our custom StringToTypeConverter
        StringToTypeConverter stringToTypeConverter = new MongoSearchCriteriaConverter();

        // Use the DefaultArgumentConversionPipeBuilder to get a DefaultArgumentConversionPipeBuilder which
        // uses our custom StringToTypeConverter
        DefaultArgumentConversionPipe.DefaultArgumentConversionPipeBuilder customArgumentConversionPipeBuilder =
                argConversionBuilder.useNonDefaultStringToTypeConverter(stringToTypeConverter);

        // Create our own DefaultArgumentConversionPipe
        DefaultArgumentConversionPipe customArgumentConversionPipe = customArgumentConversionPipeBuilder.build();

        // Finally, use our custom DefaultArgumentConversionPipe to build a QueryConversionPipeline which will have
        // our custom StringToTypeConverter
        queryConversionPipeline =
                queryConversionPipelineBuilder.useNonDefaultArgumentConversionPipe(customArgumentConversionPipe).build();

        return queryConversionPipeline;
    }
}
