package com.ideas.ngi.nucleus.config.quartz.config;

import com.google.common.annotations.VisibleForTesting;
import com.ideas.ngi.nucleus.config.mongo.GlobalMongoConfig;
import com.ideas.ngi.nucleus.config.mongo.MongoConfigProperties;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;
import org.quartz.SimpleScheduleBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import java.util.Properties;

@Configuration
public class NucleusQuartzConfiguration {
    private static final Logger LOGGER  = LoggerFactory.getLogger(NucleusQuartzConfiguration.class);

    private static final String QUARTZ_TRIGGERS = "quartz__triggers";
    static final String QUARTZ_JOBS = "quartz__jobs";

    private AutowiringQuartzJobFactory autowiringQuartzJobFactory;
    private MongoConfigProperties mongoConfigProperties;
    private NucleusQuartzConfigProperties nucleusQuartzConfigProperties;
    private MongoOperations mongoOperations;

    @Autowired
    public NucleusQuartzConfiguration(AutowiringQuartzJobFactory jobFactory,
                                      MongoConfigProperties mongoConfigProperties,
                                      NucleusQuartzConfigProperties quartzConfigProperties,
                                      @Qualifier(GlobalMongoConfig.GLOBAL_MONGO_TEMPLATE) MongoOperations mongoOperations) {
        this.autowiringQuartzJobFactory = jobFactory;
        this.mongoConfigProperties = mongoConfigProperties;
        this.nucleusQuartzConfigProperties = quartzConfigProperties;
        this.mongoOperations = mongoOperations;
    }

    @Bean
    public SchedulerFactoryBean schedulerFactoryBean() {
        return createSchedulerFactoryBean();
    }

    @Bean
    public SimpleScheduleBuilder simpleScheduleBuilder() {
        return SimpleScheduleBuilder
                .simpleSchedule()
                .withIntervalInMilliseconds(nucleusQuartzConfigProperties.getTriggerInterval());
    }

    SchedulerFactoryBean createSchedulerFactoryBean() {
        LOGGER.info("Initializing quartz scheduler");
        cleanAllJobAndTrigger();
        SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
        scheduler.setApplicationContextSchedulerContextKey("applicationContext");
        scheduler.setQuartzProperties(getQuartzProperties());
        // automatically start after startup
        scheduler.setAutoStartup(true);
        scheduler.setStartupDelay(nucleusQuartzConfigProperties.getStartupDelay());
        scheduler.setWaitForJobsToCompleteOnShutdown(true);
        scheduler.setJobFactory(autowiringQuartzJobFactory);
        return scheduler;
    }

    private void cleanAllJobAndTrigger() {
        LOGGER.info("Removing existing jobs and triggers from database {}", mongoOperations.getCollection(QUARTZ_TRIGGERS).getNamespace().getDatabaseName());
        DeleteResult triggersResult = mongoOperations.getCollection(QUARTZ_TRIGGERS).deleteMany(new Document());
        LOGGER.debug("Removed {} triggers", triggersResult.getDeletedCount());
        DeleteResult jobsResult = mongoOperations.getCollection(QUARTZ_JOBS).deleteMany(new Document());
        LOGGER.debug("Removed {} jobs", jobsResult.getDeletedCount());
    }

    @VisibleForTesting
    Properties getQuartzProperties() {
        Properties properties = new Properties();
        properties.put("org.quartz.jobStore.class", nucleusQuartzConfigProperties.getJobStoreClass());
        properties.put("org.quartz.jobStore.mongoUri", mongoConfigProperties.getSecureUri(mongoConfigProperties.getGlobalMongoUri()));
        properties.put("org.quartz.jobStore.dbName", mongoConfigProperties.getGlobalDatabaseName());
        properties.put("org.quartz.jobStore.collectionPrefix", nucleusQuartzConfigProperties.getQuartzCollectionPrefix());
        properties.put("org.quartz.threadPool.threadCount", nucleusQuartzConfigProperties.getQuartzThreadPoolSize());
        properties.put("org.quartz.scheduler.skipUpdateCheck", Boolean.valueOf(nucleusQuartzConfigProperties.isSkipUpdateCheck()).toString());
        properties.put("org.quartz.jobStore.isClustered", Boolean.valueOf(nucleusQuartzConfigProperties.isCluster()).toString());
        properties.put("org.quartz.scheduler.instanceId", nucleusQuartzConfigProperties.getInstanceId());
        properties.put("org.quartz.scheduler.instanceName", nucleusQuartzConfigProperties.getInstanceName());
        return properties;
    }
}