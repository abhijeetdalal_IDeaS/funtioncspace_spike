package com.ideas.ngi.nucleus.config.internalclient;

import com.ideas.ngi.nucleus.config.NucleusConfigProperties;
import com.ideas.ngi.nucleus.config.circuitbreaker.CircuitBreaker;
import com.ideas.ngi.nucleus.monitor.entity.NucleusErrorMessage;
import com.ideas.ngi.nucleus.monitor.entity.NucleusErrorNotification;
import com.ideas.ngi.nucleus.monitor.repository.NucleusErrorMessageRepository;
import com.ideas.ngi.nucleus.monitor.transformer.ErrorNotificationTransformer;
import com.ideas.ngi.nucleus.rest.handler.RestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ErrorMonitorMessageHandler extends AbstractMonitorMessageHandler<NucleusErrorMessage, NucleusErrorNotification> {
    @Autowired
    public ErrorMonitorMessageHandler(NucleusConfigProperties configProperties,
                                      CircuitBreaker circuitBreaker,
                                      RestHandler restHandler,
                                      ErrorNotificationTransformer transformer,
                                      NucleusErrorMessageRepository repository) {
        super(configProperties, circuitBreaker, restHandler, transformer, repository);
    }
}
