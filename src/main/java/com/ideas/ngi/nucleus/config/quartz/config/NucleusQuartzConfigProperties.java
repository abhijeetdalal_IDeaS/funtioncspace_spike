package com.ideas.ngi.nucleus.config.quartz.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@RefreshScope
public class NucleusQuartzConfigProperties {

    @Value("${org.quartz.jobStore.collectionPrefix:quartz_}")
    private String quartzCollectionPrefix;

    @Value("${org.quartz.threadPool.threadCount:10}")
    private String quartzThreadPoolSize;

    @Value("${org.quartz.scheduler.skipUpdateCheck:true}")
    private boolean skipUpdateCheck;

    @Value("${org.quartz.jobStore.isClustered:true}")
    private boolean isCluster;

    @Value("${org.quartz.scheduler.instanceId:AUTO}")
    private String instanceId;

    @Value("${org.quartz.scheduler.instanceName:ngiJobCluster}")
    private String instanceName;

    @Value("${org.quartz.jobStore.class:com.novemberain.quartz.mongodb.MongoDBJobStore}")
    private String jobStoreClass;

    @Value("${quartz.trigger.interval.milliseconds:50}")
    private int triggerInterval;

    @Value("${nucleus.quartz.startup.delay.seconds:60}")
    private int startupDelay;

    public String getQuartzCollectionPrefix() {
        return quartzCollectionPrefix;
    }

    public void setQuartzCollectionPrefix(String quartzCollectionPrefix) {
        this.quartzCollectionPrefix = quartzCollectionPrefix;
    }

    public String getQuartzThreadPoolSize() {
        return quartzThreadPoolSize;
    }

    public void setQuartzThreadPoolSize(String quartzThreadPoolSize) {
        this.quartzThreadPoolSize = quartzThreadPoolSize;
    }

    public boolean isSkipUpdateCheck() {
        return skipUpdateCheck;
    }

    public void setSkipUpdateCheck(boolean skipUpdateCheck) {
        this.skipUpdateCheck = skipUpdateCheck;
    }

    public boolean isCluster() {
        return isCluster;
    }

    public void setCluster(boolean cluster) {
        isCluster = cluster;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public String getJobStoreClass() {
        return jobStoreClass;
    }

    public void setJobStoreClass(String jobStoreClass) {
        this.jobStoreClass = jobStoreClass;
    }

    public int getTriggerInterval() {
        return triggerInterval;
    }

    public void setTriggerInterval(int triggerInterval) {
        this.triggerInterval = triggerInterval;
    }

    public int getStartupDelay() {
        return startupDelay;
    }

    public void setStartupDelay(int startupDelay) {
        this.startupDelay = startupDelay;
    }
}
