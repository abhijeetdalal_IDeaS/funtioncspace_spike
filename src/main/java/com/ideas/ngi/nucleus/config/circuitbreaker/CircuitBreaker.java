package com.ideas.ngi.nucleus.config.circuitbreaker;

import com.google.common.annotations.VisibleForTesting;
import com.ideas.ngi.nucleus.rest.client.RestClient;
import com.ideas.ngi.nucleus.rest.endpoint.RestEndpoint;
import com.ideas.ngi.nucleus.rest.util.RestEndpointParameterUtil;
import org.joda.time.Seconds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.concurrent.ThreadSafe;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
@ThreadSafe
@RefreshScope
@Qualifier("circuitBreaker")
public class CircuitBreaker {
    private static final Logger LOGGER = LoggerFactory.getLogger(CircuitBreaker.class);

    Map<String, Fuse> fuses = new ConcurrentHashMap<>();

    @Value("${circuitbreaker.failure.threshold:25}")
    private Integer failureThreshold;

    @Value("${circuitbreaker.retry.time.seconds:300}")
    private Integer retryTimePeriod;

    public <T> T handle(RestClient restClient, RestEndpoint restEndpoint, Object payload, Map<String,Object> headers, MessageChannel channel) {
        Assert.notNull(channel, "MessageChannel cannot be null!");
        Fuse fuse = findOrCreateFuse(channel.toString());
        fuse.updateState();
        switch (fuse.getState()) {
            case Fuse.OPEN:
                return handleBlockedMessage(payload, headers, channel);
            case Fuse.HALF_OPEN:
            case Fuse.CLOSED:
                return makeCall(restClient, restEndpoint, payload, fuse, headers, channel);
            default:
                return null;
        }
    }

    protected <T> T handleBlockedMessage(Object payload, Map<String,Object> headers, MessageChannel channel) {
        LOGGER.info("Fuse is open for channel {} holding back on message h:{} p:{}", channel.toString(), headers.toString(), payload.toString());
        return null;
    }

    protected <T> T handleFailedMessage(Object payload, Map<String, Object> headers, MessageChannel channel, RuntimeException e) {
        LOGGER.info("Failed message on channel {} h:{} p:{} Error: {}", channel.toString(), headers.toString(), payload.toString(), e.toString());
        return null;
    }

    protected Integer getRetryTimePeriod() {
        return this.retryTimePeriod;
    }

    private <T> T makeCall(RestClient restClient, RestEndpoint restEndpoint, Object payload, Fuse fuse, Map<String,Object> headers, MessageChannel channel) {
        try {
            Map<String, Object> allParameters = RestEndpointParameterUtil.applyValuesFromParameterExpressions(restEndpoint, new HashMap<>(), payload);
            T response = (T) restClient.execute(restEndpoint, allParameters, payload);
            fuse.reset();
            return response;
        } catch (RuntimeException e) {
            LOGGER.warn("Call to " + restEndpoint.getURL() + " failed. " + e);
            markAsFailure(payload, fuse, headers, channel, e);
            return null;
        }
    }

    // TODO: RefreshScope - may need to explicitly adjust fuses if/when properties are updated
    Fuse findOrCreateFuse(String queueName) {
        return fuses.computeIfAbsent(queueName, k -> {
            Fuse fuse = new Fuse(queueName);
            fuse.setFailureThreshold(failureThreshold);
            fuse.setRetryTimePeriod(Seconds.seconds(retryTimePeriod));
            return fuse;
        });
    }

    @VisibleForTesting
    public Map<String, Fuse> getFuses() {
        return fuses;
    }

    @VisibleForTesting
    public void setFuses(Map<String, Fuse> fuses) {
        this.fuses = fuses;
    }

    public void setFailureThreshold(Integer failureThreshold) {
        this.failureThreshold = failureThreshold;
    }

    public void setRetryTimePeriod(Integer retryTimePeriod) {
        this.retryTimePeriod = retryTimePeriod;
    }

    private void markAsFailure(Object payload, Fuse fuse, Map<String, Object> headers, MessageChannel channel, RuntimeException e) {
        fuse.recordFailure();
        handleFailedMessage(payload, headers, channel, e);
    }
}
