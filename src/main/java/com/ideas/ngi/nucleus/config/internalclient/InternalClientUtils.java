package com.ideas.ngi.nucleus.config.internalclient;

import com.ideas.ngi.nucleus.rest.client.RestClient;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;

public class InternalClientUtils {

    public static final String AUTHENTICATION_TOKEN = ".authentication.token";
    public static final String DEFAULT_PROPERTY_ID = ".default.propertyId";

    protected static final String INTERNAL_CLIENT_NAMES = "internalClientNames";
    protected static final String DEFAULT_INTERNAL_CLIENTS = "g3,intf";
    protected static final String TYPE = ".type";
    protected static final String BASE_URL = ".base.url";
    protected static final String MONITOR_URL = ".monitor.url";
    protected static final String MONITOR_REST_CLIENT = "Monitor" + RestClient.class.getSimpleName();

    private static final Map<String, String> DEFAULT_INTERNAL_CLIENT_VALUES = new HashMap<>();

    static {
        // By default set properties for g3 (including monitor) and intf (monitor only)
        DEFAULT_INTERNAL_CLIENT_VALUES.put("g3" + TYPE, InternalClientType.G3.name());
        DEFAULT_INTERNAL_CLIENT_VALUES.put("g3" + BASE_URL, "http://localhost:8080/pacman-platformsecurity/rest/");
        DEFAULT_INTERNAL_CLIENT_VALUES.put("g3" + MONITOR_URL, "http://localhost:8080/job-web/");
        DEFAULT_INTERNAL_CLIENT_VALUES.put("g3" + AUTHENTICATION_TOKEN, "Basic c3NvQGlkZWFzLmNvbTpwYXNzd29yZA==");
        DEFAULT_INTERNAL_CLIENT_VALUES.put("g3" + DEFAULT_PROPERTY_ID, "5");

        DEFAULT_INTERNAL_CLIENT_VALUES.put("intf" + TYPE, InternalClientType.INTF.name());
        DEFAULT_INTERNAL_CLIENT_VALUES.put("intf" + MONITOR_URL, "http://localhost:8080/job-web/");
        DEFAULT_INTERNAL_CLIENT_VALUES.put("intf" + AUTHENTICATION_TOKEN, "Basic c3NvQGlkZWFzLmNvbTpwYXNzd29yZA==");
    }

    /**
     * get value from environment
     *
     * @param internalClientName
     * @param key
     * @param environment
     * @return value
     */
    public static String getValue(String internalClientName, String key, Environment environment) {
        return environment.getProperty(internalClientName + key, DEFAULT_INTERNAL_CLIENT_VALUES.get(internalClientName + key));
    }
}
