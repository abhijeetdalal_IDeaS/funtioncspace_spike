package com.ideas.ngi.nucleus.config.internalclient;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ClientEnvironment {

    public static final String DELIMITER = ",";
    public final List<String> environments;


    public ClientEnvironment(String clientEnvironment) {
        this.environments = Arrays.asList(clientEnvironment.split(DELIMITER)).stream()
                .map(env -> env.trim().toLowerCase())
                .collect(Collectors.toList());
    }

    public boolean supports(String clientEnvironment) {
        String lowerCaseClientEnvironmentName = clientEnvironment.trim().toLowerCase();
        return environments.stream().anyMatch(env -> env.startsWith(lowerCaseClientEnvironmentName));
    }

    public void executeIfSupports(String clientEnvironment, Runnable command) {
        if(supports(clientEnvironment)) {
            command.run();
        }
    }

    public <T> T executeAndReturnIfSupports(String clientEnvironment, Supplier<T> target, T defaultValue) {
        return supports(clientEnvironment) ? target.get() : defaultValue;
    }
}
