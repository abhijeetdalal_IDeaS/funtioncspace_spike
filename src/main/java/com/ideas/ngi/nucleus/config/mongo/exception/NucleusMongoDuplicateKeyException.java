package com.ideas.ngi.nucleus.config.mongo.exception;

import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoServerException;
import com.mongodb.MongoWriteException;
import com.mongodb.WriteConcernResult;
import org.bson.BsonDocument;
import org.bson.BsonValue;

import java.io.Serializable;

import static java.lang.String.format;

@SuppressWarnings("unused")
public class NucleusMongoDuplicateKeyException extends MongoServerException {
    private static final long serialVersionUID = 7466099402959308525L;
    private final BsonDocument response;
    private final NucleusMongoWriteConcernResult writeConcernResult;

    private NucleusMongoDuplicateKeyException(DuplicateKeyException dke) {
        super(DuplicateKeyException.extractErrorCode(dke.getResponse()),
                format("Write failed with error code %d and error message '%s'", DuplicateKeyException.extractErrorCode(dke.getResponse()), DuplicateKeyException.extractErrorMessage(dke.getResponse())),
                dke.getServerAddress());
        this.response = dke.getResponse();
        this.writeConcernResult = new NucleusMongoWriteConcernResult(dke.getWriteConcernResult());
    }

    private NucleusMongoDuplicateKeyException(MongoWriteException dke) {
        super(dke.getCode(), dke.getMessage(), dke.getServerAddress());
        this.response = dke.getError().getDetails();
        // MongoWriteException doesn't contain WriteConcernResult, trying to derive it
        this.writeConcernResult = new NucleusMongoWriteConcernResult(
                WriteConcernResult.acknowledged(0, false, null));
    }

    public static MongoServerException create(MongoServerException ex) {
        if (ex instanceof DuplicateKeyException) {
            // Old way of getting dup key exception
            return new NucleusMongoDuplicateKeyException((DuplicateKeyException) ex);
        } else if (ex instanceof MongoWriteException) {
            // Will be getting MongoWriteException(Code- 11000), when we tried to save/update/.. a SINGLE document, but a doc with same '_id' is already in mongo
            return new NucleusMongoDuplicateKeyException((MongoWriteException) ex);
        }
        return ex;
    }

    public BsonDocument getResponse() {
        return response;
    }

    public NucleusMongoWriteConcernResult getWriteConcernResult() {
        return writeConcernResult;
    }

    public class NucleusMongoWriteConcernResult implements Serializable {
        private static final long serialVersionUID = -3440553897821532886L;
        private boolean wasAcknowledged;
        private int count;
        private boolean updateOfExisting;
        private transient BsonValue upsertedId;

        public NucleusMongoWriteConcernResult(WriteConcernResult writeConcernResult) {
            this.wasAcknowledged = writeConcernResult.wasAcknowledged();
            this.count = writeConcernResult.getCount();
            this.updateOfExisting = writeConcernResult.isUpdateOfExisting();
            this.upsertedId = writeConcernResult.getUpsertedId();
        }

        public boolean isWasAcknowledged() {
            return wasAcknowledged;
        }

        public int getCount() {
            return count;
        }

        public boolean isUpdateOfExisting() {
            return updateOfExisting;
        }

        public BsonValue getUpsertedId() {
            return upsertedId;
        }
    }
}

