package com.ideas.ngi.nucleus.config.encryption;

import org.jasypt.salt.SaltGenerator;
import org.jasypt.salt.StringFixedSaltGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RefreshScope
public class EncryptionConfig {
    @Value("${encryption.salt:salt@43543d}")
    private String salt;

    @Bean
    public SaltGenerator getSaltGenerator(){
        return new StringFixedSaltGenerator(salt);
    }
}
