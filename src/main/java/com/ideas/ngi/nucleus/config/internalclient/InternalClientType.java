package com.ideas.ngi.nucleus.config.internalclient;

public enum InternalClientType {
    G3,
    G2_CASPER,
    INTF,
    MARS
}
