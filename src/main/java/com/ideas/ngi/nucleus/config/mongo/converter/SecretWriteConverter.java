package com.ideas.ngi.nucleus.config.mongo.converter;

import com.ideas.ngi.nucleus.config.mongo.entity.Secret;
import com.ideas.ngi.nucleus.util.EncryptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

@WritingConverter
public class SecretWriteConverter implements Converter<Secret, String> {

    private final EncryptionUtil encryptionUtil;

    @Autowired
    public SecretWriteConverter(final EncryptionUtil encryptionUtil) {
        this.encryptionUtil = encryptionUtil;
    }

    @Override
    public String convert(Secret source) {
        return source == null ? null : encryptionUtil.encrypt(source.getValue());
    }
}
