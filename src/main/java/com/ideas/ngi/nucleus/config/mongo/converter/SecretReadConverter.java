package com.ideas.ngi.nucleus.config.mongo.converter;

import com.ideas.ngi.nucleus.config.mongo.entity.Secret;
import com.ideas.ngi.nucleus.util.EncryptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

@ReadingConverter
public class SecretReadConverter implements Converter<String, Secret> {

    private final EncryptionUtil encryptionUtil;

    @Autowired
    public SecretReadConverter(final EncryptionUtil encryptionUtil) {
        this.encryptionUtil = encryptionUtil;
    }

    @Override
    public Secret convert(String source) {
        return source == null ? null : new Secret(encryptionUtil.decrypt(source));
    }
}
