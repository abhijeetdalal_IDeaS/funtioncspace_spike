package com.ideas.ngi.nucleus.config.mongo.repository;

import com.google.common.collect.Sets;
import com.ideas.ngi.nucleus.config.NucleusConfigProperties;
import com.ideas.ngi.nucleus.config.context.ApplicationContextProvider;
import com.ideas.ngi.nucleus.config.mongo.MongoConfigProperties;
import com.ideas.ngi.nucleus.config.mongo.exception.NucleusMongoBulkOperationException;
import com.ideas.ngi.nucleus.data.AuditableEntity;
import com.ideas.ngi.nucleus.exception.NucleusException;
import com.ideas.ngi.nucleus.util.BatchingIterator;
import com.ideas.ngi.nucleus.util.ReflectionUtil;
import com.ideas.ngi.nucleus.validator.NucleusValidator;
import com.mongodb.bulk.BulkWriteError;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.result.DeleteResult;
import org.apache.commons.collections.CollectionUtils;
import org.bson.Document;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.repository.query.MongoEntityInformation;
import org.springframework.data.mongodb.repository.support.SimpleMongoRepository;
import org.springframework.data.util.CloseableIterator;
import org.springframework.data.util.Pair;

import java.io.Serializable;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

public class NucleusMongoRepositoryImpl<T, ID extends Serializable> extends SimpleMongoRepository<T, ID> implements NucleusMongoRepository<T, ID> {

    private static final String CREATE_DATE = "createDate";
    private static final String LAST_MODIFIED_DATE = "lastModifiedDate";
    private static final String UNDER_SCORE = "_";
    private static final String VAR_PREFIX_DOLLAR = "$";

    private final MongoEntityInformation<T, ID> metadata;
    private final MongoOperations mongoOperations;
    private final boolean isAuditableEntity;
    private final List<String> fields;
    private final List<String> updatableFieldNames;

    private NucleusConfigProperties nucleusConfigProperties;
    private NucleusValidator nucleusValidator;
    private MongoConfigProperties mongoConfigProperties;

    public NucleusMongoRepositoryImpl(MongoEntityInformation<T, ID> metadata, MongoOperations mongoOperations) {
        super(metadata, mongoOperations);

        this.metadata = metadata;
        this.mongoOperations = mongoOperations;
        this.isAuditableEntity = AuditableEntity.class.isAssignableFrom(getEntityType());

        // Build a list of fields that should be updated via BulkOperations (exclude the ID and audit fields as they are set separately)
        this.fields = ReflectionUtil.getFieldNames(getEntityType());
        this.updatableFieldNames = new ArrayList<>(fields);
        this.updatableFieldNames.remove(metadata.getIdAttribute());
        this.updatableFieldNames.remove(CREATE_DATE);
        this.updatableFieldNames.remove(LAST_MODIFIED_DATE);
    }

    private Duration getTimeout() {
        return Duration.of(getMongoConfigProperties().getSocketTimeout(), ChronoUnit.MILLIS);
    }

    @Override
    public Page<T> find(Criteria criteria, Pageable pageable) {
        Query query = criteria != null ? new Query(criteria) : new Query();
        query.with(pageable);
        if (getMongoConfigProperties().getSocketTimeout() > 0) {
            query.maxTime(getTimeout());
        }

        long totalCount = mongoOperations.count(query, metadata.getJavaType());
        List<T> results = mongoOperations.find(query, metadata.getJavaType());

        return new PageImpl<>(results, pageable, totalCount);
    }

    @Override
    public List<T> find(Criteria criteria, Sort sort) {
        Query query = criteria != null ? new Query(criteria) : new Query();
        if (sort != null) {
            query.with(sort);
        }
        return mongoOperations.find(query, metadata.getJavaType());
    }

    @Override
    public T findOne(String id) {
        return mongoOperations.findById(id, getEntityType());
    }

    @Override
    public FindIterable<Document> find(Criteria criteria) {
        Query query = criteria != null ? new Query(criteria) : new Query();
        MongoCollection<Document> dbCollection = mongoOperations.getCollection(metadata.getCollectionName());
        return dbCollection.find(query.getQueryObject());
    }

    @Override
    public Slice<T> findSlice(Criteria criteria, Pageable pageable) {
        int pageSize = pageable.getPageSize();

        Query query = criteria != null ? new Query(criteria) : new Query();
        query.with(pageable).limit(pageSize + 1);
        if (getMongoConfigProperties().getSocketTimeout() > 0) {
            query.maxTime(getTimeout());
        }

        List<T> results = mongoOperations.find(query, metadata.getJavaType());

        boolean hasNext = results.size() > pageSize;

        return new SliceImpl<>(hasNext ? results.subList(0, pageSize) : results, pageable, hasNext);
    }

    @Override
    public List<T> findLatest(Criteria criteria) {
        // The Criteria defines both the matching fields and the group by fields
        // Since we are going to sort by the lastModifiedDate desc, we want to set all other fields from the first record
        Set<String> criteriaFields = criteria.getCriteriaObject().keySet();
        GroupOperation group = group(criteriaFields.toArray(new String[0]));
        String idField = VAR_PREFIX_DOLLAR + UNDER_SCORE + metadata.getIdAttribute();
        for (String field : fields) {
            if (!field.equals(metadata.getIdAttribute())) {
                group = group.first(field).as(field);
            } else {
                group = group.first(idField).as(metadata.getIdAttribute());
            }
        }

        // Create a projection for the aggregation results to be able to build up hydrated entities
        // Need to remove the _id field as that contains the 'group by' criteria as the id and rename the 'id' field
        // to be '_id' so that it will be mapped by the entities correctly.
        ProjectionOperation projection = project(fields.toArray(new String[0])).andExclude(UNDER_SCORE + metadata.getIdAttribute()).and(VAR_PREFIX_DOLLAR + metadata.getIdAttribute()).as(idField);

        // Build and execute the aggregation - note the lastModifiedDate desc
        List<Document> pipeline = Arrays.asList(
                match(criteria).toDocument(Aggregation.DEFAULT_CONTEXT),
                sort(Sort.Direction.DESC, CREATE_DATE).toDocument(Aggregation.DEFAULT_CONTEXT),
                group.toDocument(Aggregation.DEFAULT_CONTEXT),
                projection.toDocument(Aggregation.DEFAULT_CONTEXT)
        );
        MongoCollection<Document> dbCollection = mongoOperations.getCollection(metadata.getCollectionName());
        AggregateIterable<Document> iter = dbCollection.aggregate(pipeline).allowDiskUse(true).batchSize(100);

        List<T> latestDocuments = new ArrayList<>();
        try (MongoCursor<Document> cursor = iter.iterator()) {
            while (cursor.hasNext()) {
                Document document = cursor.next();
                latestDocuments.add(mongoOperations.getConverter().read(getEntityType(), document));
            }
        }
        return latestDocuments;

    }

    @Override
    public Stream<T> stream(Criteria criteria, Sort sort) {
        CloseableIterator<T> iterator = mongoOperations.stream(Query.query(criteria).with(sort), metadata.getJavaType());
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED), false);
    }

    @Override
    public Stream<T> parallelStream(Criteria criteria, Sort sort) {
        CloseableIterator<T> iterator = mongoOperations.stream(Query.query(criteria).with(sort), metadata.getJavaType());
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED), true);
    }

    @Override
    public Class<T> getEntityType() {
        return metadata.getJavaType();
    }

    @Override
    public String getCollectionName() {
        return metadata.getCollectionName();
    }

    @Override
    public Page<T> aggregate(TypedAggregation aggregationQuery, Class<T> tClass, Pageable pageable) {
        AggregationResults<T> results = mongoOperations.aggregate(aggregationQuery, tClass);
        return new PageImpl<>(results.getMappedResults(), pageable, results.getMappedResults().size());
    }

    @Override
    public BulkOperations bulkOps(BulkOperations.BulkMode bulkMode) {
        return mongoOperations.bulkOps(bulkMode, getEntityType());
    }

    @Override
    public int bulkOpsSave(BulkOperations.BulkMode bulkMode, List<T> entities) {
        return bulkOpsSave(bulkMode, entities, null);
    }

    @Override
    public int bulkOpsSave(BulkOperations.BulkMode bulkMode, List<T> entities, int batchSize) {
        return BatchingIterator.batchedStreamOf(entities.stream(), batchSize)
                .mapToInt(batch -> bulkOpsSave(bulkMode, batch, null))
                .sum();
    }

    @Override
    public int bulkOpsSave(BulkOperations.BulkMode bulkMode, List<T> entities, CarryForwardHandler<T> carryForwardHandler) {
        return bulkOpsSave(bulkMode, entities, carryForwardHandler, 0);
    }

    @Override
    public BulkWriteResult bulkOpsUpsertMulti(List<Pair<Query, Update>> updates) {
        Class<T> entity = getEntityType();
        BulkOperations bulkOperations = mongoOperations.bulkOps(BulkOperations.BulkMode.UNORDERED, entity);
        bulkOperations.upsert(updates);
        return bulkOperations.execute();
    }

    @Override
    public BulkWriteResult bulkOpsUpdateMulti(List<Pair<Query, Update>> updates) {
        Class<T> entity = getEntityType();
        BulkOperations bulkOperations = mongoOperations.bulkOps(BulkOperations.BulkMode.UNORDERED, entity);
        bulkOperations.updateMulti(updates);
        return bulkOperations.execute();
    }


    public Update getUpdate(T entity, String... exclude) {
        // Convert the entity into a Document - done to use the MongoConverter to get the correct object conversions
        Document doc = new Document();
        this.mongoOperations.getConverter().write(entity, doc);

        Set<String> excludes = Sets.newHashSet(exclude);

        // Build an Update object for all the updatableFields
        // Unset any fields that do not have any values
        final Update update = new Update();
        updatableFieldNames.stream().filter(key -> !excludes.contains(key)).forEach(key -> {
            Object value = doc.get(key);
            if (value != null) {
                update.set(key, value);
            } else {
                update.unset(key);
            }
        });

        // If the entity is Auditable, apply the auditInfo
        if (isAuditableEntity) {
            update.currentDate(LAST_MODIFIED_DATE);
        }
        return update;
    }

    private int bulkOpsSave(BulkOperations.BulkMode bulkMode, List<T> entities, CarryForwardHandler<T> carryForwardHandler, int saveAttempts) {
        if (CollectionUtils.isEmpty(entities)) {
            return 0;
        }

        // Validate the entities before doing the bulk operations
        getNucleusValidator().validateAll(entities);

        // Create an auditDate to set on the entities
        Date auditDate = new Date();

        //Build the BulkOperations for the entities
        BulkOperations bulkOperations = bulkOps(bulkMode);
        entities.forEach(entity -> {

            // If the entity is new, do an insert so that the ID field gets set
            if (this.metadata.isNew(entity)) {

                // If the entity is an AuditableEntity, need to make sure we set the createDate and the lastModifiedDate
                if (isAuditableEntity) {
                    ((AuditableEntity) entity).setCreateDate(auditDate);
                    ((AuditableEntity) entity).setLastModifiedDate(auditDate);
                }

                // Pass the entity into the insert of the bulkOperations for future insertion
                bulkOperations.insert(entity);
                return;
            }

            // Update the entity by ID with the Update object values
            bulkOperations.updateOne(query(where(metadata.getIdAttribute()).is(metadata.getId(entity))), getUpdate(entity));
        });

        // Execute the bulkOperation and add the insert and modified counts to return the total saved
        try {
            BulkWriteResult bulkWriteResult = bulkOperations.execute();
            return bulkWriteResult.getInsertedCount() + bulkWriteResult.getModifiedCount();
        } catch (NucleusMongoBulkOperationException boe) {
            if (carryForwardHandler != null) {
                return handleCarryForward(bulkMode, entities, carryForwardHandler, boe, saveAttempts);
            }

            throw boe;
        }
    }

    @Override
    public String getIdAttribute() {
        return metadata.getIdAttribute();
    }

    @Override
    public List<T> findAllAndRemove(Criteria criteria) {
        Class<T> entity = getEntityType();
        Query query = criteria != null ? new Query(criteria) : new Query();
        return mongoOperations.findAllAndRemove(query, entity);
    }

    private int handleCarryForward(BulkOperations.BulkMode bulkMode, List<T> entities, CarryForwardHandler<T> carryForwardHandler, NucleusMongoBulkOperationException boe, int saveAttempts) {
        // Increment the number of save attempts
        saveAttempts++;

        // Only attempt to perform the carry forward save logic as many times as configured
        if (saveAttempts >= getNucleusConfigProperties().getCarryForwardSaveAttempts()) {
            throw new NucleusException("Exceeded the number of carry forward attempts trying to save " + getEntityType().getSimpleName(), boe);
        }

        // Get the indexes of the records with write errors
        List<Integer> writeErrorIndexes = boe.getErrors().stream().map(BulkWriteError::getIndex).collect(Collectors.toList());

        // Filter the entities to be reduced to only include the ones identified by the indexes in the writeErrorIndexes
        List<T> filterEntities = entities.stream().filter(entity -> writeErrorIndexes.contains(entities.indexOf(entity))).collect(Collectors.toList());

        // Call the CarryForwardHandler to perform the carry forward logic
        carryForwardHandler.carryForward(filterEntities);

        // Attempt to re-save after the carry forward logic was performed
        return bulkOpsSave(bulkMode, filterEntities, carryForwardHandler, saveAttempts);
    }

    @Override
    public int bulkOpsUpdateOne(BulkOperations.BulkMode bulkMode, List<T> entities, UpdatePerEntity<T> updatePerEntity) {
        if (CollectionUtils.isEmpty(entities)) {
            return 0;
        }

        BulkOperations bulkOperations = bulkOps(bulkMode);
        entities.forEach(entity -> {
            // Build the Update object for the entity and set the lastModifiedDate if the entityType is an AuditableEntity
            Update update = updatePerEntity.updatePerEntity(entity);
            if (isAuditableEntity) {
                update = update.currentDate(LAST_MODIFIED_DATE);
            }

            // Execute the updateOne
            bulkOperations.updateOne(query(where(metadata.getIdAttribute()).is(metadata.getId(entity))), update);
        });
        return bulkOperations.execute().getModifiedCount();
    }

    @Override
    public long delete(Criteria criteria) {
        Query query = criteria != null ? new Query(criteria) : new Query();
        DeleteResult result = mongoOperations.remove(query, metadata.getJavaType());
        return result.getDeletedCount();
    }

    private NucleusConfigProperties getNucleusConfigProperties() {
        if (nucleusConfigProperties == null) {
            nucleusConfigProperties = ApplicationContextProvider.getBeanOfType(NucleusConfigProperties.class);
        }

        return nucleusConfigProperties;
    }

    private NucleusValidator getNucleusValidator() {
        if (nucleusValidator == null) {
            nucleusValidator = ApplicationContextProvider.getBeanOfType(NucleusValidator.class);
        }
        return nucleusValidator;
    }

    private MongoConfigProperties getMongoConfigProperties() {
        if (mongoConfigProperties == null) {
            mongoConfigProperties = ApplicationContextProvider.getFirstBeanOfType(MongoConfigProperties.class);
        }
        return mongoConfigProperties;
    }
}
