package com.ideas.ngi.nucleus.config.mongo.converter;

import com.github.rutledgepaulv.rqe.conversions.StringToTypeConverter;
import com.github.rutledgepaulv.rqe.conversions.parsers.StringToInstantConverter;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;

public class MongoSearchCriteriaConverter implements StringToTypeConverter {

    private ConversionService conversionService;

    public MongoSearchCriteriaConverter() {
        DefaultConversionService conversions = new DefaultConversionService();
        conversions.addConverter(new StringToInstantConverter());
        conversions.addConverter(new NucleusStringToObjectBestEffortConverter());
        conversions.addConverter(new DateConverter());
        conversionService = conversions;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return conversionService.canConvert(String.class, clazz);
    }

    @Override
    public Object apply(String string, Class<?> aClass) {
        return conversionService.convert(string, aClass);
    }
}
