package com.ideas.ngi.nucleus.config.mongo.migration;

import com.github.mongobee.Mongobee;
import com.github.mongobee.scanner.ChangeLogManifestScanner;
import com.ideas.ngi.nucleus.config.mongo.MongoConfigProperties;
import com.ideas.ngi.nucleus.monitor.gateway.ErrorMessageGateway;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoIterable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.ideas.ngi.nucleus.config.mongo.GlobalMongoConfig.GLOBAL_MONGO_CLIENT;

@Component
public class MigrationRunnerFactory {
    private static final String COLLECTION_NAME = "ChangeLog";
    private final MongoClient mongoClient;
    private final MongoClient globalMongoClient;
    private final MongoConfigProperties mongoConfigProperties;
    private boolean migrationEnabled;
    private String mongoDatabase;
    private final ErrorMessageGateway errorMessageGateway;

    // if multi-tenancy is enabled, mongoClient would be null and mongobee would be initialized via builder.
    @Autowired
    public MigrationRunnerFactory(@Nullable MongoClient mongoClient, @Qualifier(GLOBAL_MONGO_CLIENT) MongoClient globalMongoClient, MongoConfigProperties mongoConfigProperties, ErrorMessageGateway errorMessageGateway) {
        this.mongoClient = mongoClient;
        this.globalMongoClient = globalMongoClient;
        this.mongoConfigProperties = mongoConfigProperties;
        this.mongoDatabase = this.mongoConfigProperties.getNGIDatabaseName();
        this.migrationEnabled = this.mongoConfigProperties.isMigrationsEnabled();
        this.errorMessageGateway = errorMessageGateway;
    }

    public void setMongoDatabase(String mongoDatabase) {
        this.mongoDatabase = mongoDatabase;
    }

    public static String getChangeLogCollection(String module) {
        return module.toLowerCase() + COLLECTION_NAME;
    }

    public List<String> getModules() {
        final List<String> modules = new ArrayList<>();
        MongoIterable<String> collections = mongoClient.getDatabase(mongoDatabase).listCollectionNames();
        for (String collection : collections) {
            if (collection.endsWith(COLLECTION_NAME)) {
                modules.add(collection.replace(COLLECTION_NAME, ""));
            }
        }
        return modules;
    }

    public void setMigrationEnabled(boolean migrationEnabled) {
        this.migrationEnabled = migrationEnabled;
    }

    public MongoRunnerBuilder builder() {
        return new MongoRunnerBuilder();
    }

    public class MongoRunnerBuilder {
        private final List<Class<?>> changeLogs = new ArrayList<>();
        private String moduleName;
        private boolean isForGlobalDB = false;
        private final List<MongoClientURI> clients = new ArrayList<>();

        public MongoRunnerBuilder changeLogs(List<Class<?>> changeLogs) {
            this.changeLogs.addAll(changeLogs);
            return this;
        }

        public MongoRunnerBuilder module(String moduleName) {
            this.moduleName = moduleName;
            return this;
        }

        public MongoRunnerBuilder forGlobalDB() {
            this.isForGlobalDB = true;
            return this;
        }

        public MongoRunnerBuilder clients(List<MongoClientURI> clients) {
            this.clients.addAll(clients);
            return this;
        }

        public MigrationRunner build() {
            if(isForGlobalDB){
                final Mongobee mongobee = getMongobee(globalMongoClient, mongoConfigProperties.getGlobalDatabaseName());
                return new MigrationRunner(mongobee, errorMessageGateway);
            }
            if(clients.isEmpty()){
                Mongobee engine = getMongobee(mongoClient, mongoDatabase);
                return new MigrationRunner(engine, errorMessageGateway);
            }else {
                final List<Mongobee> indexRunners = clients.stream()
                        .map(this::getMongobee)
                        .collect(Collectors.toList());
                return new MigrationRunner(indexRunners, errorMessageGateway);
            }

        }

        private Mongobee getMongobee(MongoClient client, String databaseName) {
            Mongobee engine = new Mongobee(client, getChangeLogCollection(moduleName));
            engine.setDbName(databaseName);
            engine.setEnabled(migrationEnabled);
            engine.setChangeLogScanner(new ChangeLogManifestScanner(changeLogs));
            return engine;
        }

        private Mongobee getMongobee(MongoClientURI clientURI) {
            Mongobee engine = new Mongobee(clientURI, getChangeLogCollection(moduleName));
            engine.setEnabled(migrationEnabled);
            engine.setChangeLogScanner(new ChangeLogManifestScanner(changeLogs));
            return engine;
        }
    }

}
