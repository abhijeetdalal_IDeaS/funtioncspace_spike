package com.ideas.ngi.nucleus.config.quartz.rest;

import com.ideas.ngi.nucleus.config.mongo.GlobalMongoConfig;
import com.ideas.ngi.nucleus.exception.NucleusException;
import com.mongodb.client.result.DeleteResult;
import org.apache.commons.collections.CollectionUtils;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/quartz")
public class QuartzRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuartzRestController.class);
    private static final String KEY_GROUP = "keyGroup";
    static final String QUARTZ_LOCKS = "quartz__locks";
    private static final String NO_LOCK_FOUND = "No lock found";
    private static final String REMOVED_N_LOCK = "Removed %d lock";
    private static final String REMOVED_LOCK_FOR_QUARTZ_JOB_WITH_GROUP = "Removed lock for quartz job with group:{}";
    private Scheduler scheduler;
    private MongoOperations mongoOperations;

    @Autowired

    public QuartzRestController(Scheduler scheduler, @Qualifier(GlobalMongoConfig.GLOBAL_MONGO_TEMPLATE) MongoOperations mongoOperations) {
        this.scheduler = scheduler;
        this.mongoOperations = mongoOperations;
    }

    @PostMapping("/stopAllSchedules")
    @ResponseStatus(HttpStatus.OK)
    public void stopQuartzSchedules() {
        try {
            LOGGER.info("Stopping All Quartz Schedules...");
            if (CollectionUtils.isNotEmpty(scheduler.getTriggerGroupNames())) {
                scheduler.pauseAll();
            }
            LOGGER.info("All Quartz Schedules Stopped");
        } catch (SchedulerException se) {
            throw new NucleusException("Unable to stop all quartz schedules", se);
        }
    }

    @PostMapping("/startAllSchedules")
    @ResponseStatus(HttpStatus.OK)
    public void startAllSchedules() {
        try {
            LOGGER.info("Starting All Quartz Schedules...");
            if (CollectionUtils.isNotEmpty(scheduler.getTriggerGroupNames())) {
                scheduler.resumeAll();
            }
            LOGGER.info("All Quartz Schedules Started");
        } catch (SchedulerException se) {
            throw new NucleusException("Unable to start all quartz schedules", se);
        }
    }

    @DeleteMapping("/removeLock")
    @ResponseStatus(HttpStatus.OK)
    public String removeLock(@RequestParam("group") String group) {
        DeleteResult removeResult = mongoOperations.remove(Query.query(Criteria.where(KEY_GROUP).is(group)), QUARTZ_LOCKS);
        if (removeResult.wasAcknowledged() && removeResult.getDeletedCount() != 0) {
            LOGGER.info(REMOVED_LOCK_FOR_QUARTZ_JOB_WITH_GROUP, group);
            return String.format(REMOVED_N_LOCK, removeResult.getDeletedCount());
        }
        return NO_LOCK_FOUND;
    }
}
