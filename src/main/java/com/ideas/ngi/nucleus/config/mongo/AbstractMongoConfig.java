package com.ideas.ngi.nucleus.config.mongo;

import com.ideas.ngi.nucleus.config.mongo.converter.*;
import com.ideas.ngi.nucleus.util.EncryptionUtil;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCompressor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import java.util.*;

@EnableMongoAuditing
public abstract class AbstractMongoConfig extends AbstractMongoConfiguration {

    MongoConfigProperties mongoConfigProperties;
    EncryptionUtil encryptionUtil;

    @Autowired
    public AbstractMongoConfig(MongoConfigProperties mongoConfigProperties, EncryptionUtil encryptionUtil) {
        this.mongoConfigProperties = mongoConfigProperties;
        this.encryptionUtil = encryptionUtil;
    }

    protected abstract String getUri();

    protected MongoClient createMongoClient(String secureUri, MongoClientOptions.Builder builder){

        MongoClientURI uri = new MongoClientURI(secureUri, builder);
        return new MongoClient(uri);
    }

    protected MongoClientOptions.Builder defaultClientOptions() {
        MongoClientOptions.Builder builder = new MongoClientOptions.Builder();
        builder.socketTimeout(mongoConfigProperties.getSocketTimeout());
        builder.connectionsPerHost(mongoConfigProperties.getConnectionPerHost());
        builder.connectTimeout(mongoConfigProperties.getConnectTimeout());

        if (mongoConfigProperties.isUseZlib()) {
            // Trading increased cpu for reduced data transfer across the network and reduced data storage
            builder.compressorList(Arrays.asList(MongoCompressor.createZlibCompressor()));
        }
        return builder;
    }

    @Override
    protected Collection<String> getMappingBasePackages() {
        return Collections.singletonList("com.ideas.ngi");
    }

    @Bean
    public LocalValidatorFactoryBean localValidatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }

    @Override
    @Bean
    public MongoCustomConversions customConversions() {
        List<Converter<?, ?>> customConverters = new ArrayList<>();
        customConverters.add(new DateConverter());
        customConverters.add(new DurationReadConverter());
        customConverters.add(new DurationWriteConverter());
        customConverters.add(new FileObjectConverter());
        customConverters.add(new MessageReadConverter());
        customConverters.add(new SecretReadConverter(encryptionUtil));
        customConverters.add(new SecretWriteConverter(encryptionUtil));
        customConverters.add(new BigDecimalDecimal128WriteConverter());
        customConverters.add(new Decimal128BigDecimalReadConverter());
        customConverters.add(new XMLGregorianCalendarReadConverter());
        customConverters.add(new XMLGregorianCalendarWriteConverter());
        customConverters.add(new LinkConverter());
        return new MongoCustomConversions(addCustomConverters(customConverters));
    }

    protected List addCustomConverters(List<Converter<?, ?>> converters) {
        return converters;
    }

    public MongoConfigProperties getMongoConfigProperties() {
        return mongoConfigProperties;
    }

    public EncryptionUtil getEncryptionUtil() {
        return encryptionUtil;
    }
}
