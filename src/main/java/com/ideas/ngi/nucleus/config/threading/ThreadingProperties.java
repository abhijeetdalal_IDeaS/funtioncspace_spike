package com.ideas.ngi.nucleus.config.threading;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@RefreshScope
public class ThreadingProperties {
    @Value("${nucleus.threading.corePoolSize:2000}")
    private Integer corePoolSize;
    @Value("${nucleus.threading.maxPoolSize:3500}")
    private Integer maxPoolSize;
    @Value("${nucleus.threading.queueCapacity:1000000}")
    private Integer queueCapacity;
    @Value("${nucleus.threading.keepAliveSeconds:300}")
    private Integer keepAliveSeconds;

    public Integer getCorePoolSize() {
        return corePoolSize;
    }

    public Integer getMaxPoolSize() {
        return maxPoolSize;
    }

    public Integer getQueueCapacity() {
        return queueCapacity;
    }

    public int getKeepAliveSeconds() {
        return keepAliveSeconds;
    }
}
