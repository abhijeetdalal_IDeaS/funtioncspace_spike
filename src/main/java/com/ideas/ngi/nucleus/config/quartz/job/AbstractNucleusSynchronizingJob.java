package com.ideas.ngi.nucleus.config.quartz.job;

import com.ideas.ngi.nucleus.exception.NucleusException;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.PersistJobDataAfterExecution;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.core.MessageSource;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.scheduling.quartz.QuartzJobBean;


@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public abstract class AbstractNucleusSynchronizingJob<T> extends QuartzJobBean {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected abstract MessageSource<T> getMessageSource();

    protected abstract MessageChannel getOutputChannel();

    protected Message<T> onMessage(Message<T> message) {
        return message;
    }

    @Override
    protected void executeInternal(JobExecutionContext context) {
        try {
            Message<T> message = getMessageSource().receive();
            logger.debug("Executed receive: {}", message);
            if (message != null) {
                logger.info("{} for {} executed on instance {}", message.getPayload(), context.getJobDetail().getKey().getName(), context.getScheduler().getSchedulerInstanceId());
                Message<T> handledMessage = onMessage(message);
                boolean sent = getOutputChannel().send(handledMessage);
                if (!sent) {
                    throw new NucleusException("Could not publish message " + message + " on channel " + getOutputChannel());
                }
            }
        } catch (SchedulerException e) {
            logger.error("Error while getting scheduler instanceID" + e);
        }
    }
}
