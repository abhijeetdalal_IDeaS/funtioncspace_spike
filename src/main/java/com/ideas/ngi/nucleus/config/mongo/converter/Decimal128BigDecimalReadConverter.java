package com.ideas.ngi.nucleus.config.mongo.converter;


import org.bson.types.Decimal128;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import java.math.BigDecimal;

@ReadingConverter
public class Decimal128BigDecimalReadConverter implements Converter<Decimal128, BigDecimal> {

    @Override
    public BigDecimal convert(Decimal128 value) {
        return value.bigDecimalValue();
    }

}