package com.ideas.ngi.nucleus.config.mongo.converter;

import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

import javax.xml.datatype.Duration;

@WritingConverter
public class DurationWriteConverter implements Converter<Duration, Document> {

    @Override
    public Document convert(Duration object) {
        Document document = new Document();
        document.put("duration", object.toString());
        return document;
    }
}
