package com.ideas.ngi.nucleus.config.internalclient;

import com.ideas.ngi.nucleus.config.NucleusConfigProperties;
import com.ideas.ngi.nucleus.config.circuitbreaker.CircuitBreaker;
import com.ideas.ngi.nucleus.monitor.entity.NucleusCompletedMessage;
import com.ideas.ngi.nucleus.monitor.entity.NucleusCompletedNotification;
import com.ideas.ngi.nucleus.monitor.repository.NucleusCompletedMessageRepository;
import com.ideas.ngi.nucleus.monitor.transformer.CompletedNotificationTransformer;
import com.ideas.ngi.nucleus.rest.handler.RestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CompletedMonitorMessageHandler extends AbstractMonitorMessageHandler<NucleusCompletedMessage, NucleusCompletedNotification> {
    @Autowired
    public CompletedMonitorMessageHandler(NucleusConfigProperties configProperties,
                                          CircuitBreaker circuitBreaker,
                                          RestHandler restHandler,
                                          CompletedNotificationTransformer transformer,
                                          NucleusCompletedMessageRepository repository) {
        super(configProperties, circuitBreaker, restHandler, transformer, repository);
    }
}
