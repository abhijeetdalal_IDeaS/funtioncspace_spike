package com.ideas.ngi.nucleus.config.mongo.converter;

import org.bson.Document;
import org.springframework.core.convert.converter.Converter;

import java.io.File;

public class FileObjectConverter implements Converter<Document, File> {
    @Override
    public File convert(Document source) {
        final String path = source.getString("path");
        return new File(path);
    }
}
