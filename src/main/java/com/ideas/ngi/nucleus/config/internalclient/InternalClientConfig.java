package com.ideas.ngi.nucleus.config.internalclient;

import com.ideas.ngi.nucleus.config.NucleusConfigProperties;
import com.ideas.ngi.nucleus.config.circuitbreaker.CircuitBreaker;
import com.ideas.ngi.nucleus.config.circuitbreaker.DurableCircuitBreaker;
import com.ideas.ngi.nucleus.config.internalclient.ups.UnifiedPropertyClient;
import com.ideas.ngi.nucleus.config.internalclient.ups.UnifiedPropertyClientDefaultImpl;
import com.ideas.ngi.nucleus.integration.NucleusIntegrationComponent;
import com.ideas.ngi.nucleus.integration.util.MessageHeaderUtil;
import com.ideas.ngi.nucleus.integration.util.MessagePayloadUtil;
import com.ideas.ngi.nucleus.rest.client.RestClient;
import com.ideas.ngi.nucleus.rest.client.RestClientProperties;
import com.ideas.ngi.nucleus.rest.endpoint.RestEndpoint;
import com.ideas.ngi.nucleus.rest.handler.RestHandler;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.context.IntegrationFlowBeanPostProcessor;
import org.springframework.integration.router.RecipientListRouter;
import org.springframework.messaging.MessageChannel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.ideas.ngi.nucleus.config.internalclient.InternalClientUtils.*;
import static java.util.Arrays.asList;


/**
 * Defines all beans to be used when integration with G3.
 */
@Configuration
public class InternalClientConfig implements BeanFactoryPostProcessor, EnvironmentAware {

    private Environment environment = null;

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) {
        // Iterate over all the internal client names
        for (String internalClientName : getInternalClientNames()) {
            // Register the base url for the internal client
            String beanName = internalClientName + RestClient.class.getSimpleName();
            registerRestClients(beanFactory, internalClientName, beanName, BASE_URL);

            // Register the monitor for the internal client
            String monitorBeanName = internalClientName + MONITOR_REST_CLIENT;
            registerRestClients(beanFactory, internalClientName, monitorBeanName, MONITOR_URL);
        }
    }

    private void registerRestClients(ConfigurableListableBeanFactory beanFactory, String internalClientName, String beanName, String urlProperty) {
        // If the URL property isn't set for the restClient - don't create the RestClient
        String url = getValue(internalClientName, urlProperty, environment);
        if (beanFactory.getSingleton(beanName) == null && url != null) {
            RestClient restClient = new RestClient(buildRestClientProperties(beanName, internalClientName, urlProperty));
            restClient.setBeanName(beanName);
            beanFactory.registerSingleton(beanName, restClient);
        }
    }

    /**
     * Wire up default no-op ups client
     *
     * @return bean
     */
    @Bean
    public UnifiedPropertyClient unifiedPropertyClient() {
        return new UnifiedPropertyClientDefaultImpl();
    }

    @Bean(name = "allInternalClientsRestClientRouter")
    public RecipientListRouter allInternalClientsRestClientRouter(ApplicationContext applicationContext,
                                                                  NucleusIntegrationComponent nucleusIntegrationComponent,
                                                                  IntegrationFlowBeanPostProcessor integrationFlowBeanPostProcessor,
                                                                  MessagePayloadUtil messagePayloadUtil,
                                                                  DurableCircuitBreaker circuitBreaker,
                                                                  RestHandler restHandler,
                                                                  NucleusConfigProperties properties) {
        // Get all non-Monitor RestClients
        List<RestClient> restClients = getInternalClientRestClients(applicationContext, false);

        return getBuildNamedInternalClientRoutes(nucleusIntegrationComponent,
                integrationFlowBeanPostProcessor, restClients, "", messagePayloadUtil, circuitBreaker, restHandler, properties);
    }

    @Bean(name = "rateInternalClientsRestClientRouter")
    public RecipientListRouter rateInternalClientsRestClientRouter(ApplicationContext applicationContext,
                                                                   NucleusIntegrationComponent nucleusIntegrationComponent,
                                                                   IntegrationFlowBeanPostProcessor integrationFlowBeanPostProcessor,
                                                                   MessagePayloadUtil messagePayloadUtil,
                                                                   CircuitBreaker circuitBreaker,
                                                                   RestHandler restHandler,
                                                                   NucleusConfigProperties properties) {
        // Get all non-Monitor RestClients
        List<RestClient> restClients = getInternalClientRestClients(applicationContext, false);

        // Build a Router where each endpoint utilizes the flow below
        return getBuildNamedInternalClientRoutes(nucleusIntegrationComponent,
                integrationFlowBeanPostProcessor, restClients, "Rate", messagePayloadUtil, circuitBreaker, restHandler, properties);
    }

    private RecipientListRouter getBuildNamedInternalClientRoutes(NucleusIntegrationComponent nucleusIntegrationComponent,
                                                                  IntegrationFlowBeanPostProcessor integrationFlowBeanPostProcessor,
                                                                  List<RestClient> restClients,
                                                                  String suffix,
                                                                  MessagePayloadUtil messagePayloadUtil,
                                                                  CircuitBreaker circuitBreaker,
                                                                  RestHandler restHandler,
                                                                  NucleusConfigProperties properties) {
        return buildRecipientListRouter(nucleusIntegrationComponent, integrationFlowBeanPostProcessor, restClients, (internalRestClient, messageChannel) ->
                        nucleusIntegrationComponent
                                .from(messageChannel)
                                // only process requests from applicable environments
                                .filter(p -> appliesToEnvironment(internalRestClient, p, messagePayloadUtil))
                                .handle((p, h) -> {
                                    if (properties.isCircuitBreakerEnabled()) {
                                        return circuitBreaker.handle(internalRestClient, (RestEndpoint) h.get(MessageHeaderUtil.REST_ENDPOINT), p, h, messageChannel);
                                    } else {
                                        return restHandler.handle(internalRestClient, (RestEndpoint) h.get(MessageHeaderUtil.REST_ENDPOINT), p);
                                    }
                                })
                                .get()
                , suffix);
    }

    @Bean(name = "completionMonitorsRestClientRouter")
    public RecipientListRouter completionMonitorsRestClientRouter(ApplicationContext applicationContext,
                                                                  NucleusIntegrationComponent nucleusIntegrationComponent,
                                                                  IntegrationFlowBeanPostProcessor integrationFlowBeanPostProcessor,
                                                                  MessagePayloadUtil messagePayloadUtil,
                                                                  CompletedMonitorMessageHandler completedMonitorMessageHandler) {
        // monitor RestClients
        List<RestClient> restClients = getInternalClientRestClients(applicationContext, true);
        // flow for nucleus completion monitors
        return buildRecipientListRouter(nucleusIntegrationComponent, integrationFlowBeanPostProcessor, restClients, (internalRestClient, messageChannel) ->
                        nucleusIntegrationComponent
                                .from(messageChannel)
                                // If the monitorMessage has the clientEnvironment specified - only send the monitor messages to that endpoint
                                .filter(p -> appliesToMonitorEnvironment(internalRestClient, p, messagePayloadUtil))
                                .enrichHeaders(s -> s.header(MessageHeaderUtil.REST_CLIENT, internalRestClient))
                                .enrichHeaders(s -> s.header(MessageHeaderUtil.FROM_CHANNEL, messageChannel))
                                .handle(completedMonitorMessageHandler)
                                .get()
                , "");
    }

    @Bean(name = "errorMonitorsRestClientRouter")
    public RecipientListRouter errorMonitorsRestClientRouter(ApplicationContext applicationContext,
                                                             NucleusIntegrationComponent nucleusIntegrationComponent,
                                                             IntegrationFlowBeanPostProcessor integrationFlowBeanPostProcessor,
                                                             MessagePayloadUtil messagePayloadUtil,
                                                             ErrorMonitorMessageHandler errorMonitorMessageHandler) {
        // monitor RestClients
        List<RestClient> restClients = getInternalClientRestClients(applicationContext, true);
        // flow for nucleus error monitors
        return buildRecipientListRouter(nucleusIntegrationComponent, integrationFlowBeanPostProcessor, restClients, (internalRestClient, messageChannel) ->
                        nucleusIntegrationComponent
                                .from(messageChannel)
                                // If the monitorMessage has the clientEnvironment specified - only send the monitor messages to that endpoint
                                .filter(p -> appliesToMonitorEnvironment(internalRestClient, p, messagePayloadUtil))
                                .enrichHeaders(s -> s.header(MessageHeaderUtil.REST_CLIENT, internalRestClient))
                                .enrichHeaders(s -> s.header(MessageHeaderUtil.FROM_CHANNEL, messageChannel))
                                .handle(errorMonitorMessageHandler)
                                .get()
                , "Error");
    }

    private RecipientListRouter buildRecipientListRouter(NucleusIntegrationComponent nucleusIntegrationComponent,
                                                         IntegrationFlowBeanPostProcessor integrationFlowBeanPostProcessor,
                                                         List<RestClient> internalRestClients,
                                                         InternalClientRouterFlowBuilder internalClientRouterFlowBuilder,
                                                         String suffix) {
        // Create a list of all messageChannels for the list restClient list
        List<MessageChannel> messageChannels = new ArrayList<>();

        // Iterate over the list and create an AMQP channel and Integration flow for each client
        for (RestClient internalRestClient : internalRestClients) {
            // Build the internalRestClient channel name
            String internalClientRestClientChannel = internalRestClient.getBeanName() + "AllClientChannel" + suffix;

            // Create the channel and add it to the list of internalClient message channels
            MessageChannel messageChannel = nucleusIntegrationComponent.amqpChannel(internalClientRestClientChannel);
            messageChannels.add(messageChannel);

            // Build the internalRestClient flow name
            String internalClientRestClientChannelFlowName = internalClientRestClientChannel + "Flow" + suffix;
            IntegrationFlow internalClientRestClientChannelFlow = internalClientRouterFlowBuilder.build(internalRestClient, messageChannel);

            // Register the IntegrationFlow
            integrationFlowBeanPostProcessor.postProcessBeforeInitialization(internalClientRestClientChannelFlow, internalClientRestClientChannelFlowName);
        }

        // Build the list router for all the
        RecipientListRouter recipientListRouter = new RecipientListRouter();
        recipientListRouter.setChannels(messageChannels);
        return recipientListRouter;
    }

    /**
     * Checks for a {@link com.ideas.ngi.nucleus.integration.annotation.ClientEnvironment} annotation on the request payload to determine if the
     * internal client is applicable for the request.  Provides the ability to filter to only certain environments
     */
    private boolean appliesToEnvironment(RestClient internalRestClient, Object payload, MessagePayloadUtil messagePayloadUtil) {
        String clientEnvironment = messagePayloadUtil.getClientEnvironmentName(payload);
        return StringUtils.isEmpty(clientEnvironment) || asList(clientEnvironment.split(","))
                .stream().anyMatch(getInternalClientName(internalRestClient)::equalsIgnoreCase);
    }

    /**
     * Checks for a {@link com.ideas.ngi.nucleus.integration.annotation.ClientEnvironment} annotation on the request payload to determine if the
     * internal client is applicable for the request.  If one isn't set, default it to only send to the first monitor in the list
     */
    private boolean appliesToMonitorEnvironment(RestClient restClient, Object payload, MessagePayloadUtil messagePayloadUtil) {
        // Get the payloads internal client environment
        String clientEnvironment = messagePayloadUtil.getClientEnvironmentName(payload);

        // Get the internalClientName for the rest client
        String internalClientName = getInternalClientName(restClient);

        // If it has one, see if it matches up with the restClient's internal client name
        if (StringUtils.isNotBlank(clientEnvironment)) {
            return StringUtils.equals(clientEnvironment, internalClientName);
        }

        // Otherwise only send the monitor message to the first defined internal client
        return StringUtils.equals(internalClientName, getInternalClientNames()[0]);
    }

    private String getInternalClientName(RestClient restClient) {
        String restClientName = restClient.getName();
        // Determine the internal client name by removing the 'MonitorRestClient' from the name
        if (restClientName.endsWith(MONITOR_REST_CLIENT)) {
            return restClientName.replaceAll(MONITOR_REST_CLIENT, "");
        }

        return restClient.getName().replaceAll(RestClient.class.getSimpleName(), "");
    }

    /**
     * Return a list of all RestClients that are for IDeaS internal applications
     */
    private List<RestClient> getInternalClientRestClients(ApplicationContext applicationContext, boolean isMonitor) {
        return applicationContext.getBeansOfType(RestClient.class).values().stream().filter(restClient -> restClient.isMonitor() == isMonitor && restClient.isInternalClient()).collect(Collectors.toList());
    }

    /**
     * Return a List of all Internal Client Names (g3, g2, g3-prod1, etc) to be used as the prefix
     * to look for the Internal Client settings
     *
     * @return list of internal clients
     */
    private String[] getInternalClientNames() {
        return StringUtils.split(environment.getProperty(INTERNAL_CLIENT_NAMES, DEFAULT_INTERNAL_CLIENTS), ",");
    }

    private RestClientProperties buildRestClientProperties(String restClientName, String internalClientName, String urlProperty) {
        // Get the InternalClient's type, if it's not set we are defaulting it to G3 for the time being
        InternalClientType internalClientType = InternalClientType.valueOf(environment.getProperty(internalClientName + TYPE, InternalClientType.G3.name()).toUpperCase());

        InternalClientProperties restClientProperties = new InternalClientProperties(restClientName, internalClientType, MONITOR_URL.equals(urlProperty));
        restClientProperties.setBaseUrl(getValue(internalClientName, urlProperty, environment));
        restClientProperties.setBasicAuthenticationToken(getValue(internalClientName, AUTHENTICATION_TOKEN, environment));
        restClientProperties.setDefaultPropertyId(getValue(internalClientName, DEFAULT_PROPERTY_ID, environment));
        return restClientProperties;
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public interface InternalClientRouterFlowBuilder {
        IntegrationFlow build(RestClient internalRestClient, MessageChannel messageChannel);
    }
}
