package com.ideas.ngi.nucleus.config.mongo.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

public class Secret implements Serializable {
    private static final long serialVersionUID = 6788505478635427970L;

    private final String value;

    @JsonCreator
    public Secret(@JsonProperty("value") String value) {
        Objects.requireNonNull(value);
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object that) {
        return that instanceof Secret && ((Secret) that).value.equals(this.value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }

    public static Secret valueOf(String value) {
        return value == null || value.length() == 0 ? null : new Secret(value);
    }


    public static String getValue(Secret secret) {
        return secret == null ? null : secret.getValue();
    }
}
