package com.ideas.ngi.nucleus.config.mongo.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

@ReadingConverter
public class DateConverter implements Converter<String, Date> {

    @Override
    public Date convert( String valueIn ){
        if( valueIn != null ) {

            TemporalAccessor temporalAccessor = getDateTimeFormatterWithOptionals().parseBest(valueIn, ZonedDateTime::from, LocalDateTime::from, LocalDate::from);

            if (temporalAccessor instanceof ZonedDateTime) {
                ZonedDateTime zonedDateTime = (ZonedDateTime) temporalAccessor;
                return Date.from(zonedDateTime.toInstant());
            }
            if (temporalAccessor instanceof LocalDateTime) {
                LocalDateTime localDateTime = (LocalDateTime) temporalAccessor;
                return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
            }
            if (temporalAccessor instanceof LocalDate) {
                LocalDate localDate = (LocalDate) temporalAccessor;
                return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
            }
        }
        return null;
    }

    /**
     * Formats supported:
     *  2011-12-15
     *  2012-10-14T08:45:23
     *  2012-10-14 08:45:23
     *  2012-10-14T08:45:23.005
     *  2012-10-14 08:45:23.005
     *  2012-10-14T08:45:23.005-06:00
     *  2012-10-14 08:45:23.005-06:00
     *  2012-10-14T08:45:23.005-0600
     *  2012-10-14 08:45:23.005-0600
     *  2012-10-14T08:45:23.005 CDT
     *  2012-10-14 08:45:23.005 CDT
     *  2012-10-14T08:45:23.005 [America/Chicago]
     *  2012-10-14 08:45:23.005 [America/Chicago]
     *  NOTE: If you supply an offset or timezone then whether or not the date and time is in the daylight saving
     *  time window will affect the Date result you get back in the parse() method above.
     * @return ISO DateTimeFormatter which accepts optional sections
     */
    private DateTimeFormatter getDateTimeFormatterWithOptionals(){
        // The brackets are optional sections
        // z = time zone name
        // XXX = offset with colon
        // X = offset without colon
        return DateTimeFormatter.ofPattern( "yyyy-MM-dd['T'HH:mm:ss][ HH:mm:ss][.SSS][ '['z']'][ z][XXX][X]");
    }
}
