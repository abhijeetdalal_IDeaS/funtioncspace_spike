package com.ideas.ngi.nucleus.config.internalclient;

import com.ideas.ngi.nucleus.rest.client.stateful.BasicAuthenticationRestClientProperties;

public class InternalClientProperties implements BasicAuthenticationRestClientProperties {

    private String name;
    private InternalClientType internalClientType;
    private String baseUrl;
    private String basicAuthenticationToken;
    private String defaultPropertyId;
    private boolean isMonitor;

    public InternalClientProperties(String name, InternalClientType internalClientType, boolean isMonitor) {
        this.name = name;
        this.internalClientType = internalClientType;
        this.isMonitor = isMonitor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InternalClientType getInternalClientType() {
        return internalClientType;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public InternalClientProperties setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    public String getBasicAuthenticationToken() {
        return basicAuthenticationToken;
    }

    public InternalClientProperties setBasicAuthenticationToken(String basicAuthenticationToken) {
        this.basicAuthenticationToken = basicAuthenticationToken;
        return this;
    }

    public String getDefaultPropertyId() {
        return defaultPropertyId;
    }

    public InternalClientProperties setDefaultPropertyId(String defaultPropertyId) {
        this.defaultPropertyId = defaultPropertyId;
        return this;
    }

    public boolean isMonitor() {
        return isMonitor;
    }

    public void setMonitor(boolean monitor) {
        isMonitor = monitor;
    }

    public String getHealthUrl() {
        if (defaultPropertyId != null) {
            return "?propertyId=" + defaultPropertyId;
        }
        return "";
    }

}
