package com.ideas.ngi.nucleus.config.mongo.service;

import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.visitors.MongoVisitor;
import com.github.rutledgepaulv.rqe.pipes.QueryConversionPipeline;
import com.ideas.ngi.nucleus.config.mongo.controller.QueryConversionPipelineIdeas;
import com.ideas.ngi.nucleus.config.mongo.repository.NucleusMongoRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

@Component
public class RsqlCriteriaBuilder {

    private QueryConversionPipeline queryConversionPipeline;

    public RsqlCriteriaBuilder() {
        this.queryConversionPipeline = QueryConversionPipelineIdeas.getInstance();
    }

    public Criteria buildCriteria(NucleusMongoRepository nucleusMongoRepository, String search) {
        // If there was a search String, need to build a Mongo Criteria object for it based on the Repository
        if (StringUtils.isNotEmpty(search)) {
            // Parse the RSQL into a Condition object
            Condition<?> condition = queryConversionPipeline.apply(search, nucleusMongoRepository.getEntityType());

            // Build the Mongo Criteria based on the query
            return condition.query(new MongoVisitor());
        }

        return null;
    }
}
