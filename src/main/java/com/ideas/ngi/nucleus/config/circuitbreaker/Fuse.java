package com.ideas.ngi.nucleus.config.circuitbreaker;

import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Fuse {
    private static final Logger LOGGER = LoggerFactory.getLogger(Fuse.class);

    public static final String CLOSED = "CLOSED";
    public static final String HALF_OPEN = "HALF_OPEN";
    public static final String OPEN = "OPEN";

    private String state;
    private String queueName;
    private int failureThreshold;
    private int failureCount;
    private Seconds retryTimePeriod;
    private DateTime lastFailureTime;
    private DateTime nextRetryTime;

    public Fuse(String queueName) {
        state = CLOSED;
        failureThreshold = 25;
        failureCount = 0;
        retryTimePeriod = Seconds.seconds(300);
        lastFailureTime = DateTime.now().minus(retryTimePeriod);
        this.queueName = queueName;
        nextRetryTime = lastFailureTime;
    }

    public void updateState() {
        if (failureCount > failureThreshold) {
            if ((DateTime.now().getMillis() - lastFailureTime.getMillis()) > retryTimePeriod.getSeconds() * 1000) {
                state = HALF_OPEN;
            } else {
                if (!state.equalsIgnoreCase(OPEN)) {
                    nextRetryTime = lastFailureTime.plusSeconds(retryTimePeriod.getSeconds());
                }
                state = OPEN;
            }
        } else {
            state = CLOSED;
        }
    }

    public void reset() {
        failureCount = 0;
        lastFailureTime = DateTime.now().minus(retryTimePeriod);
        state = CLOSED;
    }

    protected void recordFailure() {
        failureCount += 1;
        LOGGER.info("Channel " + queueName + " has " + failureCount + " failures");
        lastFailureTime = DateTime.now();
        updateState();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public int getFailureThreshold() {
        return failureThreshold;
    }

    public void setFailureThreshold(int failureThreshold) {
        this.failureThreshold = failureThreshold;
    }

    public int getFailureCount() {
        return failureCount;
    }

    public void setFailureCount(int failureCount) {
        this.failureCount = failureCount;
    }

    public Seconds getRetryTimePeriod() {
        return retryTimePeriod;
    }

    public void setRetryTimePeriod(Seconds retryTimePeriod) {
        this.retryTimePeriod = retryTimePeriod;
    }

    public DateTime getLastFailureTime() {
        return lastFailureTime;
    }

    public void setLastFailureTime(DateTime lastFailureTime) {
        this.lastFailureTime = lastFailureTime;
    }

    public DateTime getNextRetryTime() {
        return nextRetryTime;
    }

    public void setNextRetryTime(DateTime nextRetryTime) {
        this.nextRetryTime = nextRetryTime;
    }
}
