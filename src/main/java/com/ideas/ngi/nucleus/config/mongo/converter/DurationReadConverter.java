package com.ideas.ngi.nucleus.config.mongo.converter;

import com.ideas.ngi.nucleus.exception.NucleusException;
import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;

@ReadingConverter
public class DurationReadConverter implements Converter<Document, Duration> {
    private DatatypeFactory datatypeFactory;

    public DurationReadConverter() {
        try {
            datatypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException dce) {
            throw new NucleusException("Could not configure converter", dce);
        }
    }

    @Override
    public Duration convert(Document object) {
        return datatypeFactory.newDuration(object.get("duration").toString());
    }
}
