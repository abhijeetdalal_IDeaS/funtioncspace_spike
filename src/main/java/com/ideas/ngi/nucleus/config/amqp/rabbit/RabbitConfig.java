package com.ideas.ngi.nucleus.config.amqp.rabbit;

import com.ideas.ngi.nucleus.exception.NucleusException;
import com.rabbitmq.http.client.Client;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.ConditionalRejectingErrorHandler;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.MessagePublishingErrorHandler;

import java.net.MalformedURLException;
import java.net.URISyntaxException;

/**
 * Defines beans and configuration for using Rabbit.
 */
@Configuration
public class RabbitConfig {

    @Autowired
    private RabbitConfigProperties rabbitConfigProperties;

    @Bean
    public ConnectionFactory rabbitConnectionFactory() {
        com.rabbitmq.client.ConnectionFactory rabbitConnectionFactory = new com.rabbitmq.client.ConnectionFactory();
        rabbitConnectionFactory.setRequestedChannelMax(rabbitConfigProperties.getRequestedChannelMax());
        rabbitConnectionFactory.setAutomaticRecoveryEnabled(false);

        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(rabbitConnectionFactory);
        connectionFactory.setHost(rabbitConfigProperties.getHost());
        connectionFactory.setPort(rabbitConnectionFactory.getPort());
        connectionFactory.setUsername(rabbitConfigProperties.getRabbitUser());
        connectionFactory.setPassword(rabbitConfigProperties.getRabbitPassword());
        connectionFactory.setAddresses(rabbitConfigProperties.getAddresses());
        return connectionFactory;
    }

    @Bean
    public AmqpAdmin amqpAdmin(ConnectionFactory rabbitConnectionFactory) {
        return new RabbitAdmin(rabbitConnectionFactory);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory rabbitConnectionFactory) {
        return new RabbitTemplate(rabbitConnectionFactory);
    }

    @Bean
    public MessagePublishingErrorHandler messagePublishingErrorHandler(BeanFactory beanFactory) {
        MessagePublishingErrorHandler messagePublishingErrorHandler = new MessagePublishingErrorHandler();
        messagePublishingErrorHandler.setBeanFactory(beanFactory);
        return messagePublishingErrorHandler;
    }

    @Bean
    public ConditionalRejectingErrorHandler errorHandler() {
        return new ConditionalRejectingErrorHandler();
    }

    @Bean
    public Client rabbitManagementClient() {
        try {
            return new Client(rabbitConfigProperties.getRabbitManagementUrl(), rabbitConfigProperties.getRabbitUser(), rabbitConfigProperties.getRabbitPassword());
        } catch (MalformedURLException | URISyntaxException e) {
            throw new NucleusException("Failed to create RabbitMQ admin client", e);
        }

    }
}
