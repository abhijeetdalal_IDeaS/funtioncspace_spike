package com.ideas.ngi.nucleus.config.mongo.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import javax.xml.datatype.XMLGregorianCalendar;

@ReadingConverter
public class XMLGregorianCalendarWriteConverter implements Converter<XMLGregorianCalendar, String> {
    @Override
    public String convert(XMLGregorianCalendar source) {
        return source.toXMLFormat();
    }
}
