package com.ideas.ngi.nucleus.config.amqp;

import com.ideas.ngi.nucleus.config.amqp.rabbit.RabbitConfigProperties;
import com.ideas.ngi.nucleus.exception.NucleusRetryableException;
import com.ideas.ngi.nucleus.monitor.entity.NucleusErrorMessage;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.retry.MessageRecoverer;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.TransientDataAccessException;
import org.springframework.integration.channel.MessagePublishingErrorHandler;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class NucleusMessageRecoverer implements MessageRecoverer {

    protected static final Log LOGGER = LogFactory.getLog(NucleusMessageRecoverer.class);

    static final String MESSAGE_RECOVERER_REQUEUE_COUNT = "MESSAGE_RECOVERER_REQUEUE_COUNT";

    private final RabbitConfigProperties rabbitConfigProperties;
    private final MessagePublishingErrorHandler messagePublishingErrorHandler;
    private final ApplicationContext applicationContext;
    private final SimpleMessageConverter simpleMessageConverter = new SimpleMessageConverter();

    @Autowired
    public NucleusMessageRecoverer(MessagePublishingErrorHandler messagePublishingErrorHandler,
                                   RabbitConfigProperties rabbitConfigProperties,
                                   ApplicationContext applicationContext) {
        this.messagePublishingErrorHandler = messagePublishingErrorHandler;
        this.rabbitConfigProperties = rabbitConfigProperties;
        this.applicationContext = applicationContext;
    }

    @Override
    public void recover(org.springframework.amqp.core.Message amqpMessage, Throwable cause) {
        // Convert the AMQP message body and if it's already a GenericMessage, if it is use it's headers/payload
        // If not the Object itself is the payload and use the headers from the amqpMessage that was passed in
        Map<String,Object> headers = new HashMap<>();
        Object payload;
        Object obj = simpleMessageConverter.fromMessage(amqpMessage);
        if (obj instanceof Message<?>) {
            headers.putAll(((Message<?>) obj).getHeaders());
            payload = ((Message<?>) obj).getPayload();
        } else {
            headers.putAll(amqpMessage.getMessageProperties().getHeaders());
            payload = obj;
        }

        if (payload instanceof NucleusErrorMessage) {
            LOGGER.error("Unable to send: " + payload + " to Monitor");
        } else if (isRetryableException(cause) && isWithinRetryRequeueLimit(headers)) {
            // Since we know we are within the requeue limit, we need to increment the requeue counter
            int requeueCount = incrementMessageRecovererRequeueCount(headers);

            // Build a message for the headers/payload
            Message<?> message = MessageBuilder.withPayload(payload).copyHeaders(headers).build();

            // Queue message was received from
            String queue = amqpMessage.getMessageProperties().getConsumerQueue();

            // Log the the message is being requeued
            LOGGER.info("Requeuing Message: " + message.getPayload() + " onto Queue: " + queue + " for the " + requeueCount + " time...");

            // Requeue the message
            applicationContext.getBean(queue, MessageChannel.class).send(message);
        } else {
            // Determine the root cause - if it is a StackOverflow error, we know it isn't recoverable
            Throwable rootCause = ExceptionUtils.getRootCause(cause);
            if(rootCause instanceof StackOverflowError){
                throw new AmqpRejectAndDontRequeueException("This is not recoverable and while we may lose some data it is better than bringing down boot : ", cause);
            }

            // Since the exception isn't retryable (or never was to begin with), we should finally log it
            LOGGER.error(cause.getMessage(), rootCause);

            // Remove the requeue count from the headers - if it matters, you can see the requeue attempts in the message history
            headers.remove(MESSAGE_RECOVERER_REQUEUE_COUNT);

            // Build a message for the headers/payload
            Message<?> message = MessageBuilder.withPayload(payload).copyHeaders(headers).build();

            // Create a MessagingException to wrap both the Message and the rootCause
            MessagingException messagingException = new MessagingException(message, rootCause);
            messagePublishingErrorHandler.handleError(messagingException);
        }
    }

    private boolean isWithinRetryRequeueLimit(Map<String, Object> headers) {
        Integer count = (Integer) headers.get(MESSAGE_RECOVERER_REQUEUE_COUNT);
        return count == null || count < rabbitConfigProperties.getRetryRequeueNumberOfRetries();
    }

    private int incrementMessageRecovererRequeueCount(Map<String,Object> headers) {
        Integer count = (Integer) headers.get(MESSAGE_RECOVERER_REQUEUE_COUNT);
        count = count == null ? 1 : count + 1;
        headers.put(MESSAGE_RECOVERER_REQUEUE_COUNT, count);
        return count;
    }

    private boolean isRetryableException(Throwable cause) {
        if (rabbitConfigProperties.isEnhancedRetryEnabled()) {
            return ExceptionUtils.indexOfType(cause, NucleusRetryableException.class) > -1
                    || ExceptionUtils.indexOfType(cause, TransientDataAccessException.class) > -1;
        } else {
            return ExceptionUtils.indexOfType(cause, NucleusRetryableException.class) > -1
                    || ExceptionUtils.indexOfType(cause, DataAccessResourceFailureException.class) > -1;
        }
    }
}
