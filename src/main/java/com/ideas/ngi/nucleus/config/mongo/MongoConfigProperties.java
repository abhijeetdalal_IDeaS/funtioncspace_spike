package com.ideas.ngi.nucleus.config.mongo;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Contains all System properties related to MongoDB.
 */
@Component
@RefreshScope
public class MongoConfigProperties {

    private static Pattern CREDS_PATTERN = Pattern.compile("\\S+:\\S+@\\S+");

    // Standard connection string:       mongodb://[user:pass@]hostname:port[/authdb][?key=value[&key=value...]]
    // DNS seed list connection string:  mongodb+srv://[user:pass@]dnshostname[/authdb][?key=value[&key=value...]]
    // Please note:
    //   Using the +srv protocol requires SRV DNS records to be configured, which contain the port number(s)
    //   The default authdb name is admin
    @Value("${spring.data.mongodb.uri:mongodb://127.0.0.1:27017}")
    private String mongoUri;

    @Value("${spring.data.mongodb.global.uri:}")
    private String globalMongoUri;

    @Value("${ngi.database.name:ngi}")
    private String ngiDatabaseName;

    @Value("${ngi.database.global.name:}")
    private String globalDatabaseName;

    @Value("${ngi.mongo.username:#{null}}")
    private String ngiMongoUsername;

    @Value("${ngi.mongo.password:#{null}}")
    private String ngiMongoPassword;

    @Value("${ngi.mongo.zlib.enabled:true}")
    private boolean useZlib;

    @Value("${ngi.database.migration.enabled:true}")
    private boolean migrationsEnabled;

    @Value("${ngi.database.export.temp.dir:#{nucleusConfigProperties.dataDirectory}/mongo/export/temp}")
    private File exportTempDirectory;

    @Value("${ngi.database.export.output.dir:#{nucleusConfigProperties.dataDirectory}/mongo/export/output}")
    private File exportOutputDirectory;

    @Value("${ngi.database.command.output.dir:#{nucleusConfigProperties.dataDirectory}/mongo/command/output}")
    private File commandOutputDirectory;

    @Value("${ngi.database.bulk.enabled:true}")
    private boolean bulkImportEnabled;

    @Value("${ngi.database.bulk.batchSize:1000}")
    private int bulkImportBatchSize;

    // default of 30 seconds in all environments
    // 30 minutes in production, set arbitrarily high to accommodate long-running queries
    // should probably set default much lower in test / dev to catch bad queries before prod
    @Value("${mongo.connection.socket.timeout:30000}")
    private Integer socketTimeout;

    @Value("${mongo.connection.per.host:1000}")
    private Integer connectionPerHost;

    @Value("${mongo.connection.connect.timeout:30000}")
    private Integer connectTimeout;

    @Value("${mongo.gridfs.bucket.name:fs}")
    private String gridFsBucketName;

    public String getSecureUri(){
        return getSecureUri(getMongoUri());
    }

    public String getSecureUri(String mongoUri) {
        if (!hasCredentials()) {
            return mongoUri;
        }
        int protocolEnd = this.mongoUri.indexOf("://") + 3;
        if (protocolEnd < 0) {
            return mongoUri;
        }
        String protocol = mongoUri.substring(0, protocolEnd);
        String hostAndOptions = mongoUri.substring(protocolEnd);
        Matcher matcher = CREDS_PATTERN.matcher(hostAndOptions);
        if (matcher.matches()) {
            // uri-embedded credentials supercede separate creds properties (for now)
            return mongoUri;
        }
        StringBuilder sb = new StringBuilder(protocol);
        sb.append(this.ngiMongoUsername).append(':').append(this.ngiMongoPassword).append('@').append(hostAndOptions);
        return sb.toString();
    }

    public boolean hasCredentials() {
        return StringUtils.isNotBlank(this.ngiMongoUsername) && StringUtils.isNotBlank(this.ngiMongoPassword);
    }

    public String getMongoUri() {
        return mongoUri;
    }

    public void setMongoUri(String mongoUri) {
        this.mongoUri = mongoUri;
    }

    public String getNGIDatabaseName() {
        return ngiDatabaseName;
    }

    public void setNGIDatabaseName(String ngiDatabaseName) {
        this.ngiDatabaseName = ngiDatabaseName;
    }

    public String getNgiMongoUsername() {
        return ngiMongoUsername;
    }

    public void setNgiMongoUsername(String ngiMongoUsername) {
        this.ngiMongoUsername = ngiMongoUsername;
    }

    public String getNgiMongoPassword() {
        return ngiMongoPassword;
    }

    public void setNgiMongoPassword(String ngiMongoPassword) {
        this.ngiMongoPassword = ngiMongoPassword;
    }

    public boolean isMigrationsEnabled() {
        return migrationsEnabled;
    }

    public File getExportTempDirectory() {
        return exportTempDirectory;
    }

    public void setExportTempDirectory(File exportTempDirectory) {
        this.exportTempDirectory = exportTempDirectory;
    }

    public File getExportOutputDirectory() {
        return exportOutputDirectory;
    }

    public void setExportOutputDirectory(File exportOutputDirectory) {
        this.exportOutputDirectory = exportOutputDirectory;
    }

    public File getCommandOutputDirectory() {
        return commandOutputDirectory;
    }

    public void setCommandOutputDirectory(File commandOutputDirectory) {
        this.commandOutputDirectory = commandOutputDirectory;
    }

    public Integer getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(Integer socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    public Integer getConnectionPerHost() {
        return connectionPerHost;
    }

    public void setConnectionPerHost(Integer connectionPerHost) {
        this.connectionPerHost = connectionPerHost;
    }

    @PostConstruct
    public void init() throws IOException {
        FileUtils.forceMkdir(exportTempDirectory);
        FileUtils.forceMkdir(exportOutputDirectory);
        FileUtils.forceMkdir(commandOutputDirectory);
    }

    public boolean isBulkImportEnabled() {
        return bulkImportEnabled;
    }

    public int getBulkImportBatchSize() {
        return bulkImportBatchSize;
    }

    public void setBulkImportEnabled(boolean bulkImportEnabled) {
        this.bulkImportEnabled = bulkImportEnabled;
    }

    public void setBulkImportBatchSize(int bulkImportBatchSize) {
        this.bulkImportBatchSize = bulkImportBatchSize;
    }

    public Integer getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(Integer connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public String getGridFsBucketName() {
        return gridFsBucketName;
    }

    public void setGridFsBucketName(String gridFsBucketName) {
        this.gridFsBucketName = gridFsBucketName;
    }

    public String getFilesCollectionName() {
        return gridFsBucketName + ".files";
    }

    public String getChunksCollection() {
        return gridFsBucketName + ".chunks";
    }

    public boolean isUseZlib() {
        return useZlib;
    }

    public MongoConfigProperties setUseZlib(boolean useZlib) {
        this.useZlib = useZlib;
        return this;
    }

    public String getGlobalMongoUri() {
        return StringUtils.isEmpty(globalMongoUri) ? mongoUri : globalMongoUri;
    }

    public String getGlobalDatabaseName() {
        return StringUtils.isEmpty(globalDatabaseName) ? ngiDatabaseName : globalDatabaseName;
    }

    public void setGlobalMongoUri(String globalMongoUri) {
        this.globalMongoUri = globalMongoUri;
    }

    public void setGlobalDatabaseName(String globalDatabaseName) {
        this.globalDatabaseName = globalDatabaseName;
    }
}
