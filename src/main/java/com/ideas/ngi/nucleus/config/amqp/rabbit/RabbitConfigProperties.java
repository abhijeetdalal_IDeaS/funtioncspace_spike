package com.ideas.ngi.nucleus.config.amqp.rabbit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * Contains all System properties related to RabbitConfigProperties.
 */
@Component
@RefreshScope
public class RabbitConfigProperties {
    private static final String MANAGEMENT_URL = "http://%s:%d/api/";

    // Rabbit Host Configuration
    @Value("${spring.rabbitmq.host:localhost}")
    private String host;
    @Value("${spring.rabbitmq.port:5672}")
    private int port;
    @Value("${spring.rabbitmq.adminport:15672}")
    private int adminPort;
    @Value("${rabbit.virtualHost:/}")
    private String virtualHost;
    @Value("${spring.rabbitmq.user:guest}")
    private String rabbitUser;
    @Value("${spring.rabbitmq.password:IDeaS@123}")
    private String rabbitPassword;

    // Default Number of Queue Consumers
    @Value("${rabbit.concurrent.queue.consumers:5}")
    private Integer concurrentQueueConsumers;
    @Value("${rabbit.connection.requestedChannelMax:0}")
    private Integer requestedChannelMax;

    // Retry Backoff Configuration
    @Value("${retry.backoff.initial.interval:30000}")
    private Long retryBackoffInitialInterval;
    @Value("${retry.backoff.max.interval:30000}")
    private Long retryBackupMaxInterval;
    @Value("${retry.backoff.number.retries:2}")
    private Integer retryBackoffNumberOfRetries;
    @Value("${retry.backoff.multiplier:1}")
    private Integer retryBackoffMultiplier;

    // Retryable Exception Requeue Configuration
    @Value("${retry.requeue.number.retries:60}")
    private int retryRequeueNumberOfRetries;

    @Value("${retry.enhancedRetryEnabled:false}")
    private boolean enhancedRetryEnabled;

    @Value("${spring.rabbit.prefetch:1}")
    private int prefetch;

    @Value("${spring.rabbit.addresses:}")
    private String addresses;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getRabbitUser() {
        return rabbitUser;
    }

    public void setRabbitUser(String rabbitUser) {
        this.rabbitUser = rabbitUser;
    }

    public String getRabbitPassword() {
        return rabbitPassword;
    }

    public void setRabbitPassword(String rabbitPassword) {
        this.rabbitPassword = rabbitPassword;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getAdminPort() {
        return adminPort;
    }

    public void setAdminPort(int adminPort) {
        this.adminPort = adminPort;
    }

    public String getVirtualHost() {
        return virtualHost;
    }

    public void setVirtualHost(String virtualHost) {
        this.virtualHost = virtualHost;
    }

    public Integer getConcurrentQueueConsumers() {
        return concurrentQueueConsumers;
    }

    public void setConcurrentQueueConsumers(Integer concurrentQueueConsumers) {
        this.concurrentQueueConsumers = concurrentQueueConsumers;
    }

    public Long getRetryBackoffInitialInterval() {
        return retryBackoffInitialInterval;
    }

    public void setRetryBackoffInitialInterval(Long retryBackoffInitialInterval) {
        this.retryBackoffInitialInterval = retryBackoffInitialInterval;
    }

    public Long getRetryBackupMaxInterval() {
        return retryBackupMaxInterval;
    }

    public void setRetryBackupMaxInterval(Long retryBackupMaxInterval) {
        this.retryBackupMaxInterval = retryBackupMaxInterval;
    }

    public Integer getRetryBackoffNumberOfRetries() {
        return retryBackoffNumberOfRetries;
    }

    public void setRetryBackoffNumberOfRetries(Integer retryBackoffNumberOfRetries) {
        this.retryBackoffNumberOfRetries = retryBackoffNumberOfRetries;
    }

    public Integer getRetryBackoffMultiplier() {
        return retryBackoffMultiplier;
    }

    public void setRetryBackoffMultiplier(Integer retryBackoffMultiplier) {
        this.retryBackoffMultiplier = retryBackoffMultiplier;
    }

    public int getRetryRequeueNumberOfRetries() {
        return retryRequeueNumberOfRetries;
    }

    public void setRetryRequeueNumberOfRetries(int retryRequeueNumberOfRetries) {
        this.retryRequeueNumberOfRetries = retryRequeueNumberOfRetries;
    }

    public String getRabbitManagementUrl() {
        return String.format(MANAGEMENT_URL, getHost(), getAdminPort());
    }

    public boolean isEnhancedRetryEnabled() {
        return enhancedRetryEnabled;
    }

    public void setEnhancedRetryEnabled(boolean enhancedRetryEnabled) {
        this.enhancedRetryEnabled = enhancedRetryEnabled;
    }

    public Integer getRequestedChannelMax() {
        return requestedChannelMax;
    }

    public int getPrefetch() {
        return prefetch;
    }

    public RabbitConfigProperties setPrefetch(int prefetch) {
        this.prefetch = prefetch;
        return this;
    }

    public String getAddresses() {
        return addresses;
    }

    public RabbitConfigProperties setAddresses(String addresses) {
        this.addresses = addresses;
        return this;
    }
}
