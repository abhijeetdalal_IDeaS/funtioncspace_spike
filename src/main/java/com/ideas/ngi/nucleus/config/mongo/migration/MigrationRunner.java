package com.ideas.ngi.nucleus.config.mongo.migration;

import com.github.mongobee.Mongobee;
import com.github.mongobee.exception.MongobeeException;
import com.ideas.ngi.nucleus.exception.NucleusException;
import com.ideas.ngi.nucleus.monitor.entity.NucleusErrorMessage;
import com.ideas.ngi.nucleus.monitor.gateway.ErrorMessageGateway;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;

public class MigrationRunner implements ApplicationListener<ApplicationReadyEvent> {
    public static final String MIGRATION_JOB_NAME = "NGIMigration";
    private final List<Mongobee> engines;
    private final ErrorMessageGateway errorMessageGateway;

    public MigrationRunner(final Mongobee engine, final ErrorMessageGateway errorMessageGateway) {
        this(Collections.singletonList(engine), errorMessageGateway);
    }

    public MigrationRunner(List<Mongobee> engines, ErrorMessageGateway errorMessageGateway) {
        Assert.notNull(engines, "Mongobee instances are not initialized.");
        Assert.notEmpty(engines, "Mongobee instances are not initialized properly.");
        Assert.noNullElements(engines.toArray(), "Mongobee instances are not initialized properly. Some instances are null.");
        Assert.notNull(errorMessageGateway, "ErrorMessageGateway is not initialized.");

        this.engines = engines;
        this.errorMessageGateway = errorMessageGateway;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        for (Mongobee engine : engines) {
            try {
                engine.execute();
            } catch (MongobeeException e) {
                NucleusErrorMessage errorMessage = new NucleusErrorMessage();
                errorMessage.setMessage(e.getMessage());
                errorMessage.setPayload(engine.configMap());
                errorMessage.setJobName(MIGRATION_JOB_NAME);
                errorMessageGateway.monitorErrorMessage(errorMessage);
                throw new NucleusException("Could not execute migration." , e);
            }
        }
    }
}
