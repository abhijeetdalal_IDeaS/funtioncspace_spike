package com.ideas.ngi.nucleus.config.mongo.exception;

import com.mongodb.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.TransientDataAccessResourceException;
import org.springframework.data.mongodb.BulkOperationException;
import org.springframework.data.mongodb.UncategorizedMongoDbException;
import org.springframework.data.mongodb.core.MongoExceptionTranslator;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;


public class NucleusMongoExceptionTranslator extends MongoExceptionTranslator {
    // Tried to identify as many possible "transient" error codes
    // from https://github.com/mongodb/mongo/blob/master/src/mongo/base/error_codes.err
    // This list may need to change as mongo upgrades
    static final Set<Integer> TRANSIENT_ERROR_CODES = new HashSet<>(
            Arrays.asList(
                    133,    // FailedToSatisfyReadPreference
                    189,    // PrimarySteppedDown
                    190,    // MasterSlaveConnectionFailure
                    216,    // ElectionInProgress
                    11602,  // InterruptedDueToStepDown
                    10107   // NotMaster
            ));
    private static final Logger LOGGER = LoggerFactory.getLogger(NucleusMongoExceptionTranslator.class);

    @Override
    public DataAccessException translateExceptionIfPossible(RuntimeException ex) {
        // override handling of the DuplicateKeyException with a fully serializable exception type.
        if (ex instanceof MongoServerException && isDuplicateKeyException((MongoServerException) ex)) {
            return new org.springframework.dao.DuplicateKeyException(ex.getMessage(), NucleusMongoDuplicateKeyException.create((MongoServerException) ex));
        }


        /*
         * Earlier BulkWriteException used to get converted to BulkOperationException when called "super.translateExceptionIfPossible(ex)"
         * Now, it is getting converted to org.springframework.dao.DuplicateKeyException.
         * The app-code relies on BulkOperationException to perform error handling, so needed to handle it separately
         */
        DataAccessException exception;
        if (ex instanceof MongoBulkWriteException) {
            exception = translateBulkWriteToBulkOpsException((MongoBulkWriteException)ex);
        }else{
            try {
                exception = super.translateExceptionIfPossible(ex);
            } catch (DataAccessException e) {
                exception = e;
            }
        }

        if (exception instanceof DataAccessResourceFailureException) {
            exception = new TransientDataAccessResourceException(exception.getMessage(), exception);
        } else if (exception instanceof UncategorizedMongoDbException) {
            exception = translateTransientExceptionIfPossible((UncategorizedMongoDbException) exception);
        } else if (exception instanceof BulkOperationException) {
            exception = translateTransientBulkExceptionIfPossible((BulkOperationException) exception);
        } else if (exception instanceof NucleusMongoBulkOperationException) {
            exception = translateTransientBulkExceptionIfPossible((NucleusMongoBulkOperationException) exception);
        }

        return exception;
    }

    private DataAccessException translateTransientBulkExceptionIfPossible(NucleusMongoBulkOperationException exception) {
        if (exception.getErrors() != null) {
            Optional<com.mongodb.bulk.BulkWriteError> maybeTransientError = exception.getErrors()
                    .stream()
                    .filter(e -> TRANSIENT_ERROR_CODES.contains(e.getCode()))
                    .findFirst();

            if (maybeTransientError.isPresent()) {
                return new TransientDataAccessResourceException(exception.getMessage(), exception);
            }
        }
        return exception;
    }

    private DataAccessException translateBulkWriteToBulkOpsException(MongoBulkWriteException exception) {
        /*
         * Need a altogether new exception, since spring-data framework doesn't allow to instantiate BulkWriteException
         * And without that, we cannot instantiate BulkOperationException, so adding a new exception-type NucleusMongoBulkOperationException
         */
        return new NucleusMongoBulkOperationException(exception.getMessage(), exception);
    }

    private boolean isDuplicateKeyException(MongoServerException ex) {
        return ErrorCategory.fromErrorCode(ex.getCode()) == ErrorCategory.DUPLICATE_KEY;
    }

    private DataAccessException translateTransientBulkExceptionIfPossible(BulkOperationException exception) {
        if (exception.getErrors() != null) {
            Optional<BulkWriteError> maybeTransientError = exception.getErrors()
                    .stream()
                    .filter(e -> TRANSIENT_ERROR_CODES.contains(e.getCode()))
                    .findFirst();

            if (maybeTransientError.isPresent()) {
                return new TransientDataAccessResourceException(exception.getMessage(), exception);
            }
        }
        return exception;
    }

    private DataAccessException translateTransientExceptionIfPossible(UncategorizedMongoDbException exception) {
        Throwable cause = exception.getCause();
        if (cause instanceof MongoException) {
            MongoException mongoException = (MongoException) cause;
            if (TRANSIENT_ERROR_CODES.contains(mongoException.getCode())) {
                return new TransientDataAccessResourceException(exception.getMessage(), exception);
            }

            LOGGER.warn("Could not translate un-categorized mongo exception with code {}", mongoException.getCode());
        }

        return exception;
    }
}
