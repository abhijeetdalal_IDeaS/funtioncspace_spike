package com.ideas.ngi.nucleus.config.mongo;

import com.ideas.ngi.nucleus.config.mongo.exception.NucleusMongoExceptionTranslator;
import com.mongodb.MongoClient;
import org.springframework.dao.support.PersistenceExceptionTranslator;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

public final class NucleusMongoDbFactory extends SimpleMongoDbFactory {
    private final NucleusMongoExceptionTranslator nucleusMongoExceptionTranslator;

    public NucleusMongoDbFactory(MongoClient mongoClient, String databaseName) {
        super(mongoClient, databaseName);
        this.nucleusMongoExceptionTranslator = new NucleusMongoExceptionTranslator();
    }

    @Override
    public PersistenceExceptionTranslator getExceptionTranslator() {
        return nucleusMongoExceptionTranslator;
    }
}
