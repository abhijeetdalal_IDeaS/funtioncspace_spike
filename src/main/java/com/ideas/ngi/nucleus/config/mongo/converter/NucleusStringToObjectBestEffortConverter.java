package com.ideas.ngi.nucleus.config.mongo.converter;

import com.github.rutledgepaulv.rqe.conversions.parsers.StringToObjectBestEffortConverter;

public class NucleusStringToObjectBestEffortConverter extends StringToObjectBestEffortConverter {

    public Object convert(String source) {
        if (source.startsWith("'") && source.endsWith("'")) {
            return (source.substring(1, source.length() - 1));
        }
        return super.convert(source);
    }

}
