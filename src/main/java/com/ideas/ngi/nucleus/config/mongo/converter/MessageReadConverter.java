package com.ideas.ngi.nucleus.config.mongo.converter;


import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.util.Map;

@ReadingConverter
public class MessageReadConverter implements Converter<Document, Message<?>> {

    @Override
    @SuppressWarnings("unchecked")
    public Message<?> convert(Document source) {
        return MessageBuilder.withPayload(source.get("payload")).copyHeaders((Map<String, ?>) source.get("headers")).build();
    }

}