package com.ideas.ngi.nucleus.config.internalclient.ups;

public interface UnifiedPropertyClient {

    /**
     * get host name for client/property
     *
     * @param clientCode
     * @param propertyCode
     * @return host
     */
    String getHost(String clientCode, String propertyCode);
}
