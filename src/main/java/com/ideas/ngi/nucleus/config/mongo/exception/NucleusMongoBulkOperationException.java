package com.ideas.ngi.nucleus.config.mongo.exception;

import com.mongodb.MongoBulkWriteException;
import com.mongodb.bulk.BulkWriteError;
import com.mongodb.bulk.BulkWriteResult;
import org.springframework.dao.DataAccessException;

import java.io.Serializable;
import java.util.List;

public class NucleusMongoBulkOperationException extends DataAccessException implements Serializable {
    private static final long serialVersionUID = 73929601661154421L;

    private final transient List<BulkWriteError> errors;
    private final transient BulkWriteResult result;

    public NucleusMongoBulkOperationException(String message, MongoBulkWriteException source) {
        super(message, source);
        this.errors = source.getWriteErrors();
        this.result = source.getWriteResult();
    }

    public List<BulkWriteError> getErrors() {
        return errors;
    }

    public BulkWriteResult getResult() {
        return result;
    }
}
