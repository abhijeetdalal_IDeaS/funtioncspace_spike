package com.ideas.ngi.nucleus.config.mongo.converter;

import org.bson.types.Decimal128;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

import java.math.BigDecimal;

import static com.ideas.ngi.nucleus.util.BigDecimalUtils.DEFAULT_ROUNDING_MODE;

@WritingConverter
public class BigDecimalDecimal128WriteConverter implements Converter<BigDecimal, Decimal128> {

    @Override
    public Decimal128 convert(BigDecimal value) {
        try {
            return new Decimal128(value);
        } catch (NumberFormatException e) {
            return new Decimal128(value.setScale(8, DEFAULT_ROUNDING_MODE).stripTrailingZeros());
        }
    }
}
