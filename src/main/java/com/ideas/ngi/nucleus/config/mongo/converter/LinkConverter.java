package com.ideas.ngi.nucleus.config.mongo.converter;

import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.hateoas.Link;

/**
 * This should convert simple Spring hateoas Links added to Mongo documents intended for G3 notification payloads
 * This will handle Links produced using one of the simple link builders such as:
 *   new Link()
 *   EntityLinkBuilder
 *   BasicUriLinkBuilder
 * Known limitations:  Not for PagedResource links that contain more complex structures
 */
@ReadingConverter
public class LinkConverter implements Converter<Document, Link> {

    static final String LINK_REL = "rel";
    static final String LINK_HREF = "href";
    static final String LINK_HREF_LANG = "hreflang";
    static final String LINK_MEDIA = "media";
    static final String LINK_TITLE = "title";
    static final String LINK_TYPE = "type";
    static final String LINK_DEPRECATION = "deprecation";

    @Override
    public Link convert(Document source) {
        if (source.get(LINK_HREF) == null) {
            return null;
        }
        Link result = new Link((String) source.get(LINK_HREF));
        if (source.containsKey(LINK_HREF_LANG)) {
            result = result.withHreflang((String) source.get(LINK_HREF_LANG));
        }
        if (source.containsKey(LINK_MEDIA)) {
            result = result.withMedia((String) source.get(LINK_MEDIA));
        }
        if (source.containsKey(LINK_TITLE)) {
            result = result.withTitle((String) source.get(LINK_TITLE));
        }
        if (source.containsKey(LINK_TYPE)) {
            result = result.withType((String) source.get(LINK_TYPE));
        }
        if (source.containsKey(LINK_DEPRECATION)) {
            result = result.withDeprecation((String) source.get(LINK_DEPRECATION));
        }
        if (source.containsKey(LINK_REL)) {
            result = result.withRel((String) source.get(LINK_REL));
        }
        return result;
    }
}
