package com.ideas.ngi.nucleus.config.mongo.migration;

import com.github.mongobee.changeset.ChangeEntry;
import com.github.mongobee.dao.LockDao;
import com.mongodb.client.MongoDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MigrationService {

    private final MigrationRunnerFactory migrationRunnerFactory;
    private final MongoTemplate mongoTemplate;
    private final MongoDbFactory mongoDbFactory;

    @Autowired
    public MigrationService(MigrationRunnerFactory migrationRunnerFactory, MongoTemplate mongoTemplate, MongoDbFactory mongoDbFactory) {
        this.migrationRunnerFactory = migrationRunnerFactory;
        this.mongoTemplate = mongoTemplate;
        this.mongoDbFactory = mongoDbFactory;
    }

    public List<String> getRegisteredModules() {
        return migrationRunnerFactory.getModules();
    }

    public List<ChangeEntry> getChangesForModule(String module) {
        String collectionName = MigrationRunnerFactory.getChangeLogCollection(module);
        Query query = new Query().with(new Sort(Sort.Direction.ASC, "timestamp"));
        return mongoTemplate.find(query, ChangeEntry .class, collectionName);
    }

    public MigrationRunnerFactory.MongoRunnerBuilder newRunnerBuilder() {
        return migrationRunnerFactory.builder();
    }

    public boolean releaseLock(String module) {
        MongoDatabase db = mongoDbFactory.getDb(mongoTemplate.getDb().getName());
        LockDao lockDao = new LockDao(MigrationRunnerFactory.getChangeLogCollection(module), "lock-owner");
        if (lockDao.isLockHeld(db)) {
            lockDao.releaseLock(db);
            return true;
        }
        return false;
    }

}
