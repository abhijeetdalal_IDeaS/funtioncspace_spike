package com.ideas.ngi.nucleus.config.circuitbreaker;

import com.ideas.ngi.nucleus.exception.NucleusRetryableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;

import javax.annotation.concurrent.ThreadSafe;
import java.util.Map;

@Service
@ThreadSafe
@RefreshScope
@Qualifier("durableCircuitBreaker")
public class DurableCircuitBreaker extends CircuitBreaker {
    private static final Logger LOGGER = LoggerFactory.getLogger(DurableCircuitBreaker.class);

    @Override
    protected <T> T handleBlockedMessage(Object payload, Map<String,Object> headers, MessageChannel channel) {
        //Fuse is open, so we didn't make the call. Just throwing it back on the queue for later.
        requeueMessage(payload, headers, channel);
        return null;
    }

    @Override
    protected <T> T handleFailedMessage(Object payload, Map<String, Object> headers, MessageChannel channel, RuntimeException e) {
        if (e instanceof NucleusRetryableException) {
            requeueMessage(payload, headers, channel);
        } else {
            //not a retryable exception, throws a NucleusException and should be caught by the NucleusMessageRecoverer.
            throw e;
        }
        return null;
    }

    void requeueMessage(Object payload,  Map<String, Object> headers, MessageChannel channel) {
        //Recreate the message and send it back to the channel it came from.
        LOGGER.info("Requeuing message back to {}", channel.toString());
        Message<?> message = MessageBuilder.withPayload(payload).copyHeaders(headers).build();
        channel.send(message);
    }
}
