package com.ideas.ngi.nucleus.config.mongo.controller;

import com.github.rutledgepaulv.qbuilders.structures.FieldPath;
import com.github.rutledgepaulv.rqe.resolvers.EntityFieldTypeResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mapping.MappingException;

public class NucleusMongoPersistentEntityFieldTypeResolver extends EntityFieldTypeResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(NucleusMongoPersistentEntityFieldTypeResolver.class);

    @SuppressWarnings("squid:S1696")
    @Override
    public Class<?> apply(FieldPath path, Class<?> root) {
        try {
            return super.apply(path, root);
        } catch (MappingException | NullPointerException me) {
            LOGGER.info("Attempting to resolve a type in a path where the root element does not exist or has no " +
                    "pre-defined fields.", me);
            String[] keys = path.asKey().split("\\.");
            if (keys.length == 1) {
                throw new MappingException("Could not find path: " + path);
            }
            return super.apply(new FieldPath(keys[0]), root);
        }
    }
}

