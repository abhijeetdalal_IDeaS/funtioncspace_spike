package com.ideas.ngi.nucleus.config.internalclient.ups;

import com.ideas.ngi.nucleus.rest.client.cache.RestClientCaches;

/**
 * Default implementation, meant to be overridden by each each integration
 */
public class UnifiedPropertyClientDefaultImpl implements UnifiedPropertyClient {

    @Override
    public String getHost(String clientCode, String propertyCode) {
        return RestClientCaches.NOT_FOUND;
    }
}
