package com.ideas.ngi.nucleus.config.mongo.converter;

import com.ideas.ngi.nucleus.exception.NucleusException;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

@ReadingConverter
public class XMLGregorianCalendarReadConverter implements Converter<String, XMLGregorianCalendar> {
    private final DatatypeFactory datatypeFactory;

    public XMLGregorianCalendarReadConverter() {
        try {
            this.datatypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            throw new NucleusException("Could not instantiate datatype factory", e);
        }
    }

    @Override
    public XMLGregorianCalendar convert(String source) {
        return datatypeFactory.newXMLGregorianCalendar(source);
    }
}
