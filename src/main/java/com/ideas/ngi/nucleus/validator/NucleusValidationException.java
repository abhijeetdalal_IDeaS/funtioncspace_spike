package com.ideas.ngi.nucleus.validator;

import com.ideas.ngi.nucleus.exception.NucleusException;

import java.util.Collections;
import java.util.List;

public class NucleusValidationException extends NucleusException {
    private static final String DEFAULT_MESSAGE_HEADER = "Validation failed for %s";

    private final List<String> violationMessages;
    private final transient Object bean;

    public NucleusValidationException(Object bean, List<String> violationMessages, String message) {
        super(message);
        this.bean = bean;
        this.violationMessages = violationMessages;
    }

    public NucleusValidationException(Object bean, List<String> violationMessages) {
        this(bean, violationMessages, String.format(DEFAULT_MESSAGE_HEADER, bean.getClass().getCanonicalName()));
    }

    @Override
    public String getMessage() {
        return super.getMessage() + " " + violationMessages.toString();
    }

    public Object getBean() {
        return bean;
    }

    public List<String> getViolationMessages() {
        return Collections.unmodifiableList(violationMessages);
    }
}
