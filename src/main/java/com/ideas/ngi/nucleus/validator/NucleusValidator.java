package com.ideas.ngi.nucleus.validator;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.*;

@Component
public class NucleusValidator {

    private Validator validator;

    @Autowired
    public NucleusValidator(LocalValidatorFactoryBean validatorFactory) {
        this.validator = validatorFactory;
    }

    public void validate(Object obj) {
        Set<ConstraintViolation<Object>> violations = validator.validate(obj);

        if (CollectionUtils.isNotEmpty(violations)) {

            List<String> violationMessages = new ArrayList<>();
            Iterator<ConstraintViolation<Object>> iterator = violations.iterator();
            while (iterator.hasNext()) {
                ConstraintViolation<Object> constraintViolation = iterator.next();
                violationMessages.add("'" + constraintViolation.getPropertyPath() + "' " + constraintViolation.getMessage() + ", but was '" + constraintViolation.getInvalidValue() + "'");
            }

            throw new NucleusValidationException(obj, violationMessages);
        }
    }
    
    public void validateAll(Collection<?> collection) {
        // If the Collection is empty, nothing to validate
        if (CollectionUtils.isEmpty(collection)) {
            return;
        }

        // Build a List of all objects that need to be validated
        // As you can see, it's possible to pass in a collection 
        // of other Objects, so extracting their individutal values
        List<Object> objectsToValidate = new ArrayList<>();
        for (Object value : collection) {
            if (value instanceof Collection) {
                objectsToValidate.addAll((Collection<?>) value);
            } else {
                objectsToValidate.add(value);
            }
        }

        // Validate each object
        for (Object obj : objectsToValidate) {
            validate(obj);
        }
    }
}
