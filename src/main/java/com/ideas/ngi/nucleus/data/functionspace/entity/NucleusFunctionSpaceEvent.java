package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.nucleus.data.AuditableEntity;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.functionspace.file.delimited.annotation.DelimitedField;
import com.ideas.ngi.nucleus.integration.annotation.ClientCode;
import com.ideas.ngi.nucleus.integration.annotation.PropertyCode;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document
public class NucleusFunctionSpaceEvent extends AuditableEntity implements Serializable {

    private static final long serialVersionUID = 3948182067778320908L;

    @Id
    private String id;

    @NotNull
    private IntegrationType integrationType;

    @DelimitedField(name = "PropertyID")
    @NotNull
    private String propertyId;

    @DelimitedField(name = "BookingID")
    @NotNull
    private String bookingId;

    @DelimitedField(name = "EventID")
    @NotNull
    private String eventId;

    @DelimitedField(name = "EventType", defaultValue = "")
    private String eventTypeCode;

    @DelimitedField(name = "FunctionRoomId")
    private String roomId;

    @DelimitedField(name = "GroupID")
    private String groupId;

    @DelimitedField(name = "RoomSetup")
    private String functionRoomSetupType;

    @DelimitedField(name = "BlockStart")
    @NotNull
    private Date blockStart;

    @DelimitedField(name = "BlockEnd")
    @NotNull
    private Date blockEnd;

    @DelimitedField(name = "StartDate")
    private Date startDate;

    @DelimitedField(name = "EndDate")
    private Date endDate;

    @DelimitedField(name = "SetupTime")
    private Integer setUpTimeMinutes;

    @DelimitedField(name = "SetdownTime")
    private Integer tearDownTimeMinutes;

    @DelimitedField(name = "Status", defaultValue = "")
    @NotNull
    private String status;

    @DelimitedField(name = "ForecastAttendees")
    private Integer forecastAttendees;

    @DelimitedField(name = "ActualAttendees")
    private Integer actualAttendees;

    @DelimitedField(name = "DontMoveYN")
    private Boolean moveable;

    @DelimitedField(name = "Name")
    private String eventName;

    private String mealType;

    private List<FunctionSpaceEventRevenue> functionSpaceEventRevenues = new ArrayList<>();
    private String correlationMetadataId;
    private String correlationId;
    @ClientCode
    private String clientCode;
    @PropertyCode
    private String propertyCode;

    private Integer minimumGuaranteedAttendees;

    //should be null for master event and bookingId of master event for sub event
    private String masterOrSubEvent;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public IntegrationType getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(IntegrationType integrationType) {
        this.integrationType = integrationType;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getFunctionRoomSetupType() {
        return functionRoomSetupType;
    }

    public void setFunctionRoomSetupType(String functionRoomSetupType) {
        this.functionRoomSetupType = functionRoomSetupType;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getEventTypeCode() {
        return eventTypeCode;
    }

    public void setEventTypeCode(String eventTypeCode) {
        this.eventTypeCode = eventTypeCode;
    }

    public Date getBlockStart() {
        return blockStart;
    }

    public void setBlockStart(Date blockStart) {
        this.blockStart = blockStart;
    }

    public Date getBlockEnd() {
        return blockEnd;
    }

    public void setBlockEnd(Date blockEnd) {
        this.blockEnd = blockEnd;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getSetUpTimeMinutes() {
        return setUpTimeMinutes;
    }

    public void setSetUpTimeMinutes(Integer setUpTimeMinutes) {
        this.setUpTimeMinutes = setUpTimeMinutes;
    }

    public Integer getTearDownTimeMinutes() {
        return tearDownTimeMinutes;
    }

    public void setTearDownTimeMinutes(Integer tearDownTimeMinutes) {
        this.tearDownTimeMinutes = tearDownTimeMinutes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getForecastAttendees() {
        return forecastAttendees;
    }

    public void setForecastAttendees(Integer forecastAttendees) {
        this.forecastAttendees = forecastAttendees;
    }

    public Integer getActualAttendees() {
        return actualAttendees;
    }

    public void setActualAttendees(Integer actualAttendees) {
        this.actualAttendees = actualAttendees;
    }

    public Boolean isMoveable() {
        return moveable;
    }

    public void setMoveable(Boolean moveable) {
        this.moveable = moveable;
    }

    public List<FunctionSpaceEventRevenue> getFunctionSpaceEventRevenues() {
        return functionSpaceEventRevenues;
    }

    public void setFunctionSpaceEventRevenues(List<FunctionSpaceEventRevenue> functionSpaceEventRevenues) {
        this.functionSpaceEventRevenues = functionSpaceEventRevenues;
    }

    public String getCorrelationMetadataId() {
        return correlationMetadataId;
    }

    public void setCorrelationMetadataId(String correlationMetadataId) {
        this.correlationMetadataId = correlationMetadataId;
    }

    public String getMealType() {
        return mealType;
    }

    public void setMealType(String mealType) {
        this.mealType = mealType;
    }

    public FunctionSpaceEventRevenue findFunctionSpaceEventRevenue(String revenueGroup, String revenueType) {
        if (CollectionUtils.isEmpty(functionSpaceEventRevenues)) {
            return null;
        }

        for (FunctionSpaceEventRevenue functionSpaceEventRevenue : functionSpaceEventRevenues) {
            if (StringUtils.equals(functionSpaceEventRevenue.getRevenueGroup(), revenueGroup) && StringUtils.equals(functionSpaceEventRevenue.getRevenueType(), revenueType)) {
                return functionSpaceEventRevenue;
            }
        }

        return null;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public Integer getMinimumGuaranteedAttendees() {
        return minimumGuaranteedAttendees;
    }

    public void setMinimumGuaranteedAttendees(Integer minimumGuaranteedAttendees) {
        this.minimumGuaranteedAttendees = minimumGuaranteedAttendees;
    }

    public String getMasterOrSubEvent() {
        return masterOrSubEvent;
    }

    public void setMasterOrSubEvent(String masterOrSubEvent) {
        this.masterOrSubEvent = masterOrSubEvent;
    }
}
