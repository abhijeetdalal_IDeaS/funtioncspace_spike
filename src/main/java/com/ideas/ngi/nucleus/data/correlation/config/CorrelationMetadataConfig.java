package com.ideas.ngi.nucleus.data.correlation.config;

import com.ideas.ngi.nucleus.data.correlation.endpoint.CorrelationMetadataEndpoint;
import com.ideas.ngi.nucleus.integration.NucleusIntegrationComponent;
import com.ideas.ngi.nucleus.integration.router.NucleusReturnChannelHeaderRouter;
import com.ideas.ngi.nucleus.integration.util.MessageHeaderUtil;
import com.ideas.ngi.nucleus.util.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.router.RecipientListRouter;
import org.springframework.messaging.MessageChannel;

@Configuration
public class CorrelationMetadataConfig {

    public static final String NUCLEUS_CORRELATION_METADATA_SEND_EVENT_CHANNEL = "nucleusCorrelationMetadataSendEventChannel";
    public static final String NUCLEUS_CORRELATION_METADATA_SEND_EVENT_ROUTER_CHANNEL = "nucleusCorrelationMetadataSendEventRouterChannel";

    @Autowired private NucleusIntegrationComponent nucleusIntegrationComponent;
    @Autowired private NucleusReturnChannelHeaderRouter nucleusReturnChannelHeaderRouter;
    @Autowired
    @Qualifier("allInternalClientsRestClientRouter")
    private RecipientListRouter allInternalClientsRestClientRouter;

    @Bean(name = NUCLEUS_CORRELATION_METADATA_SEND_EVENT_CHANNEL)
    public MessageChannel nucleusCorrelationMetadataSendEventChannel() {
        return nucleusIntegrationComponent.amqpChannel(NUCLEUS_CORRELATION_METADATA_SEND_EVENT_CHANNEL);
    }
    
    @Bean(name = NUCLEUS_CORRELATION_METADATA_SEND_EVENT_ROUTER_CHANNEL)
    public MessageChannel nucleusCorrelationMetadataSendEventRouterChannel() {
        return nucleusIntegrationComponent.amqpPublishSubscribeChannel(NUCLEUS_CORRELATION_METADATA_SEND_EVENT_ROUTER_CHANNEL);
    }
    
    @Bean
    public IntegrationFlow nucleusCorrelationMetadataSendEventFlow() {
        return nucleusIntegrationComponent
                .from(NUCLEUS_CORRELATION_METADATA_SEND_EVENT_CHANNEL)
                .channel(NUCLEUS_CORRELATION_METADATA_SEND_EVENT_ROUTER_CHANNEL)
                .route(nucleusReturnChannelHeaderRouter)
                .get();
    }
    
    @Bean
    public IntegrationFlow nucleusCorrelationMetadataSendEventRouterFlow() {
        return nucleusIntegrationComponent
                .from(NUCLEUS_CORRELATION_METADATA_SEND_EVENT_ROUTER_CHANNEL)
                .enrichHeaders(nucleusIntegrationComponent.buildHeaders(MapBuilder.with(MessageHeaderUtil.REST_ENDPOINT, CorrelationMetadataEndpoint.POST_EVENT).get()))
                .route(allInternalClientsRestClientRouter)
                .get();
        
    }
}
