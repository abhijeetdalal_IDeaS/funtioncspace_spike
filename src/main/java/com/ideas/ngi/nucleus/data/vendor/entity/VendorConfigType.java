package com.ideas.ngi.nucleus.data.vendor.entity;


import com.google.common.collect.Sets;

import java.util.Collections;
import java.util.Set;

public enum VendorConfigType {
    USE_OLD_DELTA("useOldDelta", Sets.newHashSet("true", "false"), "this is for enabling the old delta flow");

    private String name;
    private Set<?> validValues;
    private String description;

    VendorConfigType(String name, Set<?> validValues, String description) {
        this.name = name;
        this.validValues = Collections.unmodifiableSet(validValues);
        this.description = description;
    }

    public static VendorConfigType getNameForString(String name) {
        if (name == null) {
            return null;
        }

        for (VendorConfigType vendorConfigType : values()) {
            if (name.equalsIgnoreCase(vendorConfigType.getName())) {
                return vendorConfigType;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<?> getValidValues() {
        return validValues;
    }

    public boolean isValidValue(Object value) {
        return validValues.isEmpty() || validValues.contains(value);
    }
}
