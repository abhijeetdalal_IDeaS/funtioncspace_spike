package com.ideas.ngi.nucleus.data.functionspace.entity;

public enum FunctionSpaceBookingCategory {

	BOOKING,
	BOOKING_WITH_EVENTS,
	BOOKING_WITH_GUEST_ROOMS,
	BOOKING_WITH_EVENTS_AND_GUESTROOMS
}
