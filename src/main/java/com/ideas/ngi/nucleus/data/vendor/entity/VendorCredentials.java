package com.ideas.ngi.nucleus.data.vendor.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ideas.ngi.nucleus.config.mongo.entity.Secret;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Objects;

import static com.ideas.ngi.nucleus.util.NullableMethodChainUtil.resolveChainAsNullable;
import static org.apache.commons.lang3.StringUtils.isAnyBlank;

public class VendorCredentials implements Serializable {
    public static final String KEY_USERNAME = "username";
    @SuppressWarnings("squid:S2068")
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_AUTHENTICATION_TOKEN = "authenticationToken";
    private static final long serialVersionUID = -3115100266384898933L;

    private String username;

    private Secret password;

    private Secret authenticationToken;

    public VendorCredentials() {
    }

    public VendorCredentials(String username, String password) {
        this(username, password, null);
    }

    public VendorCredentials(String authenticationToken) {
        this(null, null, authenticationToken);
    }

    @JsonCreator
    public VendorCredentials(
            @JsonProperty(KEY_USERNAME) String username,
            @JsonProperty(KEY_PASSWORD) String password,
            @JsonProperty(KEY_AUTHENTICATION_TOKEN) String authenticationToken) {
        this.username = username;
        this.password = Secret.valueOf(password);
        this.authenticationToken = Secret.valueOf(authenticationToken);
        if (!isAnyBlank(username, password, authenticationToken)) {
            verifyCredentials();
        }
    }

    public static VendorCredentials fromUsernameAndPassword(String username, String password) {
        return new VendorCredentials(username, password);
    }

    public static VendorCredentials fromAuthenticationToken(String authenticationToken) {
        return new VendorCredentials(authenticationToken);
    }

    void verifyCredentials() {
        if (!(hasOnlyAuthenticationToken() || hasOnlyUsernameAndPassword())) {
            throw new IllegalStateException("VendorCredentials must have either a not null authentication token and null username and password, or a not null username and password and null authentication token.");
        }
    }

    public boolean hasUsernameAndPassword() {
        return (StringUtils.isNotBlank(username) && (password != null));
    }

    private boolean hasOnlyAuthenticationToken() {
        return authenticationToken != null && username == null && password == null;
    }

    private boolean hasOnlyUsernameAndPassword() {
        return (authenticationToken == null) && hasUsernameAndPassword();
    }

    public String getPassword() {
        return password == null ? null : password.getValue();
    }

    public String getUsername() {
        return username;
    }

    public String getAuthenticationToken() {
        return authenticationToken == null ? null : authenticationToken.getValue();
    }

    public boolean authenticate(VendorCredentials that) {
        return authenticate(that, true);
    }

    public boolean authenticate(VendorCredentials that, boolean caseSensitiveUsername) {

        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        return (caseSensitiveUsername ? Objects.equals(this.username, that.username) : resolveChainAsNullable(() -> Objects.equals(this.username.toLowerCase(), that.username.toLowerCase())).orElse(true))
                && Objects.equals(this.password, that.password)
                && Objects.equals(this.authenticationToken, that.authenticationToken);
    }

    public String toString() {
        return String.format("VendorCredentials[username: %s, password: %s, token: %s]",
                username,
                password == null ? "<empty>" : "<set>",
                authenticationToken == null ? "<empty>" : "<set>");
    }
}
