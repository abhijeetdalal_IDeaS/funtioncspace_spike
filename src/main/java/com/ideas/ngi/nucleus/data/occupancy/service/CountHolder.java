package com.ideas.ngi.nucleus.data.occupancy.service;

public class CountHolder {
    Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
