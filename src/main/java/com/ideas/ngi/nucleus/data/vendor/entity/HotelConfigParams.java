package com.ideas.ngi.nucleus.data.vendor.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

public class HotelConfigParams implements Serializable {
    private static final long serialVersionUID = -6600400875971493570L;

    private String hotelCode;
    private String g3HotelCode;
    private String g2HotelCode;
    private String inboundHotelCode;
    private String propertyName;
    private String outgoingUrl;
    private String oxiInterfaceName;
    private BigDecimal taxAdjustmentValue;
    private VendorCredentials outboundCredentials;
    private VendorCredentials inboundCredentials;
    private String baseCurrencyCode;
    private Integer pastDays;
    private Integer futureDays;
    private Boolean assumeTaxIncluded;
    private Boolean assumePackageIncluded;
    private Boolean installMode;
    private Boolean scheduledDeferredDelivery;
    private Boolean inCatchup;
    private Boolean calculateNonPickedUpBlocksUsingSummaryData;
    private Boolean handlePreviouslyStraightMarketSegmentsInAms;
    private Boolean unqualifiedRatesDirectPopulationDisabled;
    private Boolean qualifiedRatesDirectPopulationDisabled;
    private String roomRevenuePackages;
    private Integer installationReservationsThreshold;
    private Boolean populatePackageDataEnabled;
    private Boolean oxiRoomStayReservationByDay;
    private Boolean htngRoomStayReservationByDay;
    private Boolean folsRoomStayReservationByDay;
    private Boolean useNetRoomRevenue;
    private Boolean htngUseBasicAuth;
    private Boolean includePseudoInRevenue;
    private Boolean includeDayUseInRevenue;
    private Boolean includeNoShowInRevenue;
    private String defaultRoomType;
    private Boolean ratePlanCleanupEnabled;
    private Boolean includeRoomTypeHotelMarketSegmentActivity;
    private Boolean preventPseudoDataInActivity;
    private Boolean isComponentRoomsActivityEnabled;
    private Boolean useCurrencyInMessage;
    private Boolean useLegacyRoomStayHandling;
    private String oxiParkMessageTypes;
    private Boolean generateMarketSegmentStatsEnabled;
    private String clientEnvironmentName;
    private Boolean generateRateCodeStatsEnabled;
    private Boolean resetVirtualSuiteCounts;
    private Boolean generateHotelActivityFromRoomTypeActivity;

    @Getter
    @Setter
    private String salesAndCateringUnitOfMeasure;

    @Getter
    @Setter
    private String salesAndCateringStatus;

    @Getter
    @Setter
    private Boolean summaryPersistenceEnabled;

    @Getter
    @Setter
    private Boolean msrtSummaryPersistenceEnabled;

    @Getter
    @Setter
    private Boolean buildMSActivityUsingPMSMS;

    public VendorCredentials getInboundCredentials() {
        return inboundCredentials;
    }

    public void setInboundCredentials(VendorCredentials inboundCredentials) {
        this.inboundCredentials = inboundCredentials;
    }

    public VendorCredentials getOutboundCredentials() {
        return outboundCredentials;
    }

    public void setOutboundCredentials(VendorCredentials outboundCredentials) {
        this.outboundCredentials = outboundCredentials;
    }

    public String getG2HotelCode() {
        return g2HotelCode;
    }

    public void setG2HotelCode(String g2HotelCode) {
        this.g2HotelCode = g2HotelCode;
    }

    public String getHotelCode() {
        return hotelCode;
    }

    public void setHotelCode(String hotelCode) {
        this.hotelCode = hotelCode;
    }

    public String getG3HotelCode() {
        return g3HotelCode;
    }

    public void setG3HotelCode(String g3HotelCode) {
        this.g3HotelCode = g3HotelCode;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getOutgoingUrl() {
        return outgoingUrl;
    }

    public void setOutgoingUrl(String outgoingUrl) {
        this.outgoingUrl = outgoingUrl;
    }

    public String getOxiInterfaceName() {
        return oxiInterfaceName;
    }

    public void setOxiInterfaceName(String oxiInterfaceName) {
        this.oxiInterfaceName = oxiInterfaceName;
    }

    public BigDecimal getTaxAdjustmentValue() {
        return taxAdjustmentValue;
    }

    public void setTaxAdjustmentValue(BigDecimal taxAdjustmentValue) {
        this.taxAdjustmentValue = taxAdjustmentValue;
    }

    public String getBaseCurrencyCode() {
        return baseCurrencyCode;
    }

    public void setBaseCurrencyCode(String baseCurrencyCode) {
        this.baseCurrencyCode = baseCurrencyCode;
    }

    public Integer getPastDays() {
        return pastDays;
    }

    public void setPastDays(Integer pastDays) {
        this.pastDays = pastDays;
    }

    public Integer getFutureDays() {
        return futureDays;
    }

    public void setFutureDays(Integer futureDays) {
        this.futureDays = futureDays;
    }

    public Boolean getAssumeTaxIncluded() {
        return assumeTaxIncluded;
    }

    public void setAssumeTaxIncluded(Boolean assumeTaxIncluded) {
        this.assumeTaxIncluded = assumeTaxIncluded;
    }

    public Boolean getAssumePackageIncluded() {
        return assumePackageIncluded;
    }

    public void setAssumePackageIncluded(Boolean assumePackageIncluded) {
        this.assumePackageIncluded = assumePackageIncluded;
    }

    public Boolean getInstallMode() {
        return installMode;
    }

    public void setInstallMode(Boolean installMode) {
        this.installMode = installMode;
    }

    public Boolean getScheduledDeferredDelivery() {
        return scheduledDeferredDelivery;
    }

    public void setScheduledDeferredDelivery(Boolean scheduledDeferredDelivery) {
        this.scheduledDeferredDelivery = scheduledDeferredDelivery;
    }

    public String getInboundHotelCode() {
        return inboundHotelCode;
    }

    public void setInboundHotelCode(String inboundHotelCode) {
        this.inboundHotelCode = inboundHotelCode;
    }

    public Boolean getInCatchup() {
        return inCatchup;
    }

    public void setInCatchup(Boolean inCatchup) {
        this.inCatchup = inCatchup;
    }

    public Boolean getCalculateNonPickedUpBlocksUsingSummaryData() {
        return calculateNonPickedUpBlocksUsingSummaryData;
    }

    public void setCalculateNonPickedUpBlocksUsingSummaryData(Boolean calculateNonPickedUpBlocksUsingSummaryData) {
        this.calculateNonPickedUpBlocksUsingSummaryData = calculateNonPickedUpBlocksUsingSummaryData;
    }

    public Boolean getHandlePreviouslyStraightMarketSegmentsInAms() {
        return handlePreviouslyStraightMarketSegmentsInAms;
    }

    public void setHandlePreviouslyStraightMarketSegmentsInAms(Boolean handlePreviouslyStraightMarketSegmentsInAms) {
        this.handlePreviouslyStraightMarketSegmentsInAms = handlePreviouslyStraightMarketSegmentsInAms;
    }

    public Boolean getUnqualifiedRatesDirectPopulationDisabled() {
        return unqualifiedRatesDirectPopulationDisabled;
    }

    public void setUnqualifiedRatesDirectPopulationDisabled(Boolean unqualifiedRatesDirectPopulationDisabled) {
        this.unqualifiedRatesDirectPopulationDisabled = unqualifiedRatesDirectPopulationDisabled;
    }

    public Boolean getQualifiedRatesDirectPopulationDisabled() {
        return qualifiedRatesDirectPopulationDisabled;
    }

    public void setQualifiedRatesDirectPopulationDisabled(Boolean qualifiedRatesDirectPopulationDisabled) {
        this.qualifiedRatesDirectPopulationDisabled = qualifiedRatesDirectPopulationDisabled;
    }

    public String getDefaultRoomType() {
        return defaultRoomType;
    }

    public void setDefaultRoomType(String defaultRoomType) {
        this.defaultRoomType = defaultRoomType;
    }

    public String getRoomRevenuePackages() {
        return roomRevenuePackages;
    }

    public void setRoomRevenuePackages(String roomRevenuePackages) {
        this.roomRevenuePackages = roomRevenuePackages;
    }

    public Integer getInstallationReservationsThreshold() {
        return installationReservationsThreshold;
    }

    public void setInstallationReservationsThreshold(Integer installationReservationsThreshold) {
        this.installationReservationsThreshold = installationReservationsThreshold;
    }

    public Boolean getPopulatePackageDataEnabled() {
        return populatePackageDataEnabled;
    }

    public void setPopulatePackageDataEnabled(Boolean populatePackageDataEnabled) {
        this.populatePackageDataEnabled = populatePackageDataEnabled;
    }

    public Boolean getOxiRoomStayReservationByDay() {
        return oxiRoomStayReservationByDay;
    }

    public void setOxiRoomStayReservationByDay(Boolean oxiRoomStayReservationByDay) {
        this.oxiRoomStayReservationByDay = oxiRoomStayReservationByDay;
    }

    public Boolean getHtngRoomStayReservationByDay() {
        return htngRoomStayReservationByDay;
    }

    public void setHtngRoomStayReservationByDay(Boolean htngRoomStayReservationByDay) {
        this.htngRoomStayReservationByDay = htngRoomStayReservationByDay;
    }

    public Boolean getFolsRoomStayReservationByDay() {
        return folsRoomStayReservationByDay;
    }

    public void setFolsRoomStayReservationByDay(Boolean folsRoomStayReservationByDay) {
        this.folsRoomStayReservationByDay = folsRoomStayReservationByDay;
    }

    public Boolean getUseNetRoomRevenue() {
        return useNetRoomRevenue;
    }

    public void setUseNetRoomRevenue(Boolean useNetRoomRevenue) {
        this.useNetRoomRevenue = useNetRoomRevenue;
    }

    public Boolean getHtngUseBasicAuth() {
        return htngUseBasicAuth;
    }

    public void setHtngUseBasicAuth(Boolean htngUseBasicAuth) {
        this.htngUseBasicAuth = htngUseBasicAuth;
    }

    public Boolean getIncludePseudoInRevenue() { return includePseudoInRevenue; }

    public void setIncludePseudoInRevenue(Boolean includePseudoInRevenue) {
        this.includePseudoInRevenue = includePseudoInRevenue;
    }

    public Boolean getIncludeDayUseInRevenue() { return includeDayUseInRevenue; }

    public void setIncludeDayUseInRevenue(Boolean includeDayUseInRevenue) {
        this.includeDayUseInRevenue = includeDayUseInRevenue;
    }

    public Boolean getIncludeNoShowInRevenue() { return includeNoShowInRevenue; }

    public void setIncludeNoShowInRevenue(Boolean includeNoShowInRevenue) {
        this.includeNoShowInRevenue = includeNoShowInRevenue;
    }

    public Boolean getRatePlanCleanupEnabled() {
        return ratePlanCleanupEnabled;
    }

    public void setRatePlanCleanupEnabled(Boolean ratePlanCleanupEnabled) {
        this.ratePlanCleanupEnabled = ratePlanCleanupEnabled;
    }

    public Boolean getIncludeRoomTypeHotelMarketSegmentActivity() {
        return includeRoomTypeHotelMarketSegmentActivity;
    }

    public void setIncludeRoomTypeHotelMarketSegmentActivity(Boolean includeRoomTypeHotelMarketSegmentActivity) {
        this.includeRoomTypeHotelMarketSegmentActivity = includeRoomTypeHotelMarketSegmentActivity;
    }

    public Boolean getPreventPseudoDataInActivity() {
        return preventPseudoDataInActivity;
    }

    public void setPreventPseudoDataInActivity(Boolean preventPseudoDataInActivity) {
        this.preventPseudoDataInActivity = preventPseudoDataInActivity;
    }

    public Boolean getComponentRoomsActivityEnabled() {
        return isComponentRoomsActivityEnabled;
    }

    public void setComponentRoomsActivityEnabled(Boolean componentRoomsActivityEnabled) {
        isComponentRoomsActivityEnabled = componentRoomsActivityEnabled;
    }

    public Boolean getUseCurrencyInMessage() {
        return useCurrencyInMessage;
    }

    public void setUseCurrencyInMessage(Boolean useCurrencyInMessage) {
        this.useCurrencyInMessage = useCurrencyInMessage;
    }

    public Boolean getUseLegacyRoomStayHandling() {
        return useLegacyRoomStayHandling;
    }

    public void setUseLegacyRoomStayHandling(Boolean useLegacyRoomStayHandling) {
        this.useLegacyRoomStayHandling = useLegacyRoomStayHandling;
    }

    public String getOxiParkMessageTypes() {
        return oxiParkMessageTypes;
    }

    public void setOxiParkMessageTypes(String parkMessageTypes) {
        this.oxiParkMessageTypes = parkMessageTypes;
    }

    public String getClientEnvironmentName() {
        return clientEnvironmentName;
    }

    public void setClientEnvironmentName(String clientEnvironmentName) {
        this.clientEnvironmentName = clientEnvironmentName;
    }

    public Boolean getGenerateMarketSegmentStatsEnabled() {
        return generateMarketSegmentStatsEnabled;
    }

    public void setGenerateMarketSegmentStatsEnabled(Boolean generateMarketSegmentStatsEnabled) {
        this.generateMarketSegmentStatsEnabled = generateMarketSegmentStatsEnabled;
    }

    public Boolean getGenerateRateCodeStatsEnabled() {
        return generateRateCodeStatsEnabled;
    }

    public void setGenerateRateCodeStatsEnabled(Boolean generateRateCodeStatsEnabled) {
        this.generateRateCodeStatsEnabled = generateRateCodeStatsEnabled;
    }

    public Boolean getResetVirtualSuiteCounts() {
        return resetVirtualSuiteCounts;
    }

    public void setResetVirtualSuiteCounts(Boolean resetVirtualSuiteCounts) {
        this.resetVirtualSuiteCounts = resetVirtualSuiteCounts;
    }

    public Boolean getGenerateHotelActivityFromRoomTypeActivity() {
        return generateHotelActivityFromRoomTypeActivity;
    }

    public void setGenerateHotelActivityFromRoomTypeActivity(Boolean generateHotelActivityFromRoomTypeActivity) {
        this.generateHotelActivityFromRoomTypeActivity = generateHotelActivityFromRoomTypeActivity;
    }
}
