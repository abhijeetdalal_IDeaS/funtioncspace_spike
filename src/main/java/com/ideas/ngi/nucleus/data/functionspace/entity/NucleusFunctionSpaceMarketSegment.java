package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.nucleus.data.AuditableEntity;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.integration.annotation.ClientCode;
import com.ideas.ngi.nucleus.integration.annotation.PropertyCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Document
public class NucleusFunctionSpaceMarketSegment extends AuditableEntity implements Serializable {

    private static final long serialVersionUID = 7676722421016855037L;

    @Id
    private String id;

    @NotNull
    private IntegrationType integrationType;

    @ClientCode
    private String clientCode;

    @PropertyCode
    private String propertyCode;

    @NotNull
    private String propertyId;
    @NotNull
    private String abbreviation;
    @NotNull
    private String marketSegmentId;

    private String name;
    private String description;

    private Date salesAndCateringLastModified;
    private String parentMarketSegmentId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public IntegrationType getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(IntegrationType integrationType) {
        this.integrationType = integrationType;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getSalesAndCateringLastModified() {
        return salesAndCateringLastModified;
    }

    public void setSalesAndCateringLastModified(Date salesAndCateringLastModified) {
        this.salesAndCateringLastModified = salesAndCateringLastModified;
    }

    public String getParentMarketSegmentId() {
        return parentMarketSegmentId;
    }

    public void setParentMarketSegmentId(String parentMarketSegmentId) {
        this.parentMarketSegmentId = parentMarketSegmentId;
    }

    public String getMarketSegmentId() {
        return marketSegmentId;
    }

    public void setMarketSegmentId(String marketSegmentId) {
        this.marketSegmentId = marketSegmentId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }
}
