package com.ideas.ngi.nucleus.data.integration.dto;

import com.ideas.ngi.nucleus.data.integration.Setting;
import com.ideas.ngi.nucleus.data.integration.SettingsNotFoundException;
import com.ideas.ngi.nucleus.data.vendor.entity.VendorConfigType;

import java.util.Optional;

public class EmptySettingsContext implements Settings {
    static final String UNKNOWN_VENDOR_ID = "ERROR:UNKNOWN";
    private final String clientCode;
    private final String propertyCode;

    public EmptySettingsContext(String clientCode, String propertyCode) {
        this.clientCode = clientCode;
        this.propertyCode = propertyCode;
    }

    @Override
    public String getVendorId() {
        return UNKNOWN_VENDOR_ID;
    }

    @Override
    public String getClientCode() { return clientCode; }

    @Override
    public String getPropertyCode() { return propertyCode; }

    @Override
    public <T> Optional<T> getValue(Setting<T> attribute) {
        return Optional.empty();
    }

    @Override
    public <T> Optional<T> getValue(VendorConfigType vendorConfigType) {
        return Optional.empty();
    }

    @Override
    public <T> SettingsContext setValue(Setting<T> attribute, T value) {
        throw new SettingsNotFoundException("No settings found for given vendor and property");
    }

    @Override
    public SettingsContext setValue(VendorConfigType vendorConfigType, Object value) {
        throw new SettingsNotFoundException("No settings found for given vendor and property");
    }

    @Override
    public boolean exists() {
        return false;
    }
}
