package com.ideas.ngi.nucleus.data.vendor.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document
public class VendorConfiguration implements Serializable {

    private VendorConfigType type;
    private Object value;

    public VendorConfiguration(VendorConfigType type, Object value) {
        this.type = type;
        this.value = value;
    }

    private boolean validateValue(Object value) {
        return type.getValidValues().contains(value);
    }

    public VendorConfigType getType() {
        return type;
    }

    public void setType(VendorConfigType type) {
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        if (validateValue(value)) {
            this.value = value;
        }
    }
}
