package com.ideas.ngi.nucleus.data.integration;

import com.ideas.ngi.nucleus.exception.NucleusException;

public class SettingsNotFoundException extends NucleusException {

    public SettingsNotFoundException(String message) {
        super(message);
    }

}
