package com.ideas.ngi.nucleus.data.integration.repository;

import com.ideas.ngi.nucleus.data.integration.entity.IntegrationPropertyConfig;

public interface IntegrationPropertyConfigRepository extends BaseIntegrationConfigRepository<IntegrationPropertyConfig> {
}
