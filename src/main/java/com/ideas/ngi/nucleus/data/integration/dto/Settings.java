package com.ideas.ngi.nucleus.data.integration.dto;

import com.ideas.ngi.nucleus.data.integration.Setting;
import com.ideas.ngi.nucleus.data.integration.SettingNotFoundException;
import com.ideas.ngi.nucleus.data.vendor.entity.VendorConfigType;

import java.util.Optional;

public interface Settings {

    String getVendorId();
    String getClientCode();
    String getPropertyCode();

    <T> Optional<T> getValue(Setting<T> attribute);
    <T> Optional<T> getValue(VendorConfigType attribute);

    <T> Settings setValue(Setting<T> attribute, T value);
    Settings setValue(VendorConfigType vendorConfigType, Object value);

    boolean exists();

    default <T> T getRequiredValue(Setting<T> attribute) {
        Optional<T> value = getValue(attribute);
        return value.orElseThrow(() -> new SettingNotFoundException(getVendorId(), getClientCode(), getPropertyCode(), attribute.getPropertyName()));
    }

    default <T> T getRequiredValue(VendorConfigType attribute) {
        Optional<T> value = getValue(attribute);
        return value.orElseThrow(() -> new SettingNotFoundException(getVendorId(), getClientCode(), getPropertyCode(), attribute.getName()));
    }

    default <T> T getValue(Setting<T> attribute, T defaultValue) {
        Optional<T> value = getValue(attribute);
        return value.orElse(defaultValue);
    }

    default <T> T getValue(VendorConfigType attribute, T defaultValue) {
        Optional<T> value = getValue(attribute);
        return value.orElse(defaultValue);
    }
}
