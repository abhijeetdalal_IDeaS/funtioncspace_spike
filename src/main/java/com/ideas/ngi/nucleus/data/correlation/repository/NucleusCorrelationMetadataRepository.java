package com.ideas.ngi.nucleus.data.correlation.repository;

import com.ideas.ngi.nucleus.config.mongo.repository.NucleusMongoRepository;
import com.ideas.ngi.nucleus.data.correlation.entity.CorrelationStatus;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.correlation.entity.NucleusCorrelationMetadata;
import com.ideas.ngi.nucleus.util.DateTimeUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.ideas.ngi.functionspace.constant.Constants.PARAM_CLIENT_CODE;

/**
 * CorrelationMetadataRepository is used for CRUD operations on the nucleus correlation
 * metadata entity and is exposes the collection via REST.
 */
@RepositoryRestResource
public interface NucleusCorrelationMetadataRepository extends NucleusMongoRepository<NucleusCorrelationMetadata, String> {

    List<NucleusCorrelationMetadata> findByIntegrationTypeAndLastModifiedDateGreaterThanEqualAndCorrelationStatusOrderByLastModifiedDateDesc(@Param("integrationType") IntegrationType integrationType,
                                                                                                                                             @Param("lastModifiedDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
                                                                                                                                             @Param("correlationStatus") CorrelationStatus correlationStatuses);

    List<NucleusCorrelationMetadata> findByIntegrationTypeAndSendingSystemPropertyIdAndCorrelationStatus(@Param("integrationType") IntegrationType integrationType,
                                                                                                         @Param("sendingSystemPropertyId") String sendingSystemPropertyId,
                                                                                                         @Param("correlationStatus") CorrelationStatus correlationStatuses);

    List<NucleusCorrelationMetadata> findByIntegrationTypeAndSendingSystemPropertyIdAndCorrelationStatusNotIn(@Param("integrationType") IntegrationType integrationType,
                                                                                                              @Param("sendingSystemPropertyId") String sendingSystemPropertyId,
                                                                                                              @Param("correlationStatuses") Set<CorrelationStatus> correlationStatuses);

    List<NucleusCorrelationMetadata> findByIntegrationTypeAndClientCodeAndNameIn(@Param("integrationType") IntegrationType integrationType,
                                                                                 @Param(PARAM_CLIENT_CODE) String clientCode,
                                                                                 @Param("name") Set<String> name);

    @RestResource(path = "findOneByIntegrationTypeAndSendingSystemPropertyIdAndName", rel = "findOneByIntegrationTypeAndSendingSystemPropertyIdAndName")
    NucleusCorrelationMetadata findFirstByIntegrationTypeAndSendingSystemPropertyIdAndName(@Param("integrationType") IntegrationType integrationType,
                                                                                           @Param("sendingSystemPropertyId") String sendingSystemPropertyId,
                                                                                           @Param("name") String name);

    @RestResource(path = "findOneByIntegrationTypeAndSendingSystemPropertyIdAndCorrelationStatusOrderByCaptureDateDesc", rel = "findOneByIntegrationTypeAndSendingSystemPropertyIdAndCorrelationStatusOrderByCaptureDateDesc")
    NucleusCorrelationMetadata findFirstByIntegrationTypeAndSendingSystemPropertyIdAndCorrelationStatusOrderByCaptureDateDesc(@Param("integrationType") IntegrationType integrationType,
                                                                                                                              @Param("sendingSystemPropertyId") String sendingSystemPropertyId,
                                                                                                                              @Param("correlationStatus") CorrelationStatus correlationStatus);

    @RestResource(path = "findOneByIntegrationTypeAndSendingSystemPropertyIdAndNameAndCorrelationStatus", rel = "findOneByIntegrationTypeAndSendingSystemPropertyIdAndNameAndCorrelationStatus")
    NucleusCorrelationMetadata findFirstByIntegrationTypeAndSendingSystemPropertyIdAndNameAndCorrelationStatus(@Param("integrationType") IntegrationType integrationType,
                                                                                                               @Param("sendingSystemPropertyId") String sendingSystemPropertyId,
                                                                                                               @Param("name") String name,
                                                                                                               @Param("correlationStatus") CorrelationStatus correlationStatus);

    Long deleteByIntegrationTypeAndSendingSystemPropertyIdAndName(@Param("integrationType") IntegrationType integrationType,
                                                                  @Param("sendingSystemPropertyId") String sendingSystemPropertyId,
                                                                  @Param("name") String name);

    Long deleteByIntegrationTypeAndSendingSystemPropertyId(@Param("integrationType") IntegrationType integrationType,
                                                           @Param("sendingSystemPropertyId") String sendingSystemPropertyId);

    Page<NucleusCorrelationMetadata> findByIntegrationTypeAndSendingSystemPropertyIdAndCorrelationStatusAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(@Param("integrationType") IntegrationType integrationType,
                                                                                                                                                             @Param("sendingSystemPropertyId") String sendingSystemPropertyId,
                                                                                                                                                             @Param("correlationStatus") CorrelationStatus correlationStatus,
                                                                                                                                                             @Param("startDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) Date startDate,
                                                                                                                                                             @Param("endDate") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_FORMAT) Date endDate,
                                                                                                                                                             Pageable pageable);

    @RestResource(path = "findOneByIntegrationTypeAndSendingSystemPropertyIdOrderByCreateDateDesc", rel = "findOneByIntegrationTypeAndSendingSystemPropertyIdOrderByCreateDateDesc")
    NucleusCorrelationMetadata findFirstByIntegrationTypeAndSendingSystemPropertyIdOrderByCreateDateDesc(IntegrationType integrationType,
                                                                                                         String sendingSystemPropertyId);

    @RestResource(path = "findOneByIntegrationTypeAndSendingSystemPropertyIdOrderByCaptureDateDesc", rel = "findOneByIntegrationTypeAndSendingSystemPropertyIdOrderByCaptureDateDesc")
    NucleusCorrelationMetadata findFirstByIntegrationTypeAndSendingSystemPropertyIdOrderByCaptureDateDesc(@Param("integrationType") IntegrationType integrationType,
                                                                                                          @Param("sendingSystemPropertyId") String sendingSystemPropertyId);

    @RestResource(path = "findOneByIntegrationTypeAndSendingSystemPropertyIdAndCorrelationStatusInOrderByCaptureDateDesc", rel = "findOneByIntegrationTypeAndSendingSystemPropertyIdAndCorrelationStatusInOrderByCaptureDateDesc")
    List<NucleusCorrelationMetadata> findFirst1ByIntegrationTypeAndSendingSystemPropertyIdAndCorrelationStatusInOrderByCaptureDateDesc(@Param("integrationType") IntegrationType integrationType,
                                                                                                                                       @Param("sendingSystemPropertyId") String sendingSystemPropertyId,
                                                                                                                                       @Param("correlationStatuses") Set<CorrelationStatus> correlationStatuses);

    NucleusCorrelationMetadata findFirstByIntegrationTypeAndOriginalFilePath(@Param("integrationType") IntegrationType integrationType, @Param("originalFilePath") String originalFilePath);

    List<NucleusCorrelationMetadata> findByIntegrationTypeAndClientCodeAndPropertyCodeAndCorrelationStatusOrderByCaptureDateDesc(@Param("integrationType") IntegrationType integrationType,
                                                                                                                                 @Param("clientCode") String clientCode,
                                                                                                                                 @Param("propertyCode") String propertyCode,
                                                                                                                                 @Param("correlationStatus") CorrelationStatus correlationStatuses);

}

