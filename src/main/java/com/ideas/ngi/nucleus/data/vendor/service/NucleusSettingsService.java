package com.ideas.ngi.nucleus.data.vendor.service;

import com.ideas.ngi.nucleus.config.internalclient.ClientEnvironment;
import com.ideas.ngi.nucleus.data.correlation.entity.BusinessModule;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.integration.Setting;
import com.ideas.ngi.nucleus.data.integration.dto.Settings;
import com.ideas.ngi.nucleus.data.integration.service.IntegrationSettingsService;
import com.ideas.ngi.nucleus.exception.NucleusException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

import static com.ideas.ngi.nucleus.data.integration.Setting.*;

@Component
public class NucleusSettingsService {

    private final IntegrationSettingsService settingsService;

    @Autowired
    public NucleusSettingsService(final IntegrationSettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public Settings getSettings(String clientCode, String propertyCode) {
        Set<IntegrationType> integrationTypes = IntegrationType.byBusinessModule(BusinessModule.RESERVATION);
        return settingsService.getSettings(clientCode, propertyCode, integrationTypes);
    }

    public Settings getSettings(String clientCode, String propertyCode, boolean checked) {
        Set<IntegrationType> integrationTypes = IntegrationType.byBusinessModule(BusinessModule.RESERVATION);
        Settings settings = settingsService.getSettings(clientCode, propertyCode, integrationTypes);
        if (checked && !settings.exists()) {
            // Raise a Problem in G3 Support UI, if VendorConfig cannot be found...
            throw new NucleusException(String.format("Integration Settings not found for Client : %s, Property : %s",
                    clientCode, propertyCode));
        }

        return settings;
    }

    public String getClientEnvironmentName(String clientCode, String propertyCode, IntegrationType payloadIntegrationType) {
        Set<IntegrationType> integrationTypes = IntegrationType.byBusinessModule(BusinessModule.RESERVATION);
        if(shouldFetchFromDB(payloadIntegrationType, integrationTypes)) {
            Settings settings = settingsService.getSettings(clientCode, propertyCode, integrationTypes);
            return getClientEnvironmentName(settings);
        }
        return null;
    }

    public ClientEnvironment getClientEnvironment(String clientCode, String propertyCode) {
        return new ClientEnvironment(getClientEnvironmentName(clientCode, propertyCode, IntegrationType.UNKNOWN));
    }

    private boolean shouldFetchFromDB(IntegrationType payloadIntegrationType, Set<IntegrationType> integrationTypes) {
        return integrationTypes.contains(payloadIntegrationType) || IntegrationType.UNKNOWN == payloadIntegrationType;
    }

    public String getClientEnvironmentName(Settings settings) {
        return settings.getValue(CLIENT_ENVIRONMENT_NAME).orElse(null);
    }

    public Boolean isUnqualifiedRatesDirectPopulationDisabled(Settings settings) {
        return settings.getValue(UNQUALIFIED_RATES_DIRECT_POPULATION_DISABLED).orElse(false);
    }

    public Boolean isQualifiedRatesDirectPopulationDisabled(Settings settings) {
        return settings.getValue(QUALIFIED_RATES_DIRECT_POPULATION_DISABLED).orElse(false);
    }

    public Boolean getCalculateNonPickedUpBlocksUsingSummaryData(String clientCode, String propertyCode) {
        Settings settings = getSettings(clientCode, propertyCode);
        return settings.getValue(Setting.CALCULATE_NON_PICKED_UP_BLOCKS_USING_SUMMARY_DATA).orElse(null);
    }

    public Boolean isHandlePreviouslyStraightMarketSegmentsInAms(String clientCode, String propertyCode) {
        Settings settings = getSettings(clientCode, propertyCode);
        return settings.getValue(Setting.HANDLE_PREVIOUSLY_STRAIGHT_MARKET_SEGMENTS_IN_AMS).orElse(false);
    }

    public Boolean isScheduledDeferredDelivery(Settings settings) {
        return settings.getValue(SCHEDULED_DEFERRED_DELIVERY).orElse(Boolean.FALSE);
    }

    public Integer getFutureDays(String clientCode, String propertyCode) {
        Settings settings = getSettings(clientCode, propertyCode);
        return settings.getValue(FUTURE_DAYS).orElse(null);
    }

    public Integer getFutureDays(Settings settings) {
        return settings.getValue(FUTURE_DAYS).orElse(null);
    }

    public Integer getPastDays(String clientCode, String propertyCode) {
        Settings settings = getSettings(clientCode, propertyCode);
        return settings.getValue(PAST_DAYS).orElse(null);
    }

    public Boolean isIncludePseudoInRevenue(Settings settings) {
        return settings.getValue(INCLUDE_PSEUDO_IN_REVENUE).orElse(Boolean.FALSE);
    }

    public Boolean isIncludeDayUseInRevenue(Settings settings) {
        return settings.getValue(INCLUDE_DAY_USE_IN_REVENUE).orElse(Boolean.FALSE);
    }

    public Boolean isIncludeNoShowInRevenue(Settings settings) {
        return settings.getValue(INCLUDE_NO_SHOW_IN_REVENUE).orElse(Boolean.FALSE);
    }

    public boolean isPreventPseudoDataInActivity(String clientCode, String propertyCode) {
        Settings settings = getSettings(clientCode, propertyCode);
        return settings.getValue(PREVENT_PSEUDO_DATA_IN_ACTIVITY).orElse(true);
    }

    public Boolean isIncludeRoomTypeHotelMarketSegmentActivity(String clientCode, String propertyCode) {
        Settings settings = getSettings(clientCode, propertyCode);
        return settings.getValue(INCLUDE_ROOM_TYPE_HOTEL_MARKET_SEGMENT_ACTIVITY).orElse(Boolean.FALSE);
    }

    public Boolean isGenerateMarketSegmentStatsEnabled(String clientCode, String propertyCode) {
        Settings settings = getSettings(clientCode, propertyCode);
        return settings.getValue(GENERATE_MARKET_SEGMENT_STATS_ENABLED).orElse(Boolean.TRUE);
    }

    public Boolean isBuildMSActivityUsingPMSMS(String clientCode, String propertyCode) {
        Settings settings = getSettings(clientCode, propertyCode);
        return settings.getValue(BUILD_MS_ACTIVITY_USING_PMS_MS).orElse(Boolean.FALSE);
    }

    public Boolean isSkipRemap(String clientCode, String propertyCode) {
        Settings settings = getSettings(clientCode, propertyCode);
        return settings.getValue(SKIP_REMAP).orElse(Boolean.FALSE);
    }

    public Boolean isSummaryPersistenceEnabled(String clientCode, String propertyCode) {
        /*
         We're moving away from storing summary info either when provided (via HTNG) or
         calculating and storing it (via FOLS & OXI) in favor of G3 computing it directly from
         reservation information.
         */
        Settings settings = getSettings(clientCode, propertyCode);
        return settings.getValue(SUMMARY_PERSISTENCE_ENABLED).orElse(Boolean.TRUE);
    }

    public Boolean isMSRTSummaryPersistenceEnabled(String clientCode, String propertyCode) {
        Settings settings = getSettings(clientCode, propertyCode);
        return settings.getValue(MSRT_SUMMARY_PERSISTENCE_ENABLED).orElse(Boolean.TRUE);
    }

    public Boolean isHotelActivityGeneratedFromRoomTypeActivity(String clientCode, String propertyCode){
        Settings settings = getSettings(clientCode, propertyCode);
        return settings.getValue(GENERATE_HOTEL_ACTIVITY_FROM_ROOM_TYPE_ACTIVITY).orElse(Boolean.FALSE);
    }

    public String getGenericScUnitOfMeasure(String clientCode, String propertyCode) {
        Settings settings = getSettings(clientCode, propertyCode);
        return settings.getValue(SALES_AND_CATERING_UNIT_OF_MEASURE).orElse(null);
    }

    public String getGenericScStatus(String clientCode, String propertyCode) {
        Settings settings = getSettings(clientCode, propertyCode);
        return settings.getValue(SALES_AND_CATERING_STATUS).orElse(null);
    }

}
