package com.ideas.ngi.nucleus.data.vendor.repository;

import com.ideas.ngi.nucleus.config.mongo.annotation.GlobalCollection;
import com.ideas.ngi.nucleus.config.mongo.repository.NucleusMongoRepository;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.vendor.entity.NucleusVendorConfigParams;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@GlobalCollection
@RepositoryRestResource
public interface NucleusVendorConfigParamsRepository extends NucleusMongoRepository<NucleusVendorConfigParams, String> {
    String VALID_INTEGRATION_CHAIN_HOTEL_QUERY = "{'integrationType' : ?0, 'chains' : {$elemMatch: {chainCode: ?1}}, "
            + "'chains.hotels' : {$elemMatch: {hotelCode: ?2}}}";

    String VENDOR_CHAIN_HOTEL_QUERY = "{'inboundVendorId' : ?0, 'chains' : {$elemMatch: {chainCode: ?1}}, "
            + "'chains.hotels' : {$elemMatch: {hotelCode: ?2}}}";

    String CHAIN_CODE_AND_PROPERTY_CODE = "{'chains' : {$elemMatch: {chainCode: ?0}}, "
            + "'chains.hotels' : {$elemMatch: {hotelCode: ?1}}}";

    String BY_INTEGRATION_TYPE_AND_CHAIN_CODE = "{'integrationType' : ?0, 'chains': {$elemMatch : {chainCode: ?1}}}";

    @Query("{'_id' : ?0, " + "'chains' : {$elemMatch: {chainCode: ?1}}, "
            + "'chains.hotels' : {$elemMatch: {hotelCode: ?2}}}")
    NucleusVendorConfigParams findValidVendorChainHotel(String vendorId, String chainCode, String hotelCode);

    @Query(VALID_INTEGRATION_CHAIN_HOTEL_QUERY)
    List<NucleusVendorConfigParams> findValidIntegrationTypeChainHotel(
            @Param("integrationType") IntegrationType integrationType,
            @Param("chainCode") String chainCode,
            @Param("hotelCode") String hotelCode);

    @Query(VENDOR_CHAIN_HOTEL_QUERY)
    NucleusVendorConfigParams findVendorChainHotelByInboundVendorId(
            @Param("inboundVendorId") String inboundVendorId,
            @Param("chainCode") String chainCode,
            @Param("hotelCode") String hotelCode);

    @Query(value = VALID_INTEGRATION_CHAIN_HOTEL_QUERY, count = true)
    long countValidIntegrationTypeChainHotel(IntegrationType integrationType, String chainCode, String hotelCode);

    @Query("{'integrationType' : ?0, " + "'chains' : {$elemMatch: {chainCode: ?1}}}")
    List<NucleusVendorConfigParams> findValidIntegrationTypeChain(IntegrationType integrationType, String chainCode);

    @Query("{'inboundVendorId' : ?0, " + "'chains' : {$elemMatch: {chainCode: ?1}}}")
    NucleusVendorConfigParams findVendorChain(String inboundVendorId, String chainCode);

    @Query(CHAIN_CODE_AND_PROPERTY_CODE)
    List<NucleusVendorConfigParams> findVendorChainHotel(String chainCode, String hotelCode);

    @RestResource
    NucleusVendorConfigParams findByInboundVendorId(@Param("inboundVendorId") String inboundVendorId);

    @RestResource(path = "findByIntegrationType", rel = "findByIntegrationType")
    NucleusVendorConfigParams findFirstByIntegrationType(
            @Param("integrationType") IntegrationType integrationType);

    @Query("{'_id' : ?0, 'chains.hotels' : {$elemMatch: {inboundHotelCode: ?1}}}")
    NucleusVendorConfigParams findValidVendorInboundHotel(String vendorId, String hotelCode);

    @Query(BY_INTEGRATION_TYPE_AND_CHAIN_CODE)
    List<NucleusVendorConfigParams> findByIntegrationAndChainCode(String integrationType, String chainCode);
}
