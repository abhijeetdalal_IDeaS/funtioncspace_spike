package com.ideas.ngi.nucleus.data.correlation.entity;

public enum BusinessModule {
    FUNCTION_SPACE,
    STR,
    DEMAND_360,
    RESERVATION,
    FOLS,
    RRA,
    REVENUE_STREAMS
}
