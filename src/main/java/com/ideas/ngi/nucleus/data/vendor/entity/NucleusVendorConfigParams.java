package com.ideas.ngi.nucleus.data.vendor.entity;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.exception.NucleusException;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

@Document
public class NucleusVendorConfigParams implements Serializable {
    private static final long serialVersionUID = -5157393935872816732L;

    @Id
    private String inboundVendorId;
    private String outboundVendorId;
    private String customReplyToAddress;
    private Boolean useCustomSoapAction;
    private String name;
    private VendorCredentials inboundCredentials;
    private VendorCredentials outboundCredentials;
    private IntegrationType integrationType;
    private List<ChainConfigParams> chains;
    private List<VendorConfiguration> configurations;
    private Boolean unqualifiedRatesDirectPopulationDisabled;
    private Boolean qualifiedRatesDirectPopulationDisabled;
    private Boolean cancelMultiUnitDecrements;
    private Integer installationReservationsThreshold;
    private Boolean oxiRoomStayReservationByDay;
    private Boolean htngRoomStayReservationByDay;
    private Boolean folsRoomStayReservationByDay;
    private Boolean htngUseBasicAuth;
    private Boolean ratePlanCleanupEnabled;
    private Boolean dedicatedValidationResponseChannel;
    private Boolean includeRoomTypeHotelMarketSegmentActivity;
    private Boolean sendHTNGCallbackRequest;
    private Boolean preventPseudoDataInActivity;
    private Boolean isComponentRoomsActivityEnabled;
    private Boolean useLegacyRoomStayHandling;
    private Boolean generateMarketSegmentStatsEnabled;
    private Boolean generateRateCodeStatsEnabled;
    private Boolean resetVirtualSuiteCounts;
    private Boolean generateHotelActivityFromRoomTypeActivity;

    @Getter
    @Setter
    private String salesAndCateringUnitOfMeasure;

    @Getter
    @Setter
    private String salesAndCateringStatus;

    @Getter
    @Setter
    private Boolean searchAlternateMarketSegmentLocationforHtng;

    @Getter
    @Setter
    private Boolean summaryPersistenceEnabled;

    @Getter
    @Setter
    private Boolean msrtSummaryPersistenceEnabled;

    @Getter
    @Setter
    private Boolean buildMSActivityUsingPMSMS;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getInboundVendorId() {
        return inboundVendorId;
    }

    public void setInboundVendorId(String inboundVendorId) {
        this.inboundVendorId = inboundVendorId;
    }

    public String getOutboundVendorId() {
        return outboundVendorId;
    }

    public void setOutboundVendorId(String outboundVendorId) {
        this.outboundVendorId = outboundVendorId;
    }

    public String getCustomReplyToAddress() {
        return customReplyToAddress;
    }

    public void setCustomReplyToAddress(String customReplyToAddress) {
        this.customReplyToAddress = customReplyToAddress;
    }

    public Boolean getUseCustomSoapAction() {
        return useCustomSoapAction;
    }

    public void setUseCustomSoapAction(Boolean useCustomSoapAction) {
        this.useCustomSoapAction = useCustomSoapAction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ChainConfigParams> getChains() {
        if (chains == null) {
            chains = new ArrayList<>();
        }
        return chains;
    }

    public void setChains(List<ChainConfigParams> chains) {
        this.chains = chains;
    }

    public ChainConfigParams getChain(String chainCode) {
        return getChains().stream().filter(c -> c.getChainCode().equals(chainCode)).findFirst().orElse(null);
    }

    public IntegrationType getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(IntegrationType integrationType) {
        this.integrationType = integrationType;
    }

    public VendorCredentials getOutboundCredentials() {
        return outboundCredentials;
    }

    public void setOutboundCredentials(VendorCredentials outboundCredentials) {
        this.outboundCredentials = outboundCredentials;
    }

    public VendorCredentials getInboundCredentials() {
        return inboundCredentials;
    }

    public void setInboundCredentials(VendorCredentials inboundCredentials) {
        this.inboundCredentials = inboundCredentials;
    }

    public VendorCredentials findOutboundCredentials(String chainCode, String hotelCode) {
        VendorCredentials credentials = findChainOrHotelOutboundCredentials(chainCode, hotelCode);
        if (credentials == null) {
            if (outboundCredentials != null) {
                return outboundCredentials;
            }
        } else {
            return credentials;
        }

        throw new NucleusException("Unable to retrieve outbound credentials");
    }

    public VendorCredentials findInboundCredentials(String chainCode, String hotelCode) {
        VendorCredentials credentials = findChainOrHotelInboundCredentials(chainCode, hotelCode);
        if (credentials != null) {
            return credentials;
        } else {
            if (inboundCredentials != null) {
                return inboundCredentials;
            }
        }

        throw new NucleusException("Unable to retrieve inbound credentials");
    }

    private VendorCredentials findChainOrHotelOutboundCredentials(String chainCode, String hotelCode) {
        ChainConfigParams chainConfigParams = getChain(chainCode);
        if (chainConfigParams != null) {
            return getChain(chainCode).findOutboundCredentials(hotelCode);
        } else {
            return null;
        }
    }

    private VendorCredentials findChainOrHotelInboundCredentials(String chainCode, String hotelCode) {
        ChainConfigParams chainConfigParams = getChain(chainCode);
        if (chainConfigParams != null) {
            return getChain(chainCode).findInboundCredentials(hotelCode);
        } else {
            return null;
        }
    }

    public HotelConfigParams getHotelConfig(String chainCode, String hotelCode) {
        ChainConfigParams chainConfigParams = getChain(chainCode);
        if (chainConfigParams != null) {
            return chainConfigParams.getHotel(hotelCode);
        }
        return null;
    }

    public String getBaseCurrencyCode(String chainCode, String hotelCode) {
        String baseCurrencyCode = null;

        ChainConfigParams chainConfigParams = getChain(chainCode);
        if (chainConfigParams != null) {
            HotelConfigParams hotelParams = chainConfigParams.getHotel(hotelCode);
            baseCurrencyCode = hotelParams != null ? hotelParams.getBaseCurrencyCode() : null;

            if (StringUtils.isBlank(baseCurrencyCode)) {
                baseCurrencyCode = chainConfigParams.getBaseCurrencyCode();
            }
        }

        return baseCurrencyCode;
    }

    public String getClientEnvironmentName(String chainCode) {
        ChainConfigParams chainConfigParams = getChain(chainCode);
        if (chainConfigParams != null) {
            return chainConfigParams.getClientEnvironmentName();
        }
        return null;
    }

    public Boolean getAssumeTaxIncluded(String chainCode, String hotelCode) {
        return getHotelConfigParam(chainCode, hotelCode, HotelConfigParams::getAssumeTaxIncluded);
    }

    public Boolean getAssumePackageIncluded(String chainCode, String hotelCode) {
        return getHotelConfigParam(chainCode, hotelCode, HotelConfigParams::getAssumePackageIncluded);
    }

    public Boolean getScheduledDeferredDelivery(String chainCode, String hotelCode) {
        return getHotelConfigParam(chainCode, hotelCode, HotelConfigParams::getScheduledDeferredDelivery);
    }

    public void setScheduledDeferredDelivery(String chainCode, String hotelCode, Boolean scheduledDeferredDelivery) {
        setHotelConfigParam(chainCode, hotelCode, hotelConfigParams -> hotelConfigParams.setScheduledDeferredDelivery(scheduledDeferredDelivery));
    }

    public List<VendorConfiguration> getConfigurations() {
        return configurations;
    }

    public VendorConfiguration addConfiguration(VendorConfigType name, Object value) {

        if (name == null || value == null) {
            return null;
        }

        if (configurations == null) {
            configurations = new ArrayList<>();
        }

        //update existing config
        for (VendorConfiguration configuration : configurations) {
            VendorConfigType type = configuration.getType();
            if (type.equals(name)) {
                configuration.setValue(value);
                return configuration;
            }
        }

        //create new configuration
        VendorConfiguration addedConfiguration = new VendorConfiguration(name, value);
        configurations.add(addedConfiguration);

        return addedConfiguration;
    }

    public Optional<VendorConfiguration> getConfiguration(VendorConfigType name) {
        if (configurations != null) {
            for (VendorConfiguration configuration : configurations) {
                if (configuration.getType().equals(name)) {
                    return Optional.of(configuration);
                }
            }
        }
        return Optional.empty();
    }

    public Boolean getUnqualifiedRatesDirectPopulationDisabled() {
        return unqualifiedRatesDirectPopulationDisabled;
    }

    public void setUnqualifiedRatesDirectPopulationDisabled(Boolean unqualifiedRatesDirectPopulationDisabled) {
        this.unqualifiedRatesDirectPopulationDisabled = unqualifiedRatesDirectPopulationDisabled;
    }

    public Boolean getQualifiedRatesDirectPopulationDisabled() {
        return qualifiedRatesDirectPopulationDisabled;
    }

    public void setQualifiedRatesDirectPopulationDisabled(Boolean qualifiedRatesDirectPopulationDisabled) {
        this.qualifiedRatesDirectPopulationDisabled = qualifiedRatesDirectPopulationDisabled;
    }

    public Boolean findQualifiedRatesDirectPopulationDisabled(String chainCode, String hotelCode) {
        return getVendorChainOrHotelConfigParam(chainCode, hotelCode, ChainConfigParams::getQualifiedRatesDirectPopulationDisabled, HotelConfigParams::getQualifiedRatesDirectPopulationDisabled, qualifiedRatesDirectPopulationDisabled);
    }

    public Boolean findUnqualifiedRatesDirectPopulationDisabled(String chainCode, String hotelCode) {
        return getVendorChainOrHotelConfigParam(chainCode, hotelCode, ChainConfigParams::getUnqualifiedRatesDirectPopulationDisabled, HotelConfigParams::getUnqualifiedRatesDirectPopulationDisabled, unqualifiedRatesDirectPopulationDisabled);
    }

    public Boolean getCancelMultiUnitDecrements() {
        return cancelMultiUnitDecrements;
    }

    public void setCancelMultiUnitDecrements(Boolean cancelMultiUnitDecrements) {
        this.cancelMultiUnitDecrements = cancelMultiUnitDecrements;
    }

    public Integer getInstallationReservationsThreshold() {
        return installationReservationsThreshold;
    }

    public void setInstallationReservationsThreshold(Integer installationReservationsThreshold) {
        this.installationReservationsThreshold = installationReservationsThreshold;
    }

    public Integer findInstallationReservationsThreshold(String chainCode, String hotelCode) {
        return getVendorChainOrHotelConfigParam(chainCode, hotelCode, ChainConfigParams::getInstallationReservationsThreshold, HotelConfigParams::getInstallationReservationsThreshold, installationReservationsThreshold);
    }

    public Boolean getInstallMode(String chainCode, String hotelCode) {
        return getHotelConfigParam(chainCode, hotelCode, HotelConfigParams::getInstallMode);
    }

    public void setInstallMode(String chainCode, String hotelCode, Boolean installMode) {
        setHotelConfigParam(chainCode, hotelCode, hotelConfigParams -> hotelConfigParams.setInstallMode(installMode));
    }

    public Boolean getInCatchupMode(String chainCode, String hotelCode) {
        return getHotelConfigParam(chainCode, hotelCode, HotelConfigParams::getInCatchup);
    }

    public void setInCatchup(String chainCode, String hotelCode, Boolean inCatchup) {
        setHotelConfigParam(chainCode, hotelCode, hotelConfigParams -> hotelConfigParams.setInCatchup(inCatchup));
    }

    public Boolean getCalculateNonPickedUpBlocksUsingSummaryData(String chainCode, String hotelCode) {
        return getHotelConfigParam(chainCode, hotelCode, HotelConfigParams::getCalculateNonPickedUpBlocksUsingSummaryData);
    }

    public void setCalculateNonPickedUpBlocksUsingSummaryData(String chainCode, String hotelCode, Boolean calculateNonPickedUpBlocksUsingSummaryData) {
        setHotelConfigParam(chainCode, hotelCode, hotelConfigParams -> hotelConfigParams.setCalculateNonPickedUpBlocksUsingSummaryData(calculateNonPickedUpBlocksUsingSummaryData));
    }

    public Boolean getHandlePreviouslyStraightMarketSegmentsInAms(String chainCode, String hotelCode) {
        return getHotelConfigParam(chainCode, hotelCode, HotelConfigParams::getHandlePreviouslyStraightMarketSegmentsInAms);
    }

    public String getDefaultRoomType(String chainCode, String hotelCode) {
        return getHotelConfigParam(chainCode, hotelCode, HotelConfigParams::getDefaultRoomType);
    }

    public String getRoomRevenuePackages(String chainCode, String hotelCode) {
        return getHotelConfigParam(chainCode, hotelCode, HotelConfigParams::getRoomRevenuePackages);
    }

    public Boolean getOxiRoomStayReservationByDay() {
        return oxiRoomStayReservationByDay;
    }

    public void setOxiRoomStayReservationByDay(Boolean oxiRoomStayReservationByDay) {
        this.oxiRoomStayReservationByDay = oxiRoomStayReservationByDay;
    }

    public Boolean getHtngRoomStayReservationByDay() {
        return htngRoomStayReservationByDay;
    }

    public void setHtngRoomStayReservationByDay(Boolean htngRoomStayReservationByDay) {
        this.htngRoomStayReservationByDay = htngRoomStayReservationByDay;
    }

    public Boolean getFolsRoomStayReservationByDay() {
        return folsRoomStayReservationByDay;
    }

    public void setFolsRoomStayReservationByDay(Boolean folsRoomStayReservationByDay) {
        this.folsRoomStayReservationByDay = folsRoomStayReservationByDay;
    }

    public Boolean getHtngUseBasicAuth() {
        return htngUseBasicAuth;
    }

    public void setHtngUseBasicAuth(Boolean htngUseBasicAuth) {
        this.htngUseBasicAuth = htngUseBasicAuth;
    }

    public Boolean getRatePlanCleanupEnabled(String chainCode, String hotelCode) {
        return getVendorChainOrHotelConfigParam(chainCode, hotelCode, ChainConfigParams::getRatePlanCleanupEnabled, HotelConfigParams::getRatePlanCleanupEnabled, ratePlanCleanupEnabled);
    }

    public Boolean getRatePlanCleanupEnabled() {
        return ratePlanCleanupEnabled;
    }

    public void setRatePlanCleanupEnabled(Boolean ratePlanCleanupEnabled) {
        this.ratePlanCleanupEnabled = ratePlanCleanupEnabled;
    }

    public Boolean getDedicatedValidationResponseChannel() {
        return dedicatedValidationResponseChannel;
    }

    public void setDedicatedValidationResponseChannel(Boolean dedicatedValidationResponseChannel) {
        this.dedicatedValidationResponseChannel = dedicatedValidationResponseChannel;
    }

    public Boolean getIncludeRoomTypeHotelMarketSegmentActivity() {
        return includeRoomTypeHotelMarketSegmentActivity;
    }

    public void setIncludeRoomTypeHotelMarketSegmentActivity(Boolean includeRoomTypeHotelMarketSegmentActivity) {
        this.includeRoomTypeHotelMarketSegmentActivity = includeRoomTypeHotelMarketSegmentActivity;
    }

    private <T> T getVendorChainOrHotelConfigParam(String chainCode, String hotelCode,
                                                   Function<ChainConfigParams, T> chainFunction,
                                                   Function<HotelConfigParams, T> hotelFunction,
                                                   T vendorConfigValue) {
        // Check at the hotel level first
        T retVal = getHotelConfigParam(chainCode, hotelCode, hotelFunction);

        // If the hotel level has a value, use it
        if (retVal != null) {
            return retVal;
        }

        // Since the hotel level didn't have a value, check the chain
        ChainConfigParams chainConfigParams = getChain(chainCode);
        if (chainConfigParams != null) {
            retVal = chainFunction.apply(chainConfigParams);
        }

        // If the chain level has a value, use it
        if (retVal != null) {
            return retVal;
        }

        // If the hotel and chain didn't have a value, simply return the vendor config value
        return vendorConfigValue;
    }

    private <T> T getHotelConfigParam(String chainCode, String hotelCode, Function<HotelConfigParams, T> function) {
        // Get the ChainConfigParams
        ChainConfigParams chainConfigParams = getChain(chainCode);
        if (chainConfigParams != null) {
            // Get the HotelConfigParams
            HotelConfigParams hotelParams = chainConfigParams.getHotel(hotelCode);

            // If the HotelConfigParams is null, just return null - otherwise return the value for the function
            return hotelParams != null ? function.apply(hotelParams) : null;
        }

        return null;
    }

    private void setHotelConfigParam(String chainCode, String hotelCode, Consumer<HotelConfigParams> function) {
        // Get the ChainConfigParams
        ChainConfigParams chainConfigParams = getChain(chainCode);
        if (chainConfigParams != null) {

            // Look up the HotelCode - if it doesn't exist, create it
            HotelConfigParams hotelParams = chainConfigParams.getHotel(hotelCode);
            if (hotelParams == null) {
                hotelParams = new HotelConfigParams();
                hotelParams.setHotelCode(hotelCode);
                chainConfigParams.getHotels().add(hotelParams);
            }

            // Call the function to set the HotelConfigParam attribute
            function.accept(hotelParams);
        }
    }

    public Boolean getSendHTNGCallbackRequest() {
        return sendHTNGCallbackRequest;
    }

    public void setSendHTNGCallbackRequest(Boolean sendHTNGCallbackRequest) {
        this.sendHTNGCallbackRequest = sendHTNGCallbackRequest;
    }

    public Boolean getPreventPseudoDataInActivity() {
        return preventPseudoDataInActivity;
    }

    public void setPreventPseudoDataInActivity(Boolean preventPseudoDataInActivity) {
        this.preventPseudoDataInActivity = preventPseudoDataInActivity;
    }

    public Boolean getComponentRoomsActivityEnabled() {
        return isComponentRoomsActivityEnabled;
    }

    public void setComponentRoomsActivityEnabled(Boolean componentRoomsActivityEnabled) {
        this.isComponentRoomsActivityEnabled = componentRoomsActivityEnabled;
    }

    public Boolean getUseLegacyRoomStayHandling() {
        return useLegacyRoomStayHandling;
    }

    public void setUseLegacyRoomStayHandling(Boolean useLegacyRoomStayHandling) {
        this.useLegacyRoomStayHandling = useLegacyRoomStayHandling;
    }

    public Boolean getGenerateMarketSegmentStatsEnabled() {
        return generateMarketSegmentStatsEnabled;
    }

    public void setGenerateMarketSegmentStatsEnabled(Boolean generateMarketSegmentStatsEnabled) {
        this.generateMarketSegmentStatsEnabled = generateMarketSegmentStatsEnabled;
    }

    public Boolean getGenerateRateCodeStatsEnabled() {
        return generateRateCodeStatsEnabled;
    }

    public void setGenerateRateCodeStatsEnabled(Boolean generateRateCodeStatsEnabled) {
        this.generateRateCodeStatsEnabled = generateRateCodeStatsEnabled;
    }

    public Boolean getResetVirtualSuiteCounts() {
        return resetVirtualSuiteCounts;
    }

    public void setResetVirtualSuiteCounts(Boolean resetVirtualSuiteCounts) {
        this.resetVirtualSuiteCounts = resetVirtualSuiteCounts;
    }

    public Boolean getGenerateHotelActivityFromRoomTypeActivity() {
        return generateHotelActivityFromRoomTypeActivity;
    }

    public void setGenerateHotelActivityFromRoomTypeActivity(Boolean generateHotelActivityFromRoomTypeActivity) {
        this.generateHotelActivityFromRoomTypeActivity = generateHotelActivityFromRoomTypeActivity;
    }

}