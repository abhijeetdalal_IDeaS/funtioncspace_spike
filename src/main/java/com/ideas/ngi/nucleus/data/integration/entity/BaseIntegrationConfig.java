package com.ideas.ngi.nucleus.data.integration.entity;

import com.google.common.collect.ImmutableMap;
import com.ideas.ngi.nucleus.data.AuditableEntity;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.integration.InvalidSettingsException;
import com.ideas.ngi.nucleus.data.integration.util.VendorPropertyAware;
import com.ideas.ngi.nucleus.data.vendor.entity.VendorConfigType;
import com.ideas.ngi.nucleus.data.vendor.entity.VendorCredentials;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;


@SuppressWarnings("unused")
@Document(collection = BaseIntegrationConfig.COLLECTION_NAME)
public abstract class BaseIntegrationConfig extends AuditableEntity implements Serializable, VendorPropertyAware {

    public static final String COLLECTION_NAME = "nucleusIntegrationConfig";
    public static final String PROPERTIES = "extendedAttributes";

    static final int PRIORITY_VENDOR = 200;
    static final int PRIORITY_CLIENT = 100;
    static final int PRIORITY_PROPERTY = 0;

    private static final Map<Class<? extends BaseIntegrationConfig>, Integer> PRIORITY_MAP =
            ImmutableMap.of(
                IntegrationVendorConfig.class, PRIORITY_VENDOR,
                IntegrationClientConfig.class,  PRIORITY_CLIENT,
                IntegrationPropertyConfig.class, PRIORITY_PROPERTY);


    @Id
    private String id;
    @NotNull
    private IntegrationType integrationType;
    @NotNull
    private String vendorId;
    private VendorCredentials inboundCredentials;
    private VendorCredentials outboundCredentials;
    private Boolean unqualifiedRatesDirectPopulationDisabled;
    private Boolean qualifiedRatesDirectPopulationDisabled;
    private Integer installationReservationsThreshold;
    private Boolean oxiRoomStayReservationByDay;
    private Boolean htngRoomStayReservationByDay;
    private Boolean folsRoomStayReservationByDay;
    private Boolean htngUseBasicAuth;
    private Boolean ratePlanCleanupEnabled;
    private Boolean includeRoomTypeHotelMarketSegmentActivity;
    private Boolean preventPseudoDataInActivity;
    private Boolean isComponentRoomsActivityEnabled;
    private Boolean useLegacyRoomStayHandling;
    private String oxiParkMessageTypes;
    private Boolean generateMarketSegmentStatsEnabled;
    private Boolean generateRateCodeStatsEnabled;
    private Boolean resetVirtualSuiteCounts;
    private Boolean generateHotelActivityFromRoomTypeActivity;

    @Getter
    @Setter
    private String salesAndCateringUnitOfMeasure;

    @Getter
    @Setter
    private String salesAndCateringStatus;

    @Getter
    @Setter
    private Boolean searchAlternateMarketSegmentLocationforHtng;

    @Getter
    @Setter
    private Boolean summaryPersistenceEnabled;

    @Getter
    @Setter
    private Boolean msrtSummaryPersistenceEnabled;

    @Getter
    @Setter
    private Boolean buildMSActivityUsingPMSMS;

    @Version
    private Long version;

    private String baseCurrencyCode;
    @NotNull
    @Field(PROPERTIES)
    @SuppressWarnings("squid:S1948")
    private Map<String, Object> extendedAttributes = new HashMap<>();
    /**
     * Used in querying individual records to determine which values should be taken first
     * if multiple values are configured in the hierarchy
     */
    @NotNull
    private Integer priority;

    BaseIntegrationConfig() {
        priority = PRIORITY_MAP.getOrDefault(this.getClass(), 999);
    }

    BaseIntegrationConfig(String vendorId, IntegrationType integrationType) {
        this();
        this.vendorId = vendorId;
        this.integrationType = integrationType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPriority() {
        return priority;
    }

    public IntegrationType getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(IntegrationType integrationType) {
        this.integrationType = integrationType;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public VendorCredentials getInboundCredentials() {
        return inboundCredentials;
    }

    public void setInboundCredentials(VendorCredentials inboundCredentials) {
        this.inboundCredentials = inboundCredentials;
    }

    public VendorCredentials getOutboundCredentials() {
        return outboundCredentials;
    }

    public void setOutboundCredentials(VendorCredentials outboundCredentials) {
        this.outboundCredentials = outboundCredentials;
    }

    public Boolean getUnqualifiedRatesDirectPopulationDisabled() {
        return unqualifiedRatesDirectPopulationDisabled;
    }

    public void setUnqualifiedRatesDirectPopulationDisabled(Boolean unqualifiedRatesDirectPopulationDisabled) {
        this.unqualifiedRatesDirectPopulationDisabled = unqualifiedRatesDirectPopulationDisabled;
    }

    public Boolean getQualifiedRatesDirectPopulationDisabled() {
        return qualifiedRatesDirectPopulationDisabled;
    }

    public void setQualifiedRatesDirectPopulationDisabled(Boolean qualifiedRatesDirectPopulationDisabled) {
        this.qualifiedRatesDirectPopulationDisabled = qualifiedRatesDirectPopulationDisabled;
    }

    public Integer getInstallationReservationsThreshold() {
        return installationReservationsThreshold;
    }

    public void setInstallationReservationsThreshold(Integer installationReservationsThreshold) {
        this.installationReservationsThreshold = installationReservationsThreshold;
    }

    public Boolean getOxiRoomStayReservationByDay() {
        return oxiRoomStayReservationByDay;
    }

    public void setOxiRoomStayReservationByDay(Boolean oxiRoomStayReservationByDay) {
        this.oxiRoomStayReservationByDay = oxiRoomStayReservationByDay;
    }

    public Boolean getHtngRoomStayReservationByDay() {
        return htngRoomStayReservationByDay;
    }

    public void setHtngRoomStayReservationByDay(Boolean htngRoomStayReservationByDay) {
        this.htngRoomStayReservationByDay = htngRoomStayReservationByDay;
    }

    public Boolean getFolsRoomStayReservationByDay() {
        return folsRoomStayReservationByDay;
    }

    public void setFolsRoomStayReservationByDay(Boolean folsRoomStayReservationByDay) {
        this.folsRoomStayReservationByDay = folsRoomStayReservationByDay;
    }

    public String getBaseCurrencyCode() {
        return baseCurrencyCode;
    }

    public void setBaseCurrencyCode(String baseCurrencyCode) {
        this.baseCurrencyCode = baseCurrencyCode;
    }

    public Boolean getHtngUseBasicAuth() {
        return htngUseBasicAuth;
    }

    public void setHtngUseBasicAuth(Boolean htngUseBasicAuth) {
        this.htngUseBasicAuth = htngUseBasicAuth;
    }

    public Map<VendorConfigType, Object> getExtendedAttributes() {
        return extendedAttributes
            .keySet()
            .stream()
            .map(VendorConfigType::getNameForString)
            .filter(Objects::nonNull)
            .collect(Collectors.toMap(Function.identity(), k -> extendedAttributes.get(k.getName())));
    }

    public void setExtendedAttributes(Map<VendorConfigType, Object> extendedAttributes) {
        this.extendedAttributes = extendedAttributes
            .entrySet()
            .stream()
            .collect(Collectors.toMap(e -> e.getKey().getName(), Map.Entry::getValue));
    }

    public void setExtendedAttribute(VendorConfigType configType, Object value) {
        Objects.requireNonNull(configType, "Config type cannot be null");
        if (value == null) {
            extendedAttributes.remove(configType.getName());
            return;
        }

        if (!configType.isValidValue(value)) {
            throw new InvalidSettingsException("Invalid value " + value + " for config type: " + configType);
        }
        extendedAttributes.put(configType.getName(), value);
    }

    @SuppressWarnings("unchecked")
    public <T> T getExtendedAttribute(VendorConfigType configType) {
        Objects.requireNonNull(configType, "Config type cannot be null");
        return (T) extendedAttributes.get(configType.getName());
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Boolean getRatePlanCleanupEnabled() {
        return ratePlanCleanupEnabled;
    }

    public void setRatePlanCleanupEnabled(Boolean ratePlanCleanupEnabled) {
        this.ratePlanCleanupEnabled = ratePlanCleanupEnabled;
    }

    public Boolean getIncludeRoomTypeHotelMarketSegmentActivity() {
        return includeRoomTypeHotelMarketSegmentActivity;
    }

    public void setIncludeRoomTypeHotelMarketSegmentActivity(Boolean includeRoomTypeHotelMarketSegmentActivity) {
        this.includeRoomTypeHotelMarketSegmentActivity = includeRoomTypeHotelMarketSegmentActivity;
    }

    public Boolean getPreventPseudoDataInActivity() {
        return preventPseudoDataInActivity;
    }

    public void setPreventPseudoDataInActivity(Boolean preventPseudoDataInActivity) {
        this.preventPseudoDataInActivity = preventPseudoDataInActivity;
    }

    public Boolean getComponentRoomsActivityEnabled() {
        return isComponentRoomsActivityEnabled;
    }

    public void setComponentRoomsActivityEnabled(Boolean componentRoomsActivityEnabled) {
        isComponentRoomsActivityEnabled = componentRoomsActivityEnabled;
    }

    public Boolean getUseLegacyRoomStayHandling() {
        return useLegacyRoomStayHandling;
    }

    public void setUseLegacyRoomStayHandling(Boolean useLegacyRoomStayHandling) {
        this.useLegacyRoomStayHandling = useLegacyRoomStayHandling;
    }

    public String getOxiParkMessageTypes() {
        return oxiParkMessageTypes;
    }

    public void setOxiParkMessageTypes(String oxiParkMessageTypes) {
        this.oxiParkMessageTypes = oxiParkMessageTypes;
    }

    public Boolean getGenerateMarketSegmentStatsEnabled() {
        return generateMarketSegmentStatsEnabled;
    }

    public void setGenerateMarketSegmentStatsEnabled(Boolean generateMarketSegmentStatsEnabled) {
        this.generateMarketSegmentStatsEnabled = generateMarketSegmentStatsEnabled;
    }

    public Boolean getGenerateRateCodeStatsEnabled() {
        return generateRateCodeStatsEnabled;
    }

    public void setGenerateRateCodeStatsEnabled(Boolean generateRateCodeStatsEnabled) {
        this.generateRateCodeStatsEnabled = generateRateCodeStatsEnabled;
    }

    public Boolean getResetVirtualSuiteCounts() {
        return resetVirtualSuiteCounts;
    }

    public void setResetVirtualSuiteCounts(Boolean resetVirtualSuiteCounts) {
        this.resetVirtualSuiteCounts = resetVirtualSuiteCounts;
    }

    public Boolean getGenerateHotelActivityFromRoomTypeActivity() {
        return generateHotelActivityFromRoomTypeActivity;
    }

    public void setGenerateHotelActivityFromRoomTypeActivity(Boolean generateHotelActivityFromRoomTypeActivity) {
        this.generateHotelActivityFromRoomTypeActivity = generateHotelActivityFromRoomTypeActivity;
    }

}
