package com.ideas.ngi.nucleus.data.integration;

import com.ideas.ngi.nucleus.exception.NucleusException;

public class InvalidSettingsException extends NucleusException {

    public InvalidSettingsException(String message) {
        super(message);
    }

    public InvalidSettingsException(String message, Throwable ex) {
        super(message, ex);
    }
}
