package com.ideas.ngi.nucleus.data.integration.util;

import com.ideas.ngi.nucleus.data.integration.entity.VendorProperty;

public interface VendorPropertyAware {
    VendorProperty toVendorProperty();
}
