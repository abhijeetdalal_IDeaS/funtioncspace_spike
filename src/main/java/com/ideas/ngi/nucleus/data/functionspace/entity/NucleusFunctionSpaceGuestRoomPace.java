package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.nucleus.data.AuditableEntity;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.integration.annotation.ClientCode;
import com.ideas.ngi.nucleus.integration.annotation.PropertyCode;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Document
public class NucleusFunctionSpaceGuestRoomPace extends AuditableEntity implements Serializable {

    private static final long serialVersionUID = -3000933973470656376L;
    public static final String ROOM_TYPE_ALL = "ALL";

    @Id
    private String id;

    @NotNull
    private IntegrationType integrationType;
    @NotNull
    private String propertyId;
    @NotNull
    private String bookingId;
    @NotNull
    private String bookingPaceId;
    @NotNull
    private String roomType;
    @NotNull
    private Date stayDate;

    private Date changeDate;
    private Integer roomNights;
    private BigDecimal roomRevenue;

    private Integer roomNightsChanged;
    private BigDecimal roomRevenueChanged;
    private String guestRoomStatus;
    private String correlationId;
    @PropertyCode
    private String propertyCode;
    @ClientCode
    private String clientCode;
    private Integer blockedRoomNights;
    private BigDecimal blockedRoomRevenue;
    private Integer contractedRoomNights;
    private BigDecimal contractedRoomRevenue;
    private BigDecimal blockedRoomRevenueChanged;
    private Integer blockedRoomNightsChanged;
    private BigDecimal contractedRoomRevenueChanged;
    private Integer contractedRoomNightsChanged;

    public String getId() {
        return id;
    }

    public NucleusFunctionSpaceGuestRoomPace(NucleusFunctionSpaceGuestRoomPace copy) {
        this.id = copy.id;
        this.integrationType = copy.integrationType;
        this.propertyId = copy.propertyId;
        this.bookingId = copy.bookingId;
        this.bookingPaceId = copy.bookingPaceId;
        this.roomType = copy.roomType;
        this.stayDate = copy.stayDate;
        this.changeDate = copy.changeDate;
        this.roomNights = copy.roomNights;
        this.roomRevenue = copy.roomRevenue;
        this.roomNightsChanged = copy.roomNightsChanged;
        this.roomRevenueChanged = copy.roomRevenueChanged;
        this.guestRoomStatus = copy.guestRoomStatus;
        this.correlationId = copy.correlationId;
        this.propertyCode = copy.propertyCode;
        this.clientCode = copy.clientCode;
        this.blockedRoomNights = copy.blockedRoomNights;
        this.blockedRoomRevenue = copy.blockedRoomRevenue;
        this.contractedRoomNights = copy.contractedRoomNights;
        this.contractedRoomRevenue = copy.contractedRoomRevenue;
        this.blockedRoomRevenueChanged = copy.blockedRoomRevenueChanged;
        this.blockedRoomNightsChanged = copy.blockedRoomNightsChanged;
        this.contractedRoomRevenueChanged = copy.contractedRoomRevenueChanged;
        this.contractedRoomNightsChanged = copy.contractedRoomNightsChanged;
    }

    public NucleusFunctionSpaceGuestRoomPace(){

    }
    public void setId(String id) {
        this.id = id;
    }

    public IntegrationType getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(IntegrationType integrationType) {
        this.integrationType = integrationType;
    }
    
    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getBookingPaceId() {
        return bookingPaceId;
    }

    public void setBookingPaceId(String bookingPaceId) {
        this.bookingPaceId = bookingPaceId;
    }

    public Date getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }

    public Date getStayDate() {
        return stayDate;
    }

    public void setStayDate(Date stayDate) {
        this.stayDate = stayDate;
    }

    public Integer getRoomNights() {
        return roomNights;
    }

    public void setRoomNights(Integer roomNights) {
        this.roomNights = roomNights;
    }

    public BigDecimal getRoomRevenue() {
        return roomRevenue;
    }

    public void setRoomRevenue(BigDecimal roomRevenue) {
        this.roomRevenue = roomRevenue;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public BigDecimal getRoomRevenueChanged() {
        return roomRevenueChanged;
    }

    public void setRoomRevenueChanged(BigDecimal roomRevenueChanged) {
        this.roomRevenueChanged = roomRevenueChanged;
    }

    public Integer getRoomNightsChanged() {
        return roomNightsChanged;
    }

    public void setRoomNightsChanged(Integer roomNightsChanged) {
        this.roomNightsChanged = roomNightsChanged;
    }

    public String getGuestRoomStatus() {
        return guestRoomStatus;
    }

    public void setGuestRoomStatus(String guestRoomStatus) {
        this.guestRoomStatus = guestRoomStatus;
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, "id", "createDate", "lastModifiedDate", "changeDate", "bookingPaceId", "roomRevenueChanged", "roomNightsChanged" , "correlationId",
                "blockedRoomRevenueChanged", "blockedRoomNightsChanged", "contractedRoomRevenueChanged" ,"contractedRoomNightsChanged");
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, "id", "createDate", "lastModifiedDate", "changeDate", "bookingPaceId", "roomRevenueChanged", "roomNightsChanged", "correlationId",
                "blockedRoomRevenueChanged", "blockedRoomNightsChanged", "contractedRoomRevenueChanged" ,"contractedRoomNightsChanged");
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setBlockedRoomNights(Integer blockedRoomNights) {
        this.blockedRoomNights = blockedRoomNights;
    }

    public Integer getBlockedRoomNights() {
        return blockedRoomNights;
    }

    public void setBlockedRoomRevenue(BigDecimal blockedRoomRevenue) {
        this.blockedRoomRevenue = blockedRoomRevenue;
    }

    public BigDecimal getBlockedRoomRevenue() {
        return blockedRoomRevenue;
    }

    public void setContractedRoomNights(Integer contractedRoomNights) {
        this.contractedRoomNights = contractedRoomNights;
    }

    public Integer getContractedRoomNights() {
        return contractedRoomNights;
    }

    public void setContractedRoomRevenue(BigDecimal contractedRoomRevenue) {
        this.contractedRoomRevenue = contractedRoomRevenue;
    }

    public BigDecimal getContractedRoomRevenue() {
        return contractedRoomRevenue;
    }

    public void setBlockedRoomRevenueChanged(BigDecimal blockedRoomRevenueChanged) {
        this.blockedRoomRevenueChanged = blockedRoomRevenueChanged;
    }

    public BigDecimal getBlockedRoomRevenueChanged() {
        return blockedRoomRevenueChanged;
    }

    public void setBlockedRoomNightsChanged(Integer blockedRoomNightsChanged) {
        this.blockedRoomNightsChanged = blockedRoomNightsChanged;
    }

    public Integer getBlockedRoomNightsChanged() {
        return blockedRoomNightsChanged;
    }

    public void setContractedRoomRevenueChanged(BigDecimal contractedRoomRevenueChanged) {
        this.contractedRoomRevenueChanged = contractedRoomRevenueChanged;
    }

    public BigDecimal getContractedRoomRevenueChanged() {
        return contractedRoomRevenueChanged;
    }

    public void setContractedRoomNightsChanged(Integer contractedRoomNightsChanged) {
        this.contractedRoomNightsChanged = contractedRoomNightsChanged;
    }

    public Integer getContractedRoomNightsChanged() {
        return contractedRoomNightsChanged;
    }
}
