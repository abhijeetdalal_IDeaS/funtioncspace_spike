package com.ideas.ngi.nucleus.data.integration.dto;

import com.ideas.ngi.nucleus.data.integration.Setting;
import com.ideas.ngi.nucleus.data.integration.entity.VendorProperty;
import com.ideas.ngi.nucleus.data.integration.repository.IntegrationConfigRepository;
import com.ideas.ngi.nucleus.data.vendor.entity.VendorConfigType;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class SettingsContext implements Settings {
    private final VendorProperty vendorProperty;
    private final IntegrationConfigRepository repository;
    private final Map<String, Object> valueCache;

    public SettingsContext(String clientCode, String propertyCode, String vendorId, IntegrationConfigRepository repository) {
        this(new VendorProperty(vendorId, clientCode, propertyCode), repository);
    }

    public SettingsContext(VendorProperty vendorProperty, IntegrationConfigRepository repository) {
        Objects.requireNonNull(vendorProperty, "Vendor property is required");
        Objects.requireNonNull(repository, "Integration config repository is required");
        this.vendorProperty = vendorProperty;
        this.repository = repository;
        this.valueCache = new HashMap<>();
    }

    @Override
    public String getVendorId() {
        return vendorProperty.getVendorId();
    }

    @Override
    public String getClientCode() { return vendorProperty.getClientCode(); }

    @Override
    public String getPropertyCode() { return vendorProperty.getPropertyCode(); }

    @SuppressWarnings("unchecked")
    @Override
    public <T> Optional<T> getValue(Setting<T> attribute) {
        T value;
        // cannot use computeIfAbsent here because computeIfAbsent only
        // recognizes null as "absent" which is not what we want for this cache
        if (valueCache.containsKey(attribute.getPropertyName())) {
            value = (T) valueCache.get(attribute.getPropertyName());
        } else {
            value = repository.getValue(vendorProperty, attribute.getPropertyName());
            valueCache.put(attribute.getPropertyName(), value);
        }
        return Optional.ofNullable(value);
    }

    @Override
    public <T> Optional<T> getValue(VendorConfigType vendorConfigType) {
        return Optional.ofNullable(repository.getValue(vendorProperty, vendorConfigType));
    }

    @Override
    public <T> SettingsContext setValue(Setting<T> attribute, T value) {
        repository.setValueAtLevel(vendorProperty, attribute.getPropertyName(), value);
        valueCache.put(attribute.getPropertyName(), value);
        return this;
    }

    @Override
    public SettingsContext setValue(VendorConfigType vendorConfigType, Object value) {
        repository.setValueAtLevel(vendorProperty, vendorConfigType, value);
        return this;
    }

    @Override
    public boolean exists() {
        return repository.exists(vendorProperty);
    }
}
