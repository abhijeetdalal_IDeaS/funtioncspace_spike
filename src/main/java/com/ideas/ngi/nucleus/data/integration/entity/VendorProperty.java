package com.ideas.ngi.nucleus.data.integration.entity;

import com.ideas.ngi.nucleus.data.integration.InvalidSettingsException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.Map;

import static com.ideas.ngi.functionspace.constant.Constants.PARAM_CLIENT_CODE;
import static com.ideas.ngi.functionspace.constant.Constants.PARAM_PROPERTY_CODE;
import static org.springframework.data.mongodb.core.query.Criteria.where;

public class VendorProperty {

    public static final String PARAM_VENDOR_ID = "vendorId";

    private final String vendorId;
    private final String clientCode;
    private final String propertyCode;

    public VendorProperty(String vendorId, String clientCode, String propertyCode) {
        if (StringUtils.isEmpty(vendorId)) {
            // Internally, the field here is "vendorId" and the value is from "/POS/Source/RequestorID/@ID"
            throw new InvalidSettingsException("Invalid configuration identifier: missing RequestorID");
        }

        if (StringUtils.isEmpty(clientCode) && StringUtils.isNotEmpty(propertyCode)) {
            throw new InvalidSettingsException("Invalid configuration identifier: missing client code");
        }

        this.vendorId = vendorId.trim();
        this.clientCode = StringUtils.trimToNull(clientCode);
        this.propertyCode = StringUtils.trimToNull(propertyCode);
    }

    public static VendorProperty fromMap(Map<String, Object> params) {
        return new VendorProperty(
                (String) params.get(PARAM_VENDOR_ID),
                (String) params.get(PARAM_CLIENT_CODE),
                (String) params.get(PARAM_PROPERTY_CODE));
    }

    public String getVendorId() {
        return vendorId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public Criteria toCriteria() {
        return where(PARAM_VENDOR_ID).is(vendorId).andOperator(
                nvl(PARAM_CLIENT_CODE, clientCode),
                nvl(PARAM_PROPERTY_CODE, propertyCode));
    }

    private Criteria nvl(String field, String value) {
        return new Criteria().orOperator(where(field).is(value), where(field).exists(false));
    }

    public Criteria toLevelCriteria() {
        Criteria criteria = where(PARAM_VENDOR_ID).is(vendorId);
        int level = BaseIntegrationConfig.PRIORITY_VENDOR;

        if (StringUtils.isNotEmpty(clientCode)) {
            level = BaseIntegrationConfig.PRIORITY_CLIENT;
            criteria.and(PARAM_CLIENT_CODE).is(clientCode);
        }

        if (StringUtils.isNotEmpty(propertyCode)) {
            level = BaseIntegrationConfig.PRIORITY_PROPERTY;
            criteria.and(PARAM_PROPERTY_CODE).is(propertyCode);
        }

        return criteria.and("priority").is(level);
    }

    public boolean isPropertyLevel() {
        return StringUtils.isNotEmpty(propertyCode);
    }

    public boolean isClientLevel() {
        return !isPropertyLevel() && StringUtils.isNotEmpty(clientCode);
    }

    public boolean isVendorLevel() {
        return !isPropertyLevel() && !isClientLevel();
    }

    public Class<? extends BaseIntegrationConfig> toLevelType() {
        if (isPropertyLevel()) {
            return IntegrationPropertyConfig.class;
        } else if (isClientLevel()) {
            return IntegrationClientConfig.class;
        } else {
            return IntegrationVendorConfig.class;
        }
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
