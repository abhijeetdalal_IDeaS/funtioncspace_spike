package com.ideas.ngi.nucleus.data.correlation.entity;

import java.util.EnumSet;
import java.util.Set;

public enum CorrelationStatus {
    IN_PROGRESS, SUCCESSFUL, FAILED, ABANDONED, DUPLICATE;

    private static final Set<CorrelationStatus> FINAL_STATUSES = EnumSet.of(SUCCESSFUL, ABANDONED);

    public static Set<CorrelationStatus> finalStatuses() {
        return FINAL_STATUSES;
    }

    public boolean isFinalStatus() {
        return FINAL_STATUSES.contains(this);
    }
}
