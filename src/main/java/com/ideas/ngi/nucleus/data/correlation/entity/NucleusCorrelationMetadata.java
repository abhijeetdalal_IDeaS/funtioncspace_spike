package com.ideas.ngi.nucleus.data.correlation.entity;

import com.ideas.ngi.nucleus.data.AuditableEntity;
import com.ideas.ngi.nucleus.data.correlation.dto.PropertyIdentifier;
import com.ideas.ngi.nucleus.integration.annotation.ClientCode;
import com.ideas.ngi.nucleus.integration.annotation.ExternalSystemPropertyId;
import com.ideas.ngi.nucleus.integration.annotation.PropertyCode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Document
public class NucleusCorrelationMetadata extends AuditableEntity implements Serializable {

    private static final long serialVersionUID = -1567428455059215169L;

    @Id
    private String id;
    @ExternalSystemPropertyId
    private String sendingSystemPropertyId;
    @ClientCode
    private String clientCode;
    @PropertyCode
    private String propertyCode;
    private String name;
    private Date captureDate;
    private CorrelationStatus correlationStatus;
    private IntegrationType integrationType;
    private BusinessModule businessModule;
    private String originalFilePath;
    private String currentFilePath;
    private Map<String, String> additionalInformation;

    public NucleusCorrelationMetadata() {
        additionalInformation = new HashMap<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSendingSystemPropertyId() {
        return sendingSystemPropertyId;
    }

    public void setSendingSystemPropertyId(String sendingSystemPropertyId) {
        this.sendingSystemPropertyId = sendingSystemPropertyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCaptureDate() {
        return captureDate;
    }

    public void setCaptureDate(Date captureDate) {
        this.captureDate = captureDate;
    }

    public CorrelationStatus getCorrelationStatus() {
        return correlationStatus;
    }

    public void setCorrelationStatus(CorrelationStatus correlationStatus) {
        this.correlationStatus = correlationStatus;
    }

    public IntegrationType getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(IntegrationType integrationType) {
        this.integrationType = integrationType;
    }

    public BusinessModule getBusinessModule() {
        return businessModule;
    }

    public void setBusinessModule(BusinessModule businessModule) {
        this.businessModule = businessModule;
    }

    public String getOriginalFilePath() {
        return originalFilePath;
    }

    public void setOriginalFilePath(String originalFilePath) {
        this.originalFilePath = originalFilePath;
    }

    public String getCurrentFilePath() {
        return currentFilePath;
    }

    public void setCurrentFilePath(String currentFilePath) {
        this.currentFilePath = currentFilePath;
    }

    public File getOriginalFile() {
        if (StringUtils.isEmpty(originalFilePath)) {
            return null;
        }

        return new File(originalFilePath);
    }

    public File getCurrentFile() {
        if (StringUtils.isEmpty(currentFilePath)) {
            return null;
        }

        return new File(currentFilePath);
    }

    public NucleusCorrelationMetadata updateCorrelationStatus(CorrelationStatus correlationStatus) {
        setCorrelationStatus(correlationStatus);
        return this;
    }

    public NucleusCorrelationMetadata updateCurrentFilePath(File file) {
        setCurrentFilePath(file.getAbsolutePath());
        return this;
    }

    public String toString() {
        return getClass().getSimpleName() + ": integrationType=" + integrationType + ", sendingPropertyId=" + sendingSystemPropertyId + ", name=" + name;
    }

    public Map<String, String> getAdditionalInformation() {
        return additionalInformation;
    }

    public String getAdditionalInformation(String key) {
        return additionalInformation.getOrDefault(key, null);
    }

    public void setAdditionalInformation(String key, String additionalInformation) {
        this.additionalInformation.put(key, additionalInformation);
    }

    public void setAdditionalInformation(Map<String, String> map) {
        this.additionalInformation.putAll(map);
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }

    public PropertyIdentifier getPropertyIdentifier() {
        return new PropertyIdentifier(sendingSystemPropertyId, clientCode, propertyCode);
    }
}
