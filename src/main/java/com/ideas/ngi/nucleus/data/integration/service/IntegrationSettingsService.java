package com.ideas.ngi.nucleus.data.integration.service;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.integration.IntegrationSettingsException;
import com.ideas.ngi.nucleus.data.integration.SettingsNotFoundException;
import com.ideas.ngi.nucleus.data.integration.dto.EmptySettingsContext;
import com.ideas.ngi.nucleus.data.integration.dto.Settings;
import com.ideas.ngi.nucleus.data.integration.dto.SettingsContext;
import com.ideas.ngi.nucleus.data.integration.entity.*;
import com.ideas.ngi.nucleus.data.integration.repository.IntegrationConfigRepository;
import com.ideas.ngi.nucleus.data.vendor.entity.VendorConfigType;
import com.mongodb.bulk.BulkWriteResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.stereotype.Component;

import java.util.EnumSet;
import java.util.Optional;
import java.util.Set;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Component
public class IntegrationSettingsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationSettingsService.class);
    private final IntegrationConfigRepository integrationConfigRepository;

    @Autowired
    public IntegrationSettingsService(IntegrationConfigRepository integrationConfigRepository) {
        this.integrationConfigRepository = integrationConfigRepository;
    }

    public Settings getSettings(String clientCode, String propertyCode, String vendorId) {
        return new SettingsContext(clientCode, propertyCode, vendorId, integrationConfigRepository);
    }

    public Settings getSettings(String clientCode, String propertyCode, IntegrationType integrationType) {
        VendorProperty vendorId = integrationConfigRepository.getVendorPropertyId(EnumSet.of(integrationType), clientCode, propertyCode);
        if (vendorId == null) {
            return new EmptySettingsContext(clientCode, propertyCode);
        } else {
            return new SettingsContext(vendorId, integrationConfigRepository);
        }
    }

    public Settings lookupSettings(String externalPropertyId, IntegrationType integrationType) {
        return Optional.ofNullable(integrationConfigRepository.lookupVendorPropertyId(integrationType, externalPropertyId))
                .<Settings>map(v -> new SettingsContext(v, integrationConfigRepository))
                .orElse(new EmptySettingsContext(null, externalPropertyId));
    }

    public Settings getSettings(String clientCode, String propertyCode, Set<IntegrationType> integrationTypes) {
        VendorProperty vendorId = integrationConfigRepository.getVendorPropertyId(integrationTypes, clientCode, propertyCode);
        if (vendorId == null) {
            return new EmptySettingsContext(clientCode, propertyCode);
        } else {
            return new SettingsContext(vendorId, integrationConfigRepository);
        }
    }

    public Page<IntegrationVendorConfig> getAllVendorSettings(Pageable page) {
        return integrationConfigRepository.findAllVendorConfigs(page);
    }

    public IntegrationVendorConfig getVendorSettings(String vendorId) {
        return integrationConfigRepository.findOne(new VendorProperty(vendorId, null, null));
    }

    public Page<IntegrationClientConfig> getAllClientSettings(String vendorId, Pageable page) {
        return integrationConfigRepository.findAllClientConfigs(vendorId, page);
    }

    public IntegrationClientConfig getClientSettings(String vendorId, String clientCode) {
        return integrationConfigRepository.findOne(new VendorProperty(vendorId, clientCode, null));
    }

    public Page<IntegrationPropertyConfig> getAllPropertySettings(String vendorId, String clientCode, Pageable page) {
        return integrationConfigRepository.findAllPropertyConfigs(vendorId, clientCode, page);
    }

    public IntegrationPropertyConfig getPropertySettings(String vendorId, String clientCode, String propertyCode) {
        return integrationConfigRepository.findOne(new VendorProperty(vendorId, clientCode, propertyCode));
    }

    public BaseIntegrationConfig createSettings(BaseIntegrationConfig config) {
        return integrationConfigRepository.insert(config);
    }

    public BaseIntegrationConfig updateSettings(String configId, BaseIntegrationConfig config) {
        BaseIntegrationConfig existingConfig = integrationConfigRepository.findOne(configId);
        if (existingConfig == null) {
            throw new SettingsNotFoundException("Configuration not found for id " + configId);
        }

        if (!existingConfig.toVendorProperty().equals(config.toVendorProperty())) {
            throw new IntegrationSettingsException("Configuration key fields cannot be changed after creation of configuration");
        }

        config.setId(configId);
        config.setCreateDate(existingConfig.getCreateDate());
        if (config.getVersion() == null) {
            config.setVersion(existingConfig.getVersion());
        }
        return integrationConfigRepository.save(config);
    }

    public BaseIntegrationConfig getSettings(String configId) {
        return integrationConfigRepository.findOne(configId);
    }

    public void deleteSettings(BaseIntegrationConfig config) {
        if (config.getId() != null) {
            integrationConfigRepository.delete(config);
        } else {
            VendorProperty vendorProperty = config.toVendorProperty();
            BaseIntegrationConfig foundConfig = integrationConfigRepository.findOne(vendorProperty);
            if (foundConfig != null) {
                integrationConfigRepository.delete(foundConfig);
            }
        }
    }

    public void deleteAll() {
        integrationConfigRepository.deleteAll();
    }

    public void deleteAllSettings(String vendorId) {
        BulkWriteResult result = integrationConfigRepository.bulkOps(BulkOperations.BulkMode.UNORDERED)
                .remove(query(where("vendorId").is(vendorId)))
                .execute();
        LOGGER.info("Deleted all configs for vendor {}.  Removed {} records", vendorId, result.getDeletedCount());
    }

    public void deleteAllSettings(String vendorId, String clientCode) {
        BulkWriteResult result = integrationConfigRepository.bulkOps(BulkOperations.BulkMode.UNORDERED)
                .remove(query(where("vendorId").is(vendorId).and("clientCode").is(clientCode)))
                .execute();
        LOGGER.info("Deleted all configs for vendor {} and client {}.  Removed {} records", vendorId, clientCode, result.getDeletedCount());
    }

    public void updateSetting(VendorProperty vendorPropertyId, String attribute, Object value) {
        VendorConfigType vendorConfigType = VendorConfigType.getNameForString(attribute);
        if (vendorConfigType == null) {
            integrationConfigRepository.setValueAtLevel(vendorPropertyId, attribute, value);
        } else {
            integrationConfigRepository.setValueAtLevel(vendorPropertyId, vendorConfigType, value);
        }
    }

    public void deleteSetting(VendorProperty vendorPropertyId, String attribute) {
        VendorConfigType vendorConfigType = VendorConfigType.getNameForString(attribute);
        if (vendorConfigType == null) {
            integrationConfigRepository.removeValueAtLevel(vendorPropertyId, attribute);
        } else {
            integrationConfigRepository.removeValueAtLevel(vendorPropertyId, vendorConfigType);
        }
    }
}
