package com.ideas.ngi.nucleus.data.integration.entity;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@SuppressWarnings("unused")
public class IntegrationPropertyConfig extends BaseIntegrationConfig {
    private static final long serialVersionUID = 609517368469431196L;

    @NotNull
    private String propertyCode;

    @NotNull
    private String clientCode;

    private String outgoingUrl;
    private String oxiInterfaceName;
    private BigDecimal taxAdjustmentValue;
    private Integer pastDays;
    private Integer futureDays;
    private Boolean assumeTaxIncluded;
    private Boolean assumePackageIncluded;
    private Boolean installMode;
    private Boolean scheduledDeferredDelivery;
    private Boolean inCatchup;
    private Boolean calculateNonPickedUpBlocksUsingSummaryData;
    private Boolean handlePreviouslyStraightMarketSegmentsInAms;
    private String defaultRoomType;
    private String externalPropertyId;
    private String roomRevenuePackages;
    private Boolean populatePackageDataEnabled;
    private Boolean useNetRoomRevenue;
    private Boolean includePseudoInRevenue;
    private Boolean includeDayUseInRevenue;
    private Boolean includeNoShowInRevenue;
    private Boolean useCurrencyInMessage;
    private String clientEnvironmentName;


    public IntegrationPropertyConfig() {
    }

    public IntegrationPropertyConfig(IntegrationType integrationType, String clientCode, String propertyCode) {
        this(integrationType.name(), integrationType, clientCode, propertyCode);
    }

    public IntegrationPropertyConfig(String vendorId, IntegrationType integrationType, String clientCode, String propertyCode) {
        super(vendorId, integrationType);
        this.clientCode = clientCode;
        this.propertyCode = propertyCode;
    }

    @Override
    public int getPriority() {
        return 0;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getOxiInterfaceName() {
        return oxiInterfaceName;
    }

    public void setOxiInterfaceName(String oxiInterfaceName) {
        this.oxiInterfaceName = oxiInterfaceName;
    }

    public BigDecimal getTaxAdjustmentValue() {
        return taxAdjustmentValue;
    }

    public void setTaxAdjustmentValue(BigDecimal taxAdjustmentValue) {
        this.taxAdjustmentValue = taxAdjustmentValue;
    }

    public Integer getPastDays() {
        return pastDays;
    }

    public String getOutgoingUrl() {
        return outgoingUrl;
    }

    public void setOutgoingUrl(String outgoingUrl) {
        this.outgoingUrl = outgoingUrl;
    }

    public void setPastDays(Integer pastDays) {
        this.pastDays = pastDays;
    }

    public Integer getFutureDays() {
        return futureDays;
    }

    public void setFutureDays(Integer futureDays) {
        this.futureDays = futureDays;
    }

    public Boolean getAssumeTaxIncluded() {
        return assumeTaxIncluded;
    }

    public void setAssumeTaxIncluded(Boolean assumeTaxIncluded) {
        this.assumeTaxIncluded = assumeTaxIncluded;
    }

    public Boolean getAssumePackageIncluded() {
        return assumePackageIncluded;
    }

    public void setAssumePackageIncluded(Boolean assumePackageIncluded) {
        this.assumePackageIncluded = assumePackageIncluded;
    }

    public Boolean getInstallMode() {
        return installMode;
    }

    public void setInstallMode(Boolean installMode) {
        this.installMode = installMode;
    }

    public Boolean getScheduledDeferredDelivery() {
        return scheduledDeferredDelivery;
    }

    public void setScheduledDeferredDelivery(Boolean scheduledDeferredDelivery) {
        this.scheduledDeferredDelivery = scheduledDeferredDelivery;
    }

    public Boolean getInCatchup() {
        return inCatchup;
    }

    public void setInCatchup(Boolean inCatchup) {
        this.inCatchup = inCatchup;
    }

    public Boolean getCalculateNonPickedUpBlocksUsingSummaryData() {
        return calculateNonPickedUpBlocksUsingSummaryData;
    }

    public void setCalculateNonPickedUpBlocksUsingSummaryData(Boolean calculateNonPickedUpBlocksUsingSummaryData) {
        this.calculateNonPickedUpBlocksUsingSummaryData = calculateNonPickedUpBlocksUsingSummaryData;
    }

    public Boolean getHandlePreviouslyStraightMarketSegmentsInAms() {
        return handlePreviouslyStraightMarketSegmentsInAms;
    }

    public void setHandlePreviouslyStraightMarketSegmentsInAms(Boolean handlePreviouslyStraightMarketSegmentsInAms) {
        this.handlePreviouslyStraightMarketSegmentsInAms = handlePreviouslyStraightMarketSegmentsInAms;
    }

    public String getDefaultRoomType() {
        return defaultRoomType;
    }

    public void setDefaultRoomType(String defaultRoomType) {
        this.defaultRoomType = defaultRoomType;
    }

    public String getExternalPropertyId() {
        return externalPropertyId;
    }

    public void setExternalPropertyId(String externalPropertyId) {
        this.externalPropertyId = externalPropertyId;
    }

    public String getRoomRevenuePackages() {
        return roomRevenuePackages;
    }

    public void setRoomRevenuePackages(String roomRevenuePackages) {
        this.roomRevenuePackages = roomRevenuePackages;
    }

    public Boolean getPopulatePackageDataEnabled() {
        return populatePackageDataEnabled;
    }

    public void setPopulatePackageDataEnabled(Boolean populatePackageDataEnabled) {
        this.populatePackageDataEnabled = populatePackageDataEnabled;
    }

    public Boolean getUseNetRoomRevenue() {
        return useNetRoomRevenue;
    }

    public void setUseNetRoomRevenue(Boolean useNetRoomRevenue) {
        this.useNetRoomRevenue = useNetRoomRevenue;
    }

    public Boolean getIncludePseudoInRevenue() { return includePseudoInRevenue; }

    public void setIncludePseudoInRevenue(Boolean includePseudoInRevenue) {
        this.includePseudoInRevenue = includePseudoInRevenue;
    }

    public Boolean getIncludeDayUseInRevenue() { return includeDayUseInRevenue; }

    public void setIncludeDayUseInRevenue(Boolean includeDayUseInRevenue) {
        this.includeDayUseInRevenue = includeDayUseInRevenue;
    }

    public Boolean getIncludeNoShowInRevenue() { return includeNoShowInRevenue; }

    public void setIncludeNoShowInRevenue(Boolean includeNoShowInRevenue) {
        this.includeNoShowInRevenue = includeNoShowInRevenue;
    }

    public Boolean getUseCurrencyInMessage() {
        return useCurrencyInMessage;
    }

    public void setUseCurrencyInMessage(Boolean useCurrencyInMessage) {
        this.useCurrencyInMessage = useCurrencyInMessage;
    }

    public String getClientEnvironmentName() {
        return clientEnvironmentName;
    }

    public void setClientEnvironmentName(String clientEnvironmentName) {
        this.clientEnvironmentName = clientEnvironmentName;
    }

    @Override
    public VendorProperty toVendorProperty() {
        return new VendorProperty(getVendorId(), getClientCode(), getPropertyCode());
    }
}
