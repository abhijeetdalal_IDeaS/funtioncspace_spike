package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.functionspace.dto.FunctionSpaceData;
import com.ideas.ngi.nucleus.data.correlation.entity.NucleusCorrelationMetadata;
import org.springframework.integration.transformer.GenericTransformer;
import org.springframework.stereotype.Component;

@Component
public class FSCorrelationTransformer implements GenericTransformer<NucleusCorrelationMetadata, FunctionSpaceData> {

    @Override
    public FunctionSpaceData transform(NucleusCorrelationMetadata source) {
        return new FunctionSpaceData(source);
    }
}
