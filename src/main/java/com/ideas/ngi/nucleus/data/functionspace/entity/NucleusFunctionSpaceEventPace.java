package com.ideas.ngi.nucleus.data.functionspace.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Document
public class NucleusFunctionSpaceEventPace extends NucleusFunctionSpaceEvent implements Serializable {

    private static final long serialVersionUID = -3126187146881169521L;

    @NotNull
    private Date changeDate;

    public Date getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }
}