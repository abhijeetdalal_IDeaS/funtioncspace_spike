package com.ideas.ngi.nucleus.data.integration.repository;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.integration.entity.BaseIntegrationConfig;
import com.ideas.ngi.nucleus.data.integration.entity.VendorProperty;
import com.ideas.ngi.nucleus.data.vendor.entity.VendorConfigType;

import java.util.Set;

public interface IntegrationConfigRepositoryCustom {

    <T extends BaseIntegrationConfig> T findOne(VendorProperty property);

    /**
     * Get a "resolved" field value for vendor property
     * @param identifier the vendor-property identifier
     * @param fieldName the field to resolve
     * @param <T> the type of return value
     * @return the result
     */
    <T> T getValue(VendorProperty identifier, String fieldName);

    /**
     * Get a "resolved" field value for vendor property
     *
     * @param identifier the vendor-property identifier
     * @param configType the field to resolve
     * @param <T> the type of return value
     * @return the result
     */
    <T> T getValue(VendorProperty identifier, VendorConfigType configType);

    /**
     * Get a field value for vendor property at the level of specification given in the identifier
     * e.g. if identifier is vendorId only, resolve the field value at the vendor level only,
     * if the identifier is vendor and client, resolve the field value at the client level only,
     * if the identifier is vendor, client and property, resolve the field value at the property level only
     *
     * @param identifier the vendor-property identifier
     * @param fieldName the field to resolve
     * @param <T> the type of return value
     * @return the result
     */
    <T> T getValueAtLevel(VendorProperty identifier, String fieldName);

    /**
     * Get a field value for vendor property at the level of specification given in the identifier
     * e.g. if identifier is vendorId only, resolve the field value at the vendor level only,
     * if the identifier is vendor and client, resolve the field value at the client level only,
     * if the identifier is vendor, client and property, resolve the field value at the property level only
     *
     * @param identifier the vendor-property identifier
     * @param configType the field to resolve
     * @param <T> the type of return value
     * @return the result
     */
    <T> T getValueAtLevel(VendorProperty identifier, VendorConfigType configType);

    /**
     * Updates a field value for vendor property at the level of specification given in the identifier
     * e.g. if identifier is vendorId only, resolve the field value at the vendor level only,
     * if the identifier is vendor and client, resolve the field value at the client level only,
     * if the identifier is vendor, client and property, resolve the field value at the property level only
     *
     * @param identifier the vendor-property identifier
     * @param fieldName the field to resolve
     * @param value the updated value
     */
    void setValueAtLevel(VendorProperty identifier, String fieldName, Object value);

    /**
     * Updates a field value for vendor property at the level of specification given in the identifier
     * e.g. if identifier is vendorId only, resolve the field value at the vendor level only,
     * if the identifier is vendor and client, resolve the field value at the client level only,
     * if the identifier is vendor, client and property, resolve the field value at the property level only
     *
     * @param identifier the vendor-property identifier
     * @param configType the field to resolve
     * @param value the updated value
     */
    void setValueAtLevel(VendorProperty identifier, VendorConfigType configType, Object value);

    /**
     * Removes a field value for vendor property at the level of specification given in the identifier
     * e.g. if identifier is vendorId only, removes the field value at the vendor level only,
     * if the identifier is vendor and client, removes the field value at the client level only,
     * if the identifier is vendor, client and property, removes the field value at the property level only
     *
     * @param identifier the vendor-property identifier
     * @param fieldName the field to remove
     */
    default void removeValueAtLevel(VendorProperty identifier, String fieldName) {
        setValueAtLevel(identifier, fieldName, null);
    }

    /**
     * Removes a field value for vendor property at the level of specification given in the identifier
     * e.g. if identifier is vendorId only, removes the field value at the vendor level only,
     * if the identifier is vendor and client, removes the field value at the client level only,
     * if the identifier is vendor, client and property, removes the field value at the property level only
     *
     * @param identifier the vendor-property identifier
     * @param configType the field to remove
     */
    default void removeValueAtLevel(VendorProperty identifier, VendorConfigType configType) {
        setValueAtLevel(identifier, configType, null);
    }

    /**
     * Resolves the vendorid for a given integrationtype client and property
     * @param integrationType the integration type to lookup
     * @param clientCode the target client code
     * @param propertyCode the target property code
     * @return the fully-constructed vendor property if vendor id found; otherwise null
     */
    VendorProperty getVendorPropertyId(Set<IntegrationType> integrationType, String clientCode, String propertyCode);

    /**
     * Looks up vendor property id by external hotel code
     * @param integrationType integration type
     * @param externalPropertyId the external property identifier
     * @return the fully-constructed vendor property if vendor id found; otherwise null
     */
    VendorProperty lookupVendorPropertyId(IntegrationType integrationType, String externalPropertyId);

    /**
     * Checks to see if a configuration exists at the level specified
     * @param vendorProperty The vendor property configuration to check
     * @return boolean indicating whether or not configuration exists at that level
     */
    boolean exists(VendorProperty vendorProperty);

}
