package com.ideas.ngi.nucleus.data.integration.entity;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;

import javax.validation.constraints.NotNull;

@SuppressWarnings("unused")
public class IntegrationClientConfig extends BaseIntegrationConfig {
    private static final long serialVersionUID = -200294883425693691L;

    @NotNull
    private String clientCode;
    private String clientEnvironmentName;
    private Boolean populatePackageDataEnabled;
    private Boolean useNetRoomRevenue;
    private Boolean includePseudoInRevenue;
    private Boolean includeDayUseInRevenue;
    private Boolean includeNoShowInRevenue;
    private Boolean sendHTNGCallbackRequest;
    private Boolean useCurrencyInMessage;
    private String tokenUrl;
    private String baseUrl;
    private String clientId;
    private String clientSecret;
    private String env;

    public IntegrationClientConfig() {}
    public IntegrationClientConfig(String vendorId, IntegrationType integrationType, String clientCode) {
        super(vendorId, integrationType);
        this.clientCode = clientCode;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getClientEnvironmentName() {
        return clientEnvironmentName;
    }

    public void setClientEnvironmentName(String clientEnvironmentName) {
        this.clientEnvironmentName = clientEnvironmentName;
    }

    public Boolean getPopulatePackageDataEnabled() {
        return populatePackageDataEnabled;
    }

    public void setPopulatePackageDataEnabled(Boolean populatePackageDataEnabled) {
        this.populatePackageDataEnabled = populatePackageDataEnabled;
    }

    public Boolean getUseNetRoomRevenue() {
        return useNetRoomRevenue;
    }

    public void setUseNetRoomRevenue(Boolean useNetRoomRevenue) {
        this.useNetRoomRevenue = useNetRoomRevenue;
    }

    public Boolean getIncludePseudoInRevenue() { return includePseudoInRevenue; }

    public void setIncludePseudoInRevenue(Boolean includePseudoInRevenue) {
        this.includePseudoInRevenue = includePseudoInRevenue;
    }

    public Boolean getIncludeDayUseInRevenue() { return includeDayUseInRevenue; }

    public void setIncludeDayUseInRevenue(Boolean includeDayUseInRevenue) {
        this.includeDayUseInRevenue = includeDayUseInRevenue;
    }

    public Boolean getIncludeNoShowInRevenue() { return includeNoShowInRevenue; }

    public void setIncludeNoShowInRevenue(Boolean includeNoShowInRevenue) {
        this.includeNoShowInRevenue = includeNoShowInRevenue;
    }

    public Boolean getSendHTNGCallbackRequest() {
        return sendHTNGCallbackRequest;
    }

    public void setSendHTNGCallbackRequest(Boolean sendHTNGCallbackRequest) {
        this.sendHTNGCallbackRequest = sendHTNGCallbackRequest;
    }

    public Boolean getUseCurrencyInMessage() {
        return useCurrencyInMessage;
    }

    public void setUseCurrencyInMessage(Boolean useCurrencyInMessage) {
        this.useCurrencyInMessage = useCurrencyInMessage;
    }

    public String getTokenUrl() {
        return tokenUrl;
    }

    public IntegrationClientConfig setTokenUrl(String tokenUrl) {
        this.tokenUrl = tokenUrl;
        return this;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public IntegrationClientConfig setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    public String getClientId() {
        return clientId;
    }

    public IntegrationClientConfig setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public IntegrationClientConfig setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
        return this;
    }

    public String getEnv() {
        return env;
    }

    public IntegrationClientConfig setEnv(String env) {
        this.env = env;
        return this;
    }

    @Override
    public VendorProperty toVendorProperty() {
        return new VendorProperty(getVendorId(), getClientCode(), null);
    }
}
