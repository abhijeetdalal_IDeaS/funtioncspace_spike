package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.nucleus.data.AuditableEntity;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.functionspace.file.delimited.annotation.DelimitedField;
import com.ideas.ngi.nucleus.integration.annotation.ClientCode;
import com.ideas.ngi.nucleus.integration.annotation.PropertyCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Document
public class NucleusFunctionSpaceFunctionRoom extends AuditableEntity implements Serializable {

    private static final long serialVersionUID = 3860573367126669601L;

    public static final String DEFAULT_FUNCTION_ROOM_ID = "0000";

    @Id
    private String id;
    
    @NotNull
    private IntegrationType integrationType;

    @ClientCode
    private String clientCode;

    @PropertyCode
    private String propertyCode;

    @DelimitedField(name="PropertyID")
    @NotNull
    private String propertyId;

    @DelimitedField(name="FunctionRoomID")
    @NotNull
    private String functionRoomId;
    
    @DelimitedField(name="FunctionRoomAbbreviation")
    private String functionRoomAbbreviation;

    @DelimitedField(name="MeetingRoomType")
    private String functionRoomType;

    @DelimitedField(name="AreaInSquareFeet")
    @NotNull
    @Min(value=0)
    private BigDecimal areaSqFeet;

    @DelimitedField(name="AreaInSquareMeters")
    @NotNull
    @Min(value=0)
    private BigDecimal areaSqMeters;

    @DelimitedField(name="MinOccupancy")
    private Integer minOccupancy;

    @DelimitedField(name="MaxOccupancy")
    private Integer maxOccupancy;
    
    @DelimitedField(name="FunctionRoomAbbreviation")
    private String alias;

    @DelimitedField(name="Room")
    private String name;

    @DelimitedField(name="Description")
    private String description;
    
    @DelimitedField(name="DiaryName")
    private String diaryName;
    
    @DelimitedField(name="Building")
    private String building;

    @DelimitedField(name="Floor")
    private String floorName;
    
    @DelimitedField(name="LengthInFeet")
    @NotNull
    @Min(value=0)
    private BigDecimal lengthInFeet;

    @DelimitedField(name="LengthInMeters")
    @NotNull
    @Min(value=0)
    private BigDecimal lengthInMeters;
    
    @DelimitedField(name="WidthInFeet")
    @NotNull
    @Min(value=0)
    private BigDecimal widthInFeet;

    @DelimitedField(name="WidthInMeters")
    @NotNull
    @Min(value=0)
    private BigDecimal widthInMeters;
    
    @DelimitedField(name="ForceAlternateYN")
    private Boolean forceAlternate;

    @DelimitedField(name="ShareableYN")
    private Boolean shareable;

    @DelimitedField(name="DiaryDisplayYN")
    private Boolean diaryDisplay;
    
    @DelimitedField(name="MinimumRevenue")
    private BigDecimal minimumRevenue;
    
    @DelimitedField(name="ComboYN")
    private Boolean combo;

    @DelimitedField(name="MinAdvance")
    private Integer minAdvance;
    
    @DelimitedField(name="RoomCategory")
    private String roomCategory;

    @DelimitedField(name="RoomClass")
    private String roomClass;
    
    @DelimitedField(name="SuiteType")
    private String suiteType;

    @DelimitedField(name="ExcludedEventType")
    private String excludedEventType;

    private Integer maxAdvance;
    private Set<String> functionRoomPartIds = new HashSet<>();
    private String correlationMetadataId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public IntegrationType getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(IntegrationType integrationType) {
        this.integrationType = integrationType;
    }
    
    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getFunctionRoomId() {
        return functionRoomId;
    }

    public void setFunctionRoomId(String functionRoomId) {
        this.functionRoomId = functionRoomId;
    }

    public String getFunctionRoomAbbreviation() {
        return functionRoomAbbreviation;
    }

    public void setFunctionRoomAbbreviation(String functionRoomAbbreviation) {
        this.functionRoomAbbreviation = functionRoomAbbreviation;
    }

    public String getFunctionRoomType() {
        return functionRoomType;
    }

    public void setFunctionRoomType(String functionRoomType) {
        this.functionRoomType = functionRoomType;
    }

    public BigDecimal getAreaSqFeet() {
        return areaSqFeet;
    }

    public void setAreaSqFeet(BigDecimal areaSqFeet) {
        this.areaSqFeet = areaSqFeet;
    }

    public BigDecimal getAreaSqMeters() {
        return areaSqMeters;
    }

    public void setAreaSqMeters(BigDecimal areaSqMeters) {
        this.areaSqMeters = areaSqMeters;
    }

    public Integer getMinOccupancy() {
        return minOccupancy;
    }

    public void setMinOccupancy(Integer minOccupancy) {
        this.minOccupancy = minOccupancy;
    }

    public Integer getMaxOccupancy() {
        return maxOccupancy;
    }

    public void setMaxOccupancy(Integer maxOccupancy) {
        this.maxOccupancy = maxOccupancy;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiaryName() {
        return diaryName;
    }

    public void setDiaryName(String diaryName) {
        this.diaryName = diaryName;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getFloorName() {
        return floorName;
    }

    public void setFloorName(String floorName) {
        this.floorName = floorName;
    }

    public BigDecimal getLengthInFeet() {
        return lengthInFeet;
    }

    public void setLengthInFeet(BigDecimal lengthInFeet) {
        this.lengthInFeet = lengthInFeet;
    }

    public BigDecimal getLengthInMeters() {
        return lengthInMeters;
    }

    public void setLengthInMeters(BigDecimal lengthInMeters) {
        this.lengthInMeters = lengthInMeters;
    }

    public BigDecimal getWidthInFeet() {
        return widthInFeet;
    }

    public void setWidthInFeet(BigDecimal widthInFeet) {
        this.widthInFeet = widthInFeet;
    }

    public BigDecimal getWidthInMeters() {
        return widthInMeters;
    }

    public void setWidthInMeters(BigDecimal widthInMeters) {
        this.widthInMeters = widthInMeters;
    }

    public Boolean isForceAlternate() {
        return forceAlternate;
    }

    public void setForceAlternate(Boolean forceAlternate) {
        this.forceAlternate = forceAlternate;
    }

    public Boolean isShareable() {
        return shareable;
    }

    public void setShareable(Boolean shareable) {
        this.shareable = shareable;
    }

    public Boolean isDiaryDisplay() {
        return diaryDisplay;
    }

    public void setDiaryDisplay(Boolean diaryDisplay) {
        this.diaryDisplay = diaryDisplay;
    }

    public BigDecimal getMinimumRevenue() {
        return minimumRevenue;
    }

    public void setMinimumRevenue(BigDecimal minimumRevenue) {
        this.minimumRevenue = minimumRevenue;
    }

    public Boolean isCombo() {
        return combo;
    }

    public void setCombo(Boolean combo) {
        this.combo = combo;
    }

    public Integer getMinAdvance() {
        return minAdvance;
    }

    public void setMinAdvance(Integer minAdvance) {
        this.minAdvance = minAdvance;
    }

    public String getRoomCategory() {
        return roomCategory;
    }

    public void setRoomCategory(String roomCategory) {
        this.roomCategory = roomCategory;
    }

    public String getRoomClass() {
        return roomClass;
    }

    public void setRoomClass(String roomClass) {
        this.roomClass = roomClass;
    }

    public String getSuiteType() {
        return suiteType;
    }

    public void setSuiteType(String suiteType) {
        this.suiteType = suiteType;
    }

    public String getExcludedEventType() {
        return excludedEventType;
    }

    public void setExcludedEventType(String excludedEventType) {
        this.excludedEventType = excludedEventType;
    }

    public Integer getMaxAdvance() {
        return maxAdvance;
    }

    public void setMaxAdvance(Integer maxAdvance) {
        this.maxAdvance = maxAdvance;
    }

    public Set<String> getFunctionRoomPartIds() {
        return functionRoomPartIds;
    }

    public void setFunctionRoomPartIds(Set<String> functionRoomPartIds) {
        this.functionRoomPartIds = functionRoomPartIds;
    }

    public String getCorrelationMetadataId() {
        return correlationMetadataId;
    }

    public void setCorrelationMetadataId(String correlationMetadataId) {
        this.correlationMetadataId = correlationMetadataId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }
}
