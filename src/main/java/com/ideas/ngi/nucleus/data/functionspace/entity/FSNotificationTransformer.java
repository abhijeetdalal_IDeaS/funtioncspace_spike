package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.functionspace.dto.FunctionSpaceData;
import com.ideas.ngi.functionspace.dto.FunctionSpaceDataNotification;
import com.ideas.ngi.nucleus.rest.hateoas.ProxyLinkBuilderTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.BaseUriLinkBuilder;
import org.springframework.integration.transformer.GenericTransformer;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class FSNotificationTransformer implements GenericTransformer<FunctionSpaceData, FunctionSpaceDataNotification> {
    private ProxyLinkBuilderTransformer linkBuilderTransformer;
    private static final String FS_CORR = "/correlationMetadataService/search?integrationType={integrationType}&sendingSystemPropertyId={propertyId}&correlationStatus=SUCCESSFUL&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}";
    private static final String FS_EVENT_TYPE = "/functionSpace/eventTypes/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}";
    private static final String FS_MARKET_SEG = "/functionSpace/marketSegments/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}";
    private static final String FS_ROOM = "/functionSpace/rooms/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}";
    private static final String FS_COMBO_ROOM = "/functionSpace/comboRooms/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}";
    private static final String FS_BOOKING = "/functionSpace/bookings/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}";
    private static final String FS_BOOKING_INDEXED = "/functionSpace/bookings/search/optimized?clientCode={clientCode}&propertyCode={propertyCode}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}";
    private static final String FS_BOOKING_PACE = "/functionSpace/bookings/pace/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}";
    private static final String FS_GUEST_ROOM = "/functionSpace/bookedGuestRooms/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}";
    private static final String FS_EVENTS = "/functionSpace/events/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}";
    private static final String FS_EVENTS_INDEXED = "/functionSpace/events/search/optimized?clientCode={clientCode}&propertyCode={propertyCode}&startDate={startDate}&format=haljson&page={page}&size={size}";
    
    @Autowired
    public FSNotificationTransformer(ProxyLinkBuilderTransformer linkBuilderTransformer) {
        this.linkBuilderTransformer = linkBuilderTransformer;
    }

    @Override
    public FunctionSpaceDataNotification transform(FunctionSpaceData source) {
        FunctionSpaceDataNotification notification = new FunctionSpaceDataNotification(source);

        notification.setCorrelationLink(linkBuilderTransformer.transform(new BaseUriLinkBuilder(UriComponentsBuilder
                .fromPath(FS_CORR))).withRel("fsCorrelation"));
        notification.setEventTypeLink(linkBuilderTransformer.transform(new BaseUriLinkBuilder(UriComponentsBuilder
                .fromPath(FS_EVENT_TYPE))).withRel("fsEventType"));
        notification.setMarketSegmentLink(linkBuilderTransformer.transform(new BaseUriLinkBuilder(UriComponentsBuilder
                .fromPath(FS_MARKET_SEG))).withRel("fsMarketSegment"));
        notification.setRoomLink(linkBuilderTransformer.transform(new BaseUriLinkBuilder(UriComponentsBuilder
                .fromPath(FS_ROOM))).withRel("fsRoom"));
        notification.setRoomComboLink(linkBuilderTransformer.transform(new BaseUriLinkBuilder(UriComponentsBuilder
                .fromPath(FS_COMBO_ROOM))).withRel("fsRoomCombo"));
        notification.setBookingLink(linkBuilderTransformer.transform(new BaseUriLinkBuilder(UriComponentsBuilder
                .fromPath(FS_BOOKING))).withRel("fsBooking"));
        notification.setBookingIndexedLink(linkBuilderTransformer.transform(new BaseUriLinkBuilder(UriComponentsBuilder
                .fromPath(FS_BOOKING_INDEXED))).withRel("fsBookingIndexed"));
        notification.setBookingPaceLink(linkBuilderTransformer.transform(new BaseUriLinkBuilder(UriComponentsBuilder
                .fromPath(FS_BOOKING_PACE))).withRel("fsBookingPace"));
        notification.setBookingGuestLink(linkBuilderTransformer.transform(new BaseUriLinkBuilder(UriComponentsBuilder
                .fromPath(FS_GUEST_ROOM))).withRel("fsBookingGuest"));
        notification.setEventLink(linkBuilderTransformer.transform(new BaseUriLinkBuilder(UriComponentsBuilder
                .fromPath(FS_EVENTS))).withRel("fsEvent"));
        notification.setEventIndexedLink(linkBuilderTransformer.transform(new BaseUriLinkBuilder(UriComponentsBuilder
                .fromPath(FS_EVENTS_INDEXED))).withRel("fsEventIndexed"));

        return notification;
    }
}
