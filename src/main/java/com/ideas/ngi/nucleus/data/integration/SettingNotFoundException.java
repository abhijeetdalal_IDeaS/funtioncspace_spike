package com.ideas.ngi.nucleus.data.integration;

import com.ideas.ngi.nucleus.exception.NucleusException;

public class SettingNotFoundException extends NucleusException {
    private static final String MESSAGE = "Could not find a value for attribute: %s, vendor id: %s, client: %s, property: %s";

    public SettingNotFoundException(String vendorId, String clientCode, String propertyCode, String attribute) {
        super(String.format(MESSAGE, attribute, vendorId, clientCode, propertyCode));
    }
}
