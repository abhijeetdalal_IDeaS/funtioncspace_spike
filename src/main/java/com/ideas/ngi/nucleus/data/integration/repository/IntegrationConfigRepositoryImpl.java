package com.ideas.ngi.nucleus.data.integration.repository;

import com.ideas.ngi.nucleus.config.mongo.GlobalMongoConfig;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.integration.IntegrationSettingsException;
import com.ideas.ngi.nucleus.data.integration.InvalidSettingsException;
import com.ideas.ngi.nucleus.data.integration.entity.BaseIntegrationConfig;
import com.ideas.ngi.nucleus.data.integration.entity.IntegrationPropertyConfig;
import com.ideas.ngi.nucleus.data.integration.entity.VendorProperty;
import com.ideas.ngi.nucleus.data.vendor.entity.VendorConfigType;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.MongoPersistentEntity;
import org.springframework.data.mongodb.core.mapping.MongoPersistentProperty;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static com.ideas.ngi.nucleus.data.integration.entity.BaseIntegrationConfig.COLLECTION_NAME;
import static com.ideas.ngi.nucleus.data.integration.entity.BaseIntegrationConfig.PROPERTIES;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Component
public class IntegrationConfigRepositoryImpl implements IntegrationConfigRepositoryCustom {
    private static final String FIELD_CLASS = "_class";
    private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationConfigRepositoryImpl.class);
    private final MongoOperations mongoOperations;

    private static final PageRequest SINGLE_VALUE_REQUEST = PageRequest.of(0, 1, Sort.by("priority"));

    @Autowired
    public IntegrationConfigRepositoryImpl(@Qualifier(GlobalMongoConfig.GLOBAL_MONGO_TEMPLATE) MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends BaseIntegrationConfig> T findOne(VendorProperty property) {
        return (T) mongoOperations.findOne(query(property.toLevelCriteria()), property.toLevelType());
    }

    @Override
    public <T> T getValue(VendorProperty identifier, String fieldName) {
        Query query = query(identifier.toCriteria())
                .with(SINGLE_VALUE_REQUEST);
        return getSingleValue(query, fieldName);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getValue(VendorProperty identifier, VendorConfigType configType) {
        Query query = query(identifier.toCriteria())
                .with(SINGLE_VALUE_REQUEST);
        return getSingleValue(query, configType);
    }

    private String getQualfiiedFieldName(VendorConfigType vendorConfigType) {
        return PROPERTIES.concat(".").concat(vendorConfigType.getName());
    }

    @Override
    public <T> T getValueAtLevel(VendorProperty identifier, String fieldName) {
        Query query = query(identifier.toLevelCriteria());
        return getSingleValue(query, fieldName);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getValueAtLevel(VendorProperty identifier, VendorConfigType configType) {
        Query query = query(identifier.toLevelCriteria());
        return getSingleValue(query, configType);
    }

    @Retryable(value = OptimisticLockingFailureException.class)
    @Override
    public void setValueAtLevel(VendorProperty identifier, String fieldName, Object value) {
        Query query = query(identifier.toLevelCriteria());

        // determine the level at which this configuration is being updated
        Class<? extends BaseIntegrationConfig> targetType = identifier.toLevelType();

        // use the converter to take advantage of all the good spring stuff (conversion, field naming, etc)
        MongoPersistentEntity<?> entity = mongoOperations.getConverter().getMappingContext().getPersistentEntity(targetType);
        if (entity == null) {
            throw new InvalidSettingsException("No entity exists for type: " + targetType.getSimpleName());
        }
        MongoPersistentProperty property = entity.getPersistentProperty(fieldName);
        if (property == null) {
            throw new InvalidSettingsException("No setting exists for " + fieldName + " on " + targetType.getSimpleName());
        }

        Object targetConfig = mongoOperations.findOne(query, targetType);
        if (targetConfig == null) {
            throw new IntegrationSettingsException(
                    String.format("No settings found for vendor: %s, client: %s, property: %s",
                            identifier.getVendorId(), identifier.getClientCode(), identifier.getPropertyCode()));
        }

        // update the value
        entity.getPropertyAccessor(targetConfig).setProperty(property, value);
        mongoOperations.save(targetConfig);
    }

    @Retryable(value = OptimisticLockingFailureException.class)
    @Override
    public void setValueAtLevel(VendorProperty identifier, VendorConfigType configType, Object value) {
        Query query = query(identifier.toLevelCriteria());

        BaseIntegrationConfig targetConfig = mongoOperations.findOne(query, identifier.toLevelType());
        if (targetConfig == null) {
            throw new IntegrationSettingsException(
                    String.format("No settings found for vendor: %s, client: %s, property: %s",
                            identifier.getVendorId(), identifier.getClientCode(), identifier.getPropertyCode()));
        }

        targetConfig.setExtendedAttribute(configType, value);
        mongoOperations.save(targetConfig);
    }

    @Override
    public VendorProperty getVendorPropertyId(Set<IntegrationType> integrationTypes, String clientCode, String propertyCode) {
        Query query = query(where("integrationType").in(integrationTypes)
                .and("clientCode").is(Objects.requireNonNull(clientCode))
                .and("propertyCode").is(Objects.requireNonNull(propertyCode)));

        String vendorId = getSingleValue(query, "vendorId");
        return vendorId == null ? null : new VendorProperty(vendorId, clientCode, propertyCode);
    }

    @Override
    public VendorProperty lookupVendorPropertyId(IntegrationType integrationType, String externalPropertyId) {
        Query query = query(where("integrationType").is(integrationType)
                .and("externalPropertyId").is(Objects.requireNonNull(externalPropertyId)));

        IntegrationPropertyConfig propertyConfig = mongoOperations.findOne(query, IntegrationPropertyConfig.class);
        return propertyConfig == null ? null : propertyConfig.toVendorProperty();
    }


    @Override
    public boolean exists(VendorProperty vendorProperty) {
        return mongoOperations.count(query(vendorProperty.toLevelCriteria()), BaseIntegrationConfig.COLLECTION_NAME) > 0;
    }

    @SuppressWarnings("unchecked")
    private <T> T getSingleValue(Query query, VendorConfigType configType) {
        String qualifiedName = getQualfiiedFieldName(configType);
        query.addCriteria(where(qualifiedName).exists(true))
            .fields().include(qualifiedName).include(FIELD_CLASS);

        // fetch the data, in its raw form from mongo
        Optional<Document> result = Optional.ofNullable(mongoOperations.findOne(query, Document.class, COLLECTION_NAME));
        return (T) result.map(r -> (Document) r.get(PROPERTIES))
                .map(r -> r.get(configType.getName()))
                .orElse(null);
    }

    @SuppressWarnings("unchecked")
    private <T> T getSingleValue(Query query, String fieldName) {
        query.addCriteria(where(fieldName).exists(true))
                .fields().include(fieldName).include(FIELD_CLASS);

        // fetch the data, in its raw form from mongo
        Document result = mongoOperations.findOne(query, Document.class, COLLECTION_NAME);
        if (result == null || result.get(fieldName) == null) {
            return null;
        }

        // convert the raw result to the result type
        String className = (String) result.get(FIELD_CLASS);
        Class<?> sourceType;
        try {
            sourceType = Class.forName(className);
        } catch (ClassNotFoundException e) {
            LOGGER.warn("Could not find class for settings type: {}", className, e);
            throw new InvalidSettingsException("Result of " + className + " does not match any known types");
        }

        // use the converter to take advantage of all the good spring stuff (conversion, field naming, etc)
        MongoPersistentProperty property = findProperty(sourceType, fieldName);
        Object source = mongoOperations.getConverter().read(sourceType, result);
        try {
            return (T) property.getField().get(source);
        } catch (IllegalAccessException e) {
            LOGGER.warn("Access to field {} is restricted on {}", fieldName, className, e);
            throw new InvalidSettingsException("Access to field " + fieldName + " is restricted on " + className);
        }
    }

    private MongoPersistentProperty findProperty(Class<?> persistentEntity, String fieldName) {
        MongoPersistentEntity<?> entity = mongoOperations.getConverter().getMappingContext().getPersistentEntity(persistentEntity);
        if (entity == null) {
            throw new InvalidSettingsException("No entity exists for type: " + persistentEntity.getSimpleName());
        }

        MongoPersistentProperty property = entity.getPersistentProperty(fieldName);
        if (property == null) {
            throw new InvalidSettingsException("No persistent property exists for " + fieldName + " on " + persistentEntity.getSimpleName());
        }

        return property;
    }
}
