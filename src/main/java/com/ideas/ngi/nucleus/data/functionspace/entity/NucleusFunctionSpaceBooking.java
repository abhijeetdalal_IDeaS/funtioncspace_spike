package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.nucleus.data.AuditableEntity;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.functionspace.file.delimited.annotation.DelimitedField;
import com.ideas.ngi.nucleus.integration.annotation.ClientCode;
import com.ideas.ngi.nucleus.integration.annotation.PropertyCode;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document
public class NucleusFunctionSpaceBooking extends AuditableEntity implements Serializable {

    private static final long serialVersionUID = 4979319803739507836L;

    @Id
    private String id;

    @NotNull
    private IntegrationType integrationType;

    @DelimitedField(name = "PropertyID")
    @NotNull
    private String propertyId;

    @DelimitedField(name = "BookingID")
    @NotNull
    private String bookingId;

    @DelimitedField(name = "MarketCode", defaultValue = "")
    @NotNull
    private String marketSegmentCode;

    @DelimitedField(name = "BookingType", defaultValue = "")
    private String bookingType;

    @DelimitedField(name = "BookingStatus")
    private String bookingStatus;

    @DelimitedField(name = "AccountNameID")
    private String accountNameId;

    @DelimitedField(name = "Arrival")
    @NotNull
    private Date arrivalDate;

    @DelimitedField(name = "CancellationDate")
    private Date cancellationDate;

    @DelimitedField(name = "Attendees")
    private Integer attendees;

    @DelimitedField(name = "OpptyCreatedDate")
    private Date opportunityCreateDate;

    @DelimitedField(name = "CutoffDate")
    private Date cutoffDate;

    @DelimitedField(name = "CutoffDays")
    private Integer cutoffDays;

    @DelimitedField(name = "BookingStatus", defaultValue = "")
    @NotNull
    private String status;

    @DelimitedField(name = "BlockCode")
    private String blockCode;

    @DelimitedField(name = "BlockName")
    private String blockName;

    @DelimitedField(name = "CatStatus")
    private String catStatus;

    @DelimitedField(name = "CateringOnlyYN")
    private Boolean isCateringOnly;

    @DelimitedField(name = "DecisionDate")
    private Date decisionDate;

    @DelimitedField(name = "Departure")
    @NotNull
    private Date departureDate;

    @DelimitedField(name = "InsertDate")
    private Date insertDate;

    @DelimitedField(name = "UpdateDate")
    private Date updateDate;

    @ClientCode
    private String clientCode;
    @PropertyCode
    private String propertyCode;
    private String correlationId;

    private FunctionSpaceBookingCategory functionSpaceBookingCategory = FunctionSpaceBookingCategory.BOOKING;
    private String correlationMetadataId;
    private List<FunctionSpaceBookingRevenue> functionSpaceBookingRevenues = new ArrayList<>();

    private String blockOwnerPrimary;

    private String cateringOwner;

    private String blockStatusWhenLost;

    private String lostBusinessReason;

    private String lostToDestination;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public IntegrationType getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(IntegrationType integrationType) {
        this.integrationType = integrationType;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public FunctionSpaceBookingCategory getFunctionSpaceBookingCategory() {
        return functionSpaceBookingCategory;
    }

    public void setFunctionSpaceBookingCategory(FunctionSpaceBookingCategory functionSpaceBookingCategory) {
        this.functionSpaceBookingCategory = functionSpaceBookingCategory;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getMarketSegmentCode() {
        return marketSegmentCode;
    }

    public void setMarketSegmentCode(String marketSegmentCode) {
        this.marketSegmentCode = marketSegmentCode;
    }

    public String getAccountNameId() {
        return accountNameId;
    }

    public void setAccountNameId(String accountNameId) {
        this.accountNameId = accountNameId;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getCancellationDate() {
        return cancellationDate;
    }

    public void setCancellationDate(Date cancellationDate) {
        this.cancellationDate = cancellationDate;
    }

    public Integer getAttendees() {
        return attendees;
    }

    public void setAttendees(Integer attendees) {
        this.attendees = attendees;
    }

    public Date getOpportunityCreateDate() {
        return opportunityCreateDate;
    }

    public void setOpportunityCreateDate(Date opportunityCreateDate) {
        this.opportunityCreateDate = opportunityCreateDate;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public Date getCutoffDate() {
        return cutoffDate;
    }

    public void setCutoffDate(Date cutoffDate) {
        this.cutoffDate = cutoffDate;
    }

    public Integer getCutoffDays() {
        return cutoffDays;
    }

    public void setCutoffDays(Integer cutoffDays) {
        this.cutoffDays = cutoffDays;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBlockCode() {
        return blockCode;
    }

    public void setBlockCode(String blockCode) {
        this.blockCode = blockCode;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getCatStatus() {
        return catStatus;
    }

    public void setCatStatus(String catStatus) {
        this.catStatus = catStatus;
    }

    public Boolean isCateringOnly() {
        return isCateringOnly;
    }

    public void setCateringOnly(Boolean isCateringOnly) {
        this.isCateringOnly = isCateringOnly;
    }

    public Date getDecisionDate() {
        return decisionDate;
    }

    public void setDecisionDate(Date decisionDate) {
        this.decisionDate = decisionDate;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getCorrelationMetadataId() {
        return correlationMetadataId;
    }

    public void setCorrelationMetadataId(String correlationMetadataId) {
        this.correlationMetadataId = correlationMetadataId;
    }

    public List<FunctionSpaceBookingRevenue> getFunctionSpaceBookingRevenues() {
        return functionSpaceBookingRevenues;
    }

    public void setFunctionSpaceBookingRevenues(List<FunctionSpaceBookingRevenue> functionSpaceBookingRevenues) {
        this.functionSpaceBookingRevenues = functionSpaceBookingRevenues;
    }

    public void addFunctionSpaceBookingRevenue(FunctionSpaceBookingRevenue functionSpaceBookingRevenue) {
        if (functionSpaceBookingRevenues == null) {
            functionSpaceBookingRevenues = new ArrayList<>();
        }
        functionSpaceBookingRevenues.add(functionSpaceBookingRevenue);
    }

    public FunctionSpaceBookingRevenue findFunctionSpaceBookingRevenue(String revenueGroup, String revenueType) {
        if (CollectionUtils.isEmpty(functionSpaceBookingRevenues)) {
            return null;
        }

        for (FunctionSpaceBookingRevenue functionSpaceBookingRevenue : functionSpaceBookingRevenues) {
            if (StringUtils.equals(functionSpaceBookingRevenue.getRevenueGroup(), revenueGroup) && StringUtils.equals(functionSpaceBookingRevenue.getRevenueType(), revenueType)) {
                return functionSpaceBookingRevenue;
            }
        }
        return null;
    }

    public void applyFunctionSpaceBookingCategory(FunctionSpaceBookingCategory additionalFunctionSpaceBookingCategory) {
        // If the current category is 'BOOKING', then we should set the category to which over category
        // is it will be for either guest rooms, events or both
        if (FunctionSpaceBookingCategory.BOOKING.equals(this.functionSpaceBookingCategory)) {
            this.functionSpaceBookingCategory = additionalFunctionSpaceBookingCategory;
        }


        // If the booking has guest rooms and the additional category is event set the bookings with guestrooms & events category
        // If the booking has events and the additional category is guest rooms, set the bookings with guestrooms & events category
        if (isFunctionSpaceBookingCategory(FunctionSpaceBookingCategory.BOOKING_WITH_GUEST_ROOMS)
                && additionalFunctionSpaceBookingCategory.equals(FunctionSpaceBookingCategory.BOOKING_WITH_EVENTS)
                || isFunctionSpaceBookingCategory(FunctionSpaceBookingCategory.BOOKING_WITH_EVENTS)
                && additionalFunctionSpaceBookingCategory.equals(FunctionSpaceBookingCategory.BOOKING_WITH_GUEST_ROOMS)) {
            this.functionSpaceBookingCategory = FunctionSpaceBookingCategory.BOOKING_WITH_EVENTS_AND_GUESTROOMS;
        }
    }

    private boolean isFunctionSpaceBookingCategory(FunctionSpaceBookingCategory category) {
        return this.functionSpaceBookingCategory.equals(category);

    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }

    public String getBlockOwnerPrimary() {
        return blockOwnerPrimary;
    }

    public void setBlockOwnerPrimary(String blockOwnerPrimary) {
        this.blockOwnerPrimary = blockOwnerPrimary;
    }

    public String getCateringOwner() {
        return cateringOwner;
    }

    public void setCateringOwner(String cateringOwner) {
        this.cateringOwner = cateringOwner;
    }

    public String getBlockStatusWhenLost() {
        return blockStatusWhenLost;
    }

    public void setBlockStatusWhenLost(String blockStatusWhenLost) {
        this.blockStatusWhenLost = blockStatusWhenLost;
    }

    public String getLostBusinessReason() {
        return lostBusinessReason;
    }

    public void setLostBusinessReason(String lostBusinessReason) {
        this.lostBusinessReason = lostBusinessReason;
    }

    public String getLostToDestination() {
        return lostToDestination;
    }

    public void setLostToDestination(String lostToDestination) {
        this.lostToDestination = lostToDestination;
    }
}