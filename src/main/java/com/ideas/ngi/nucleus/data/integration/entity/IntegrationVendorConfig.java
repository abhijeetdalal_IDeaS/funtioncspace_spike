package com.ideas.ngi.nucleus.data.integration.entity;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@SuppressWarnings("unused")
public class IntegrationVendorConfig extends BaseIntegrationConfig {
    private static final long serialVersionUID = 1755234215482584151L;

    private String customReplyToAddress;
    private Boolean useCustomSoapAction;
    private Boolean cancelMultiUnitDecrements;
    private Boolean dedicatedValidationResponseChannel;
    private Boolean sendHTNGCallbackRequest;

    public IntegrationVendorConfig() {}
    public IntegrationVendorConfig(String vendorId, IntegrationType integrationType) {
        super(vendorId, integrationType);
    }

    public String getCustomReplyToAddress() {
        return customReplyToAddress;
    }

    public void setCustomReplyToAddress(String customReplyToAddress) {
        this.customReplyToAddress = customReplyToAddress;
    }

    public Boolean getUseCustomSoapAction() {
        return useCustomSoapAction;
    }

    public void setUseCustomSoapAction(Boolean useCustomSoapAction) {
        this.useCustomSoapAction = useCustomSoapAction;
    }

    public Boolean getCancelMultiUnitDecrements() {
        return cancelMultiUnitDecrements;
    }

    public void setCancelMultiUnitDecrements(Boolean cancelMultiUnitDecrements) {
        this.cancelMultiUnitDecrements = cancelMultiUnitDecrements;
    }

    public Boolean getDedicatedValidationResponseChannel() {
        return dedicatedValidationResponseChannel != null ? dedicatedValidationResponseChannel : false;
    }

    public void setDedicatedValidationResponseChannel(Boolean htngDedicatedValidationResponseChannel) {
        this.dedicatedValidationResponseChannel = htngDedicatedValidationResponseChannel;
    }

    @Override
    public VendorProperty toVendorProperty() {
        return new VendorProperty(getVendorId(), null, null);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    public Boolean getSendHTNGCallbackRequest() {
        return sendHTNGCallbackRequest;
    }

    public void setSendHTNGCallbackRequest(Boolean sendHTNGCallbackRequest) {
        this.sendHTNGCallbackRequest = sendHTNGCallbackRequest;
    }
}
