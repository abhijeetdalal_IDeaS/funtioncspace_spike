package com.ideas.ngi.nucleus.data;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.io.Serializable;
import java.util.Date;

public abstract class AuditableEntity implements Serializable {

    private static final long serialVersionUID = 5519386806625909345L;
    public static final String LAST_MODIFIED_FIELD = "lastModifiedDate";
    public static final String CREATED_FIELD = "createDate";


    @CreatedDate
    private Date createDate;
    @LastModifiedDate
    private Date lastModifiedDate;
     
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, "id", LAST_MODIFIED_FIELD, CREATED_FIELD);

    }
    
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, "id", LAST_MODIFIED_FIELD, CREATED_FIELD);
    }
}
