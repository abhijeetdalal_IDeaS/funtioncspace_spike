package com.ideas.ngi.nucleus.data.correlation.endpoint;

import com.ideas.ngi.nucleus.rest.endpoint.RestEndpoint;
import org.springframework.http.HttpMethod;

import java.util.List;

public enum CorrelationMetadataEndpoint implements RestEndpoint {

    POST_EVENT(HttpMethod.POST, "ngiEventService/event/?propertyId={defaultPropertyId}&sendingSystemPropertyId={sendingSystemPropertyId}&integrationType={integrationType}&clientCode={clientCode}&propertyCode={propertyCode}", List.class);
    
    private final HttpMethod httpMethod;
    private final String url;
    private final Class<?> responseType;

    CorrelationMetadataEndpoint(HttpMethod httpMethod, String url, Class<?> responseType) {
        this.httpMethod = httpMethod;
        this.url = url;
        this.responseType = responseType;
    }
    
    @Override
    public HttpMethod getHttpMethod() {
        return httpMethod;
    }
    
    @Override
    public String getURL() {
        return url;
    }

    @Override
    public Class<?> getResponseType() {
        return responseType;
    }

}
