package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.nucleus.data.AuditableEntity;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.functionspace.file.delimited.annotation.DelimitedField;
import com.ideas.ngi.nucleus.integration.annotation.ClientCode;
import com.ideas.ngi.nucleus.integration.annotation.PropertyCode;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Document
public class NucleusFunctionSpaceBookingPace extends AuditableEntity implements Serializable {

    private static final long serialVersionUID = -6335942385212938495L;

    @Id
    private String id;

    @NotNull
    private IntegrationType integrationType;

    @DelimitedField(name="PropertyID")
    @NotNull
    private String propertyId;

    @DelimitedField(name="BookingID")
    @NotNull
    private String bookingId;

    @DelimitedField(name="BookingChangeID")
    @NotNull
    private String bookingPaceId;

    @DelimitedField(name="ChangeDate")
    @NotNull
    private Date changeDate;

    @DelimitedField(name="StayDate")
    private Date stayDate;

    @DelimitedField(name="RoomNights")
    private Integer roomNights;
    
    @DelimitedField(name="RoomRevenue")
    private BigDecimal roomRevenue;

    @DelimitedField(name="BookingStatus", defaultValue="")
    private String bookingStatus;

    @DelimitedField(name="UpdateDate")
    private Date updateDate;

    @DelimitedField(name = "Arrival")
    private Date arrivalDate;

    @DelimitedField(name = "Departure")
    private Date departureDate;
    
    private String correlationMetadataId;
    private String cateringStatus;
    private String guestRoomStatus;
    @PropertyCode
    private String propertyCode;
    @ClientCode
    private String clientCode;
    private String correlationId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public IntegrationType getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(IntegrationType integrationType) {
        this.integrationType = integrationType;
    }
    
    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getBookingPaceId() {
        return bookingPaceId;
    }

    public void setBookingPaceId(String bookingPaceId) {
        this.bookingPaceId = bookingPaceId;
    }

    public Date getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }

    public Date getStayDate() {
        return stayDate;
    }

    public void setStayDate(Date stayDate) {
        this.stayDate = stayDate;
    }

    public Integer getRoomNights() {
        return roomNights;
    }

    public void setRoomNights(Integer roomNights) {
        this.roomNights = roomNights;
    }

    public BigDecimal getRoomRevenue() {
        return roomRevenue;
    }

    public void setRoomRevenue(BigDecimal roomRevenue) {
        this.roomRevenue = roomRevenue;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getCorrelationMetadataId() {
        return correlationMetadataId;
    }

    public void setCorrelationMetadataId(String correlationMetadataId) {
        this.correlationMetadataId = correlationMetadataId;
    }

    public String getCateringStatus() {
        return cateringStatus;
    }

    public void setCateringStatus(String cateringStatus) {
        this.cateringStatus = cateringStatus;
    }

    public String getGuestRoomStatus() {
        return guestRoomStatus;
    }

    public void setGuestRoomStatus(String guestRoomStatus) {
        this.guestRoomStatus = guestRoomStatus;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }

    public void generateBookingPaceId() {
        if (StringUtils.isEmpty(bookingPaceId)) {
            Objects.requireNonNull(changeDate, "Change date is required to generate a pace id");
            Objects.requireNonNull(bookingId, "Booking id is required to generate a pace id");

            String tempPaceId = bookingId + "-" + changeDate.getTime();
            if (stayDate != null) {
                tempPaceId += "-"+ stayDate.getTime();
            }

            bookingPaceId = tempPaceId;
        }
    }

    public boolean isEquals(NucleusFunctionSpaceBookingPace other) {
        return EqualsBuilder.reflectionEquals(this, other, "id", "createDate", "lastModifiedDate", "changeDate", "bookingPaceId", "correlationId");
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }
}
