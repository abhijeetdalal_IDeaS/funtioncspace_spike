package com.ideas.ngi.nucleus.data.vendor.entity;

import com.ideas.ngi.nucleus.integration.annotation.ClientEnvironment;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChainConfigParams implements Serializable {
    private static final long serialVersionUID = 2050324673515491233L;

    private String chainCode;
    private String g2ChainCode;
    private String g3ChainCode;
    private VendorCredentials outboundCredentials;
    private VendorCredentials inboundCredentials;
    @ClientEnvironment
    private String clientEnvironmentName;
    private String baseCurrencyCode;
    private List<HotelConfigParams> hotels;
    private Boolean unqualifiedRatesDirectPopulationDisabled;
    private Boolean qualifiedRatesDirectPopulationDisabled;
    private Integer installationReservationsThreshold;
    private Boolean populatePackageDataEnabled;
    private Boolean oxiRoomStayReservationByDay;
    private Boolean htngRoomStayReservationByDay;
    private Boolean folsRoomStayReservationByDay;
    private Boolean useNetRoomRevenue;
    private Boolean htngUseBasicAuth;
    private Boolean includePseudoInRevenue;
    private Boolean includeDayUseInRevenue;
    private Boolean includeNoShowInRevenue;
    private Boolean ratePlanCleanupEnabled;
    private Boolean includeRoomTypeHotelMarketSegmentActivity;
    private Boolean sendHTNGCallbackRequest;
    private Boolean preventPseudoDataInActivity;
    private Boolean isComponentRoomsActivityEnabled;
    private Boolean useCurrencyInMessage;
    private Boolean useLegacyRoomStayHandling;
    private String oxiParkMessageTypes;
    private Boolean generateMarketSegmentStatsEnabled;
    private Boolean generateRateCodeStatsEnabled;
    private Boolean resetVirtualSuiteCounts;
    private Boolean generateHotelActivityFromRoomTypeActivity;

    @Getter
    @Setter
    private String salesAndCateringUnitOfMeasure;

    @Getter
    @Setter
    private String salesAndCateringStatus;

    @Getter
    @Setter
    private Boolean summaryPersistenceEnabled;

    @Getter
    @Setter
    private Boolean msrtSummaryPersistenceEnabled;

    @Getter
    @Setter
    private Boolean buildMSActivityUsingPMSMS;

    public String getChainCode() {
        return chainCode;
    }

    public void setChainCode(String chainCode) {
        this.chainCode = chainCode;
    }

    String getG3ChainCode() {
        return g3ChainCode;
    }

    public void setG3ChainCode(String g3ChainCode) {
        this.g3ChainCode = g3ChainCode;
    }

    public List<HotelConfigParams> getHotels() {
        if (hotels == null) {
            hotels = new ArrayList<>();
        }
        return hotels;
    }

    public void setHotels(List<HotelConfigParams> hotels) {
        this.hotels = hotels;
    }

    public HotelConfigParams getHotel(String hotelCode) {
        return getHotels().stream().filter(h -> h.getHotelCode().equals(hotelCode)).findFirst().orElse(null);
    }

    public VendorCredentials getOutboundCredentials() {
        return outboundCredentials;
    }

    public void setOutboundCredentials(VendorCredentials outboundCredentials) {
        this.outboundCredentials = outboundCredentials;
    }

    public VendorCredentials getInboundCredentials() {
        return inboundCredentials;
    }

    public void setInboundCredentials(VendorCredentials inboundCredentials) {
        this.inboundCredentials = inboundCredentials;
    }

    public Boolean getOxiRoomStayReservationByDay() {
        return oxiRoomStayReservationByDay;
    }

    public void setOxiRoomStayReservationByDay(Boolean oxiRoomStayReservationByDay) {
        this.oxiRoomStayReservationByDay = oxiRoomStayReservationByDay;
    }

    public Boolean getHtngRoomStayReservationByDay() {
        return htngRoomStayReservationByDay;
    }

    public void setHtngRoomStayReservationByDay(Boolean htngRoomStayReservationByDay) {
        this.htngRoomStayReservationByDay = htngRoomStayReservationByDay;
    }

    public Boolean getFolsRoomStayReservationByDay() {
        return folsRoomStayReservationByDay;
    }

    public Boolean getRatePlanCleanupEnabled() {
        return ratePlanCleanupEnabled;
    }

    public void setRatePlanCleanupEnabled(Boolean ratePlanCleanupEnabled) {
        this.ratePlanCleanupEnabled = ratePlanCleanupEnabled;
    }

    public void setFolsRoomStayReservationByDay(Boolean folsRoomStayReservationByDay) {
        this.folsRoomStayReservationByDay = folsRoomStayReservationByDay;
    }

    VendorCredentials findOutboundCredentials(String hotelCode) {
        HotelConfigParams hotelConfigParams = getHotel(hotelCode);
        if (hotelConfigParams != null && hotelConfigParams.getOutboundCredentials() != null) {
            return hotelConfigParams.getOutboundCredentials();
        } else if (outboundCredentials != null) {
            return outboundCredentials;
        }

        return null;
    }

    VendorCredentials findInboundCredentials(String hotelCode) {
        HotelConfigParams hotelConfigParams = getHotel(hotelCode);
        if (hotelConfigParams != null && hotelConfigParams.getInboundCredentials() != null) {
            return hotelConfigParams.getInboundCredentials();
        } else if (inboundCredentials != null) {
            return inboundCredentials;
        }

        return null;
    }

    public String findBaseCurrencyCode(String hotelCode) {
        HotelConfigParams hotelConfigParams = getHotel(hotelCode);
        if (hotelConfigParams == null || hotelConfigParams.getBaseCurrencyCode() == null) {
            return baseCurrencyCode;
        } else {
            return hotelConfigParams.getBaseCurrencyCode();
        }
    }

    String getG2ChainCode() {
        return g2ChainCode;
    }

    public void setG2ChainCode(String g2ChainCode) {
        this.g2ChainCode = g2ChainCode;
    }

    public String getClientEnvironmentName() {
        return clientEnvironmentName;
    }

    public void setClientEnvironmentName(String clientEnvironmentName) {
        this.clientEnvironmentName = clientEnvironmentName;
    }

    public String getBaseCurrencyCode() {
        return baseCurrencyCode;
    }

    public void setBaseCurrencyCode(String baseCurrencyCode) {
        this.baseCurrencyCode = baseCurrencyCode;
    }

    public Boolean getUnqualifiedRatesDirectPopulationDisabled() {
        return unqualifiedRatesDirectPopulationDisabled;
    }

    public void setUnqualifiedRatesDirectPopulationDisabled(Boolean unqualifiedRatesDirectPopulationDisabled) {
        this.unqualifiedRatesDirectPopulationDisabled = unqualifiedRatesDirectPopulationDisabled;
    }

    public Boolean getQualifiedRatesDirectPopulationDisabled() {
        return qualifiedRatesDirectPopulationDisabled;
    }

    public void setQualifiedRatesDirectPopulationDisabled(Boolean qualifiedRatesDirectPopulationDisabled) {
        this.qualifiedRatesDirectPopulationDisabled = qualifiedRatesDirectPopulationDisabled;
    }

    public Integer getInstallationReservationsThreshold() {
        return installationReservationsThreshold;
    }

    public void setInstallationReservationsThreshold(Integer installationReservationsThreshold) {
        this.installationReservationsThreshold = installationReservationsThreshold;
    }

    public Boolean getPopulatePackageDataEnabled() {
        return populatePackageDataEnabled;
    }

    public void setPopulatePackageDataEnabled(Boolean populatePackageDataEnabled) {
        this.populatePackageDataEnabled = populatePackageDataEnabled;
    }

    public Boolean findPopulatePackageDataEnabled(String hotelCode) {
        HotelConfigParams hotelConfigParams = getHotel(hotelCode);
        if (hotelConfigParams != null && hotelConfigParams.getPopulatePackageDataEnabled() != null) {
            return hotelConfigParams.getPopulatePackageDataEnabled();
        }
        return populatePackageDataEnabled;
    }

    public Boolean getUseNetRoomRevenue() {
        return useNetRoomRevenue;
    }

    public void setUseNetRoomRevenue(Boolean useNetRoomRevenue) {
        this.useNetRoomRevenue = useNetRoomRevenue;
    }

    public Boolean findUseNetRoomRevenue(String hotelCode) {
        HotelConfigParams hotelConfigParams = getHotel(hotelCode);
        if (hotelConfigParams != null && hotelConfigParams.getUseNetRoomRevenue() != null) {
            return hotelConfigParams.getUseNetRoomRevenue();
        }
        return useNetRoomRevenue;
    }

    public Boolean getHtngUseBasicAuth() {
        return htngUseBasicAuth;
    }

    public void setHtngUseBasicAuth(Boolean htngUseBasicAuth) {
        this.htngUseBasicAuth = htngUseBasicAuth;
    }

    public Boolean getIncludePseudoInRevenue() { return includePseudoInRevenue; }

    void setIncludePseudoInRevenue(Boolean includePseudoInRevenue) {
        this.includePseudoInRevenue = includePseudoInRevenue;
    }

    public Boolean getIncludeDayUseInRevenue() { return includeDayUseInRevenue; }

    void setIncludeDayUseInRevenue(Boolean includeDayUseInRevenue) {
        this.includeDayUseInRevenue = includeDayUseInRevenue;
    }

    public Boolean getIncludeNoShowInRevenue() { return includeNoShowInRevenue; }

    void setIncludeNoShowInRevenue(Boolean includeNoShowInRevenue) {
        this.includeNoShowInRevenue = includeNoShowInRevenue;
    }

    public Boolean getIncludeRoomTypeHotelMarketSegmentActivity() {
        return includeRoomTypeHotelMarketSegmentActivity;
    }

    public void setIncludeRoomTypeHotelMarketSegmentActivity(Boolean includeRoomTypeHotelMarketSegmentActivity) {
        this.includeRoomTypeHotelMarketSegmentActivity = includeRoomTypeHotelMarketSegmentActivity;
    }

    public Boolean getSendHTNGCallbackRequest() {
        return sendHTNGCallbackRequest;
    }

    public void setSendHTNGCallbackRequest(Boolean sendHTNGCallbackRequest) {
        this.sendHTNGCallbackRequest = sendHTNGCallbackRequest;
    }

    public Boolean getPreventPseudoDataInActivity() {
        return preventPseudoDataInActivity;
    }

    public void setPreventPseudoDataInActivity(Boolean preventPseudoDataInActivity) {
        this.preventPseudoDataInActivity = preventPseudoDataInActivity;
    }

    public Boolean getComponentRoomsActivityEnabled() {
        return isComponentRoomsActivityEnabled;
    }

    public void setComponentRoomsActivityEnabled(Boolean componentRoomsActivityEnabled) {
        isComponentRoomsActivityEnabled = componentRoomsActivityEnabled;
    }

    public Boolean getUseCurrencyInMessage() {
        return useCurrencyInMessage;
    }

    public void setUseCurrencyInMessage(Boolean useCurrencyInMessage) {
        this.useCurrencyInMessage = useCurrencyInMessage;
    }

    public Boolean getUseLegacyRoomStayHandling() {
        return useLegacyRoomStayHandling;
    }

    public void setUseLegacyRoomStayHandling(Boolean useLegacyRoomStayHandling) {
        this.useLegacyRoomStayHandling = useLegacyRoomStayHandling;
    }

    public String getOxiParkMessageTypes() {
        return oxiParkMessageTypes;
    }

    public void setOxiParkMessageTypes(String parkMessageTypes) {
        this.oxiParkMessageTypes = parkMessageTypes;
    }

    public Boolean getGenerateMarketSegmentStatsEnabled() {
        return generateMarketSegmentStatsEnabled;
    }

    public void setGenerateMarketSegmentStatsEnabled(Boolean generateMarketSegmentStatsEnabled) {
        this.generateMarketSegmentStatsEnabled = generateMarketSegmentStatsEnabled;
    }

    public Boolean getGenerateRateCodeStatsEnabled() {
        return generateRateCodeStatsEnabled;
    }

    public void setGenerateRateCodeStatsEnabled(Boolean generateRateCodeStatsEnabled) {
        this.generateRateCodeStatsEnabled = generateRateCodeStatsEnabled;
    }

    public Boolean getResetVirtualSuiteCounts() {
        return resetVirtualSuiteCounts;
    }

    public void setResetVirtualSuiteCounts(Boolean resetVirtualSuiteCounts) {
        this.resetVirtualSuiteCounts = resetVirtualSuiteCounts;
    }

    public Boolean getGenerateHotelActivityFromRoomTypeActivity() {
        return generateHotelActivityFromRoomTypeActivity;
    }

    public void setGenerateHotelActivityFromRoomTypeActivity(Boolean generateHotelActivityFromRoomTypeActivity) {
        this.generateHotelActivityFromRoomTypeActivity = generateHotelActivityFromRoomTypeActivity;
    }
}
