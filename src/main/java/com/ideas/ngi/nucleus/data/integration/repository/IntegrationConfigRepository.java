package com.ideas.ngi.nucleus.data.integration.repository;

import com.ideas.ngi.nucleus.config.mongo.annotation.GlobalCollection;
import com.ideas.ngi.nucleus.config.mongo.repository.NucleusMongoRepository;
import com.ideas.ngi.nucleus.data.integration.entity.BaseIntegrationConfig;
import com.ideas.ngi.nucleus.data.integration.entity.IntegrationClientConfig;
import com.ideas.ngi.nucleus.data.integration.entity.IntegrationPropertyConfig;
import com.ideas.ngi.nucleus.data.integration.entity.IntegrationVendorConfig;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@GlobalCollection
@RepositoryRestResource(collectionResourceRel = "nucleusIntegrationSettings")
public interface IntegrationConfigRepository extends NucleusMongoRepository<BaseIntegrationConfig, String>,
        IntegrationConfigRepositoryCustom {

    @Query("{ clientCode: { $exists: false }, propertyCode: { $exists: false } }")
    Page<IntegrationVendorConfig> findAllVendorConfigs(Pageable page);

    @Query("{ vendorId: ?0, clientCode: { $exists: true }, propertyCode: { $exists: false } }")
    Page<IntegrationClientConfig> findAllClientConfigs(String vendorId, Pageable page);

    @Query("{ vendorId: ?0, clientCode: ?1, propertyCode: { $exists: true } }")
    Page<IntegrationPropertyConfig> findAllPropertyConfigs(String vendorId, String clientCode, Pageable page);

}
