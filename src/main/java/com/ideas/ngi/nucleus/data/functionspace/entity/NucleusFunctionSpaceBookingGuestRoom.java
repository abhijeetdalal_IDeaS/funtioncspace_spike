package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ideas.ngi.nucleus.data.AuditableEntity;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.functionspace.file.delimited.annotation.DelimitedField;
import com.ideas.ngi.nucleus.integration.annotation.ClientCode;
import com.ideas.ngi.nucleus.integration.annotation.PropertyCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

@Document
public class NucleusFunctionSpaceBookingGuestRoom extends AuditableEntity implements Serializable {

    private static final long serialVersionUID = 4801728617194553663L;

    @Id
    private String id;

    @NotNull
    private IntegrationType integrationType;

    @DelimitedField(name = "PropertyID")
    @NotNull
    private String propertyId;

    @DelimitedField(name = "BookingID")
    @NotNull
    private String bookingId;

    @DelimitedField(name = "RoomDate")
    @NotNull
    private Date occupancyDate;

    @DelimitedField(name = "RoomCategory")
    @NotNull
    private String roomCategory;

    @DelimitedField(name = "GuestRoomID")
    private String guestRoomId;

    @DelimitedField(name = "SCRoomCategory")
    private String scRoomCategory;

    @DelimitedField(name = "RoomClass")
    private String roomClass;

    @DelimitedField(name = "Status")
    private String status;

    @DelimitedField(name = "GuestRoomRateSingle")
    private BigDecimal guestRoomRateSingle;

    @DelimitedField(name = "GuestRoomRateDouble")
    private BigDecimal guestRoomRateDouble;

    @DelimitedField(name = "GuestRoomRateTriple")
    private BigDecimal guestRoomRateTriple;

    @DelimitedField(name = "GuestRoomRateQuad")
    private BigDecimal guestRoomRateQuad;

    @DelimitedField(name = "ContractedSingleRooms")
    private Integer contractedSingleRooms;

    @DelimitedField(name = "ContractedDoubleRooms")
    private Integer contractedDoubleRooms;

    @DelimitedField(name = "ContractedTripleRooms")
    private Integer contractedTripleRooms;

    @DelimitedField(name = "ContractedQuadRooms")
    private Integer contractedQuadRooms;

    @DelimitedField(name = "ContractedRoomsTotal")
    private Integer contractedRoomsTotal;

    @DelimitedField(name = "BlockedSingleRooms")
    private Integer blockedSingleRooms;

    @DelimitedField(name = "BlockedDoubleRooms")
    private Integer blockedDoubleRooms;

    @DelimitedField(name = "BlockedTripleRooms")
    private Integer blockedTripleRooms;

    @DelimitedField(name = "BlockedQuadRooms")
    private Integer blockedQuadRooms;

    @DelimitedField(name = "ForecastedRoomsTotal")
    private Integer forecastedRoomsTotal;

    @DelimitedField(name = "PickupSingleRooms")
    private Integer pickupSingleRooms;

    @DelimitedField(name = "PickupDoubleRooms")
    private Integer pickupDoubleRooms;

    @DelimitedField(name = "PickupTripleRooms")
    private Integer pickupTripleRooms;

    @DelimitedField(name = "PickupQuadRooms")
    private Integer pickupQuadRooms;

    @DelimitedField(name = "PickupRoomsTotal")
    private Integer pickupRoomsTotal;

    @Nullable
    private Boolean runOfHouse = false;
    private String correlationMetadataId;
    private boolean hasBookedRooms = false;
    @ClientCode
    private String clientCode;
    @PropertyCode
    private String propertyCode;
    private String correlationId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public IntegrationType getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(IntegrationType integrationType) {
        this.integrationType = integrationType;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public Date getOccupancyDate() {
        return occupancyDate;
    }

    public void setOccupancyDate(Date occupancyDate) {
        this.occupancyDate = occupancyDate;
    }

    public String getGuestRoomId() {
        return guestRoomId;
    }

    public void setGuestRoomId(String guestRoomId) {
        this.guestRoomId = guestRoomId;
    }

    public String getRoomCategory() {
        return roomCategory;
    }

    public void setRoomCategory(String roomCategory) {
        this.roomCategory = roomCategory;
    }

    public String getScRoomCategory() {
        return scRoomCategory;
    }

    public void setScRoomCategory(String scRoomCategory) {
        this.scRoomCategory = scRoomCategory;
    }

    public String getRoomClass() {
        return roomClass;
    }

    public void setRoomClass(String roomClass) {
        this.roomClass = roomClass;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getGuestRoomRateSingle() {
        return guestRoomRateSingle;
    }

    public void setGuestRoomRateSingle(BigDecimal guestRoomRateSingle) {
        this.guestRoomRateSingle = guestRoomRateSingle;
    }

    public BigDecimal getGuestRoomRateDouble() {
        return guestRoomRateDouble;
    }

    public void setGuestRoomRateDouble(BigDecimal guestRoomRateDouble) {
        this.guestRoomRateDouble = guestRoomRateDouble;
    }

    public BigDecimal getGuestRoomRateTriple() {
        return guestRoomRateTriple;
    }

    public void setGuestRoomRateTriple(BigDecimal guestRoomRateTriple) {
        this.guestRoomRateTriple = guestRoomRateTriple;
    }

    public BigDecimal getGuestRoomRateQuad() {
        return guestRoomRateQuad;
    }

    public void setGuestRoomRateQuad(BigDecimal guestRoomRateQuad) {
        this.guestRoomRateQuad = guestRoomRateQuad;
    }

    public Integer getContractedSingleRooms() {
        return contractedSingleRooms;
    }

    public void setContractedSingleRooms(Integer contractedSingleRooms) {
        this.contractedSingleRooms = contractedSingleRooms;
    }

    public Integer getContractedDoubleRooms() {
        return contractedDoubleRooms;
    }

    public void setContractedDoubleRooms(Integer contractedDoubleRooms) {
        this.contractedDoubleRooms = contractedDoubleRooms;
    }

    public Integer getContractedTripleRooms() {
        return contractedTripleRooms;
    }

    public void setContractedTripleRooms(Integer contractedTripleRooms) {
        this.contractedTripleRooms = contractedTripleRooms;
    }

    public Integer getContractedQuadRooms() {
        return contractedQuadRooms;
    }

    public void setContractedQuadRooms(Integer contractedQuadRooms) {
        this.contractedQuadRooms = contractedQuadRooms;
    }

    public Integer getContractedRoomsTotal() {
        return contractedRoomsTotal;
    }

    public void setContractedRoomsTotal(Integer contractedRoomsTotal) {
        this.contractedRoomsTotal = contractedRoomsTotal;
    }

    public Integer getBlockedSingleRooms() {
        return blockedSingleRooms;
    }

    public void setBlockedSingleRooms(Integer blockedSingleRooms) {
        this.blockedSingleRooms = blockedSingleRooms;
    }

    public Integer getBlockedDoubleRooms() {
        return blockedDoubleRooms;
    }

    public void setBlockedDoubleRooms(Integer blockedDoubleRooms) {
        this.blockedDoubleRooms = blockedDoubleRooms;
    }

    public Integer getBlockedTripleRooms() {
        return blockedTripleRooms;
    }

    public void setBlockedTripleRooms(Integer blockedTripleRooms) {
        this.blockedTripleRooms = blockedTripleRooms;
    }

    public Integer getBlockedQuadRooms() {
        return blockedQuadRooms;
    }

    public void setBlockedQuadRooms(Integer blockedQuadRooms) {
        this.blockedQuadRooms = blockedQuadRooms;
    }

    public Integer getForecastedRoomsTotal() {
        return forecastedRoomsTotal;
    }

    public void setForecastedRoomsTotal(Integer forecastedRoomsTotal) {
        this.forecastedRoomsTotal = forecastedRoomsTotal;
    }

    public Integer getPickupSingleRooms() {
        return pickupSingleRooms;
    }

    public void setPickupSingleRooms(Integer pickupSingleRooms) {
        this.pickupSingleRooms = pickupSingleRooms;
    }

    public Integer getPickupDoubleRooms() {
        return pickupDoubleRooms;
    }

    public void setPickupDoubleRooms(Integer pickupDoubleRooms) {
        this.pickupDoubleRooms = pickupDoubleRooms;
    }

    public Integer getPickupTripleRooms() {
        return pickupTripleRooms;
    }

    public void setPickupTripleRooms(Integer pickupTripleRooms) {
        this.pickupTripleRooms = pickupTripleRooms;
    }

    public Integer getPickupQuadRooms() {
        return pickupQuadRooms;
    }

    public void setPickupQuadRooms(Integer pickupQuadRooms) {
        this.pickupQuadRooms = pickupQuadRooms;
    }

    public Integer getPickupRoomsTotal() {
        return pickupRoomsTotal;
    }

    public void setPickupRoomsTotal(Integer pickupRoomsTotal) {
        this.pickupRoomsTotal = pickupRoomsTotal;
    }

    public String getCorrelationMetadataId() {
        return correlationMetadataId;
    }

    public void setCorrelationMetadataId(String correlationMetadataId) {
        this.correlationMetadataId = correlationMetadataId;
    }

    /**
     * The way the 'hashBookedRooms' variable gets set is done on purpose to make sure we actually update that variable
     * any time this method gets called. It's important to note that this method get executed by the
     * FunctionSpaceBookingGuestRoomEventListener before the object is being stored. The 'hasBookedRooms' variable is
     * only used to indicate that the record does contain booked rooms (the Delphi integration received a lot of records
     * that don't have any impact on rooms) so that we can more efficiently pull the records from NGI into our client
     * applications.
     */
    @SuppressWarnings("squid:S1067")
    @JsonIgnore
    public boolean hasBookedRooms() {
        hasBookedRooms = !(isZero(contractedSingleRooms) &&
                isZero(contractedDoubleRooms) &&
                isZero(contractedTripleRooms) &&
                isZero(contractedQuadRooms) &&
                isZero(contractedRoomsTotal) &&
                isZero(blockedSingleRooms) &&
                isZero(blockedDoubleRooms) &&
                isZero(blockedTripleRooms) &&
                isZero(blockedQuadRooms) &&
                isZero(forecastedRoomsTotal) &&
                isZero(pickupSingleRooms) &&
                isZero(pickupDoubleRooms) &&
                isZero(pickupTripleRooms) &&
                isZero(pickupQuadRooms) &&
                isZero(pickupRoomsTotal));

        return hasBookedRooms;
    }

    public Integer getBlockedRoomsTotal(){
        return sum(blockedSingleRooms, blockedDoubleRooms, blockedTripleRooms, blockedQuadRooms);
    }

    private Integer sum(Integer... values){
        return Arrays.stream(values).filter(Objects::nonNull).reduce(Integer::sum).orElse(null);
    }

    public boolean isHasBookedRooms() {
        return hasBookedRooms;
    }

    private boolean isZero(Integer value) {
        return value == null || value.intValue() == 0;
    }

    @Nullable
    public Boolean getRunOfHouse() {
        return runOfHouse;
    }

    public void setRunOfHouse(@Nullable Boolean runOfHouse) {
        this.runOfHouse = runOfHouse;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }

    public String getPropertyCode() {
        return propertyCode;
    }
}
