package com.ideas.ngi.nucleus.data.integration;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.vendor.entity.VendorCredentials;

import java.math.BigDecimal;

public interface Setting<T> {
    Setting<Boolean> ASSUME_PACKAGE_INCLUDED = new SimpleSetting<>("assumePackageIncluded", Boolean.class);
    Setting<Boolean> ASSUME_TAX_INCLUDED = new SimpleSetting<>("assumeTaxIncluded", Boolean.class);
    Setting<String> BASE_CURRENCY_CODE = new SimpleSetting<>("baseCurrencyCode", String.class);
    Setting<Boolean> CALCULATE_NON_PICKED_UP_BLOCKS_USING_SUMMARY_DATA = new SimpleSetting<>("calculateNonPickedUpBlocksUsingSummaryData", Boolean.class);
    Setting<Boolean> CANCEL_MULTI_UNIT_DECREMENTS = new SimpleSetting<>("cancelMultiUnitDecrements", Boolean.class);
    Setting<String> CLIENT_ENVIRONMENT_NAME = new SimpleSetting<>("clientEnvironmentName", String.class);
    Setting<String> CUSTOM_REPLY_TO_ADDRESS = new SimpleSetting<>("customReplyToAddress", String.class);
    Setting<String> DEFAULT_ROOM_TYPE = new SimpleSetting<>("defaultRoomType", String.class);
    Setting<Integer> FUTURE_DAYS = new SimpleSetting<>("futureDays", Integer.class);
    Setting<Boolean> HANDLE_PREVIOUSLY_STRAIGHT_MARKET_SEGMENTS_IN_AMS = new SimpleSetting<>("handlePreviouslyStraightMarketSegmentsInAms", Boolean.class);
    Setting<Boolean> IN_CATCHUP = new SimpleSetting<>("inCatchup", Boolean.class);
    Setting<VendorCredentials> INBOUND_CREDENTIALS = new SimpleSetting<>("inboundCredentials", VendorCredentials.class);
    Setting<Boolean> INSTALL_MODE = new SimpleSetting<>("installMode", Boolean.class);
    Setting<Integer> INSTALLATION_RESERVATIONS_THRESHOLD = new SimpleSetting<>("installationReservationsThreshold", Integer.class);
    Setting<VendorCredentials> OUTBOUND_CREDENTIALS = new SimpleSetting<>("outboundCredentials", VendorCredentials.class);
    Setting<String> OUTGOING_URL = new SimpleSetting<>("outgoingUrl", String.class);
    Setting<String> OXI_INTERFACE_NAME = new SimpleSetting<>("oxiInterfaceName", String.class);
    Setting<Boolean> OXI_ROOM_STAY_RESERVATION_BY_DAY = new SimpleSetting<>("oxiRoomStayReservationByDay", Boolean.class);
    Setting<Boolean> HTNG_ROOM_STAY_RESERVATION_BY_DAY = new SimpleSetting<>("htngRoomStayReservationByDay", Boolean.class);
    Setting<Boolean> FOLS_ROOM_STAY_RESERVATION_BY_DAY = new SimpleSetting<>("folsRoomStayReservationByDay", Boolean.class);
    Setting<Integer> PAST_DAYS = new SimpleSetting<>("pastDays", Integer.class);
    Setting<Boolean> POPULATE_PACKAGE_DATA_ENABLED = new SimpleSetting<>("populatePackageDataEnabled", Boolean.class);
    Setting<Boolean> QUALIFIED_RATES_DIRECT_POPULATION_DISABLED = new SimpleSetting<>("qualifiedRatesDirectPopulationDisabled", Boolean.class);
    Setting<String> ROOM_REVENUE_PACKAGES = new SimpleSetting<>("roomRevenuePackages", String.class);
    Setting<Boolean> SCHEDULED_DEFERRED_DELIVERY = new SimpleSetting<>("scheduledDeferredDelivery", Boolean.class);
    Setting<BigDecimal> TAX_ADJUSTMENT_VALUE = new SimpleSetting<>("taxAdjustmentValue", BigDecimal.class);
    Setting<Boolean> UNQUALIFIED_RATES_DIRECT_POPULATION_DISABLED = new SimpleSetting<>("unqualifiedRatesDirectPopulationDisabled", Boolean.class);
    Setting<Boolean> USE_CUSTOM_SOAP_ACTION = new SimpleSetting<>("useCustomSoapAction", Boolean.class);
    Setting<Boolean> USE_NET_ROOM_REVENUE = new SimpleSetting<>("useNetRoomRevenue", Boolean.class);
    Setting<Boolean> HTNG_USE_BASIC_AUTH = new SimpleSetting<>("htngUseBasicAuth", Boolean.class);
    Setting<Boolean> INCLUDE_PSEUDO_IN_REVENUE = new SimpleSetting<>("includePseudoInRevenue", Boolean.class);
    Setting<Boolean> INCLUDE_DAY_USE_IN_REVENUE = new SimpleSetting<>("includeDayUseInRevenue", Boolean.class);
    Setting<Boolean> INCLUDE_NO_SHOW_IN_REVENUE = new SimpleSetting<>("includeNoShowInRevenue", Boolean.class);
    Setting<Boolean> RATE_PLAN_CLEANUP_ENABLED = new SimpleSetting<>("ratePlanCleanupEnabled", Boolean.class);
    Setting<IntegrationType> INTEGRATION_TYPE = new SimpleSetting<>("integrationType", IntegrationType.class);
    Setting<Boolean> DEDICATED_VALIDATION_RESPONSE_CHANNEL = new SimpleSetting<>("dedicatedValidationResponseChannel", Boolean.class);
    Setting<Boolean> SEND_HTNG_CALL_REQUEST = new SimpleSetting<>("sendHTNGCallbackRequest", Boolean.class);
    Setting<Boolean> INCLUDE_ROOM_TYPE_HOTEL_MARKET_SEGMENT_ACTIVITY = new SimpleSetting<>("includeRoomTypeHotelMarketSegmentActivity", Boolean.class);
    Setting<Boolean> PREVENT_PSEUDO_DATA_IN_ACTIVITY = new SimpleSetting<>("preventPseudoDataInActivity", Boolean.class);
    Setting<Boolean> IS_COMPONENT_ROOMS_ACTIVITY_ENABLED = new SimpleSetting<>("isComponentRoomsActivityEnabled", Boolean.class);
    Setting<Boolean> USE_CURRENCY_IN_MESSAGE = new SimpleSetting<>("useCurrencyInMessage", Boolean.class);
    Setting<Boolean> USE_LEGACY_ROOM_STAY_HANDLING = new SimpleSetting<>("useLegacyRoomStayHandling", Boolean.class);
    Setting<String> OXI_PARK_MESSAGE_TYPES = new SimpleSetting<>("oxiParkMessageTypes", String.class);
    Setting<Boolean> GENERATE_MARKET_SEGMENT_STATS_ENABLED = new SimpleSetting<>("generateMarketSegmentStatsEnabled", Boolean.class);
    Setting<Boolean> SKIP_REMAP = new SimpleSetting<>("skipRemapEnabled", Boolean.class);
    Setting<Boolean> SEARCH_ALTERNATE_MARKET_SEGMENT_LOCATION_FOR_HTNG = new SimpleSetting<>("searchAlternateMarketSegmentLocationforHtng", Boolean.class);
    Setting<Boolean> GENERATE_RATE_CODE_STATS_ENABLED = new SimpleSetting<>("generateRateCodeStatsEnabled", Boolean.class);
    Setting<Boolean> RESET_VIRTUAL_SUITE_COUNTS = new SimpleSetting<>("resetVirtualSuiteCounts", Boolean.class);
    Setting<Boolean> SUMMARY_PERSISTENCE_ENABLED = new SimpleSetting<>("summaryPersistenceEnabled", Boolean.class);
    Setting<Boolean> MSRT_SUMMARY_PERSISTENCE_ENABLED = new SimpleSetting<>("msrtSummaryPersistenceEnabled", Boolean.class);
    Setting<Boolean> GENERATE_HOTEL_ACTIVITY_FROM_ROOM_TYPE_ACTIVITY = new SimpleSetting<>("generateHotelActivityFromRoomTypeActivity", Boolean.class);
    Setting<Boolean> BUILD_MS_ACTIVITY_USING_PMS_MS = new SimpleSetting<>("buildMSActivityUsingPMSMS", Boolean.class);
    Setting<String> SALES_AND_CATERING_STATUS = new SimpleSetting<>("salesAndCateringStatus", String.class);
    Setting<String> SALES_AND_CATERING_UNIT_OF_MEASURE = new SimpleSetting<>("salesAndCateringUnitOfMeasure", String.class);

    String getPropertyName();

    class SimpleSetting<T> implements Setting<T> {
        private final String propertyName;
        private Class<T> type;

        SimpleSetting(String propertyName, Class<T> type) {
            this.propertyName = propertyName;
            this.type = type;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public Class<T> getType() {
            return type;
        }
    }
}
