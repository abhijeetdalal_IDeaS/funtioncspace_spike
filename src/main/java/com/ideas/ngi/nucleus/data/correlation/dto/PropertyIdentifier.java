package com.ideas.ngi.nucleus.data.correlation.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class PropertyIdentifier implements Serializable {
    private static final long serialVersionUID = -21505934173266080L;

    private final String sendingSystemPropertyId;
    private final String clientCode;
    private final String propertyCode;

    public PropertyIdentifier(String sendingSystemPropertyId) {
        this.sendingSystemPropertyId = sendingSystemPropertyId;
        this.clientCode = null;
        this.propertyCode = null;
    }

    public PropertyIdentifier(String sendingSystemPropertyId, String clientCode) {
        this.sendingSystemPropertyId = sendingSystemPropertyId;
        this.clientCode = clientCode;
        this.propertyCode = sendingSystemPropertyId;
    }

    public PropertyIdentifier(String sendingSystemPropertyId, String clientCode, String propertyCode) {
        this.sendingSystemPropertyId = sendingSystemPropertyId;
        this.clientCode = clientCode;
        this.propertyCode = propertyCode;
    }

    public String getSendingSystemPropertyId() {
        return sendingSystemPropertyId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
