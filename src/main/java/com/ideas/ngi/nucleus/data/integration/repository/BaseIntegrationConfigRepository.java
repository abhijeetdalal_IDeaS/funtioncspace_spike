package com.ideas.ngi.nucleus.data.integration.repository;

import com.ideas.ngi.nucleus.config.mongo.repository.NucleusMongoRepository;
import com.ideas.ngi.nucleus.data.AuditableEntity;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseIntegrationConfigRepository<T extends AuditableEntity> extends NucleusMongoRepository<T, String> {
}