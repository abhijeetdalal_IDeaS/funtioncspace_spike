package com.ideas.ngi.nucleus.data.correlation.entity;

import java.util.EnumSet;
import java.util.Set;

public enum IntegrationType {
    FUNCTION_SPACE_CLIENT,
    OXI_SC,
    UNQUALIFIED_RATES,
    QUALIFIED_RATES,
    STR_MONTHLY,
    STR_WEEKLY,
    ACTIVITY_DATA,
    ACTIVITY_DATA_CDP,
    OXI_PMS,
    DEMAND360_DATA_LOAD,
    FOLS,
    RRA,
    AHWS_SC,
    HTNG,
    HILTON,
    MARS,
    OPERA_SC,
    REVENUE_STREAMS,
    APALEO,
    UNKNOWN;

    public static Set<IntegrationType> getSourceIntegrationTypes() {
        return EnumSet.complementOf(
                EnumSet.of(QUALIFIED_RATES, UNQUALIFIED_RATES, ACTIVITY_DATA, ACTIVITY_DATA_CDP, UNKNOWN));
    }

    public static Set<IntegrationType> byBusinessModule(BusinessModule businessModule) {
        switch (businessModule) {
            case RESERVATION:
                return EnumSet.of(OXI_PMS, HTNG, FOLS, APALEO, ACTIVITY_DATA, ACTIVITY_DATA_CDP, UNQUALIFIED_RATES, QUALIFIED_RATES);
            case FUNCTION_SPACE:
                return EnumSet.of(OXI_SC, AHWS_SC, FUNCTION_SPACE_CLIENT);
            case RRA:
                return EnumSet.of(RRA);
            default:
                return EnumSet.noneOf(IntegrationType.class);
        }
    }

    public static IntegrationType getIntegrationType(String header) {
        try {
            return IntegrationType.valueOf(header);
        } catch (IllegalArgumentException e) {
            return IntegrationType.UNKNOWN;
        }
    }
}
