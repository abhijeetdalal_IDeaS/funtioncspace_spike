package com.ideas.ngi.nucleus.data.integration;

import com.ideas.ngi.nucleus.exception.NucleusException;

public class IntegrationSettingsException extends NucleusException {

    public IntegrationSettingsException(String message) {
        super(message);
    }

    public IntegrationSettingsException(String message, Throwable ex) {
        super(message, ex);
    }
}
