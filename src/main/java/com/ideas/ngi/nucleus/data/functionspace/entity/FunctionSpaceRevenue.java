package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class FunctionSpaceRevenue implements Serializable {

    private static final long serialVersionUID = 4787203659526717844L;

    private String revenueType;
    private String revenueGroup;
    private BigDecimal forecastRevenue;
    private BigDecimal expectedRevenue;
    private BigDecimal guaranteedRevenue;
    private BigDecimal actualRevenue;
    private BigDecimal dailyDelegateRate;
    private String dailyDelegatePackageName;
    private Date insertDate;
    private Date updateDate;

    public String getRevenueType() {
        return revenueType;
    }

    public void setRevenueType(String revenueType) {
        this.revenueType = revenueType;
    }

    public String getRevenueGroup() {
        return revenueGroup;
    }

    public void setRevenueGroup(String revenueGroup) {
        this.revenueGroup = revenueGroup;
    }

    public BigDecimal getForecastRevenue() {
        return forecastRevenue;
    }

    public void setForecastRevenue(BigDecimal forecastRevenue) {
        this.forecastRevenue = forecastRevenue;
    }

    public BigDecimal getExpectedRevenue() {
        return expectedRevenue;
    }

    public void setExpectedRevenue(BigDecimal expectedRevenue) {
        this.expectedRevenue = expectedRevenue;
    }

    public BigDecimal getGuaranteedRevenue() {
        return guaranteedRevenue;
    }

    public void setGuaranteedRevenue(BigDecimal guaranteedRevenue) {
        this.guaranteedRevenue = guaranteedRevenue;
    }

    public BigDecimal getActualRevenue() {
        return actualRevenue;
    }

    public void setActualRevenue(BigDecimal actualRevenue) {
        this.actualRevenue = actualRevenue;
    }

    public BigDecimal getDailyDelegateRate() {
        return dailyDelegateRate;
    }

    public void setDailyDelegateRate(BigDecimal dailyDelegateRate) {
        this.dailyDelegateRate = dailyDelegateRate;
    }

    public String getDailyDelegatePackageName() {
        return dailyDelegatePackageName;
    }

    public void setDailyDelegatePackageName(String dailyDelegatePackageName) {
        this.dailyDelegatePackageName = dailyDelegatePackageName;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @JsonIgnore
    public void markAsDeleted() {
        forecastRevenue = BigDecimal.ZERO;
        expectedRevenue = BigDecimal.ZERO;
        guaranteedRevenue = BigDecimal.ZERO;
        actualRevenue = BigDecimal.ZERO;
    }

    @JsonIgnore
    public boolean isDeleted() {
        return forecastRevenue.equals(BigDecimal.ZERO) &&
                expectedRevenue.equals(BigDecimal.ZERO) &&
                guaranteedRevenue.equals(BigDecimal.ZERO) &&
                actualRevenue.equals(BigDecimal.ZERO);
    }
}
