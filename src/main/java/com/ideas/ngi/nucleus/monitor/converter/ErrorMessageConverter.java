package com.ideas.ngi.nucleus.monitor.converter;

import com.ideas.ngi.nucleus.exception.FlatWrapperException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.support.ErrorMessage;

public class ErrorMessageConverter extends SimpleMessageConverter {

    @Override
    protected Message createMessage(Object object, MessageProperties messageProperties) {
        if (! (object instanceof ErrorMessage ) && ! (object instanceof Throwable)) {
            return super.createMessage(object, messageProperties);
        }
        if (object instanceof Throwable) {
            return super.createMessage(new MessagingException(((Throwable) object).getMessage()), messageProperties);
        }
        ErrorMessage message = (ErrorMessage) object;
        Throwable exception = message.getPayload();
        if (MessagingException.class.isAssignableFrom(exception.getClass())) {
            MessagingException messagingException = (MessagingException) exception;
            FlatWrapperException flatException = new FlatWrapperException(messagingException.getMostSpecificCause());
            return super.createMessage(new MessagingException(messagingException.getFailedMessage(), flatException), messageProperties);
        } else {
            return super.createMessage(new MessagingException(exception.getMessage()), messageProperties);
        }
    }
}
