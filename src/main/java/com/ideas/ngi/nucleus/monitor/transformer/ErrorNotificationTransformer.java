package com.ideas.ngi.nucleus.monitor.transformer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ideas.ngi.nucleus.monitor.entity.NucleusErrorMessage;
import com.ideas.ngi.nucleus.monitor.entity.NucleusErrorNotification;
import com.ideas.ngi.nucleus.monitor.service.ErrorMessageService;
import com.ideas.ngi.nucleus.monitor.service.MonitorMessageService;
import com.ideas.ngi.nucleus.rest.hateoas.ProxyLinkBuilderTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Transforms a NucleusErrorMessage into a NucleusErrorNotification with a truncated payload and hateoas links
 */
@Component
public class ErrorNotificationTransformer extends AbstractNotificationTransformer<NucleusErrorMessage, NucleusErrorNotification> {
    private ProxyLinkBuilderTransformer linkBuilderTransformer;

    @Autowired
    public ErrorNotificationTransformer(ObjectMapper objectMapper,
                                        ProxyLinkBuilderTransformer linkBuilderTransformer) {
        super(objectMapper);
        this.linkBuilderTransformer = linkBuilderTransformer;
    }

    @Override
    protected Class<NucleusErrorNotification> provideClass() {
        return NucleusErrorNotification.class;
    }

    @Override
    protected NucleusErrorNotification createDefaultInstance(NucleusErrorMessage errorMessage) {
        NucleusErrorNotification notificationMessage = new NucleusErrorNotification();
        notificationMessage.setId(errorMessage.getId());
        return notificationMessage;
    }

    @Override
    protected void setLinks(NucleusErrorMessage errorMessage, NucleusErrorNotification notificationMessage) {
        if (errorMessage.getPayload() != null || errorMessage.getPayloadReferenceId() != null) {
            notificationMessage.setPayloadLink(linkBuilderTransformer.transform(linkTo(methodOn(MonitorMessageService.class).downloadErrorPayload(errorMessage.getId()))).withRel("enclosure"));
        }
        notificationMessage.setRetryLink(linkBuilderTransformer.transform(linkTo(methodOn(ErrorMessageService.class).retry(errorMessage.getId()))).withSelfRel());
        notificationMessage.setAbandonLink(linkBuilderTransformer.transform(linkTo(methodOn(ErrorMessageService.class).abandon(errorMessage.getId()))).withSelfRel());
    }
}
