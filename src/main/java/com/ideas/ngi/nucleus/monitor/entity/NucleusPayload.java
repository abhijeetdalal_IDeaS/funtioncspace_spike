package com.ideas.ngi.nucleus.monitor.entity;

import java.io.Serializable;

public interface NucleusPayload extends Serializable {

    Object getStoredId();

    void setStoredId(Object storedId);
}
