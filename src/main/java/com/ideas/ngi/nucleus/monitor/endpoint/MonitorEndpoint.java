package com.ideas.ngi.nucleus.monitor.endpoint;

import com.ideas.ngi.nucleus.rest.endpoint.RestEndpoint;
import org.springframework.http.HttpMethod;

/**
 * Enum containing the references to the Monitor REST endpoints
 */
public enum MonitorEndpoint implements RestEndpoint {

    CREATE_COMPLETED_JOB_INSTANCE(HttpMethod.POST, "ngi/completedJobInstance?jobName={jobName}", Long.class),
    CREATE_FAILED_JOB_INSTANCE(HttpMethod.POST, "ngi/failedJobInstance?jobName={jobName}", Long.class);

    private final HttpMethod httpMethod;
    private final String url;
    private final Class<?> responseType;

    MonitorEndpoint(HttpMethod httpMethod, String url, Class<?> responseType) {
        this.httpMethod = httpMethod;
        this.url = url;
        this.responseType = responseType;
    }

    @Override
    public HttpMethod getHttpMethod() {
        return httpMethod;
    }

    @Override
    public String getURL() {
        return url;
    }

    @Override
    public Class<?> getResponseType() {
        return responseType;
    }

}
