package com.ideas.ngi.nucleus.monitor.service;

import com.ideas.ngi.nucleus.monitor.entity.NucleusCompletedMessage;
import com.ideas.ngi.nucleus.monitor.entity.NucleusErrorMessage;
import com.ideas.ngi.nucleus.monitor.repository.NucleusCompletedMessageRepository;
import com.ideas.ngi.nucleus.monitor.repository.NucleusErrorMessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Endpoints for any monitor message, completed or error types
 */
@RestController
public class MonitorMessageService {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private final NucleusErrorMessageRepository errorMessageRepository;
    private final NucleusCompletedMessageRepository completedMessageRepository;
    private final PayloadStrategyFactory payloadStrategyFactory;

    @Autowired
    public MonitorMessageService(NucleusErrorMessageRepository errorMessageRepository,
                                 NucleusCompletedMessageRepository completedMessageRepository,
                                 PayloadStrategyFactory payloadStrategyFactory) {
        this.errorMessageRepository = errorMessageRepository;
        this.completedMessageRepository = completedMessageRepository;
        this.payloadStrategyFactory = payloadStrategyFactory;
    }

    @GetMapping(value = "/errorMessage/payload/{id}")
    public ResponseEntity<byte[]> downloadErrorPayload(@PathVariable("id") String messageId) {
        NucleusErrorMessage errorMessage = errorMessageRepository.findOne(messageId);
        if (errorMessage == null) {
            LOGGER.warn("Unable to find ErrorMessage id {}", messageId);
            return new ResponseEntity<>("".getBytes(), HttpStatus.NOT_FOUND);
        }
        PayloadDownloadStrategy strategy = payloadStrategyFactory.newInstance(errorMessage);
        if (strategy == null) {
            return new ResponseEntity<>("".getBytes(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return strategy.getPayloadResponse();
    }

    @GetMapping(value = "/completedMessage/payload/{id}")
    public ResponseEntity<byte[]> downloadCompletedPayload(@PathVariable("id") String messageId) {
        NucleusCompletedMessage completedMessage = completedMessageRepository.findOne(messageId);
        if (completedMessage == null) {
            LOGGER.warn("Unable to find CompletedMessage id {}", messageId);
            return new ResponseEntity<>("".getBytes(), HttpStatus.NOT_FOUND);
        }
        PayloadDownloadStrategy strategy = payloadStrategyFactory.newInstance(completedMessage);
        if (strategy == null) {
            return new ResponseEntity<>("".getBytes(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return strategy.getPayloadResponse();
    }
}
