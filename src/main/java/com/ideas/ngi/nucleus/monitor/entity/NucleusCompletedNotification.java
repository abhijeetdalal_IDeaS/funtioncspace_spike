package com.ideas.ngi.nucleus.monitor.entity;

import org.springframework.hateoas.Link;

public class NucleusCompletedNotification extends NucleusCompletedMessage {
    private Link payloadLink;

    public Link getPayloadLink() {
        return payloadLink;
    }

    public void setPayloadLink(Link payloadLink) {
        this.payloadLink = payloadLink;
    }
}
