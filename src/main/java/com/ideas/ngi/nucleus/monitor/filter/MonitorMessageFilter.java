package com.ideas.ngi.nucleus.monitor.filter;

import com.ideas.ngi.nucleus.config.NucleusConfigProperties;
import com.ideas.ngi.nucleus.monitor.entity.NucleusMonitorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.core.GenericSelector;
import org.springframework.stereotype.Component;

@Component
public class MonitorMessageFilter implements GenericSelector<NucleusMonitorMessage> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MonitorMessageFilter.class);

    private NucleusConfigProperties nucleusConfigProperties;

    @Autowired
    public MonitorMessageFilter(NucleusConfigProperties nucleusConfigProperties) {
        this.nucleusConfigProperties = nucleusConfigProperties;
    }

    @Override
    public boolean accept(NucleusMonitorMessage source) {
        if (!nucleusConfigProperties.isExternalMonitorEnabled()) {
            LOGGER.warn("External monitor is not enabled.  Monitor message will not be sent to external monitor");
            return false;
        }
        return source.getJobName() != null;
    }
}
