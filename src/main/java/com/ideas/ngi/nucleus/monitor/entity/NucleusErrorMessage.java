package com.ideas.ngi.nucleus.monitor.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * The ErrorMessage entity is used to track any errors that occur in processing.
 * It contains an unique id, retryChannel (the channel the message failed in),
 * description (the exception's message), the date/time the ErrorMessage was
 * created, and the type of JEMS job it should create and the message payload
 * itself.
 */
@Document
public class NucleusErrorMessage extends NucleusMonitorMessage implements Serializable {

    private static final long serialVersionUID = 7507879066398592394L;

    public enum Status {
        OPEN,
        CLOSED,
        ABANDONED
    }

    @Id
    private String id;

    private String retryChannel;

    private String errorChannel;

    private String message;

    private String exceptionClassName;

    private Status status = Status.OPEN;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRetryChannel() {
        return retryChannel;
    }

    public void setRetryChannel(String retryChannel) {
        this.retryChannel = retryChannel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getExceptionClassName() {
        return exceptionClassName;
    }

    public void setExceptionClassName(String exceptionClassName) {
        this.exceptionClassName = exceptionClassName;
    }

    public String getErrorChannel() {
        return errorChannel;
    }

    public void setErrorChannel(String errorChannel) {
        this.errorChannel = errorChannel;
    }

    public Status getStatus() {
        if (status == null) {
            status = Status.OPEN;
        }
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @JsonIgnore
    public boolean isOpen() {
        return getStatus() == Status.OPEN;
    }
}
