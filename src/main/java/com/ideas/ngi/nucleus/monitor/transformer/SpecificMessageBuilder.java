package com.ideas.ngi.nucleus.monitor.transformer;

import com.ideas.ngi.nucleus.monitor.entity.NucleusMonitorMessage;
import org.springframework.messaging.MessageHeaders;

public interface SpecificMessageBuilder {
    void populateMonitorMessage(NucleusMonitorMessage monitorMessage, Object payload, MessageHeaders headers);

    boolean canProcess(Object payload);
}
