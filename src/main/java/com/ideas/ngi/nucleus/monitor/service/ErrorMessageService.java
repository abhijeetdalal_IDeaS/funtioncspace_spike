package com.ideas.ngi.nucleus.monitor.service;

import com.ideas.ngi.nucleus.config.mongo.service.RsqlCriteriaBuilder;
import com.ideas.ngi.nucleus.data.correlation.entity.CorrelationStatus;
import com.ideas.ngi.nucleus.data.correlation.entity.NucleusCorrelationMetadata;
import com.ideas.ngi.nucleus.data.correlation.repository.NucleusCorrelationMetadataRepository;
import com.ideas.ngi.nucleus.exception.NucleusException;
import com.ideas.ngi.functionspace.file.FileService;
import com.ideas.ngi.functionspace.file.gridfs.NucleusGridFSService;
import com.ideas.ngi.nucleus.integration.util.MessageHeaderUtil;
import com.ideas.ngi.nucleus.monitor.config.ErrorMessageConfig;
import com.ideas.ngi.nucleus.monitor.entity.NucleusErrorMessage;
import com.ideas.ngi.nucleus.monitor.entity.NucleusPayload;
import com.ideas.ngi.nucleus.monitor.repository.NucleusErrorMessageRepository;
import com.ideas.ngi.nucleus.rest.endpoint.GenericErrorRestEndpoint;
import com.ideas.ngi.nucleus.rest.wrapper.SimpleObjectRestWrapper;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.integration.support.DefaultMessageBuilderFactory;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.ideas.ngi.functionspace.constant.Constants.PARAM_CLIENT_CODE;
import static com.ideas.ngi.functionspace.constant.Constants.PARAM_PROPERTY_CODE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * ErrorMessageService sends an ErrorMessage to Monitor for exposure of the
 * failure.
 */
@RestController
public class ErrorMessageService {

    public static final String ERROR_RETRY_CHANNEL = "errorRetryChannel";

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private final ApplicationContext applicationContext;
    private final NucleusErrorMessageRepository errorMessageRepository;
    private final NucleusCorrelationMetadataRepository correlationMetadataRepository;
    private final DefaultMessageBuilderFactory defaultMessageBuilderFactory;
    private final FileService fileService;
    private final NucleusGridFSService nucleusGridFSService;
    private final RsqlCriteriaBuilder criteriaBuilder;


    @Autowired
    public ErrorMessageService(ApplicationContext applicationContext,
                               NucleusErrorMessageRepository errorMessageRepository,
                               NucleusCorrelationMetadataRepository correlationMetadataRepository,
                               DefaultMessageBuilderFactory defaultMessageBuilderFactory,
                               FileService fileService,
                               NucleusGridFSService nucleusGridFSService,
                               RsqlCriteriaBuilder criteriaBuilder) {
        this.applicationContext = applicationContext;
        this.errorMessageRepository = errorMessageRepository;
        this.correlationMetadataRepository = correlationMetadataRepository;
        this.defaultMessageBuilderFactory = defaultMessageBuilderFactory;
        this.fileService = fileService;
        this.nucleusGridFSService = nucleusGridFSService;
        this.criteriaBuilder = criteriaBuilder;
    }

    @DeleteMapping(path = "/errorMessage/property", produces = {APPLICATION_JSON_VALUE})
    @ResponseBody
    public SimpleObjectRestWrapper<Long> delete(@RequestParam(PARAM_CLIENT_CODE) String clientCode, @RequestParam(PARAM_PROPERTY_CODE) String propertyCode) {
        LOGGER.info("Deleting errorMessage for clientCode: {} and propertyCode: {}", clientCode, propertyCode);
        return new SimpleObjectRestWrapper<>(errorMessageRepository.deleteByClientCodeAndPropertyCode(clientCode, propertyCode));
    }

    @DeleteMapping(path = "/errorMessage/job", produces = {APPLICATION_JSON_VALUE})
    @ResponseBody
    public SimpleObjectRestWrapper<Long> delete(@RequestParam("jobName") String jobName) {
        LOGGER.info("Deleting errorMessage for jobName: {}", jobName);
        return new SimpleObjectRestWrapper<>(errorMessageRepository.deleteByJobName(jobName));
    }

    @PostMapping("/errorMessage/retry")
    public Map<String, String> retryBulk(@RequestParam(value = "search") String search) {
        Assert.hasText(search, "Search parameter is required");
        LOGGER.info("Executing bulk retry for query {}", search);
        Criteria criteria = criteriaBuilder.buildCriteria(errorMessageRepository, search);
        return errorMessageRepository.stream(criteria)
                .filter(NucleusErrorMessage::isOpen)
                .collect(Collectors.toMap(NucleusErrorMessage::getId, e -> {
                    try {
                        return Boolean.toString(retryInternal(e));
                    } catch (NucleusException ex) {
                        LOGGER.warn("Could not re-try error: " + e.getId(), ex);
                        return "false: " + ex.getMessage();
                    }
                }));
    }

    /**
     * Retries an ErrorMessage by looking up it's id and placing a copy of the
     * message onto the channel it last failed in.
     */
    @GetMapping("/errorMessage/retry")
    public SimpleObjectRestWrapper<Boolean> retry(@RequestParam(value = "id") String id) {
        LOGGER.info("Attempting to retry ErrorMessage: {}", id);

        // Look up ErrorMessage
        NucleusErrorMessage errorMessage = errorMessageRepository.findOne(id);
        if (errorMessage == null || !errorMessage.isOpen()) {
            LOGGER.warn("Unable to find ErrorMessage with id: {}", id);
            return new SimpleObjectRestWrapper<>(false);
        }

        return new SimpleObjectRestWrapper<>(retryInternal(errorMessage));
    }

    private boolean retryInternal(NucleusErrorMessage errorMessage) {
        boolean sendSuccessful = false;
        try {
            if (errorMessage.getRetryChannel() == null) {
                sendSuccessful = true;
                return false;
            }

            // get the saved headers, if any
            Map<String, Object> headers = MessageHeaderUtil.copyHeaders(errorMessage.getHeaders());
            // Need to stick the jobName back onto the headers
            headers.put(MessageHeaderUtil.JOB_NAME, errorMessage.getJobName());
            //This was added because if an error channel other than the default was being used, it was being lost from
            // the headers and then not being loaded correctly in the NucleusErrorHandler if another failure occurred
            headers.put(MessageHeaders.ERROR_CHANNEL, errorMessage.getErrorChannel());
            // Using the ErrorMessage's retryChannel, look up the MessageChannel
            // bean so that a message can be placed back on it and processed
            headers.put(ERROR_RETRY_CHANNEL, errorMessage.getRetryChannel());

            //Reconstitute a generic REST endpoint configuration from the metadata
            if (null != errorMessage.getUrl()) {
                headers.put(MessageHeaderUtil.REST_ENDPOINT, new GenericErrorRestEndpoint(errorMessage.getHttpMethod(),
                        errorMessage.getUrl(), errorMessage.getResponseType().getClass().getTypeName()));
            }

            Object payload = getPayload(errorMessage);
            if (payload == null) {
                throw new NucleusException("Could not retry message.  Either payload or retry channel are null [id=" + errorMessage.getId() + "]");
            }

            // Use the Error Message Retry Channel to route to the ErrorMessage's retryChannel
            LOGGER.info("putting error message contents back on channel: {} payload: {}", errorMessage.getRetryChannel(), payload.getClass());
            MessageChannel errorRetryChannel = (MessageChannel) applicationContext.getBean(ErrorMessageConfig.ERROR_MESSAGE_RETRY_CHANNEL);
            sendSuccessful = errorRetryChannel.send(defaultMessageBuilderFactory.withPayload(payload).copyHeaders(headers).build());
        } finally {
            // Only delete the old error message
            if (sendSuccessful) {
                errorMessage.setStatus(NucleusErrorMessage.Status.CLOSED);
                errorMessageRepository.save(errorMessage);
            }
        }

        return sendSuccessful;
    }

    Object getPayload(NucleusErrorMessage errorMessage) {
        Object payloadReferenceId = errorMessage.getPayloadReferenceId();
        if (payloadReferenceId != null) {
            GridFsResource file = nucleusGridFSService.findResource(payloadReferenceId);
            if (file != null) {
                try (ObjectInputStream objectInputStream = new ObjectInputStream(file.getInputStream())) {
                    NucleusPayload nucleusPayload = (NucleusPayload) objectInputStream.readObject();
                    nucleusPayload.setStoredId(payloadReferenceId);
                    return nucleusPayload;
                } catch (IOException | ClassNotFoundException e) {
                    throw new NucleusException("Could not serialize the gridFS message", e);
                }
            }
        }
        return errorMessage.getPayload();
    }

    /**
     * Retries an ErrorMessage by looking up it's id and placing a copy of the
     * message onto the channel it last failed in.
     */
    @GetMapping("/errorMessage/abandon")
    public SimpleObjectRestWrapper<Boolean> abandon(@RequestParam(value = "id") String id) {
        LOGGER.info("Attempting to abandon ErrorMessage: {}", id);

        // Look up ErrorMessage
        NucleusErrorMessage errorMessage = errorMessageRepository.findOne(id);
        if (errorMessage == null || !errorMessage.isOpen()) {
            LOGGER.warn("Unable to find ErrorMessage with id: {}", id);
            return new SimpleObjectRestWrapper<>(false);
        }

        // If the payload is a NucleusCorrelationMetadata, look up the record and abandon it
        // this will allow the next file to be processed.
        Object payload = errorMessage.getPayload();
        // TODO: this should be extracted an implement a generic "abandon" discard channel implementation
        if (payload instanceof NucleusCorrelationMetadata) {

            // Get the metadata record and set it's status to abandoned
            NucleusCorrelationMetadata correlationMetadata = correlationMetadataRepository.findOne(((NucleusCorrelationMetadata) payload).getId());
            if (correlationMetadata != null) {
                correlationMetadata.setCorrelationStatus(CorrelationStatus.ABANDONED);

                // If there is a file associated with the CorrelationMetadata, copy the file to the 'failed' directory.
                File file = correlationMetadata.getCurrentFile();
                if (file != null) {
                    File failedDirectory = new File(findFailedFileDirectory(file).getAbsolutePath() + File.separator + "failed");
                    Optional<File> parentDirectory = findParentDirectory(file);
                    fileService.moveFileToDirectory(file, failedDirectory);
                    correlationMetadata.setCurrentFilePath(failedDirectory.getAbsolutePath() + File.separator + file.getName());

                    parentDirectory.ifPresent(FileUtils::deleteQuietly);

                    correlationMetadataRepository.save(correlationMetadata);
                }
            }
        }

        // Delete the ErrorMessage
        errorMessage.setStatus(NucleusErrorMessage.Status.ABANDONED);
        errorMessageRepository.save(errorMessage);
        return new SimpleObjectRestWrapper<>(true);
    }

    File findFailedFileDirectory(File file) {
        File baseDirectory;

        String parentFileName = file.getParentFile().getName();
        String fileName = file.getName().substring(0, file.getName().indexOf('.'));

        // If the parent file name is the same as the file name (minus the extension)
        // then the file is in a directory that was likely unzipped so we should move up
        // that directory's parent directory
        if (parentFileName.equalsIgnoreCase(fileName)) {
            baseDirectory = file.getParentFile().getParentFile().getParentFile();
        } else {
            baseDirectory = file.getParentFile().getParentFile();
        }
        return baseDirectory;
    }

    Optional<File> findParentDirectory(File file) {
        File parentDirectory = null;

        String parentFileName = file.getParentFile().getName();
        String fileName = file.getName().substring(0, file.getName().indexOf('.'));

        // If the parent file name is the same as the file name (minus the extension)
        // then the file is in a directory that was likely unzipped. We will want to
        // remove that directory
        if (parentFileName.equalsIgnoreCase(fileName)) {
            parentDirectory = file.getParentFile();
        }

        return Optional.ofNullable(parentDirectory);
    }
}
