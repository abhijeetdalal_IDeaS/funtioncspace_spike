package com.ideas.ngi.nucleus.monitor.transformer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ideas.ngi.nucleus.monitor.entity.NucleusMonitorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.transformer.GenericTransformer;
import org.springframework.util.Assert;

public abstract class AbstractNotificationTransformer<S extends NucleusMonitorMessage, T extends NucleusMonitorMessage> implements GenericTransformer<S, T> {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    protected ObjectMapper objectMapper;

    public AbstractNotificationTransformer(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    protected abstract Class<T> provideClass();
    protected abstract T createDefaultInstance(S monitorMessage);
    protected abstract void setLinks(S monitorMessage, T notificationMessage);

    @Override
    public T transform(S monitorMessage) {
        Assert.notNull(monitorMessage, "Nucleus monitor message cannot be null");

        T notificationMessage;
        try {
            // clone the original completion message
            notificationMessage = objectMapper.readValue(objectMapper.writeValueAsString(monitorMessage), provideClass());
        } catch(Exception e) {
            logger.error("Could not clone notification message from monitor message id {}, creating minimal notification", monitorMessage.getId(), e);
            notificationMessage = createDefaultInstance(monitorMessage);
            copyClientProperties(monitorMessage, notificationMessage);
        }
        reducePayload(monitorMessage, notificationMessage, monitorMessage.getId());
        setLinks(monitorMessage, notificationMessage);
        return notificationMessage;
    }

    private void reducePayload(NucleusMonitorMessage monitorMessage, NucleusMonitorMessage notificationMessage, String id) {
        if (monitorMessage.getPayload() != null) {
            try {
                // serialize and truncate payload
                String payloadJson = objectMapper.writeValueAsString(monitorMessage.getPayload()).replace("\"", "");
                notificationMessage.setPayload(payloadJson);
            } catch (Exception ignored) {
                notificationMessage.setPayload("");
                logger.warn("Could not truncate payload for {}, setting empty string, id {}", monitorMessage.getClass().getSimpleName(), id, ignored);
            }
        }

    }

    private void copyClientProperties(NucleusMonitorMessage monitorMessage, NucleusMonitorMessage notificationMessage) {
        notificationMessage.setClientCode(monitorMessage.getClientCode());
        notificationMessage.setClientId(monitorMessage.getClientId());
        notificationMessage.setPropertyCode(monitorMessage.getPropertyCode());
        notificationMessage.setPropertyId(monitorMessage.getPropertyId());
        notificationMessage.setClientEnvironmentName(monitorMessage.getClientEnvironmentName());
    }
}
