package com.ideas.ngi.nucleus.monitor.gateway;

import com.ideas.ngi.nucleus.monitor.config.ErrorMessageConfig;
import com.ideas.ngi.nucleus.monitor.entity.NucleusErrorMessage;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

/**
 * The ErrorMessageGateway can be used to put a message onto the erorChannel to
 * send a message to the Monitor to show a failed process.
 */
@MessagingGateway
public interface ErrorMessageGateway {

    @Gateway(requestChannel = ErrorMessageConfig.ERROR_MESSAGE_FLOW_CHANNEL)
    void errorMessage(Object message);

    @Gateway(requestChannel = ErrorMessageConfig.MONITOR_ERROR_MESSAGE_CHANNEL)
    void monitorErrorMessage(NucleusErrorMessage errorMessage);
}
