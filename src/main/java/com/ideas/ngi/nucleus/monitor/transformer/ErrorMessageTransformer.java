package com.ideas.ngi.nucleus.monitor.transformer;

import com.ideas.ngi.nucleus.exception.FlatWrapperException;
import com.ideas.ngi.nucleus.exception.NucleusException;
import com.ideas.ngi.nucleus.integration.util.MessageHeaderUtil;
import com.ideas.ngi.nucleus.monitor.entity.NucleusErrorMessage;
import com.ideas.ngi.nucleus.rest.endpoint.RestEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.transformer.GenericTransformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandlingException;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * Transforms a MessagingException into a ErrorMessage
 */

@Component
public class ErrorMessageTransformer extends AbstractMessageTransformer implements GenericTransformer<MessagingException, NucleusErrorMessage> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public NucleusErrorMessage transform(MessagingException messagingException) {
        Assert.notNull(messagingException, "Messaging exception cannot be null");

        Throwable cause = messagingException.getMostSpecificCause();
        logger.warn("MessagingException: {}", cause.getMessage(), messagingException);

        // Populate the ErrorMessage
        NucleusErrorMessage errorMessage = new NucleusErrorMessage();

        // Set the cause message and class name
        errorMessage.setExceptionClassName(cause.getClass().getTypeName());
        if (cause instanceof NucleusException) {
            errorMessage.setMessage(cause.getMessage());
        } else if (cause instanceof FlatWrapperException) {
            errorMessage.setMessage(cause.getMessage());
            errorMessage.setExceptionClassName(((FlatWrapperException) cause).getCausingExceptionClassName());
        } else {
            errorMessage.setMessage(cause.toString());
        }

        // populate the failed message details (if necessary)
        Message<?> failedMessage = messagingException.getFailedMessage();
        if (failedMessage != null) {
            // Extract a common components
            MessageHeaders headers = messagingException.getFailedMessage().getHeaders();
            Object payload = messagingException.getFailedMessage().getPayload();

            if (payload instanceof MessageHandlingException) {
                payload = ((MessageHandlingException) payload).getFailedMessage().getPayload();
            }

            applyRules(errorMessage, headers, payload);

            // Want to get a first-class reference to the last channel to make it
            // easier to determine where we need to reprocess the message in the
            // flow
            errorMessage.setRetryChannel(MessageHeaderUtil.findLastChannel(headers));
            errorMessage.setErrorChannel(MessageHeaderUtil.findErrorChannel(headers));
            RestEndpoint restEndpoint = MessageHeaderUtil.findRestEndpoint(headers);
            setRestEndpointInformation(errorMessage, restEndpoint);
        }

        return errorMessage;
    }

    private void setRestEndpointInformation(NucleusErrorMessage errorMessage, RestEndpoint restEndpoint) {
        if (restEndpoint != null) {
            errorMessage.setUrl(restEndpoint.getURL());
            errorMessage.setHttpMethod(restEndpoint.getHttpMethod().name());
            errorMessage.setResponseType(restEndpoint.getResponseType().getTypeName());
        }
    }
}
