package com.ideas.ngi.nucleus.monitor.repository;

import com.ideas.ngi.nucleus.config.mongo.annotation.GlobalCollection;
import com.ideas.ngi.nucleus.config.mongo.repository.NucleusMongoRepository;
import com.ideas.ngi.nucleus.monitor.entity.NucleusCompletedMessage;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

import static com.ideas.ngi.functionspace.constant.Constants.PARAM_CLIENT_CODE;
import static com.ideas.ngi.functionspace.constant.Constants.PARAM_PROPERTY_CODE;


/**
 * CompletedMessageRepository is used for CRUD operations on the CompletedMessage entity
 * and is exposes the collection via REST.
 */
@GlobalCollection
@RepositoryRestResource
public interface NucleusCompletedMessageRepository extends NucleusMongoRepository<NucleusCompletedMessage, String> {

    List<NucleusCompletedMessage> findByExternalSystemPropertyIdAndJobNameOrderByJobInstanceIdAsc(
            @Param("externalSystemPropertyId") String externalSystemPropertyId,
            @Param("jobName") String jobName);

    long deleteByExternalSystemPropertyId(
            @Param("externalSystemPropertyId") String externalSystemPropertyId);

    List<NucleusCompletedMessage> findByClientCodeAndPropertyCodeAndJobNameOrderByJobInstanceIdAsc(
            @Param(PARAM_CLIENT_CODE) String clientCode,
            @Param(PARAM_PROPERTY_CODE) String propertyCode,
            @Param("jobName") String jobName);

    @RestResource(exported=false)
    long deleteByClientCodeAndPropertyCode(
            @Param(PARAM_CLIENT_CODE) String clientCode,
            @Param(PARAM_PROPERTY_CODE) String propertyCode);

    long deleteByClientCodeAndPropertyCodeAndJobName(
            @Param(PARAM_CLIENT_CODE) String clientCode,
            @Param(PARAM_PROPERTY_CODE) String propertyCode,
            @Param("jobName") String jobName);

    @RestResource(exported=false)
    long deleteByJobName(
            @Param("jobName") String jobName);

    List<NucleusCompletedMessage> findByJobName(String jobName);

    List<NucleusCompletedMessage> findByJobNameAndJobInstanceIdIsNotNull(String jobName);

    long deleteByJobInstanceId(Long jobInstanceId);
}
