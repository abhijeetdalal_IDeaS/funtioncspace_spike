package com.ideas.ngi.nucleus.monitor.service;

import org.springframework.http.ResponseEntity;

public interface PayloadDownloadStrategy {
    byte[] getPayloadBytes() throws Exception;
    ResponseEntity<byte[]> getPayloadResponse();
}
