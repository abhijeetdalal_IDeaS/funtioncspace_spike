package com.ideas.ngi.nucleus.monitor.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

/**
 * payload handled as serializable to JSON
 */
public class SerializedJsonPayload implements PayloadDownloadStrategy {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private ObjectMapper objectMapper;
    private Object payload;
    private String messageId;
    private String messageType;

    public SerializedJsonPayload(ObjectMapper objectMapper, Object payload, String messageId, String messageType) {
        this.objectMapper = objectMapper;
        this.payload = payload;
        this.messageId = messageId;
        this.messageType = messageType;
    }

    @Override
    public byte[] getPayloadBytes() throws Exception {
        if (payload == null) {
            return new byte[0];
        }
        return this.objectMapper.writeValueAsString(payload).getBytes();
    }

    @Override
    public ResponseEntity<byte[]> getPayloadResponse() {
        try {
            String fileName = "payload-" + messageType + "-" + messageId + ".json";
            byte[] serializedPayload = getPayloadBytes();
            return ResponseEntity.ok()
                    .contentLength(serializedPayload.length)
                    .contentType(new MediaType("text", "json"))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName)
                    .body(serializedPayload);
        } catch(Exception e) {
            LOGGER.error("Error serializing payload to JSON for message id {}",  messageId, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("".getBytes());
        }
    }
}
