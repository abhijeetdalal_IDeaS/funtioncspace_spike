package com.ideas.ngi.nucleus.monitor.config;

import com.ideas.ngi.nucleus.integration.NucleusIntegrationComponent;
import com.ideas.ngi.nucleus.integration.util.MessageHeaderUtil;
import com.ideas.ngi.nucleus.monitor.endpoint.MonitorEndpoint;
import com.ideas.ngi.nucleus.monitor.entity.NucleusCompletedMessage;
import com.ideas.ngi.nucleus.monitor.filter.MonitorMessageFilter;
import com.ideas.ngi.nucleus.monitor.repository.NucleusCompletedMessageRepository;
import com.ideas.ngi.nucleus.monitor.transformer.CompletedMessageTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.router.RecipientListRouter;
import org.springframework.messaging.MessageChannel;

/**
 * The CompletedMessageConfig defines the completedMessageFlow that will be
 * executed any-time a message completed successfully. Any message put on the
 * 'completedMessageChannel' will save the CompletedMessage and send a message
 * to Monitor to provide monitoring support for successful messages.
 */
@Configuration
public class CompletedMessageConfig {

    public static final String COMPLETED_MESSAGE_CHANNEL = "completedMessageChannel";

    @Autowired
    private NucleusIntegrationComponent nucleusIntegrationComponent;
    @Autowired
    private RecipientListRouter completionMonitorsRestClientRouter;
    @Autowired
    private CompletedMessageTransformer completedMessageTransformer;
    @Autowired
    private NucleusCompletedMessageRepository completedMessageRepository;

    @Bean(name = COMPLETED_MESSAGE_CHANNEL)
    public MessageChannel completedMessageChannel() {
        return nucleusIntegrationComponent.direct(COMPLETED_MESSAGE_CHANNEL);
    }

    @Bean
    public IntegrationFlow completedMessageFlow(MonitorMessageFilter monitorFilter) {
        return nucleusIntegrationComponent
                .from(COMPLETED_MESSAGE_CHANNEL)
                .transform(completedMessageTransformer)
                .<NucleusCompletedMessage>handle((p, h) -> completedMessageRepository.save(p))
                .filter(monitorFilter)
                .enrichHeaders(s -> s.header(MessageHeaderUtil.REST_ENDPOINT, MonitorEndpoint.CREATE_COMPLETED_JOB_INSTANCE))
                .route(completionMonitorsRestClientRouter)
                .get();
    }
}
