package com.ideas.ngi.nucleus.monitor.transformer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ideas.ngi.nucleus.monitor.entity.NucleusCompletedMessage;
import com.ideas.ngi.nucleus.monitor.entity.NucleusCompletedNotification;
import com.ideas.ngi.nucleus.monitor.service.MonitorMessageService;
import com.ideas.ngi.nucleus.rest.hateoas.ProxyLinkBuilderTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Transforms a NucleusCompletedMessage into a NucleusCompletedNotification with a truncated payload and hateoas links
 */
@Component
public class CompletedNotificationTransformer extends AbstractNotificationTransformer<NucleusCompletedMessage, NucleusCompletedNotification> {
    private ProxyLinkBuilderTransformer linkBuilderTransformer;

    @Autowired
    public CompletedNotificationTransformer(ObjectMapper objectMapper,
                                            ProxyLinkBuilderTransformer linkBuilderTransformer) {
        super(objectMapper);
        this.linkBuilderTransformer = linkBuilderTransformer;
    }

    @Override
    protected Class<NucleusCompletedNotification> provideClass() {
        return NucleusCompletedNotification.class;
    }

    @Override
    protected NucleusCompletedNotification createDefaultInstance(NucleusCompletedMessage completedMessage) {
        NucleusCompletedNotification notificationMessage = new NucleusCompletedNotification();
        notificationMessage.setId(completedMessage.getId());
        return notificationMessage;
    }

    @Override
    protected void setLinks(NucleusCompletedMessage nucleusCompletedMessage, NucleusCompletedNotification notification) {
        if (nucleusCompletedMessage.getPayload() != null || nucleusCompletedMessage.getPayloadReferenceId() != null) {
            notification.setPayloadLink(linkBuilderTransformer.transform(linkTo(methodOn(MonitorMessageService.class).downloadCompletedPayload(nucleusCompletedMessage.getId()))).withRel("enclosure"));
        }
    }
}
