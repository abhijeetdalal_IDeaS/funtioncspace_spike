package com.ideas.ngi.nucleus.monitor.repository;

import com.ideas.ngi.nucleus.config.mongo.annotation.GlobalCollection;
import com.ideas.ngi.nucleus.config.mongo.repository.NucleusMongoRepository;
import com.ideas.ngi.nucleus.monitor.entity.NucleusErrorMessage;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

import static com.ideas.ngi.functionspace.constant.Constants.PARAM_CLIENT_CODE;
import static com.ideas.ngi.functionspace.constant.Constants.PARAM_PROPERTY_CODE;
import static com.ideas.ngi.nucleus.util.DateTimeUtil.DATE_TIME_T_FORMAT;

/**
 * ErrorMessageRepository is used for CRUD operations on the ErrorMessage entity
 * and is exposes the collection via REST.
 */
@GlobalCollection
@RepositoryRestResource
public interface NucleusErrorMessageRepository extends NucleusMongoRepository<NucleusErrorMessage, String> {

    List<NucleusErrorMessage> findByClientCodeAndPropertyCodeOrderByJobInstanceIdAsc(
            @Param(PARAM_CLIENT_CODE) String clientCode,
            @Param(PARAM_PROPERTY_CODE) String propertyCode);

    @RestResource(path = "findByJobInstanceId", rel = "findByJobInstanceId")
    NucleusErrorMessage findFirstByJobInstanceId(
            @Param("jobInstanceId") String jobInstanceId);

    long deleteByClientCodeAndPropertyCode(
            @Param(PARAM_CLIENT_CODE) String clientCode,
            @Param(PARAM_PROPERTY_CODE) String propertyCode);

    List<NucleusErrorMessage> findByJobName(String jobName);

    List<NucleusErrorMessage> findByJobNameAndJobInstanceIdIsNotNull(String jobName);

    List<NucleusErrorMessage> findByJobNameAndJobInstanceIdIsNotNullOrderByJobInstanceIdAsc(
            @Param(PARAM_CLIENT_CODE) String clientCode,
            @Param(PARAM_PROPERTY_CODE) String propertyCode);

    List<NucleusErrorMessage> findByMessage(String message);

    List<NucleusErrorMessage> findByExternalSystemPropertyIdAndJobNameOrderByJobInstanceIdAsc(
            @Param("externalSystemPropertyId") String externalSystemPropertyId,
            @Param("jobName") String jobName);

    long deleteByExternalSystemPropertyId(
            @Param("externalSystemPropertyId") String externalSystemPropertyId);

    @RestResource(exported = false)
    long deleteByJobName(String jobName);

    List<NucleusErrorMessage> findByCreateDateAfter(@Param("createDate") @DateTimeFormat(pattern = DATE_TIME_T_FORMAT) Date createDate);

}
