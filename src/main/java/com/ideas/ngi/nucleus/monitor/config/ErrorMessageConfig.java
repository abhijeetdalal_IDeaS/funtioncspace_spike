package com.ideas.ngi.nucleus.monitor.config;

import com.ideas.ngi.nucleus.config.NucleusConfigProperties;
import com.ideas.ngi.nucleus.config.threading.ThreadingProperties;
import com.ideas.ngi.nucleus.integration.NucleusIntegrationComponent;
import com.ideas.ngi.nucleus.integration.util.MessageHeaderUtil;
import com.ideas.ngi.nucleus.monitor.converter.ErrorMessageConverter;
import com.ideas.ngi.nucleus.monitor.endpoint.MonitorEndpoint;
import com.ideas.ngi.nucleus.monitor.entity.NucleusErrorMessage;
import com.ideas.ngi.nucleus.monitor.filter.MonitorMessageFilter;
import com.ideas.ngi.nucleus.monitor.repository.NucleusErrorMessageRepository;
import com.ideas.ngi.nucleus.monitor.transformer.ErrorMessageTransformer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.context.IntegrationContextUtils;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.router.RecipientListRouter;
import org.springframework.jmx.export.MBeanExporter;
import org.springframework.jmx.support.RegistrationPolicy;
import org.springframework.messaging.MessageChannel;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.HashMap;
import java.util.Map;

/**
 * The ErrorConfig defines the errorFlow that will be executed any-time a failed
 * message gets put onto the 'errorInputChannel'. Currently, it will save a copy
 * of the message and send the message to Monitor so it can be re-tried at a later
 * time.
 */
@Configuration
public class ErrorMessageConfig {

    public static final String ERROR_MESSAGE_FLOW_CHANNEL = "errorMessageFlowChannel";
    public static final String ERROR_MESSAGE_RETRY_CHANNEL = "errorMessageRetryChannel";
    public static final String MONITOR_ERROR_MESSAGE_CHANNEL = "monitorErrorMessageChannel";

    @Autowired
    private ThreadingProperties threadingProperties;

    @Autowired
    private NucleusIntegrationComponent nucleusIntegrationComponent;
    @Autowired
    private RecipientListRouter errorMonitorsRestClientRouter;
    @Autowired
    private NucleusErrorMessageRepository errorMessageRepository;
    @Autowired
    private ErrorMessageTransformer errorMessageTransformer;
    @Autowired
    private NucleusConfigProperties nucleusConfigProperties;

    @Bean(name = IntegrationContextUtils.ERROR_CHANNEL_BEAN_NAME)
    public MessageChannel errorChannel() {
        return nucleusIntegrationComponent.amqpChannel(IntegrationContextUtils.ERROR_CHANNEL_BEAN_NAME, nucleusConfigProperties.getConcurrentQueueConsumers(),
                nucleusErrorMessageThreadPoolTaskExecutor(), new ErrorMessageConverter());
    }

    @Bean(name = MONITOR_ERROR_MESSAGE_CHANNEL)
    public MessageChannel monitorErrorMessageChannel() {
        return nucleusIntegrationComponent.amqpChannel(MONITOR_ERROR_MESSAGE_CHANNEL, nucleusConfigProperties.getConcurrentQueueConsumers(),
                nucleusErrorMessageThreadPoolTaskExecutor());
    }

    @Bean(name = ERROR_MESSAGE_FLOW_CHANNEL)
    public MessageChannel errorMessageFlowChannel() {
        return nucleusIntegrationComponent.direct(ERROR_MESSAGE_FLOW_CHANNEL);
    }

    @Bean(name = ERROR_MESSAGE_RETRY_CHANNEL)
    public MessageChannel errorMessageRetryChannel() {
        return nucleusIntegrationComponent.amqpChannel(ERROR_MESSAGE_RETRY_CHANNEL, nucleusConfigProperties.getConcurrentQueueConsumers(),
                nucleusErrorMessageThreadPoolTaskExecutor());
    }

    @Bean
    public IntegrationFlow errorChannelFlow() {
        return nucleusIntegrationComponent
                .from(IntegrationContextUtils.ERROR_CHANNEL_BEAN_NAME)
                .channel(ERROR_MESSAGE_FLOW_CHANNEL)
                .get();
    }

    @Bean
    public IntegrationFlow errorMessageFlow() {
        return nucleusIntegrationComponent
                .from(ERROR_MESSAGE_FLOW_CHANNEL)
                .transform(errorMessageTransformer)
                .channel(MONITOR_ERROR_MESSAGE_CHANNEL)
                .get();
    }

    @Bean
    public IntegrationFlow errorMessageRetryFlow() {
        return nucleusIntegrationComponent
                .from(ERROR_MESSAGE_RETRY_CHANNEL)
                .route("headers['errorRetryChannel']")
                .get();
    }

    @Bean
    public IntegrationFlow monitorSendErrorMessageFlow(MonitorMessageFilter monitorFilter) {
        return nucleusIntegrationComponent
                .from(MONITOR_ERROR_MESSAGE_CHANNEL)
                .<NucleusErrorMessage>handle((p, h) -> setAppHost(p))
                .<NucleusErrorMessage>handle((p, h) -> errorMessageRepository.save(p))
                .filter(monitorFilter)
                .enrichHeaders(s -> s.header(MessageHeaderUtil.REST_ENDPOINT, MonitorEndpoint.CREATE_FAILED_JOB_INSTANCE))
                .route(errorMonitorsRestClientRouter)
                .get();
    }

    private NucleusErrorMessage setAppHost(NucleusErrorMessage nucleusErrorMessage) {
        if (StringUtils.isBlank(nucleusErrorMessage.getAppHostName())) {
            nucleusErrorMessage.setAppHostName(nucleusConfigProperties.getHostname());
        }
        return nucleusErrorMessage;
    }

    @Bean(name = "nucleusErrorMessageThreadPoolTaskExecutor")
    public ThreadPoolTaskExecutor nucleusErrorMessageThreadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setThreadNamePrefix("NucleusErrorMessageThreadExecutor-");
        executor.setCorePoolSize(threadingProperties.getCorePoolSize());
        executor.setMaxPoolSize(threadingProperties.getMaxPoolSize());
        executor.setQueueCapacity(threadingProperties.getQueueCapacity());
        executor.setKeepAliveSeconds(threadingProperties.getKeepAliveSeconds());

        return executor;
    }

    @Bean
    protected MBeanExporter nucleusErrorMessageThreadPoolExporter(@Qualifier("nucleusErrorMessageThreadPoolTaskExecutor") ThreadPoolTaskExecutor nucleusErrorMessageThreadPoolTaskExecutor) {
        MBeanExporter exporter = new MBeanExporter();
        Map<String,Object> beans = new HashMap<>();
        exporter.setRegistrationPolicy(RegistrationPolicy.IGNORE_EXISTING);
        beans.put("com.ideas.ngi.nucleus:type=taskExecutor,name=nucleusErrorMessageThreadPoolTaskExecutor", nucleusErrorMessageThreadPoolTaskExecutor);
        beans.put("com.ideas.ngi.nucleus:type=executor,name=nucleusErrorMessageThreadPoolExecutor", nucleusErrorMessageThreadPoolTaskExecutor.getThreadPoolExecutor());
        beans.put("com.ideas.ngi.nucleus:type=queue,name=nucleusErrorMessageLinkedBlockingQueue", nucleusErrorMessageThreadPoolTaskExecutor.getThreadPoolExecutor().getQueue());
        exporter.setBeans(beans);
        return exporter;
    }
}
