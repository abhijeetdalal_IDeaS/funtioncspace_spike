package com.ideas.ngi.nucleus.monitor.transformer;

import com.ideas.ngi.nucleus.monitor.entity.NucleusCompletedMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.transformer.GenericTransformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;

/**
 * Transforms a Message into a CompletedMessage
 */
@Component
public class CompletedMessageTransformer extends AbstractMessageTransformer implements GenericTransformer<Message, NucleusCompletedMessage> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public NucleusCompletedMessage transform(Message message) {
        logger.debug("Transforming Message: " + message + " into a CompletedMessage");

        // Extract the common fields
        Object payload = message.getPayload();
        MessageHeaders headers = message.getHeaders();

        // Construct the CompletedMessage object
        NucleusCompletedMessage completedMessage = new NucleusCompletedMessage();

        // Set all MonitorMessage fields
        applyRules(completedMessage, headers, payload);

        // Return the CompletedMessage
        return completedMessage;
    }

}
