package com.ideas.ngi.nucleus.monitor.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ideas.ngi.nucleus.monitor.entity.NucleusMonitorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PayloadStrategyFactory {

    private ObjectMapper objectMapper;

    @Autowired
    public PayloadStrategyFactory(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public PayloadDownloadStrategy newInstance(NucleusMonitorMessage monitorMessage) {
        if (monitorMessage != null && monitorMessage.getPayload() != null) {
            return new SerializedJsonPayload(objectMapper, monitorMessage.getPayload(), monitorMessage.getId(), monitorMessage.getClass().getSimpleName());
        }
        // TODO: implement other downloads based on other payload formats, e.g. GridFS (serializable or not?), file/URL based
        return null;
    }
}
