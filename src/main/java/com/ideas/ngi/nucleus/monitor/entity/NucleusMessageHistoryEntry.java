package com.ideas.ngi.nucleus.monitor.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * MessageHistoryEntry is used to be able to identify what components of the
 * flow have been touched for a given Message.
 */
public class NucleusMessageHistoryEntry implements Serializable {

    private static final long serialVersionUID = -8063378581679626163L;

    private String name;
    private String type;
    private Date startDate;
    private Date endDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
