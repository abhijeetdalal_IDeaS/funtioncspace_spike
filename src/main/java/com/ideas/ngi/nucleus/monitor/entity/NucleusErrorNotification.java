package com.ideas.ngi.nucleus.monitor.entity;

import org.springframework.hateoas.Link;

public class NucleusErrorNotification extends NucleusErrorMessage {

    private Link payloadLink;
    private Link retryLink;
    private Link abandonLink;

    public Link getPayloadLink() {
        return payloadLink;
    }

    public void setPayloadLink(Link payloadLink) {
        this.payloadLink = payloadLink;
    }

    public Link getRetryLink() {
        return retryLink;
    }

    public void setRetryLink(Link retryLink) {
        this.retryLink = retryLink;
    }

    public Link getAbandonLink() {
        return abandonLink;
    }

    public void setAbandonLink(Link abandonLink) {
        this.abandonLink = abandonLink;
    }
}
