package com.ideas.ngi.nucleus.monitor.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * The CompletedMessage entity is used to track all successful Messages that
 * occur in processing. It contains an unique id, the job instance id of the
 * JEMS job it created, the job name, message payload, and message history.
 */
@Document
public class NucleusCompletedMessage extends NucleusMonitorMessage implements Serializable {
    private static final long serialVersionUID = -4679862758855783481L;

    @Id
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
