package com.ideas.ngi.nucleus.monitor.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ideas.ngi.nucleus.integration.annotation.ClientCode;
import com.ideas.ngi.nucleus.integration.annotation.ClientEnvironment;
import com.ideas.ngi.nucleus.integration.annotation.PropertyCode;
import org.apache.commons.lang3.tuple.Pair;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@SuppressWarnings("squid:S1948")
public abstract class NucleusMonitorMessage implements Serializable {

    private static final long serialVersionUID = -4990617176439462236L;

    private Long jobInstanceId;

    private String jobName;

    @ClientCode
    private String clientCode;

    private String clientId;

    @PropertyCode
    private String propertyCode;

    private String propertyId;

    private String externalSystemPropertyId;

    @ClientEnvironment
    private String clientEnvironmentName;

    private Object payload;

    @JsonIgnore
    private List<Pair<String, Object>> headers;

    private Object payloadReferenceId;

    private String url;

    private String httpMethod;
    private String responseType;
    private List<NucleusMessageHistoryEntry> messageHistoryEntries;

    private Date createDate = new Date();

    private String appHostName;

    public abstract String getId();

    public Long getJobInstanceId() {
        return jobInstanceId;
    }

    public void setJobInstanceId(Long jobInstanceId) {
        this.jobInstanceId = jobInstanceId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

    public Object getPayloadReferenceId() {
        return payloadReferenceId;
    }

    public void setPayloadReferenceId(Object payloadReferenceId) {
        this.payloadReferenceId = payloadReferenceId;
    }

    public String getExternalSystemPropertyId() {
        return externalSystemPropertyId;
    }

    public void setExternalSystemPropertyId(String externalSystemPropertyId) {
        this.externalSystemPropertyId = externalSystemPropertyId;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public List<NucleusMessageHistoryEntry> getMessageHistoryEntries() {
        return messageHistoryEntries;
    }

    public void setMessageHistoryEntries(List<NucleusMessageHistoryEntry> messageHistoryEntries) {
        this.messageHistoryEntries = messageHistoryEntries;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String toString() {
        return getClass().getSimpleName() + "[id=" + getId() + ",jobName=" + getJobName() + ",payload=" + payload + "]";
    }

    public String getClientEnvironmentName() {
        return clientEnvironmentName;
    }

    public void setClientEnvironmentName(String clientEnvironmentName) {
        this.clientEnvironmentName = clientEnvironmentName;
    }

    public String getAppHostName() {
        return appHostName;
    }

    public void setAppHostName(String appHostName) {
        this.appHostName = appHostName;
    }

    public List<Pair<String, Object>> getHeaders() {
        return headers;
    }

    public void setHeaders(List<Pair<String, Object>> headers) {
        this.headers = headers;
    }
}
