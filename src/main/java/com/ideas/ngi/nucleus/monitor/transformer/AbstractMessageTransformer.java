package com.ideas.ngi.nucleus.monitor.transformer;

import com.ideas.ngi.nucleus.config.NucleusConfigProperties;
import com.ideas.ngi.nucleus.monitor.entity.NucleusMonitorMessage;
import com.ideas.ngi.nucleus.monitor.util.DefaultMessageBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;

import java.util.List;

abstract class AbstractMessageTransformer {

    //constructor injection was not used as there is one required dependency and one optional dependency
    @Autowired
    private DefaultMessageBuilder defaultMessageBuilder;

    @Autowired
    private NucleusConfigProperties nucleusConfigProperties;

    //constructor injection was not used as there is one required dependency and one optional dependency
    @Autowired(required = false)
    private List<SpecificMessageBuilder> specificMessageBuilders;

    void applyRules(NucleusMonitorMessage errorMessage, MessageHeaders headers, Object payload) {
        boolean specificRulesApplied = applySpecificRules(errorMessage, headers, payload);
        if (!specificRulesApplied) {
            defaultMessageBuilder.populateMonitorMessage(errorMessage, payload, headers);
        }
        if (StringUtils.isBlank(errorMessage.getAppHostName())) {
            errorMessage.setAppHostName(nucleusConfigProperties.getHostname());
        }
    }

    private boolean applySpecificRules(NucleusMonitorMessage errorMessage, MessageHeaders headers, Object payload) {
        boolean specificRulesApplied = false;
        if (CollectionUtils.isNotEmpty(specificMessageBuilders)) {
            for (SpecificMessageBuilder specificMessageBuilder : specificMessageBuilders) {
                if (specificMessageBuilder.canProcess(payload)) {
                    specificMessageBuilder.populateMonitorMessage(errorMessage, payload, headers);
                    specificRulesApplied = true;
                }
            }
        }
        return specificRulesApplied;
    }

    public void setSpecificMessageBuilders(List<SpecificMessageBuilder> specificMessageBuilders) {
        this.specificMessageBuilders = specificMessageBuilders;
    }
}
