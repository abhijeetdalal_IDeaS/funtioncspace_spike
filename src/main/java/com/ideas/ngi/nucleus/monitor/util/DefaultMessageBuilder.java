package com.ideas.ngi.nucleus.monitor.util;

import com.ideas.ngi.nucleus.integration.util.MessageHeaderUtil;
import com.ideas.ngi.nucleus.integration.util.MessagePayloadUtil;
import com.ideas.ngi.nucleus.monitor.entity.NucleusMonitorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collection;

@Component
public class DefaultMessageBuilder {

    private MessagePayloadUtil messagePayloadUtil;

    @Autowired
    public DefaultMessageBuilder(MessagePayloadUtil messagePayloadUtil) {
        this.messagePayloadUtil = messagePayloadUtil;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultMessageBuilder.class);

    public void populateMonitorMessage(NucleusMonitorMessage monitorMessage, Object payload, MessageHeaders headers) {
        LOGGER.debug("Populating MonitorMessage: {} from payload: {} and headers: {}", monitorMessage, payload, headers);

        monitorMessage.setJobName(MessageHeaderUtil.getJobName(headers));
        monitorMessage.setHeaders(MessageHeaderUtil.copyHeaders(headers));

        // Build a list of MessageHistoryEntry objects from the MessageHistory
        monitorMessage.setMessageHistoryEntries(MessageHeaderUtil.getMessageHistoryEntries(headers));

        // If we have a Collection as the payload, use the first object in the
        // collection to determine the property identifiable information
        // (this assumes all messages are for the same property!)
        Object first = payload;
        if (payload instanceof Collection && !CollectionUtils.isEmpty((Collection<?>) payload)) {
            first = ((Collection<?>) payload).iterator().next();
        }

        // DE7320: Certain message types can flood the monitoring system with unnecessary and unused information,
        // so we will suppress adding the payload to the monitor message for those message types.
        if (messagePayloadUtil.isIncludePayload(first)) {
            monitorMessage.setPayload(payload);
        }

        // determine the property identifiable information from the payload
        monitorMessage.setClientCode(messagePayloadUtil.getClientCode(first));
        monitorMessage.setClientId(messagePayloadUtil.getClientId(first));
        monitorMessage.setPropertyCode(messagePayloadUtil.getPropertyCode(first));
        monitorMessage.setPropertyId(messagePayloadUtil.getPropertyId(first));
        monitorMessage.setExternalSystemPropertyId(messagePayloadUtil.getExternalSystemPropertyId(first));
        monitorMessage.setClientEnvironmentName(messagePayloadUtil.getClientEnvironmentName(first));
    }
}
