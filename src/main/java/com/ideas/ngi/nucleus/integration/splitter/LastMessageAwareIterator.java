package com.ideas.ngi.nucleus.integration.splitter;

import com.ideas.ngi.nucleus.integration.util.MessageHeaderUtil;
import org.springframework.integration.support.MessageBuilder;

import java.util.Iterator;

public class LastMessageAwareIterator implements Iterator<MessageBuilder<?>> {
    private Iterator<?> iter;

    public LastMessageAwareIterator(Iterator<?> iter) {
        this.iter = iter;
    }

    @Override
    public boolean hasNext() {
        return iter.hasNext();
    }

    @Override
    public MessageBuilder<?> next() {
        MessageBuilder<?> next = (MessageBuilder<?>) iter.next();
        if (!hasNext() && next instanceof MessageBuilder<?>) {
            next = ((MessageBuilder<?>) next).setHeader(MessageHeaderUtil.ITERABLE_LAST_MESSAGE, Boolean.TRUE);
        }

        return next;
    }
}
