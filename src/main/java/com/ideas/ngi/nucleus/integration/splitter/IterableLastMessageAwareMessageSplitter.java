package com.ideas.ngi.nucleus.integration.splitter;

import org.springframework.integration.splitter.DefaultMessageSplitter;
import org.springframework.messaging.Message;

import java.util.Iterator;


public class IterableLastMessageAwareMessageSplitter extends DefaultMessageSplitter {

    @Override
    protected void produceOutput(Object result, Message<?> requestMessage) {
        super.produceOutput(new LastMessageAwareIterator((Iterator<?>) result), requestMessage);
    }
}
