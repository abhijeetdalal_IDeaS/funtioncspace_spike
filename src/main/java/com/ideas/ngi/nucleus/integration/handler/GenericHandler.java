package com.ideas.ngi.nucleus.integration.handler;

import org.springframework.messaging.MessageHeaders;

import java.util.Map;

public interface GenericHandler<P> extends org.springframework.integration.handler.GenericHandler<P> {

    @Override
    default Object handle(P payload, MessageHeaders headers) {
        return handle(payload, (Map<String, Object>)headers);
    }

    Object handle(P payload, Map<String, Object> headers);

}
