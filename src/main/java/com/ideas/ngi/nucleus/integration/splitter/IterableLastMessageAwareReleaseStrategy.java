package com.ideas.ngi.nucleus.integration.splitter;

import com.ideas.ngi.nucleus.integration.util.MessageHeaderUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.integration.aggregator.ReleaseStrategy;
import org.springframework.integration.store.MessageGroup;
import org.springframework.messaging.Message;

import java.util.Collection;

public class IterableLastMessageAwareReleaseStrategy implements ReleaseStrategy {

    @Override
    public boolean canRelease(MessageGroup messageGroup) {
        // Need to look at the last message to see if it has the header key
        Collection<Message<?>> messages = messageGroup.getMessages();
        if (CollectionUtils.isNotEmpty(messages)) {
            
            // Get the last Message in the collection and check it's sequence size
            Message<?> lastMessage = (Message<?>) CollectionUtils.get(messages, messages.size() - 1);
            if (lastMessage.getHeaders().containsKey(MessageHeaderUtil.ITERABLE_LAST_MESSAGE)) {
                return true;
            }
        }
        
        return false;
    }
}
