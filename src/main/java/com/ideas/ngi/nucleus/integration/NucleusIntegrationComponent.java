package com.ideas.ngi.nucleus.integration;

import com.ideas.ngi.nucleus.config.amqp.NucleusMessageRecoverer;
import com.ideas.ngi.nucleus.config.amqp.rabbit.RabbitConfigProperties;
import com.ideas.ngi.nucleus.exception.NucleusRetryableException;
import com.ideas.ngi.functionspace.file.ftp.FTPConfigProperties;
import com.ideas.ngi.functionspace.file.sftp.NucleusSftpSessionFactory;
import com.ideas.ngi.nucleus.integration.splitter.IterableLastMessageAwareMessageSplitter;
import com.ideas.ngi.nucleus.integration.splitter.IterableLastMessageAwareReleaseStrategy;
import com.jcraft.jsch.ChannelSftp;
import org.aopalliance.aop.Advice;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.config.RetryInterceptorBuilder.StatelessRetryInterceptorBuilder;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.retry.MessageRecoverer;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.dao.TransientDataAccessResourceException;
import org.springframework.integration.aggregator.AggregatingMessageHandler;
import org.springframework.integration.amqp.dsl.Amqp;
import org.springframework.integration.amqp.dsl.AmqpMessageChannelSpec;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.context.IntegrationContextUtils;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.*;
import org.springframework.integration.file.DirectoryScanner;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.dsl.Files;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.gateway.MessagingGatewaySupport;
import org.springframework.integration.sftp.dsl.Sftp;
import org.springframework.integration.sftp.inbound.SftpInboundFileSynchronizingMessageSource;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.retry.RetryListener;
import org.springframework.retry.backoff.BackOffPolicy;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.util.ErrorHandler;

import java.io.File;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * NucleusIntegrationComponent provides a single place to construct Nucleus
 * defined flow components. It will allow us to set parameters on the channels
 * in a single place (when required) from system properties, etc.
 */
@Component
public class NucleusIntegrationComponent {

    private final ConnectionFactory rabbitConnectionFactory;
    private final NucleusMessageRecoverer nucleusMessageRecoverer;
    private final RabbitConfigProperties rabbitConfigProperties;
    private final ErrorHandler errorHandler;
    private final ThreadPoolTaskExecutor defaultThreadPoolTaskExecutor;
    private final Environment environment;

    private static final int SFTP_SESSION_CACHE_POOL_SIZE = 5;
    private static final int SESSION_CACHE_TIMEOUT = 1000;

    @Autowired
    public NucleusIntegrationComponent(ConnectionFactory rabbitConnectionFactory,
                                       NucleusMessageRecoverer nucleusMessageRecoverer,
                                       RabbitConfigProperties rabbitConfigProperties,
                                       ErrorHandler errorHandler,
                                       @Qualifier("nucleusThreadPoolTaskExecutor") ThreadPoolTaskExecutor threadPoolTaskExecutor,
                                       Environment environment
    ) {
        this.rabbitConnectionFactory = rabbitConnectionFactory;
        this.nucleusMessageRecoverer = nucleusMessageRecoverer;
        this.rabbitConfigProperties = rabbitConfigProperties;
        this.errorHandler = errorHandler;
        this.defaultThreadPoolTaskExecutor = threadPoolTaskExecutor;
        this.environment = environment;
    }

    public MessageChannel direct(String id) {
        return MessageChannels.direct(id).get();
    }

    public MessageChannel direct(String id, ChannelInterceptor channelInterceptor) {
        DirectChannel directChannel = (DirectChannel) direct(id);
        directChannel.addInterceptor(channelInterceptor);
        return directChannel;
    }

    public MessageChannel amqpChannel(String id) {
        return amqpChannel(id, null, defaultThreadPoolTaskExecutor, defaultRetryAdvice(), null);
    }

    public MessageChannel amqpChannel(String id, Integer concurrentQueueConsumers) {
        return amqpChannel(id, concurrentQueueConsumers, defaultThreadPoolTaskExecutor, defaultRetryAdvice(), null);
    }

    public MessageChannel amqpChannel(String id, Integer concurrentQueueConsumers, Advice advice) {
        return amqpChannel(id, concurrentQueueConsumers, defaultThreadPoolTaskExecutor, advice != null ? advice : defaultRetryAdvice(), null);
    }

    public MessageChannel amqpChannel(String id, Integer concurrentQueueConsumers, ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        return amqpChannel(id, concurrentQueueConsumers, threadPoolTaskExecutor, defaultRetryAdvice(), null);
    }

    public MessageChannel amqpChannel(String id, Integer concurrentQueueConsumers, ThreadPoolTaskExecutor threadPoolTaskExecutor,
                                      MessageConverter amqpMessageConverter) {
        return amqpChannel(id, concurrentQueueConsumers, threadPoolTaskExecutor, defaultRetryAdvice(), amqpMessageConverter);
    }

    public MessageChannel amqpChannel(String id, Integer concurrentQueueConsumers, ThreadPoolTaskExecutor threadPoolTaskExecutor, Advice advice,
                                      MessageConverter amqpMessageConverter) {
        AmqpMessageChannelSpec<?> messageChannelSpec = Amqp.channel(id, rabbitConnectionFactory)
                .concurrentConsumers(getConcurrentQueueConsumers(id, concurrentQueueConsumers))
                .advice(advice)
                .prefetchCount(rabbitConfigProperties.getPrefetch())
                .taskExecutor(threadPoolTaskExecutor)
                .errorHandler(errorHandler);
        if (amqpMessageConverter != null) {
            // the amqp message converter is invoked from within the rabbit template for general queue serde
            // the channel message converter is used to convert messages when a channel has datatypes configured and
            //   an unsupported message datatype is encountered
            messageChannelSpec.amqpMessageConverter(amqpMessageConverter);
        }
        return messageChannelSpec.get();
    }

    protected Integer getConcurrentQueueConsumers(String id, Integer concurrentQueueConsumers) {
        // Providing the ability to override any channel consumer count should the need arise
        String overridenQueueConsumers = environment.getProperty(id + ".concurrent.queue.consumers");
        if (StringUtils.isNotEmpty(overridenQueueConsumers)) {
            concurrentQueueConsumers = Integer.valueOf(overridenQueueConsumers);
        }

        // If no consumer count has been set, use the default
        if (concurrentQueueConsumers == null) {
            concurrentQueueConsumers = rabbitConfigProperties.getConcurrentQueueConsumers();
        }

        return concurrentQueueConsumers;
    }

    public MessageChannel amqpPublishSubscribeChannel(String id) {
        return Amqp.publishSubscribeChannel(id, rabbitConnectionFactory)
                .concurrentConsumers(rabbitConfigProperties.getConcurrentQueueConsumers())
                .advice(defaultRetryAdvice())
                .errorHandler(errorHandler).get();
    }

    Advice defaultRetryAdvice() {
        ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        backOffPolicy.setInitialInterval(rabbitConfigProperties.getRetryBackoffInitialInterval());
        backOffPolicy.setMultiplier(rabbitConfigProperties.getRetryBackoffMultiplier());
        backOffPolicy.setMaxInterval(rabbitConfigProperties.getRetryBackupMaxInterval());

        return retryAdvice(rabbitConfigProperties.getRetryBackoffNumberOfRetries(), backOffPolicy);
    }

    public Advice retryAdvice(int maxAttempts, BackOffPolicy backOffPolicy) {
        return retryAdvice(maxAttempts, backOffPolicy, nucleusMessageRecoverer);
    }

    public Advice retryAdvice(int maxAttempts, BackOffPolicy backOffPolicy, MessageRecoverer messageRecoverer) {
        Map<Class<? extends Throwable>, Boolean> retryableExceptions = new HashMap<>();
        retryableExceptions.put(NucleusRetryableException.class, true);
        if (rabbitConfigProperties.isEnhancedRetryEnabled()) {
            retryableExceptions.put(TransientDataAccessResourceException.class, true);
        }
        SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy(maxAttempts, retryableExceptions, true);

        RetryTemplate retryTemplate = new RetryTemplate();
        retryTemplate.setListeners(new RetryListener[]{new MethodLoggingRetryListener()});
        retryTemplate.setRetryPolicy(retryPolicy);
        retryTemplate.setBackOffPolicy(backOffPolicy);

        return StatelessRetryInterceptorBuilder
                .stateless()
                .retryOperations(retryTemplate)
                .recoverer(messageRecoverer)
                .build();
    }

    public IntegrationFlowBuilder from(String channelName) {
        return IntegrationFlows.from(channelName).enrichHeaders(
                h -> h.header(MessageHeaders.REPLY_CHANNEL, IntegrationContextUtils.NULL_CHANNEL_BEAN_NAME));
    }

    public IntegrationFlowBuilder from(MessageChannel messageChannel) {
        return IntegrationFlows.from(messageChannel).enrichHeaders(
                h -> h.header(MessageHeaders.REPLY_CHANNEL, IntegrationContextUtils.NULL_CHANNEL_BEAN_NAME));
    }

    public IntegrationFlowBuilder from(MessagingGatewaySupport messagingGatewaySupport) {
        return IntegrationFlows.from(messagingGatewaySupport);

    }

    public <S extends MessageSourceSpec<S, ? extends MessageSource<?>>> IntegrationFlowBuilder from(
            S messageProducerSpec, Consumer<SourcePollingChannelAdapterSpec> endpointConfigurer) {
        return IntegrationFlows.from(messageProducerSpec, endpointConfigurer).enrichHeaders(
                h -> h.header(MessageHeaders.REPLY_CHANNEL, IntegrationContextUtils.NULL_CHANNEL_BEAN_NAME));
    }

    public Consumer<HeaderEnricherSpec> buildHeaders(Map<String, Object> headers) {
        return e -> e.defaultOverwrite(true).shouldSkipNulls(false).messageProcessor(m -> headers).get();
    }

    IterableLastMessageAwareMessageSplitter iterableLastMessageAwareMessageSplitter() {
        return new IterableLastMessageAwareMessageSplitter();
    }

    Consumer<GenericEndpointSpec<AggregatingMessageHandler>> iterableLastMessageAwareAggregator() {
        return (a) -> a.get().getT2().setReleaseStrategy(new IterableLastMessageAwareReleaseStrategy());
    }

    private SftpInboundFileSynchronizingMessageSource sftpInboundFileSynchronizingMessageSource(FTPConfigProperties ftpConfigProperties) {
        return sftpInboundFileSynchronizingMessageSource(
                ftpConfigProperties,
                ftpConfigProperties.getFilenamePatternFilter(),
                ftpConfigProperties.getLocalDataDirectory(),
                ftpConfigProperties.getRemoteDirectory());
    }

    public SftpInboundFileSynchronizingMessageSource sftpInboundFileSynchronizingMessageSource(FTPConfigProperties ftpConfigProperties, String filePattern, File localDirLocation, String remoteDirLocation) {
        return Sftp.inboundAdapter(sftpSessionFactory(ftpConfigProperties), Comparator.comparing(File::getName))
                .patternFilter(filePattern)
                .autoCreateLocalDirectory(true)
                .deleteRemoteFiles(false)
                .localDirectory(localDirLocation)
                .preserveTimestamp(true)
                .remoteDirectory(remoteDirLocation)
                .get();
    }

    public MessageSource<File> inboundFileAdapter(File incomingDirectory, DirectoryScanner scanner, boolean scanEachPoll) {
        FileReadingMessageSource messageSource = new FileReadingMessageSource();
        messageSource.setDirectory(incomingDirectory);
        messageSource.setScanEachPoll(scanEachPoll);
        messageSource.setScanner(scanner);
        messageSource.setAutoCreateDirectory(true);
        return messageSource;
    }

    public SessionFactory<ChannelSftp.LsEntry> sftpSessionFactory(FTPConfigProperties ftpConfigProperties) {
        return sftpSessionFactory(ftpConfigProperties, s -> { /* NO-OP */ });
    }

    private SessionFactory<ChannelSftp.LsEntry> sftpSessionFactory(FTPConfigProperties ftpConfigProperties, Consumer<DefaultSftpSessionFactory> configurer) {
        if (ftpConfigProperties.isSftpConfigured()) {
            NucleusSftpSessionFactory sessionFactory = new NucleusSftpSessionFactory(ftpConfigProperties);

            // apply any additional configuration
            configurer.accept(sessionFactory);

            CachingSessionFactory<ChannelSftp.LsEntry> cachingSessionFactory = new CachingSessionFactory<>(sessionFactory, SFTP_SESSION_CACHE_POOL_SIZE);
            cachingSessionFactory.setSessionWaitTimeout(SESSION_CACHE_TIMEOUT);
            return cachingSessionFactory;
        } else {
            return null;
        }
    }

    public MessageSource<File> inboundFileSynchronizingMessageSource(FTPConfigProperties configProperties) {
        if (configProperties.isSftpConfigured()) {
            return sftpInboundFileSynchronizingMessageSource(configProperties);
        } else {
            return Files.inboundAdapter(configProperties.getLocalDataDirectory(), Comparator.comparing(File::getName))
                    .patternFilter(configProperties.getFilenamePatternFilter())
                    .preventDuplicates(true)
                    .get();
        }
    }
}
