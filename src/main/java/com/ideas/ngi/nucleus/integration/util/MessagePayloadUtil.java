package com.ideas.ngi.nucleus.integration.util;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.vendor.service.NucleusSettingsService;
import com.ideas.ngi.nucleus.exception.NucleusException;
import com.ideas.ngi.nucleus.integration.annotation.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Component
public class MessagePayloadUtil {

    private NucleusSettingsService nucleusSettingsService;

    @Autowired
    public MessagePayloadUtil(NucleusSettingsService nucleusSettingsService) {
        this.nucleusSettingsService = nucleusSettingsService;
    }

    public String getPropertyId(Object payload) {
        return handleResult(getValueForAnnotation(payload, PropertyId.class));
    }

    public String getClientId(Object payload) {
        return handleResult(getValueForAnnotation(payload, ClientId.class));
    }

    public String getClientCode(Object payload) {
        return handleResult(getValueForAnnotation(payload, ClientCode.class));
    }

    public String getPropertyCode(Object payload) {
        return handleResult(getValueForAnnotation(payload, PropertyCode.class));
    }

    public Boolean isIncludePayload(Object payload) {
        Object includePayload = getValueForAnnotation(payload, IncludePayload.class);
        return includePayload != null ? (Boolean)includePayload : Boolean.TRUE;
    }

    public String getClientEnvironmentName(Object payload) {
        Object clientEnvironment = getValueForAnnotation(payload, ClientEnvironment.class);
        if (clientEnvironment == null) {
            String clientCode = getClientCode(payload);
            String propertyCode = getPropertyCode(payload);
            IntegrationType integrationType = getIntegrationTypeWithDefault(payload);
            if (StringUtils.isNotBlank(clientCode) && StringUtils.isNotBlank(propertyCode)) {
                clientEnvironment = nucleusSettingsService.getClientEnvironmentName(clientCode, propertyCode, integrationType);
            }
        }
        return handleResult(clientEnvironment);
    }

    private IntegrationType getIntegrationTypeWithDefault(Object payload) {
        try {
            return getIntegrationType(payload);
        } catch(NucleusException e) {
            return IntegrationType.UNKNOWN;
        }
    }

    public IntegrationType getIntegrationType(Object payload) {
        TypeFinder<IntegrationType> typeFinder = new TypeFinder<>(IntegrationType.class, payload);
        ReflectionUtils.doWithFields(payload.getClass(), typeFinder);
        return typeFinder.getValue();
    }

    static class TypeFinder<T> implements ReflectionUtils.FieldCallback {
        private List<T> values;
        private Class<T> type;
        private Object source;

        TypeFinder(Class<T> type, Object source) {
            this.type = type;
            this.values = new ArrayList<>();
            this.source = source;
        }

        @SuppressWarnings("unchecked")
        @Override
        public void doWith(Field field) throws IllegalAccessException {
            if (field.getType().isAssignableFrom(type)) {
                field.setAccessible(true);
                values.add((T) field.get(source));
            }
        }

        public T getValue() {
            if (values.isEmpty()) {
                throw new NucleusException("Could not find any fields of type: " + type.getName());
            } else if (values.size() > 1) {
                throw new NucleusException("Found more than one field of type: " + type.getName());
            }
            return values.get(0);
        }

        public List<T> getValues() {
            return values;
        }
    }

    public String getExternalSystemPropertyId(Object payload) {
        return handleResult(getValueForAnnotation(payload, ExternalSystemPropertyId.class));
    }

    private Object getValueForAnnotation(Object payload, Class<? extends Annotation> annotationClass) {
        if (payload instanceof Map<?, ?>) {
            return ((Map<?, ?>) payload).get(WordUtils.uncapitalize(annotationClass.getSimpleName()));
        }

        try {
            // Get a list of classes in the payload class hierarchy
            List<Class<?>> classHierarchy = new ArrayList<>();
            Class<?> currentClass = payload.getClass();
            while (!currentClass.equals(Object.class)) {
                classHierarchy.add(currentClass);
                currentClass = currentClass.getSuperclass();
            }

            // Iterate over all the classes in the hierarchy to see if there is a field with that annotation
            for (Class<?> clazz : classHierarchy) {
                Field field = findAnnotatedField(clazz, annotationClass);
                if (field != null) {
                    field.setAccessible(true);
                    return field.get(payload);
                }

                Method method = findAnnotatedMethod(clazz, annotationClass);
                if (method != null) {
                    method.setAccessible(true);
                    return method.invoke(payload);
                }
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new NucleusException("Unable to access the " + annotationClass, e);
        }

        return null;
    }

    private Field findAnnotatedField(Class<?> clazz, Class<? extends Annotation> annotation) {
        Field[] fields = clazz.getDeclaredFields();
        if (fields != null) {
            for (Field field : fields) {
                if (field.isAnnotationPresent(annotation)) {
                    return field;
                }
            }
        }
        return null;
    }

    private Method findAnnotatedMethod(Class<?> clazz, Class<? extends Annotation> annotation) {
        Method[] methods = clazz.getMethods();
        Method annotatedMethod = null;
        if (methods != null) {
            for (Method method : methods) {
                if (AnnotationUtils.findAnnotation(method, annotation) != null) {
                    annotatedMethod = method;
                    break;
                }
            }
        }

        if (annotatedMethod != null && annotatedMethod.getParameterCount() > 0) {
            throw new IllegalArgumentException("Annoated method for " +
                    annotation + " is expected to have zero arguments");
        }

        return annotatedMethod;
    }


    private String handleResult(Object result) {
        return result != null ? String.valueOf(result) : null;
    }
}
