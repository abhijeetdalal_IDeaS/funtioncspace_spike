package com.ideas.ngi.nucleus.integration.util;

import com.google.common.collect.ImmutableSet;
import com.ideas.ngi.nucleus.monitor.config.ErrorMessageConfig;
import com.ideas.ngi.nucleus.monitor.entity.NucleusMessageHistoryEntry;
import com.ideas.ngi.nucleus.monitor.service.ErrorMessageService;
import com.ideas.ngi.nucleus.rest.endpoint.RestEndpoint;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.history.MessageHistory;
import org.springframework.messaging.MessageHeaders;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Utility class for accessing pieces of the MessageHeaders.
 */
public class MessageHeaderUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageHeaderUtil.class);
    public static final String JOB_NAME = "jobName";
    public static final String CHANNEL_TYPE = "channel";
    public static final String RETURN_CHANNEL = "returnChannel";
    public static final String DESTINATION_CHANNEL = "destinationChannel";
    public static final String FROM_CHANNEL = "fromChannel";
    public static final String REST_ENDPOINT = "restEndpoint";
    public static final String REST_CLIENT = "restClient";
    public static final String CORRELATION_METADATA = "correlationMetadata";
    public static final String ITERABLE_LAST_MESSAGE = "iterableLastMessage";

    private static final Set<String> IGNORE_HEADERS = ImmutableSet.of(
            MessageHistory.HEADER_NAME,
            MessageHeaders.TIMESTAMP,
            MessageHeaders.ID);

    private MessageHeaderUtil() {
    }

    /**
     * Returns the JobName from the Message headers
     */
    public static String getJobName(MessageHeaders messageHeaders) {
        return (String) messageHeaders.get(JOB_NAME);
    }

    /**
     * Uses the MessageHistory from the MessageHeaders to create a list of
     * MessageHistoryEntry
     */
    public static List<NucleusMessageHistoryEntry> getMessageHistoryEntries(MessageHeaders messageHeaders) {
        MessageHistory messageHistory = messageHeaders.get(MessageHistory.HEADER_NAME, MessageHistory.class);
        if (messageHistory == null) {
            LOGGER.warn("MessageHistory is not enabled, cannot determine the last channel");
            return Collections.emptyList();
        }

        // Loop over the messageHistory entries to create MessageHistoryEntry
        // objects
        // Keep a reference to the previous MessageHistoryEntry so that you can
        // tie the end date of the previous MessageHistoryEntry to the start
        // date of the current ErrorMessageHistory
        List<NucleusMessageHistoryEntry> messageHistoryEntries = new ArrayList<>();
        NucleusMessageHistoryEntry previousMessageHistoryEntry = null;
        for (Object entry : messageHistory.toArray()) {
            // Doing a toArray() to make testing easier, so you can mock
            // MessageHistory
            Properties messagesMessageHistoryEntry = (Properties) entry;

            // If the message history contains 'channel#', igonre it from the list as these just become cumbersome
            if (StringUtils.contains(messagesMessageHistoryEntry.getProperty(MessageHistory.NAME_PROPERTY), "#")) {
                continue;
            }

            // Build the MessageHistoryEntry object
            NucleusMessageHistoryEntry messageHistoryEntry = new NucleusMessageHistoryEntry();
            messageHistoryEntry.setName(messagesMessageHistoryEntry.getProperty(MessageHistory.NAME_PROPERTY));
            messageHistoryEntry.setType(messagesMessageHistoryEntry.getProperty(MessageHistory.TYPE_PROPERTY));

            // Set the 'timestamp' as the start date as it is done before the
            // pre-send phase of the send
            messageHistoryEntry.setStartDate(new Date(Long.parseLong(messagesMessageHistoryEntry.getProperty(MessageHistory.TIMESTAMP_PROPERTY))));

            // If we have a previous ErrorMessageHistory, set it's end date as
            // equal to the start date of the new history record
            if (previousMessageHistoryEntry != null) {
                previousMessageHistoryEntry.setEndDate(messageHistoryEntry.getStartDate());
            }

            // add it to the list of ErrorMessageHistory
            messageHistoryEntries.add(messageHistoryEntry);

            // Need to keep track of the last ErrorMessageHistory so the endDate
            // can be set
            previousMessageHistoryEntry = messageHistoryEntry;
        }

        // The last ErrorMessageHistory won't have an endDate set, therefore
        // just use the current date/time
        if (previousMessageHistoryEntry != null) {
            previousMessageHistoryEntry.setEndDate(new Date());
        }

        return messageHistoryEntries;
    }

    public static String findLastChannel(MessageHeaders messageHeaders) {
        MessageHistory messageHistory = messageHeaders.get(MessageHistory.HEADER_NAME, MessageHistory.class);
        if (messageHistory == null) {
            LOGGER.warn("MessageHistory is not enabled, cannot determine the last channel");
            return null;
        }

        String lastChannel = null;
        for (Object entry : messageHistory.toArray()) {
            Properties messagesMessageHistoryEntry = (Properties) entry;
            if (CHANNEL_TYPE.equals(messagesMessageHistoryEntry.getProperty(MessageHistory.TYPE_PROPERTY))) {
                lastChannel = messagesMessageHistoryEntry.getProperty(MessageHistory.NAME_PROPERTY);
            }
        }

        // If we are already in a retry, don't use reference to error message retry channel,
        // Use the reference to the error retry channel from the headers.
        if (ErrorMessageConfig.ERROR_MESSAGE_RETRY_CHANNEL.equals(lastChannel) && messageHeaders.containsKey(ErrorMessageService.ERROR_RETRY_CHANNEL)) {
            lastChannel = (String) messageHeaders.get(ErrorMessageService.ERROR_RETRY_CHANNEL);
        }

        return lastChannel;
    }

    public static String findErrorChannel(MessageHeaders messageHeaders) {
        if (messageHeaders == null) {
            LOGGER.warn("MessageHistory is not enabled, cannot determine the error channel");
            return null;
        }
        return (String) messageHeaders.get(MessageHeaders.ERROR_CHANNEL);
    }

    public static RestEndpoint findRestEndpoint(MessageHeaders messageHeaders) {
        if (messageHeaders == null || messageHeaders.get(REST_ENDPOINT) == null) {
            LOGGER.warn("No REST endpoint configured, cannot determine the REST endpoint");
            return null;
        }
        return (RestEndpoint) messageHeaders.get(REST_ENDPOINT);
    }

    public static List<Pair<String, Object>> copyHeaders(MessageHeaders messageHeaders) {
        return messageHeaders.entrySet().stream()
                .filter(e -> !IGNORE_HEADERS.contains(e.getKey()))
                .filter(e -> e.getValue() instanceof Serializable)
                .map(e -> new ImmutablePair<>(e.getKey(), e.getValue()))
                .collect(Collectors.toList());
    }

    public static Map<String, Object> copyHeaders(List<Pair<String, Object>> messageHeaders) {
        if (messageHeaders == null) {
            return new HashMap<>();
        } else {
            return messageHeaders.stream().collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
        }
    }
}
