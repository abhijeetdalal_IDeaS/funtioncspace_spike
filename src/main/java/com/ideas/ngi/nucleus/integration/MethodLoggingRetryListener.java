package com.ideas.ngi.nucleus.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.listener.RetryListenerSupport;

public class MethodLoggingRetryListener extends RetryListenerSupport {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodLoggingRetryListener.class);

    @Override
    public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering retry on " + context.getAttribute(RetryContext.NAME) + ", try #" + context.getRetryCount(), throwable);
        }
    }
}
