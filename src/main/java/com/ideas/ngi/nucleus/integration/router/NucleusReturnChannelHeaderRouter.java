package com.ideas.ngi.nucleus.integration.router;

import com.ideas.ngi.nucleus.integration.util.MessageHeaderUtil;
import org.springframework.integration.router.HeaderValueRouter;
import org.springframework.stereotype.Component;

@Component
public class NucleusReturnChannelHeaderRouter extends HeaderValueRouter {

    public NucleusReturnChannelHeaderRouter() {
        super(MessageHeaderUtil.RETURN_CHANNEL);
    }
}
