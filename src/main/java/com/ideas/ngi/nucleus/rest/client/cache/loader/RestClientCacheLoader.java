package com.ideas.ngi.nucleus.rest.client.cache.loader;

import com.google.common.cache.CacheLoader;
import com.ideas.ngi.nucleus.config.internalclient.InternalClientProperties;
import com.ideas.ngi.nucleus.config.internalclient.InternalClientType;
import com.ideas.ngi.nucleus.rest.client.RestClient;
import com.ideas.ngi.nucleus.rest.client.RestClientProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import static com.ideas.ngi.nucleus.config.internalclient.InternalClientUtils.*;

@Component
public class RestClientCacheLoader extends CacheLoader<String, RestClient> {
    private Environment environment;

    @Autowired
    public RestClientCacheLoader(Environment environment) {
        this.environment = environment;
    }

    @Override
    public RestClient load(String url) {
        RestClientProperties restClientProperties = new InternalClientProperties(url, InternalClientType.G2_CASPER, false)
                .setBaseUrl(url)
                .setBasicAuthenticationToken(getValue(InternalClientType.G2_CASPER.name(), AUTHENTICATION_TOKEN, environment))
                .setDefaultPropertyId(getValue(InternalClientType.G2_CASPER.toString(), DEFAULT_PROPERTY_ID, environment));
        return new RestClient(restClientProperties);
    }
}
