package com.ideas.ngi.nucleus.rest.client;

import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriTemplateHandler;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public interface RestClientProperties {
    
    String getName();
    
    String getBaseUrl();

    RestClientProperties setBaseUrl(String baseUrl);
    
    default String getHealthUrl() {
        return null;
    }

    default List<ClientHttpRequestInterceptor> getInterceptors() {
        return Collections.emptyList();
    }

    default RestTemplate createRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setInterceptors(getInterceptors());
        restTemplate.setUriTemplateHandler(createUriHandler());

        return restTemplate;
    }

    default UriTemplateHandler createUriHandler() {
        DefaultUriBuilderFactory builder = new DefaultUriBuilderFactory();
        builder.setEncodingMode(DefaultUriBuilderFactory.EncodingMode.TEMPLATE_AND_VALUES);
        return builder;
    }

    default RestTemplate createRestTemplate(Map<String, Object> params) {
        return createRestTemplate();
    }
}
