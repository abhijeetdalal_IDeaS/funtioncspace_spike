package com.ideas.ngi.nucleus.rest.hateoas;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.webmvc.support.BaseUriLinkBuilder;
import org.springframework.hateoas.core.LinkBuilderSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class ProxyLinkBuilderTransformer {

    private String ngiLoadBalancerUrl;

    @Autowired
    public ProxyLinkBuilderTransformer(@Value("${ngi.loadBalancerUrl:http://localhost:9090}") String ngiLoadBalancerUrl) {
        if (StringUtils.isNotBlank(ngiLoadBalancerUrl) && ngiLoadBalancerUrl.endsWith("/")) {
            this.ngiLoadBalancerUrl = ngiLoadBalancerUrl.substring(0, ngiLoadBalancerUrl.length() - 1);
        } else {
            this.ngiLoadBalancerUrl = ngiLoadBalancerUrl;
        }
    }

    public LinkBuilderSupport<BaseUriLinkBuilder> transform(LinkBuilderSupport<?> linkBuilder) {
        String completeUri = linkBuilder.toString();
        // want just the full path without scheme + hostname
        String completePath = completeUri.substring(completeUri.indexOf(linkBuilder.toUri().getPath()));
        if (! completePath.startsWith("/")) {
            completePath = "/" + completePath;
        }
        if (StringUtils.isNotBlank(ngiLoadBalancerUrl)) {
            return new BaseUriLinkBuilder(UriComponentsBuilder.fromHttpUrl(ngiLoadBalancerUrl + completePath));
        } else {
            return new BaseUriLinkBuilder(UriComponentsBuilder.fromUri(linkBuilder.toUri()));
        }
    }

}
