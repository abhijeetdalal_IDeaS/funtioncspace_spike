package com.ideas.ngi.nucleus.rest.client.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.ideas.ngi.nucleus.rest.client.RestClient;
import com.ideas.ngi.nucleus.rest.client.cache.loader.RestClientCacheLoader;
import com.ideas.ngi.nucleus.rest.client.cache.loader.UrlCacheLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class RestClientCaches {
    public static final String NOT_FOUND = "NOT_FOUND";

    private static Cache<ClientPropertyKey, String> urlCache;
    private static Cache<String, RestClient> restClientCache;

    @Autowired
    public RestClientCaches(UrlCacheLoader urlCacheLoader, RestClientCacheLoader restClientCacheLoader) {
        this.urlCache = CacheBuilder
                .newBuilder()
                .refreshAfterWrite(60, TimeUnit.MINUTES)
                .build(urlCacheLoader);

        this.restClientCache = CacheBuilder
                .newBuilder()
                .refreshAfterWrite(60, TimeUnit.MINUTES)
                .build(restClientCacheLoader);
    }

    public static RestClient getRestClient(String clientCode, String propertyCode) {
        ClientPropertyKey clientPropertyKey = new ClientPropertyKey();
        clientPropertyKey.setClientCode(clientCode);
        clientPropertyKey.setPropertyCode(propertyCode);

        String host = urlCache != null ? urlCache.getIfPresent(clientPropertyKey) : NOT_FOUND;
        return NOT_FOUND.equals(host) || restClientCache == null || host == null ?
                null : restClientCache.getIfPresent(host);
    }
}
