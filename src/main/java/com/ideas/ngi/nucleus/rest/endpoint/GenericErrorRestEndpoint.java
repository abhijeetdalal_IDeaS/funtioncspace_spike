package com.ideas.ngi.nucleus.rest.endpoint;


import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.http.HttpMethod;

import java.io.Serializable;

public class GenericErrorRestEndpoint implements RestEndpoint, Serializable{
    private static final Logger logger = LoggerFactory.getLogger(GenericErrorRestEndpoint.class);

    private static final long serialVersionUID = 7763636731295048894L;
    private final String httpMethod;
    private final String url;
    private final String responseType;

    @PersistenceConstructor
    public GenericErrorRestEndpoint(String httpMethod, String url, String responseType) {
        this.httpMethod = httpMethod;
        this.url = url;
        this.responseType = responseType;
    }

    public static GenericErrorRestEndpoint copyOf(RestEndpoint endpoint) {
        return new GenericErrorRestEndpoint(endpoint.getHttpMethod().name(),
                endpoint.getURL(), endpoint.getResponseType().getName());
    }

    @Override
    public HttpMethod getHttpMethod() {
        return HttpMethod.valueOf(httpMethod);
    }

    @Override
    public String getURL() {
        return url;
    }

    @Override
    public Class<?> getResponseType() {
        try {
            return Class.forName(responseType);
        } catch (ClassNotFoundException e) {
            logger.info("Unable to find the class by name for : " +responseType, e);
            return null;
        }
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
