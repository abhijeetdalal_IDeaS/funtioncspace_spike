package com.ideas.ngi.nucleus.rest.endpoint;

import org.springframework.http.HttpMethod;

import java.io.Serializable;

/**
 * Interface for the enum to define REST endpoints
 */
public interface RestEndpoint extends Serializable {
    
    String NGI_PUBLIC_REST_ROOT_CONTEXT = "/ngipublic/rest";
    
    HttpMethod getHttpMethod();

    String getURL();
    
    Class<?> getResponseType();
}
