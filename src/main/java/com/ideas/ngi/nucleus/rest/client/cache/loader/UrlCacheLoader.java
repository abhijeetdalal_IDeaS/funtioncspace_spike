package com.ideas.ngi.nucleus.rest.client.cache.loader;

import com.google.common.cache.CacheLoader;
import com.ideas.ngi.nucleus.config.internalclient.ups.UnifiedPropertyClient;
import com.ideas.ngi.nucleus.rest.client.cache.ClientPropertyKey;
import com.ideas.ngi.nucleus.rest.client.cache.RestClientCaches;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UrlCacheLoader extends CacheLoader<ClientPropertyKey, String> {
    private UnifiedPropertyClient unifiedPropertyClient;


    @Autowired
    public UrlCacheLoader(UnifiedPropertyClient unifiedPropertyClient) {
        this.unifiedPropertyClient = unifiedPropertyClient;
    }

    @Override
    public String load(ClientPropertyKey clientPropertyKey) {
        String host = unifiedPropertyClient.getHost(
                clientPropertyKey.getClientCode(),
                clientPropertyKey.getPropertyCode());
        return host == null || host.isEmpty() ? RestClientCaches.NOT_FOUND : host;
    }
}
