package com.ideas.ngi.nucleus.rest.client;

import com.google.common.annotations.VisibleForTesting;
import com.ideas.ngi.nucleus.config.internalclient.InternalClientProperties;
import com.ideas.ngi.nucleus.exception.NucleusException;
import com.ideas.ngi.nucleus.exception.NucleusRetryableException;
import com.ideas.ngi.nucleus.rest.client.stateful.BasicAuthenticationRestTemplate;
import com.ideas.ngi.nucleus.rest.endpoint.RestEndpoint;
import com.ideas.ngi.nucleus.rest.util.RestEndpointParameterUtil;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

public class RestClient implements BeanNameAware {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    static final String TARGET_APPEARS_TO_BE_DOWN = "REST connection failed; target appears to be down.";
    static final String CONNECTION_FAILED_404_NOT_FOUND = "REST connection failed; 404 Not Found";
    static final String TARGET_THREW_AN_ERROR = "Target server threw an error";

    private String beanName;

    @VisibleForTesting
    public RestClientProperties getRestClientProperties() {
        return restClientProperties;
    }

    private RestClientProperties restClientProperties;
    private RestTemplate template;

    public RestClient(RestClientProperties restClientProperties) {
        this.restClientProperties = restClientProperties;
    }

    public Object execute(RestEndpoint restEndpoint, Map<String, Object> parameters, Object payload) {
        // Call the appropriate HttpMethod
        Object response;
        try {
            HttpMethod httpMethod = restEndpoint.getHttpMethod();
            if (HttpMethod.GET.equals(httpMethod)) {
                response = get(restEndpoint, parameters);
            } else if (HttpMethod.POST.equals(httpMethod)) {
                response = post(restEndpoint, payload, parameters);
            } else if (HttpMethod.PUT.equals(httpMethod)) {
                put(restEndpoint, payload, parameters);
                response = payload;
            } else if (HttpMethod.DELETE.equals(httpMethod)) {
                delete(restEndpoint, parameters);
                response = payload;
            } else {
                throw new IllegalArgumentException("HttpMethod: " + httpMethod
                        + " is currently not supported in the RestHandler");
            }
        } catch (ResourceAccessException rae) {
            throw new NucleusRetryableException(TARGET_APPEARS_TO_BE_DOWN, rae);
        } catch (HttpClientErrorException httpClientErrorException) {
            if (httpClientErrorException.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new NucleusRetryableException(CONNECTION_FAILED_404_NOT_FOUND, httpClientErrorException);
            }
            if (httpClientErrorException.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
                throw new NucleusException(TARGET_THREW_AN_ERROR, httpClientErrorException);
            }

            throw httpClientErrorException;
        }

        return response;
    }

    /**
     * Executes a GET command on a given RestEndpoint
     */
    public <T> T get(RestEndpoint restEndpoint) {
        return get(restEndpoint, null);
    }

    /**
     * Executes a GET command on a given RestEndpoint for the request object, and parameters
     */
    @SuppressWarnings("unchecked")
    public <T> T get(RestEndpoint restEndpoint, HttpEntity<?> requestEntity, Map<String, Object> parameters) {
        ResponseEntity responseEntity;

        checkForValidRestEndpoint(restEndpoint);
        String url = buildURL(restEndpoint);

        logger.debug("GET - Endpoint: {} for url: {} with parameters: {} with Headers: {}",
                restEndpoint, url, parameters, requestEntity.getHeaders());
        if (MapUtils.isEmpty(parameters)) {
            responseEntity = getRestTemplate().exchange(url, HttpMethod.GET, requestEntity, restEndpoint.getResponseType());
        } else {
            responseEntity = getRestTemplate().exchange(url, HttpMethod.GET, requestEntity, restEndpoint.getResponseType(), parameters);
        }

        return (T) responseEntity.getBody();
    }

    /**
     * Executes a GET command on a given RestEndpoint, the parameters are used
     * to overlay variables defined in the URL
     */
    @SuppressWarnings("unchecked")
    public <T> T get(RestEndpoint restEndpoint, Map<String, Object> parameters) {
        checkForValidRestEndpoint(restEndpoint);

        String url = buildURL(restEndpoint);
        Map<String, Object> buildParameters = buildParameters(restEndpoint, parameters);
        logger.debug("GET - Endpoint: {} for url: {} with parameters: {}", restEndpoint, url, buildParameters);
        return (T) getRestTemplate().getForObject(url, restEndpoint.getResponseType(), buildParameters);
    }

    public <T> T getForSSLTest(RestEndpoint restEndpoint, Map<String, Object> parameters) {
        checkForValidRestEndpoint(restEndpoint);

        String url = buildURL(restEndpoint);
        Map<String, Object> buildParameters = buildParameters(restEndpoint, parameters);
        logger.debug("GET - Endpoint: {} for url: {} with parameters: {}", restEndpoint, url, buildParameters);
        return (T) (restClientProperties.createRestTemplate(parameters)).getForObject(url, restEndpoint.getResponseType(), buildParameters);
    }

    public RestTemplate getRestTemplate() {
        if (template == null) {
            template = restClientProperties.createRestTemplate();
        }
        return template;
    }

    /**
     * Executes a POST command on a given RestEndpoint for the request object
     */
    public <T> T post(RestEndpoint restEndpoint, Object request) {
        return post(restEndpoint, request, null);
    }

    /**
     * Executes a POST command on a given RestEndpoint, the parameters are used
     * to overlay variables defined in the URL
     */
    public <T> T post(RestEndpoint restEndpoint, Object request, Map<String, Object> parameters) {
        checkForValidRestEndpoint(restEndpoint);

        String url = buildURL(restEndpoint);
        logger.debug("POST - Endpoint: {} for url {} with Request {} and Parameters {}", restEndpoint, url, request, parameters);
        return (T) getRestTemplate().postForObject(url, request, restEndpoint.getResponseType(), buildParameters(restEndpoint, parameters));
    }

    /**
     * Executes a PUT command on a given RestEndpoint for the request object
     */
    public void put(RestEndpoint restEndpoint, Object request) {
        put(restEndpoint, request, null);
    }

    /**
     * Executes a PUT command on a given RestEndpoint, the parameters are used
     * to overlay variables defined in the URL
     */
    public void put(RestEndpoint restEndpoint, Object request, Map<String, Object> parameters) {
        checkForValidRestEndpoint(restEndpoint);
        String url = buildURL(restEndpoint);
        logger.debug("PUT - Endpoint: {} for url {} with Request: {} and Parameters: {}", restEndpoint, url, restEndpoint, parameters);
        getRestTemplate().put(url, request, buildParameters(restEndpoint, parameters));
    }

    /**
     * Executes a DELETE command on a given RestEndpoint
     */
    public void delete(RestEndpoint restEndpoint) {
        delete(restEndpoint, null);
    }

    /**
     * Executes a DELETE command on a given RestEndpoint, the parameters are
     * used to overlay variables defined in the URL
     */
    public void delete(RestEndpoint restEndpoint, Map<String, Object> parameters) {
        checkForValidRestEndpoint(restEndpoint);
        String url = buildURL(restEndpoint);
        logger.debug("DELETE - Endpoint: {} for url {} with Parameters: {}", restEndpoint, url, parameters);
        getRestTemplate().delete(url, buildParameters(restEndpoint, parameters));
    }

    /**
     * Checks to see if the RestEndpoint is valid
     */
    private void checkForValidRestEndpoint(RestEndpoint restEndpoint) {
        if (restEndpoint == null) {
            throw new IllegalArgumentException("Must specify a non-null endpoint");
        }
    }

    /**
     * Builds a Map of Parameters for the given RestEndpoint
     */
    private Map<String, Object> buildParameters(RestEndpoint restEndpoint, Map<String, Object> parameters) {
        return RestEndpointParameterUtil.applyValuesFromParameterExpressions(restEndpoint, parameters,
                restClientProperties);
    }

    /**
     * Returns the URL to be executed. It utilizes the RestClientProperties for
     * the base of the URL.
     */
    private String buildURL(RestEndpoint restEndpoint) {
        return restClientProperties.getBaseUrl() + restEndpoint.getURL();
    }

    /**
     * Returns a boolean indicating whether or not a particular RestClient is
     * for an InternalClient
     */
    public boolean isInternalClient() {
        return restClientProperties instanceof InternalClientProperties;
    }

    public boolean isMonitor() {
        return isInternalClient() && ((InternalClientProperties) restClientProperties).isMonitor();
    }

    public void setStatefulRestTemplate(BasicAuthenticationRestTemplate template) {
        this.template = template;
    }

    /**
     * Return the name
     */
    public String getName() {
        if (restClientProperties != null) {
            return restClientProperties.getName();
        }

        return beanName;
    }

    public String getBaseUrl() {
        if (restClientProperties != null) {
            return restClientProperties.getBaseUrl();
        }

        return null;
    }

    public String getHealthUrl() {
        if (restClientProperties != null) {
            return restClientProperties.getHealthUrl();
        }

        return null;
    }

    public String getBeanName() {
        return beanName;
    }

    @Override
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

}