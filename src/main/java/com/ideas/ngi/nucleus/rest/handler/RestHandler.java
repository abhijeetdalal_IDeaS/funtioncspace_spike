package com.ideas.ngi.nucleus.rest.handler;

import com.ideas.ngi.nucleus.rest.client.RestClient;
import com.ideas.ngi.nucleus.rest.endpoint.RestEndpoint;
import com.ideas.ngi.nucleus.rest.util.RestEndpointParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.*;

/**
 * The RestHandler can be used to call a RestEndpoint on a RestClient. It also
 * provides the ability to set the REST response onto the payload and return the
 * payload if that functionality is required.
 */
@Component
public class RestHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestHandler.class);

    public static final String PAGE = "page";
    public static final String SIZE = "size";

    public <T> T handle(RestClient restClient, RestEndpoint restEndpoint, Object payload) {
        return handle(restClient, restEndpoint, null, payload);
    }

    @SuppressWarnings("unchecked")
    public <T> T handle(RestClient restClient, RestEndpoint restEndpoint, Map<String, Object> parameters, Object payload) {
        Assert.notNull(restClient, "RestClient must not be null");
        Assert.notNull(restEndpoint, "RestEndpoint must not be null");

        LOGGER.info("Calling {} on {} (base url: {}), with payload: {}", restEndpoint, restClient.getBeanName(), restClient.getBaseUrl(), payload);

        // Build a Map of parameters
        Map<String, Object> allParameters = new HashMap<>();
        if (parameters != null) {
            allParameters.putAll(parameters);
        }

        allParameters = RestEndpointParameterUtil.applyValuesFromParameterExpressions(restEndpoint, allParameters, payload);

        // Call the appropriate HttpMethod
        return (T) restClient.execute(restEndpoint, allParameters, payload);
    }

    public <T> Page<T> handle(RestClient restClient, RestEndpoint restEndpoint, Object payload, Pageable pageable) {
        return handle(restClient, restEndpoint, new HashMap<>(), payload, pageable);
    }

    @SuppressWarnings("unchecked")
    public <T> Page<T> handle(RestClient restClient, RestEndpoint restEndpoint, Map<String, Object> parameters, Object payload, Pageable pageable) {
        // Update the page/size parameters so they are reflected on the URL
        parameters.put(PAGE, pageable.getPageNumber());
        parameters.put(SIZE, pageable.getPageSize());

        // Make the REST call
        Object[] response = handle(restClient, restEndpoint, parameters, payload);
        if (response == null) {
            return new PageImpl<>(new ArrayList<>());
        }

        // The totalSize indicates whether or not we need to check for another page
        // Since we don't know the total number, we are going to say it's the
        // (pageNumber + 1) * pageSize + 1 to force another page to be loaded
        int total = response.length == pageable.getPageSize() ? (pageable.getPageNumber() + 1) * pageable.getPageSize() + 1 : response.length;

        // Return the PageImpl
        return new PageImpl<>((List<T>) Arrays.asList(response), pageable, total);
    }
}
