package com.ideas.ngi.nucleus.rest.util;

import com.ideas.ngi.nucleus.integration.annotation.PropertyId;
import com.ideas.ngi.nucleus.rest.endpoint.RestEndpoint;
import com.ideas.ngi.nucleus.util.DateTimeUtil;
import com.ideas.ngi.nucleus.util.ReflectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class RestEndpointParameterUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestEndpointParameterUtil.class);

    private static final String PROPERTY_ID = "propertyId";

    private RestEndpointParameterUtil() {
    }

    public static Map<String, Object> applyValuesFromParameterExpressions(RestEndpoint restEndpoint, Map<String, Object> parameters, Object payload) {
        // Create a parameters map if it is null
        if (parameters == null) {
            parameters = new HashMap<>();
        }

        // Build a UriComponents class to be able to extract the query params
        UriComponents uriComponents = UriComponentsBuilder.fromUriString(restEndpoint.getURL()).build();

        // Loop over the query params in order to add any parameters that were
        // not initially passed in
        MultiValueMap<String, String> queryParams = uriComponents.getQueryParams();
        for (String queryParam : queryParams.keySet()) {

            // Determine the expression to try to find (remember it looks like:
            // {expression}) and we just want the 'expression' piece.
            String expression = queryParams.getFirst(queryParam);
            if (expression.startsWith("{")) {
                expression = expression.substring(1, expression.length() - 1);

                // Determine if there is a specific date format that should be applied to the string
                String dateFormat = null;
                if (expression.contains(":")) {
                    dateFormat = expression.substring(expression.indexOf(':') + 1, expression.length());
                    expression = expression.substring(0, expression.indexOf(':'));
                }

                // Evaluate the expression and set the parameter if it has a value
                if (parameters.get(expression) == null) {
                    ParameterValue valueResult = getValueFromPayload(payload, expression, dateFormat);
                    if (valueResult.exists) {
                        parameters.put(expression, valueResult.value);
                    }
                }
            }
        }

        return parameters;
    }

    private static ParameterValue getValueFromPayload(Object payload, String expression, String dateFormat) {
        ParameterValue valueResult = getValue(payload, expression);

        // If the value is not null, set the expression/value in the parameters
        if (valueResult.value != null) {

            // If the value is a Date and there is a dateFormat, need to apply the format to the parameter
            if (valueResult.value instanceof Date && dateFormat != null) {
                valueResult.value = DateTimeUtil.formatDate((Date) valueResult.value, dateFormat);
            }
        } else if (expression.equals(PROPERTY_ID)) {
            valueResult = ParameterValue.ofNullable(ReflectionUtil.getValueWithAnnotation(payload, PropertyId.class));
        }

        return valueResult;
    }

    /**
     * Using SpEL to attempt to extract the expression from an Object.
     */
    private static ParameterValue getValue(Object obj, String expression) {
        // If the object being evaluated is a Collection, don't use the Collection itself
        // Look at the first Object in the Collection
        Object inspectionObject = obj;
        if (inspectionObject instanceof Collection && !CollectionUtils.isEmpty((Collection<?>) inspectionObject)) {
            inspectionObject = ((Collection<?>) inspectionObject).toArray()[0];
        }

        try {
            Object value = new SpelExpressionParser().parseExpression(expression).getValue(inspectionObject);
            return ParameterValue.of(value);
        } catch (SpelEvaluationException see) {
            LOGGER.trace("Unable to getValue() using expression: " + expression + " in " + obj, see);
            return ParameterValue.notFound();
        }
    }

    static class ParameterValue {
        Object value;
        boolean exists;

        static ParameterValue of(Object value) {
            return new ParameterValue(value, true);
        }

        static ParameterValue notFound() {
            return new ParameterValue(null, false);
        }

        static ParameterValue ofNullable(Object value) {
            return value == null ? notFound() : of(value);
        }

        private ParameterValue(Object value, boolean exists) {
            this.value = value;
            this.exists = exists;
        }
    }

}
