package com.ideas.ngi.nucleus.rest.client.stateful;

import com.ideas.ngi.nucleus.rest.client.RestClientProperties;
import org.springframework.web.client.RestTemplate;

public interface BasicAuthenticationRestClientProperties extends RestClientProperties {

    String getBasicAuthenticationToken();

    BasicAuthenticationRestClientProperties setBasicAuthenticationToken(String basicAuthenticationToken);

    @Override
    default RestTemplate createRestTemplate() {
        RestTemplate restTemplate = new BasicAuthenticationRestTemplate(this);
        restTemplate.setInterceptors(getInterceptors());
        restTemplate.setUriTemplateHandler(createUriHandler());
        return restTemplate;
    }
}
