package com.ideas.ngi.nucleus.rest.client.cache;

import lombok.Data;

@Data
public class ClientPropertyKey {
    private String clientCode;
    private String propertyCode;
}
