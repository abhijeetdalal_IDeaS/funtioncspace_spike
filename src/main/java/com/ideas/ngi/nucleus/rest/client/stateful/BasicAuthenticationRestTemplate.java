package com.ideas.ngi.nucleus.rest.client.stateful;

import com.ideas.ngi.nucleus.util.DateTimeUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.util.Assert;
import org.springframework.web.client.*;
import org.springframework.web.util.UriTemplate;

import javax.xml.transform.Source;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

public class BasicAuthenticationRestTemplate extends RestTemplate {
    private final CookieStore cookieStore;

    //For assertions in test case
    protected CookieStore getCookieStore() {
        return cookieStore;
    }

    public BasicAuthenticationRestTemplate(BasicAuthenticationRestClientProperties restClientProperties) {
        super();
        cookieStore = new BasicCookieStore();

        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create().setDefaultCookieStore(cookieStore).useSystemProperties();

        if (restClientProperties != null) {
            String basicAuthenticationToken = restClientProperties.getBasicAuthenticationToken();
            if (StringUtils.isNotEmpty(basicAuthenticationToken)) {
                httpClientBuilder = httpClientBuilder.setDefaultHeaders(Arrays.asList(new BasicHeader("Authorization", basicAuthenticationToken)));
            }
        }

        CloseableHttpClient httpClient = httpClientBuilder.build();

        setMessageConverters(getConverters());

        HttpComponentsClientHttpRequestFactory requestFactory;
        requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
        setRequestFactory(requestFactory);
    }

    public <T> T execute(String url, HttpMethod method, Map<String, ?> urlVariables) throws RestClientException {
        URI expanded = new UriTemplate(url).expand(urlVariables);
        return doExecute(expanded, method, null, null, null);
    }

    public <T> T post(String url, HttpMethod method, String requestBody,
                      Map<String, ?> urlVariables) throws RestClientException {
        URI expanded = new UriTemplate(url).expand(urlVariables);
        return doExecute(expanded, method, null, null, requestBody);
    }

    protected <T> T doExecute(URI url, HttpMethod method, RequestCallback requestCallback,
                              ResponseExtractor<T> responseExtractor, String requestBody) throws RestClientException {

        Assert.notNull(url, "'url' must not be null");
        Assert.notNull(method, "'method' must not be null");
        ClientHttpResponse response = null;
        try {
            ClientHttpRequest request = createRequest(url, method);
            if (request.getMethod() == HttpMethod.POST) {

                try (OutputStream outputStream = request.getBody();
                     OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream)) {
                    outputStreamWriter.write(requestBody);
                    outputStreamWriter.flush();
                    // hardcoding content type for now
                    request.getHeaders().set("Content-type", "text/plain");
                }
            }

            addHeaders(request);

            if (requestCallback != null) {
                requestCallback.doWithRequest(request);
            }


            response = request.execute();
            handleResponse(url, method, response);
            if (responseExtractor != null) {
                return responseExtractor.extractData(response);
            } else {
                return null;
            }
        } catch (IOException ex) {
            throw new ResourceAccessException("I/O error on " + method.name() +
                    " request for \"" + url + "\": " + ex.getMessage(), ex);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    private void addHeaders(ClientHttpRequest request) {
        if (cookieStore.getCookies().size() > 0) {
            request.getHeaders().add(HttpHeaders.COOKIE, cookieStore.getCookies().stream().map(cookie -> cookie.getName() + "=" + cookie.getValue()).collect(Collectors.joining(";")));
        }

    }

    /**
     * There is a conflict with the MappingJackson2HttpMessageConverter and
     * the Jaxb2RootElementHttpMessageConverter, which is on the classpath for XML stuff. Unfortunately
     * when trying to sen data to G3 if this is present in the converter list it is preferred, so I'm
     * forcing it out as we don't send XML via restTemplate.  At least thus far
     *
     * @return
     */
    private List<HttpMessageConverter<?>> getConverters() {
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new ByteArrayHttpMessageConverter());
        messageConverters.add(new StringHttpMessageConverter());
        messageConverters.add(new ResourceHttpMessageConverter());
        messageConverters.add(new SourceHttpMessageConverter<Source>());
        messageConverters.add(new AllEncompassingFormHttpMessageConverter());
        messageConverters.add(new MappingJackson2HttpMessageConverter(Jackson2ObjectMapperBuilder.json().failOnUnknownProperties(false).simpleDateFormat(DateTimeUtil.DATE_TIME_T_FORMAT).timeZone(TimeZone.getDefault()).build()));
        return messageConverters;
    }
}
