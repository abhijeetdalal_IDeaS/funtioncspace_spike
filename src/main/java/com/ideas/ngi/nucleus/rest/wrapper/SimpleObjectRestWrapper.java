package com.ideas.ngi.nucleus.rest.wrapper;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SimpleObjectRestWrapper<T> implements Serializable {

	private T data;

	public SimpleObjectRestWrapper(T data) {
		this.data = data;
	}

	public T getData() {
		return data;
	}

}
