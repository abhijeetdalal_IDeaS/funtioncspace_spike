package com.ideas.ngi.nucleus.exception;

/**
 * Wrapper Exception to flatten the original causing exception and guarantee Serializable
 */
public class FlatWrapperException extends RuntimeException {
    private final String causingExceptionClassName;

    public FlatWrapperException(Throwable cause) {
        super(cause.getMessage());
        this.causingExceptionClassName = cause.getClass().getTypeName();
    }

    public String getCausingExceptionClassName() {
        return this.causingExceptionClassName;
    }
}
