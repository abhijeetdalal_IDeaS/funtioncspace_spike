package com.ideas.ngi.nucleus.exception;

/**
 * Base exception class that gets thrown by Nucleus when an exception occurs, but it is understood that this could be
 * a potential issue where a target is down or for whatever reason retrying this activity may resolve the issue.
 */
public class NucleusRetryableException extends RuntimeException {

    private static final long serialVersionUID = -6478843730478521052L;

    public NucleusRetryableException(String message, Throwable cause) {
        super(message, cause);
    }

    public NucleusRetryableException(String message) {
        super(message);
    }
}
