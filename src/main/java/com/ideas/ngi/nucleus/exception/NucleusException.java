package com.ideas.ngi.nucleus.exception;

/**
 * Base exception class that gets thrown by Nucleus when an exception occurs.
 */
public class NucleusException extends RuntimeException {

    private static final long serialVersionUID = -2706843730474712057L;

    public NucleusException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public NucleusException(String message) {
        super(message);
    }
}
