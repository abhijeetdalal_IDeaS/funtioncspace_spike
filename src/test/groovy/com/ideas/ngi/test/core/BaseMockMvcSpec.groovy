package com.ideas.ngi.test.core

import com.ideas.ngi.nucleus.config.TestFunctionSpaceApplicationConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

@ContextConfiguration(classes = [TestFunctionSpaceApplicationConfig.class])
@TestPropertySource(locations = ['/application.properties', '/test-application.properties'])
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
abstract class BaseMockMvcSpec extends Specification {
    @Autowired
    protected MockMvc mvc

    def getResourceFile(filePath) {
        return new File(getClass().getResource(filePath).toURI())
    }
    def getResourceFileText(filePath) {
        return getResourceFile(filePath).getText('UTF-8')
    }
}