package com.ideas.ngi.functionspace.service

import com.ideas.ngi.common.test.categories.ComponentTest
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType
import com.ideas.ngi.nucleus.data.functionspace.entity.FunctionSpaceBookingCategory
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBooking
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceBookingRepository
import com.ideas.ngi.test.core.BaseMockMvcSpec
import org.junit.experimental.categories.Category
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.mongodb.core.query.Criteria
import spock.lang.Unroll

import java.time.LocalDateTime

@Category(ComponentTest.class)
class FunctionSpaceBookingServiceSpec extends BaseMockMvcSpec {
    @Autowired
    NucleusFunctionSpaceBookingRepository repository
    @Autowired
    FunctionSpaceBookingService service

    def cleanup() {
        Criteria criteria = Criteria.where("propertyId").regex("TESTPROPERTY");
        repository.findAllAndRemove(criteria);
    }

    @Unroll
    def "when finding fs booking by type for #integrationType|#propertyId|#bookingCategory|#startDate|#endDate expect #elementsExpected result"() {
        given:
        persist(new NucleusFunctionSpaceBooking(integrationType: IntegrationType.AHWS_SC,
                propertyId: 'TESTPROPERTY_1', marketSegmentCode: 'ms', bookingId: '2', status: 'allgood',
                functionSpaceBookingCategory: FunctionSpaceBookingCategory.BOOKING_WITH_EVENTS, arrivalDate: new Date(), departureDate: new Date()))

        when:
        Page result = service.findByIntegrationTypeAndNotCategoryAndDateRange(integrationType, propertyId, bookingCategory, startDate, endDate, new PageRequest(0, 10))

        then:
        elementsExpected == result.totalElements

        where:
        elementsExpected | integrationType         | propertyId       | bookingCategory                                  | startDate                         | endDate
        1L               | IntegrationType.AHWS_SC | 'TESTPROPERTY_1' | FunctionSpaceBookingCategory.BOOKING             | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
        0                | IntegrationType.MARS    | 'TESTPROPERTY_1' | FunctionSpaceBookingCategory.BOOKING             | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
        0                | IntegrationType.AHWS_SC | 'TESTPROPERTY_2' | FunctionSpaceBookingCategory.BOOKING             | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
        0                | IntegrationType.AHWS_SC | 'TESTPROPERTY_1' | FunctionSpaceBookingCategory.BOOKING             | LocalDateTime.now().minusHours(2) | LocalDateTime.now().minusHours(1)
        0                | IntegrationType.AHWS_SC | 'TESTPROPERTY_1' | FunctionSpaceBookingCategory.BOOKING_WITH_EVENTS | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
    }

    @Unroll
    def "when finding fs booking by code for #clientCode|#propertyCode|#bookingCategory|#startDate|#endDate expect #elementsExpected result"() {
        given:
        persist(new NucleusFunctionSpaceBooking(integrationType: IntegrationType.AHWS_SC, clientCode: 'CL', propertyCode: 'PR',
                propertyId: 'TESTPROPERTY_1', marketSegmentCode: 'ms', bookingId: '2', status: 'allgood',
                functionSpaceBookingCategory: FunctionSpaceBookingCategory.BOOKING_WITH_EVENTS, arrivalDate: new Date(), departureDate: new Date()))

        when:
        Page result = service.findByClientAndPropertyAndNotCategoryAndDateRange(clientCode, propertyCode, bookingCategory, startDate, endDate, new PageRequest(0, 10))

        then:
        elementsExpected == result.totalElements

        where:
        elementsExpected | clientCode | propertyCode | bookingCategory                                  | startDate                         | endDate
        1L               | 'CL'       | 'PR'         | FunctionSpaceBookingCategory.BOOKING             | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
        0                | 'CX'       | 'PR'         | FunctionSpaceBookingCategory.BOOKING             | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
        0                | 'CL'       | 'PX'         | FunctionSpaceBookingCategory.BOOKING             | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
        0                | 'CL'       | 'PR'         | FunctionSpaceBookingCategory.BOOKING             | LocalDateTime.now().minusHours(2) | LocalDateTime.now().minusHours(1)
        0                | 'CL'       | 'PR'         | FunctionSpaceBookingCategory.BOOKING_WITH_EVENTS | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
    }

    def persist(NucleusFunctionSpaceBooking data) {
        repository.save(data)
    }
}
