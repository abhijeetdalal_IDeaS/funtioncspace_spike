package com.ideas.ngi.functionspace.service

import com.ideas.ngi.common.test.categories.ComponentTest
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEventType
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceEventTypeRepository
import com.ideas.ngi.test.core.BaseMockMvcSpec
import org.junit.experimental.categories.Category
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.mongodb.core.query.Criteria
import spock.lang.Unroll

import java.time.LocalDateTime

@Category(ComponentTest.class)
class FunctionSpaceEventTypeServiceSpec extends BaseMockMvcSpec {
    @Autowired
    NucleusFunctionSpaceEventTypeRepository repository
    @Autowired
    FunctionSpaceEventTypeService service

    def cleanup() {
        Criteria criteria = Criteria.where("propertyId").regex("TESTPROPERTY");
        repository.findAllAndRemove(criteria);
    }

    @Unroll
    def "when finding fs event type by type for #integrationType|#propertyId|#startDate|#endDate expect #elementsExpected result"() {
        given:
        persist(new NucleusFunctionSpaceEventType(integrationType: IntegrationType.AHWS_SC, propertyId: 'TESTPROPERTY_1', abbreviation: 'ab'))

        when:
        Page result = service.findByIntegrationTypeAndDateRange(integrationType, propertyId, startDate, endDate, new PageRequest(0, 10))

        then:
        elementsExpected == result.totalElements

        where:
        elementsExpected | integrationType         | propertyId       | startDate                         | endDate
        1L               | IntegrationType.AHWS_SC | 'TESTPROPERTY_1' | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
        0                | IntegrationType.MARS    | 'TESTPROPERTY_1' | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
        0                | IntegrationType.AHWS_SC | 'TESTPROPERTY_2' | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
        0                | IntegrationType.AHWS_SC | 'TESTPROPERTY_1' | LocalDateTime.now().minusHours(2) | LocalDateTime.now().minusHours(1)
    }

    def persist(NucleusFunctionSpaceEventType data){
        repository.save(data)
    }
}
