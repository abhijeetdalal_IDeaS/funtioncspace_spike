package com.ideas.ngi.functionspace.service

import com.ideas.ngi.common.test.categories.ComponentTest
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceFunctionRoom
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceFunctionRoomRepository
import com.ideas.ngi.test.core.BaseMockMvcSpec
import org.junit.experimental.categories.Category
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.mongodb.core.query.Criteria
import spock.lang.Unroll

import java.time.LocalDateTime

@Category(ComponentTest.class)
class FunctionSpaceFunctionRoomServiceSpec extends BaseMockMvcSpec {
    @Autowired
    NucleusFunctionSpaceFunctionRoomRepository repository
    @Autowired
    FunctionSpaceFunctionRoomService service

    def cleanup() {
        Criteria criteria = Criteria.where("propertyId").regex("TESTPROPERTY");
        repository.findAllAndRemove(criteria);
    }

    @Unroll
    def "when finding fs room by type for #integrationType|#propertyId|#startDate|#endDate expect #elementsExpected result"() {
        given:
        persist(new NucleusFunctionSpaceFunctionRoom(integrationType: IntegrationType.AHWS_SC,
                propertyId: 'TESTPROPERTY_1', functionRoomId: '1', areaSqFeet: 10, areaSqMeters: 3,
                lengthInFeet: 20, lengthInMeters: 7, widthInFeet: 12, widthInMeters: 4))

        when:
        Page result = service.findByIntegrationTypeAndDateRange(integrationType, propertyId, startDate, endDate, new PageRequest(0, 10))

        then:
        elementsExpected == result.totalElements

        where:
        elementsExpected | integrationType         | propertyId       | startDate                         | endDate
        1L               | IntegrationType.AHWS_SC | 'TESTPROPERTY_1' | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
        0                | IntegrationType.MARS    | 'TESTPROPERTY_1' | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
        0                | IntegrationType.AHWS_SC | 'TESTPROPERTY_2' | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
        0                | IntegrationType.AHWS_SC | 'TESTPROPERTY_1' | LocalDateTime.now().minusHours(2) | LocalDateTime.now().minusHours(1)
    }

    @Unroll
    def "when finding fs combo room by type for #integrationType|#propertyId|#startDate|#endDate expect #elementsExpected result"() {
        given:
        persist(new NucleusFunctionSpaceFunctionRoom(integrationType: IntegrationType.AHWS_SC, combo: isCombo,
                propertyId: 'TESTPROPERTY_1', functionRoomId: '1', areaSqFeet: 10, areaSqMeters: 3,
                lengthInFeet: 20, lengthInMeters: 7, widthInFeet: 12, widthInMeters: 4))

        when:
        Page result = service.findComboRoomsByIntegrationTypeAndDateRange(integrationType, propertyId, startDate, endDate, new PageRequest(0, 10))

        then:
        elementsExpected == result.totalElements

        where:
        elementsExpected | integrationType         | propertyId       | isCombo | startDate                         | endDate
        1L               | IntegrationType.AHWS_SC | 'TESTPROPERTY_1' | true  | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
        0                | IntegrationType.MARS    | 'TESTPROPERTY_1' | true  | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
        0                | IntegrationType.AHWS_SC | 'TESTPROPERTY_2' | true  | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
        0                | IntegrationType.AHWS_SC | 'TESTPROPERTY_1' | true  | LocalDateTime.now().minusHours(2) | LocalDateTime.now().minusHours(1)
        0                | IntegrationType.AHWS_SC | 'TESTPROPERTY_1' | false | LocalDateTime.now().minusHours(1) | LocalDateTime.now().plusHours(1)
    }

    def persist(NucleusFunctionSpaceFunctionRoom data){
        repository.save(data)
    }
}
