package com.ideas.ngi.functionspace.config;

import com.ideas.ngi.common.test.categories.ComponentTest;
import com.ideas.ngi.functionspace.service.*;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.functionspace.dto.FunctionSpaceDeletionDto;
import com.ideas.ngi.nucleus.data.functionspace.entity.*;
import com.ideas.ngi.functionspace.gateway.FunctionSpaceGateway;
import com.ideas.ngi.functionspace.IntegrationFlowTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@Category(ComponentTest.class)
public class FunctionSpaceConfigTest extends IntegrationFlowTest {

    @Autowired
    FunctionSpaceEventTypeService eventTypeService;
    @Autowired
    FunctionSpaceMarketSegmentService marketSegmentService;
    @Autowired
    FunctionSpaceFunctionRoomService functionRoomService;
    @Autowired
    FunctionSpaceBookingService bookingService;
    @Autowired
    FunctionSpaceBookingGuestRoomService guestRoomService;
    @Autowired
    FunctionSpaceBookingPaceService bookingPaceService;
    @Autowired
    FunctionSpaceGuestRoomPaceService guestRoomPaceService;
    @Autowired
    FunctionSpaceEventService eventService;
    @Autowired
    private MongoOperations mongoOperations;
    
    @Autowired
    FunctionSpaceGateway functionSpaceGateway;
    
    private IntegrationType integrationType = IntegrationType.FUNCTION_SPACE_CLIENT;
    private String clientCode = "SANDBOXNUCLEUS";
    private String propertyCode = "FSC01";

    @Before
    @After
    public void setupAndTeardown() {
        eventTypeService.delete(clientCode, propertyCode);
        marketSegmentService.delete(clientCode, propertyCode);
        functionRoomService.delete(clientCode, propertyCode);
        bookingService.delete(clientCode, propertyCode);
        guestRoomService.delete(clientCode, propertyCode);
        bookingPaceService.delete(clientCode, propertyCode);
        guestRoomPaceService.delete(clientCode, propertyCode);
        eventService.delete(clientCode, propertyCode);
    }
    
    @Test
    public void deleteProperty() {
        String propertyId = "12345";
        NucleusFunctionSpaceEventType eventType = new NucleusFunctionSpaceEventType();
        eventType.setIntegrationType(integrationType);

        eventType.setPropertyId(propertyId);
        eventType.setClientCode(clientCode);
        eventType.setPropertyCode(propertyCode);
        eventType.setAbbreviation("Abbrev");
        eventTypeService.save(eventType);
        
        NucleusFunctionSpaceMarketSegment marketSegment = new NucleusFunctionSpaceMarketSegment();
        marketSegment.setIntegrationType(integrationType);
        marketSegment.setPropertyId(propertyId);
        marketSegment.setClientCode(clientCode);
        marketSegment.setPropertyCode(propertyCode);
        marketSegment.setAbbreviation("Abbrev");
        marketSegment.setMarketSegmentId("Abbrev");
        marketSegmentService.save(marketSegment);
        
        NucleusFunctionSpaceFunctionRoom functionRoom = new NucleusFunctionSpaceFunctionRoom();
        functionRoom.setIntegrationType(integrationType);
        functionRoom.setPropertyId(propertyId);
        functionRoom.setClientCode(clientCode);
        functionRoom.setPropertyCode(propertyCode);
        functionRoom.setFunctionRoomId("123");
        functionRoom.setAreaSqFeet(new BigDecimal(100));
        functionRoom.setAreaSqMeters(new BigDecimal(100));
        functionRoom.setLengthInFeet(new BigDecimal(100));
        functionRoom.setLengthInMeters(new BigDecimal(100));
        functionRoom.setWidthInFeet(new BigDecimal(100));
        functionRoom.setWidthInMeters(new BigDecimal(100));
        functionRoomService.save(Collections.singletonList(functionRoom));
        
        NucleusFunctionSpaceBooking booking = new NucleusFunctionSpaceBooking();
        booking.setIntegrationType(integrationType);
        booking.setPropertyId(propertyId);
        booking.setClientCode(clientCode);
        booking.setPropertyCode(propertyCode);
        booking.setBookingId("111");
        booking.setBookingType("bookingType");
        booking.setArrivalDate(new Date());
        booking.setDepartureDate(new Date());
        booking.setStatus("status");
        booking.setMarketSegmentCode("marketSegmentCode");
        bookingService.save(booking);

        NucleusFunctionSpaceBookingGuestRoom guestRoom = new NucleusFunctionSpaceBookingGuestRoom();
        guestRoom.setIntegrationType(integrationType);
        guestRoom.setPropertyId(propertyId);
        guestRoom.setClientCode(clientCode);
        guestRoom.setPropertyCode(propertyCode);
        guestRoom.setBookingId("111");
        guestRoom.setRoomCategory("roomCategory");
        guestRoom.setOccupancyDate(new Date());
        guestRoomService.save(Collections.singletonList(guestRoom));
        
        NucleusFunctionSpaceBookingPace bookingPace = new NucleusFunctionSpaceBookingPace();
        bookingPace.setIntegrationType(integrationType);
        bookingPace.setPropertyId(propertyId);
        bookingPace.setClientCode(clientCode);
        bookingPace.setPropertyCode(propertyCode);
        bookingPace.setBookingId("111");
        bookingPace.setBookingPaceId("111222");
        bookingPace.setBookingStatus("bookingStatus");
        bookingPace.setChangeDate(new Date());
        bookingPaceService.save(bookingPace);

        NucleusFunctionSpaceGuestRoomPace guestroomPace = new NucleusFunctionSpaceGuestRoomPace();
        guestroomPace.setIntegrationType(integrationType);
        guestroomPace.setPropertyId(propertyId);
        guestroomPace.setClientCode(clientCode);
        guestroomPace.setPropertyCode(propertyCode);
        guestroomPace.setBookingId("111");
        guestroomPace.setBookingPaceId("111222");
        guestroomPace.setStayDate(new Date());
        guestroomPace.setRoomType("roomType");
        guestRoomPaceService.save(Collections.singletonList(guestroomPace));

        NucleusFunctionSpaceEvent event = new NucleusFunctionSpaceEvent();
        event.setIntegrationType(integrationType);
        event.setPropertyId(propertyId);
        event.setClientCode(clientCode);
        event.setPropertyCode(propertyCode);
        event.setBookingId("111");
        event.setEventId("222");
        event.setEventTypeCode("eventTypeCode");
        event.setBlockStart(new Date());
        event.setBlockEnd(new Date());
        event.setStatus("status");
        eventService.save(Collections.singletonList(event));
        
        FunctionSpaceDeletionDto functionSpaceDeletionDto = new FunctionSpaceDeletionDto(integrationType, propertyId, clientCode, propertyCode);
        FunctionSpaceDeletionDto retVal = functionSpaceGateway.deletePropertyData(functionSpaceDeletionDto);

        assertEquals(retVal, functionSpaceDeletionDto);
        assertEquals(1, functionSpaceDeletionDto.getNumberOfEventTypesDeleted());
        assertEquals(1, functionSpaceDeletionDto.getNumberOfMarketSegmentsDeleted());
        assertEquals(1, functionSpaceDeletionDto.getNumberOfFunctionRoomsDeleted());
        assertEquals(1, functionSpaceDeletionDto.getNumberOfBookingsDeleted());
        assertEquals(1, functionSpaceDeletionDto.getNumberOfBookingGuestRoomsDeleted());
        assertEquals(1, functionSpaceDeletionDto.getNumberOfBookingPaceGuestRoomsDeleted());
        assertEquals(1, functionSpaceDeletionDto.getNumberOfBookingPacesDeleted());
        assertEquals(1, functionSpaceDeletionDto.getNumberOfEventsDeleted());

        assertEquals(0, eventTypeService.findAll(clientCode, propertyCode).size());
        assertEquals(0, marketSegmentService.findAll(clientCode, propertyCode).size());
        assertEquals(0, functionRoomService.findAll(clientCode, propertyCode).size());
        assertEquals(0, bookingService.findAll(clientCode, propertyCode).size());
        assertEquals(0, guestRoomService.findAll(clientCode, propertyCode).size());
        assertEquals(0, bookingPaceService.findAll(clientCode, propertyCode).size());
        assertEquals(0, guestRoomPaceService.findAll(clientCode, propertyCode).size());
        assertEquals(0, eventService.findAll(clientCode, propertyCode).size());
    }
}
