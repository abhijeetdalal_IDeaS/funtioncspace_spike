package com.ideas.ngi.functionspace.config;

import com.ideas.ngi.functionspace.IntegrationFlowTest;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.functionspace.dto.FunctionSpaceData;
import com.ideas.ngi.functionspace.dto.FunctionSpaceDataNotification;
import com.ideas.ngi.nucleus.integration.util.MessageHeaderUtil;
import com.ideas.ngi.nucleus.monitor.config.CompletedMessageConfig;
import com.ideas.ngi.nucleus.monitor.entity.NucleusCompletedMessage;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class FSNotificationConfigTest extends IntegrationFlowTest {

    @Autowired
    @Qualifier(FSNotificationConfig.FS_NOTIFICATION_SEND_CHANNEL)
    MessageChannel fsNotificationSendChannel;

    @Autowired
    FSNotificationConfig fsNotificationConfig;

    @Autowired
    ApplicationContext context;

    @Test
    public void flow() {
        FunctionSpaceData data = new FunctionSpaceData();
        data.setIntegrationType(IntegrationType.ACTIVITY_DATA);
        data.setSendingSystemPropertyId("systemPropertyId");
        data.setPropertyCode("property");
        data.setFullSync(false);
        data.setClientCode("client");

        Message<FunctionSpaceData> message = MessageBuilder.withPayload(data).setHeader(MessageHeaderUtil.RETURN_CHANNEL, CompletedMessageConfig.COMPLETED_MESSAGE_CHANNEL).build();
        fsNotificationSendChannel.send(message);

        ArgumentCaptor<String> urlCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Object> payloadCaptor = ArgumentCaptor.forClass(Object.class);
        ArgumentCaptor<Class<?>> responseTypeCaptor = ArgumentCaptor.forClass(Class.class);
        ArgumentCaptor<Map<String,Object>> paramsCaptor = ArgumentCaptor.forClass(Map.class);
        verify(g3RestTemplate, timeout(3000).atLeastOnce()).postForObject(urlCaptor.capture(), payloadCaptor.capture(), responseTypeCaptor.capture(), paramsCaptor.capture());
        assertTrue(payloadCaptor.getValue() instanceof FunctionSpaceDataNotification);

        List<NucleusCompletedMessage> messages = completedMessageRepository.findAll();
        assertTrue(messages.get(0).getPayload() instanceof FunctionSpaceData);
    }

    @Test
    public void sendChannel() {
        assertEquals("fsNotificationSendChannel", context.getBean(FSNotificationConfig.FS_NOTIFICATION_SEND_CHANNEL).toString());
    }

}