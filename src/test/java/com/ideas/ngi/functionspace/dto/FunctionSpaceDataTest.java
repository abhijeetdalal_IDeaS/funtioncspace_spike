package com.ideas.ngi.functionspace.dto;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import org.junit.Test;

import static org.junit.Assert.*;

public class FunctionSpaceDataTest {

    @Test
    public void setGet() {
        FunctionSpaceData data = new FunctionSpaceData();

        data.setClientCode("clientCode");
        data.setFullSync(true);
        data.setPropertyCode("propertyCode");
        data.setSendingSystemPropertyId("id");
        data.setIntegrationType(IntegrationType.ACTIVITY_DATA);

        assertEquals("clientCode", data.getClientCode());
        assertTrue(data.isFullSync());
        assertEquals("propertyCode", data.getPropertyCode());
        assertEquals("id", data.getSendingSystemPropertyId());
        assertEquals(IntegrationType.ACTIVITY_DATA, data.getIntegrationType());
    }
}