package com.ideas.ngi.functionspace.dto;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FunctionSpaceDataNotificationTest {

    @Test
    public void setGet() {
        String locationId = "SANDBOX-AHWS01";
        FunctionSpaceDataNotification functionSpaceNotification = new FunctionSpaceDataNotification();

        functionSpaceNotification.setClientCode("SANDBOX");
        functionSpaceNotification.setPropertyCode("AHWS01");
        functionSpaceNotification.setSendingSystemPropertyId(locationId);
        functionSpaceNotification.setIntegrationType(IntegrationType.AHWS_SC);

        assertEquals("SANDBOX", functionSpaceNotification.getClientCode());
        assertEquals("AHWS01", functionSpaceNotification.getPropertyCode());
        assertEquals(locationId, functionSpaceNotification.getSendingSystemPropertyId());
        assertEquals(IntegrationType.AHWS_SC, functionSpaceNotification.getIntegrationType());
    }
}
