package com.ideas.ngi.functionspace.repository;

import com.ideas.ngi.common.test.categories.ComponentTest;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEventPace;
import com.ideas.ngi.functionspace.IntegrationFlowTest;
import com.ideas.ngi.nucleus.util.DateTimeUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.InvocationTargetException;

import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.*;
import static org.junit.Assert.*;

@Category(ComponentTest.class)
public class NucleusFunctionSpaceEventPaceRepositoryTest extends IntegrationFlowTest {

    @Autowired
    private NucleusFunctionSpaceEventPaceRepository functionSpaceEventPaceRepository;

    @Before
    @After
    public void setup() {
        functionSpaceEventPaceRepository.deleteAll();
    }

    @Test
    public void findFirstByClientCodeAndPropertyCodeAndBookingIdAndEventIdOrderByLastModifiedDateDesc() throws InvocationTargetException, IllegalAccessException {
        functionSpaceEventPaceRepository.save(buildNucleusFunctionSpaceEventPace());

        NucleusFunctionSpaceEventPace returnedObj = functionSpaceEventPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndEventIdOrderByLastModifiedDateDesc(
                CLIENT_CODE, PROPERTY_CODE, "bookingId", "eventId");

        assertNotNull(returnedObj);
        assertEquals("eventTypeCode", returnedObj.getEventTypeCode());
        assertTrue(returnedObj.isMoveable());
        assertEquals(DateTimeUtil.buildDate(2019, 07, 14), returnedObj.getChangeDate());
    }
}