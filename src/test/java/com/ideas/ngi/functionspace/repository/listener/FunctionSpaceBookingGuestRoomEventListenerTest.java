package com.ideas.ngi.functionspace.repository.listener;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBookingGuestRoom;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;

public class FunctionSpaceBookingGuestRoomEventListenerTest {

	@Mock
    NucleusFunctionSpaceBookingGuestRoom functionSpaceBookingGuestRoom;
	
	FunctionSpaceBookingGuestRoomEventListener listener = new FunctionSpaceBookingGuestRoomEventListener();

    @Before
    public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void onBeforeConvert() {
		// We want to make sure the hasBookedRooms method gets called before saving the entity
		Mockito.when(functionSpaceBookingGuestRoom.hasBookedRooms()).thenReturn(true);

		BeforeConvertEvent<NucleusFunctionSpaceBookingGuestRoom> event = new BeforeConvertEvent<>(functionSpaceBookingGuestRoom, "functionSpaceBookingGuestRoom");
		
		listener.onBeforeConvert(event);

		Mockito.verify(functionSpaceBookingGuestRoom).hasBookedRooms();		
	}
}
