package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.nucleus.data.functionspace.entity.FunctionSpaceEventRevenue;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEventPace;
import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceEventPaceRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.CLIENT_CODE;
import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.PROPERTY_CODE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FunctionSpaceEventPaceServiceTest {

    @Mock
    private NucleusFunctionSpaceEventPaceRepository functionSpaceEventPaceRepository;
    @InjectMocks
    private FunctionSpaceEventPaceService functionSpaceEventPaceService;
    private NucleusFunctionSpaceEventPace functionSpaceEventPace;

    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private PageRequest pageRequest;

    @Before
    public void setUp() throws InvocationTargetException, IllegalAccessException {
        startDate = LocalDateTime.of(2018, Month.NOVEMBER, 01, 5, 30);
        endDate = LocalDateTime.of(2018, Month.NOVEMBER, 05, 8, 30);
        pageRequest = new PageRequest(1, 1);
        functionSpaceEventPace = NucleusFunctionSpaceObjectMother.buildNucleusFunctionSpaceEventPace();
    }

    @Test
    public void save() {
        List<NucleusFunctionSpaceEventPace> nucleusFunctionSpaceEventPaces = Arrays.asList(functionSpaceEventPace);
        when(functionSpaceEventPaceRepository.saveAll(nucleusFunctionSpaceEventPaces)).thenReturn(nucleusFunctionSpaceEventPaces);

        functionSpaceEventPaceService.save(nucleusFunctionSpaceEventPaces);

        verify(functionSpaceEventPaceRepository).saveAll(nucleusFunctionSpaceEventPaces);
    }

    @Test
    public void delete() {
        when(functionSpaceEventPaceRepository.deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE)).thenReturn(1L);

        functionSpaceEventPaceService.delete(CLIENT_CODE, PROPERTY_CODE);

        verify(functionSpaceEventPaceRepository).deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void findAllByDateRange() {
        functionSpaceEventPaceService.findAllByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
        verify(functionSpaceEventPaceRepository, atLeastOnce()).findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(any(), any(), any(), any(), any());
    }

    @Test
    public void findLatest() {
        when(functionSpaceEventPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndEventIdOrderByLastModifiedDateDesc(
                CLIENT_CODE, PROPERTY_CODE, "bookingId", "eventId")).thenReturn(functionSpaceEventPace);

        functionSpaceEventPaceService.findLatest(CLIENT_CODE, PROPERTY_CODE, "bookingId", "eventId");

        verify(functionSpaceEventPaceRepository).findFirstByClientCodeAndPropertyCodeAndBookingIdAndEventIdOrderByLastModifiedDateDesc(
                CLIENT_CODE, PROPERTY_CODE, "bookingId", "eventId");
    }

    @Test
    public void equalsWithPrev() throws InvocationTargetException, IllegalAccessException {
        NucleusFunctionSpaceEventPace prevFunctionSpaceEventPace = NucleusFunctionSpaceObjectMother.buildNucleusFunctionSpaceEventPace();
        prevFunctionSpaceEventPace.getFunctionSpaceEventRevenues().get(0).setUpdateDate(new Date());

        FunctionSpaceEventRevenue functionSpaceEventRevenue = new FunctionSpaceEventRevenue();
        functionSpaceEventRevenue.setRevenueGroup("AVI");

        FunctionSpaceEventRevenue functionSpaceEventRevenue1 = new FunctionSpaceEventRevenue();
        functionSpaceEventRevenue1.setRevenueGroup("RENTAL");

        List<FunctionSpaceEventRevenue> functionSpaceEventRevenues = prevFunctionSpaceEventPace.getFunctionSpaceEventRevenues();
        functionSpaceEventRevenues.add(functionSpaceEventRevenue);
        functionSpaceEventRevenues.add(functionSpaceEventRevenue1);
        prevFunctionSpaceEventPace.setFunctionSpaceEventRevenues(functionSpaceEventRevenues);

        functionSpaceEventPace.getFunctionSpaceEventRevenues().add(functionSpaceEventRevenue1);
        functionSpaceEventPace.getFunctionSpaceEventRevenues().add(functionSpaceEventRevenue);

        assertTrue(functionSpaceEventPaceService.equalsWithPrev(functionSpaceEventPace, prevFunctionSpaceEventPace));
    }

    @Test
    public void notEqualWithPrev() throws InvocationTargetException, IllegalAccessException {
        NucleusFunctionSpaceEventPace prevFunctionSpaceEventPace = NucleusFunctionSpaceObjectMother.buildNucleusFunctionSpaceEventPace();
        prevFunctionSpaceEventPace.setMoveable(false);

        assertFalse(functionSpaceEventPaceService.equalsWithPrev(functionSpaceEventPace, prevFunctionSpaceEventPace));
    }
}