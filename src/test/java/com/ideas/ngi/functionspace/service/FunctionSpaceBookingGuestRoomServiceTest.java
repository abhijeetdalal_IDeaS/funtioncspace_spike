package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBookingGuestRoom;
import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceBookingGuestRoomRepository;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.Pair;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.CLIENT_CODE;
import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.PROPERTY_CODE;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class FunctionSpaceBookingGuestRoomServiceTest {
    private static final String BOOKING_ID = "BookingId";
    private static final String ROOM_CATEGORY = "RoomCat";

    private LocalDate occupancyDate;

    @Mock
    private NucleusFunctionSpaceBookingGuestRoomRepository bookingGuestRoomRepository;

    @InjectMocks
    private FunctionSpaceBookingGuestRoomService functionSpaceBookingGuestRoomService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        occupancyDate = LocalDate.of(2017, 10, 1);
    }

    @Test
    public void findOne() {
        NucleusFunctionSpaceBookingGuestRoom bookingGuestRoom = NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingGuestRoom();
        when(bookingGuestRoomRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndOccupancyDateAndRoomCategory(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, occupancyDate, ROOM_CATEGORY)).thenReturn(bookingGuestRoom);
        NucleusFunctionSpaceBookingGuestRoom retBookingGuestRoom = functionSpaceBookingGuestRoomService.findOne(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, occupancyDate, ROOM_CATEGORY);
        assertSame(bookingGuestRoom, retBookingGuestRoom);
        verify(bookingGuestRoomRepository).findFirstByClientCodeAndPropertyCodeAndBookingIdAndOccupancyDateAndRoomCategory(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, occupancyDate, ROOM_CATEGORY);
    }

    @Test
    public void findAll() {
        List<NucleusFunctionSpaceBookingGuestRoom> bookingGuestRooms = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingGuestRoom());
        when(bookingGuestRoomRepository.findByClientCodeAndPropertyCodeAndBookingId(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID)).thenReturn(bookingGuestRooms);
        List<NucleusFunctionSpaceBookingGuestRoom> retBookingGuestRooms = functionSpaceBookingGuestRoomService.findAll(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID);
        assertTrue(CollectionUtils.isNotEmpty(retBookingGuestRooms));
        assertSame(bookingGuestRooms.get(0), retBookingGuestRooms.get(0));

        verify(bookingGuestRoomRepository).findByClientCodeAndPropertyCodeAndBookingId(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID);
    }

    @Test
    public void delete() {
        when(bookingGuestRoomRepository.deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE)).thenReturn(1L);
        functionSpaceBookingGuestRoomService.delete(CLIENT_CODE, PROPERTY_CODE);
        verify(bookingGuestRoomRepository).deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void findAllByDateRange() {
        LocalDateTime startDate = LocalDate.of(2017, 10, 1).atStartOfDay();
        LocalDateTime endDate = LocalDate.of(2017, 10, 31).atStartOfDay();
        PageRequest pageRequest = PageRequest.of(1, 1);
        List<NucleusFunctionSpaceBookingGuestRoom> bookingGuestRooms = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingGuestRoom());
        Page<NucleusFunctionSpaceBookingGuestRoom> page = new PageImpl<>(bookingGuestRooms);
        when(bookingGuestRoomRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest)).thenReturn(page);
        Page<NucleusFunctionSpaceBookingGuestRoom> bookingGuestRoomPages = functionSpaceBookingGuestRoomService.findAllByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
        assertNotNull(bookingGuestRoomPages);
        assertEquals(1, bookingGuestRoomPages.getTotalElements());
        verify(bookingGuestRoomRepository).findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
    }

    @Test
    public void findAllBookedRoomsByDateRange() {
        LocalDateTime startDate = LocalDate.of(2017, 10, 1).atStartOfDay();
        LocalDateTime endDate = LocalDate.of(2017, 10, 31).atStartOfDay();
        PageRequest pageRequest = PageRequest.of(1, 1);
        List<NucleusFunctionSpaceBookingGuestRoom> bookingGuestRooms = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingGuestRoom());
        Page<NucleusFunctionSpaceBookingGuestRoom> page = new PageImpl<>(bookingGuestRooms);
        when(bookingGuestRoomRepository.findByClientCodeAndPropertyCodeAndHasBookedRoomsIsTrueAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest)).thenReturn(page);
        Page<NucleusFunctionSpaceBookingGuestRoom> bookingGuestRoomPages = functionSpaceBookingGuestRoomService.findAllBookedRoomsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
        assertNotNull(bookingGuestRoomPages);
        assertEquals(1, bookingGuestRoomPages.getTotalElements());
        verify(bookingGuestRoomRepository).findByClientCodeAndPropertyCodeAndHasBookedRoomsIsTrueAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
    }

    @Test
    public void save() {
        List<NucleusFunctionSpaceBookingGuestRoom> bookingGuestRooms = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingGuestRoom());
        when(bookingGuestRoomRepository.saveAll(bookingGuestRooms)).thenReturn(bookingGuestRooms);
        functionSpaceBookingGuestRoomService.save(bookingGuestRooms);
        verify(bookingGuestRoomRepository).saveAll(bookingGuestRooms);
    }

    @Test
    public void countByPropertyIdAndBookingIdAndStatusIsNot() {
        when(bookingGuestRoomRepository.countByClientCodeAndPropertyCodeAndBookingIdAndStatusIsNot(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, "Cancelled")).thenReturn(0);
        functionSpaceBookingGuestRoomService.findCount(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, "Cancelled");
        verify(bookingGuestRoomRepository).countByClientCodeAndPropertyCodeAndBookingIdAndStatusIsNot(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, "Cancelled");
    }

    @Test
    public void upsertAll() {
        List<NucleusFunctionSpaceBookingGuestRoom> functionSpaceBookingGuestRooms = new ArrayList<>();
        functionSpaceBookingGuestRooms.add(new NucleusFunctionSpaceBookingGuestRoom());
        List<Pair<Query, Update>> updates = new ArrayList<>();
        Mockito.when(bookingGuestRoomRepository.bulkOpsUpsertMulti(updates)).thenReturn(null);
        functionSpaceBookingGuestRoomService.upsertAll(functionSpaceBookingGuestRooms);
        verify(bookingGuestRoomRepository, atLeastOnce()).bulkOpsUpsertMulti(ArgumentMatchers.any());
    }
}