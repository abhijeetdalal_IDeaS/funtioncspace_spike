package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.functionspace.dto.FunctionSpaceGuestRoomPaceWithBookingInfoDto;
import com.ideas.ngi.nucleus.data.functionspace.entity.*;
import com.ideas.ngi.nucleus.test.RestControllerTestHelper;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.*;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class FunctionSpaceRestControllerTest {
    private static final String PROPERTY_ID = "TestProperty";

    private static final String FETCH_MISSING_FUNCTION_ROOM_IDS_RECEIVED_IN_EVENTS_URL = "/functionSpace/missingFunctionRoomIdsReceivedInEvents?clientCode=BSTN&propertyCode=H2";
    private static final String ROOM_ID_1 = "123";
    private static final String ROOM_ID_2 = "1234";

    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private PageRequest pageRequest;

    @Mock
    private FunctionSpaceBookingService bookingService;
    @Mock
    private FunctionSpaceEventService eventService;

    @Mock
    private FunctionSpaceEventPaceService functionSpaceEventPaceService;

    @Mock
    private FunctionSpaceFunctionRoomService functionRoomService;

    @Mock
    private FunctionSpaceEventTypeService functionSpaceEventTypeService;

    @Mock
    private FunctionSpaceBookingGuestRoomService functionSpaceBookingGuestRoomService;

    @Mock
    private FunctionSpaceBookingPaceService functionSpaceBookingPaceService;

    @Mock
    private FunctionSpaceGuestRoomPaceService functionSpaceGuestRoomPaceService;

    @Mock
    private FunctionSpaceGuestRoomTypeService functionSpaceGuestRoomTypeService;

    @Mock
    private FunctionSpaceMarketSegmentService functionSpaceMarketSegmentService;

    @Mock
    private Pageable pageable;
    @Mock
    PagedResourcesAssembler<NucleusFunctionSpaceGuestRoomPace> pagedResourcesAssembler;
    private MockMvc mockMvc;
    @Captor
    private ArgumentCaptor<LocalDateTime> endDateCaptor;

    @InjectMocks
    private FunctionSpaceRestController restController;
    private static final String PAGED_OCCUPANCY_SUMMARY_MARKET_CODE_RATE_CODE_URL = "/functionSpace/guestRooms/pace/byDay/withBookingInfo?clientCode=SANDBOX&propertyCode=AHWS01&startDate=2015-02-02 08:54:14.708&endDate=2019-02-02 08:54:14.708&page=1&size=5&sort=1";

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        pageRequest = PageRequest.of(1, 1);
        startDate = LocalDate.of(2017, 1, 1).atStartOfDay();
        endDate = LocalDate.of(2017, 1, 30).atStartOfDay();
        mockMvc = RestControllerTestHelper.createMockMvc(restController);
    }

    private void assertPage(Page<?> data, int size) {
        assertNotNull(data);
        assertEquals(size, data.getContent().size());
    }

    @Test
    public void getAllBookingsByDateRange() {
        Page<NucleusFunctionSpaceBooking> page = new PageImpl<>(Collections.singletonList(buildFunctionSpaceBooking()));
        when(bookingService.findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), ArgumentMatchers.any(LocalDateTime.class), ArgumentMatchers.eq(pageRequest))).thenReturn(page);

        Page<NucleusFunctionSpaceBooking> closedRangePage = restController.getAllBookingsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
        assertPage(closedRangePage, 1);

        Page<NucleusFunctionSpaceBooking> openRangePage = restController.getAllBookingsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, null, pageRequest);
        assertPage(openRangePage, 1);

        verify(bookingService, times(2)).findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), endDateCaptor.capture(), ArgumentMatchers.eq(pageRequest));
        assertSame(endDate, endDateCaptor.getAllValues().get(0));
        assertNotNull(endDateCaptor.getAllValues().get(1));
    }

    @Test
    public void getAllBookingEventsByDateRange() throws Exception {
        Page<NucleusFunctionSpaceEventPace> page = new PageImpl<>(Collections.singletonList(buildNucleusFunctionSpaceEventPace()));
        when(functionSpaceEventPaceService.findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), ArgumentMatchers.any(LocalDateTime.class), ArgumentMatchers.eq(pageRequest))).thenReturn(page);

        Page<NucleusFunctionSpaceEventPace> closedRangePage = restController.getAllEventsPaceByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
        assertPage(closedRangePage, 1);

        Page<NucleusFunctionSpaceEventPace> openRangePage = restController.getAllEventsPaceByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, null, pageRequest);
        assertPage(openRangePage, 1);

        verify(functionSpaceEventPaceService, times(2)).findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), endDateCaptor.capture(), ArgumentMatchers.eq(pageRequest));
        assertSame(endDate, endDateCaptor.getAllValues().get(0));
        assertNotNull(endDateCaptor.getAllValues().get(1));
    }

    @Test
    public void getAllEventsPaceByDateRange() {
        Page<NucleusFunctionSpaceEvent> page = new PageImpl<>(Collections.singletonList(buildFunctionSpaceEvent()));
        when(eventService.findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), ArgumentMatchers.any(LocalDateTime.class), ArgumentMatchers.eq(pageRequest))).thenReturn(page);

        Page<NucleusFunctionSpaceEvent> closedRangePage = restController.getAllEventsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
        assertPage(closedRangePage, 1);

        Page<NucleusFunctionSpaceEvent> openRangePage = restController.getAllEventsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, null, pageRequest);
        assertPage(openRangePage, 1);

        verify(eventService, times(2)).findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), endDateCaptor.capture(), ArgumentMatchers.eq(pageRequest));
        assertSame(endDate, endDateCaptor.getAllValues().get(0));
        assertNotNull(endDateCaptor.getAllValues().get(1));
    }

    @Test
    public void getAllBookingEventTypesByDateRange() {
        Page<NucleusFunctionSpaceEventType> page = new PageImpl<>(Collections.singletonList(buildFunctionSpaceEventType()));
        when(functionSpaceEventTypeService.findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), ArgumentMatchers.any(LocalDateTime.class), ArgumentMatchers.eq(pageRequest))).thenReturn(page);

        Page<NucleusFunctionSpaceEventType> closedRangePage = restController.getAllEventTypesByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
        assertPage(closedRangePage, 1);

        Page<NucleusFunctionSpaceEventType> openRangePage = restController.getAllEventTypesByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, null, pageRequest);
        assertPage(openRangePage, 1);

        verify(functionSpaceEventTypeService, times(2)).findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), endDateCaptor.capture(), ArgumentMatchers.eq(pageRequest));
        assertSame(endDate, endDateCaptor.getAllValues().get(0));
        assertNotNull(endDateCaptor.getAllValues().get(1));
    }

    @Test
    public void getAllFunctionRoomsByDateRange() {
        Page<NucleusFunctionSpaceFunctionRoom> page = new PageImpl<>(Collections.singletonList(buildFunctionSpaceComboFunctionRoom()));
        when(functionRoomService.findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), ArgumentMatchers.any(LocalDateTime.class), ArgumentMatchers.eq(pageRequest))).thenReturn(page);

        Page<NucleusFunctionSpaceFunctionRoom> closedRangePage = restController.getAllFunctionRoomsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
        assertPage(closedRangePage, 1);

        Page<NucleusFunctionSpaceFunctionRoom> openRangePage = restController.getAllFunctionRoomsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, null, pageRequest);
        assertPage(openRangePage, 1);

        verify(functionRoomService, times(2)).findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), endDateCaptor.capture(), ArgumentMatchers.eq(pageRequest));
        assertSame(endDate, endDateCaptor.getAllValues().get(0));
        assertNotNull(endDateCaptor.getAllValues().get(1));
    }

    @Test
    public void getAllFunctionComboRoomsByDateRange() {
        Page<NucleusFunctionSpaceFunctionRoom> page = new PageImpl<>(Collections.singletonList(buildFunctionSpaceComboFunctionRoom()));
        when(functionRoomService.findAllComboRoomsByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), ArgumentMatchers.any(LocalDateTime.class), ArgumentMatchers.eq(pageRequest))).thenReturn(page);

        Page<NucleusFunctionSpaceFunctionRoom> closedRangePage = restController.getAllComboRoomsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
        assertPage(closedRangePage, 1);

        Page<NucleusFunctionSpaceFunctionRoom> openRangePage = restController.getAllComboRoomsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, null, pageRequest);
        assertPage(openRangePage, 1);

        verify(functionRoomService, times(2)).findAllComboRoomsByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), endDateCaptor.capture(), ArgumentMatchers.eq(pageRequest));
        assertSame(endDate, endDateCaptor.getAllValues().get(0));
        assertNotNull(endDateCaptor.getAllValues().get(1));
    }

    @Test
    public void getAllGuestRoomsByDateRange() {
        Page<NucleusFunctionSpaceBookingGuestRoom> page = new PageImpl<>(Collections.singletonList(buildFunctionSpaceBookingGuestRoom()));
        when(functionSpaceBookingGuestRoomService.findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), ArgumentMatchers.any(LocalDateTime.class), ArgumentMatchers.eq(pageRequest))).thenReturn(page);

        Page<NucleusFunctionSpaceBookingGuestRoom> closedRangePage = restController.getAllGuestRoomsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
        assertPage(closedRangePage, 1);

        Page<NucleusFunctionSpaceBookingGuestRoom> openRangePage = restController.getAllGuestRoomsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, null, pageRequest);
        assertPage(openRangePage, 1);

        verify(functionSpaceBookingGuestRoomService, times(2)).findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), endDateCaptor.capture(), ArgumentMatchers.eq(pageRequest));
        assertSame(endDate, endDateCaptor.getAllValues().get(0));
        assertNotNull(endDateCaptor.getAllValues().get(1));
    }

    @Test
    public void getAllBookedGuestRoomsByDateRange() {
        Page<NucleusFunctionSpaceBookingGuestRoom> page = new PageImpl<>(Collections.singletonList(buildFunctionSpaceBookingGuestRoom()));
        when(functionSpaceBookingGuestRoomService.findAllBookedRoomsByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), ArgumentMatchers.any(LocalDateTime.class), ArgumentMatchers.eq(pageRequest))).thenReturn(page);

        Page<NucleusFunctionSpaceBookingGuestRoom> closedRangePage = restController.getAllBookedGuestRoomsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
        assertPage(closedRangePage, 1);

        Page<NucleusFunctionSpaceBookingGuestRoom> openRangePage = restController.getAllBookedGuestRoomsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, null, pageRequest);
        assertPage(openRangePage, 1);

        verify(functionSpaceBookingGuestRoomService, times(2)).findAllBookedRoomsByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), endDateCaptor.capture(), ArgumentMatchers.eq(pageRequest));
        assertSame(endDate, endDateCaptor.getAllValues().get(0));
        assertNotNull(endDateCaptor.getAllValues().get(1));
    }

    @Test
    public void getAllGuestRoomTypesByDateRange() {
        Page<NucleusFunctionSpaceGuestRoomType> page = new PageImpl<>(Collections.singletonList(buildFunctionSpaceBookingGuestRoomType()));
        when(functionSpaceGuestRoomTypeService.findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), ArgumentMatchers.any(LocalDateTime.class), ArgumentMatchers.eq(pageRequest))).thenReturn(page);

        Page<NucleusFunctionSpaceGuestRoomType> closedRangePage = restController.getAllGuestRoomTypesByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
        assertPage(closedRangePage, 1);

        Page<NucleusFunctionSpaceGuestRoomType> openRangePage = restController.getAllGuestRoomTypesByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, null, pageRequest);
        assertPage(openRangePage, 1);

        verify(functionSpaceGuestRoomTypeService, times(2)).findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), endDateCaptor.capture(), ArgumentMatchers.eq(pageRequest));
        assertSame(endDate, endDateCaptor.getAllValues().get(0));
        assertNotNull(endDateCaptor.getAllValues().get(1));
    }

    @Test
    public void getAllBookingPacesByDateRange() {
        Page<NucleusFunctionSpaceBookingPace> page = new PageImpl<>(Collections.singletonList(buildFunctionSpaceBookingPace()));
        when(functionSpaceBookingPaceService.findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), ArgumentMatchers.any(LocalDateTime.class), ArgumentMatchers.eq(pageRequest))).thenReturn(page);

        Page<NucleusFunctionSpaceBookingPace> closedRangePage = restController.getAllBookingPacesByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
        assertPage(closedRangePage, 1);

        Page<NucleusFunctionSpaceBookingPace> openRangePage = restController.getAllBookingPacesByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, null, pageRequest);
        assertPage(openRangePage, 1);

        verify(functionSpaceBookingPaceService, times(2)).findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), endDateCaptor.capture(), ArgumentMatchers.eq(pageRequest));
        assertSame(endDate, endDateCaptor.getAllValues().get(0));
        assertNotNull(endDateCaptor.getAllValues().get(1));
    }

    @Test
    public void getAllMarketSegmentsByDateRange() {
        Page<NucleusFunctionSpaceMarketSegment> page = new PageImpl<>(Collections.singletonList(buildFunctionSpaceMarketSegment()));
        when(functionSpaceMarketSegmentService.findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), ArgumentMatchers.any(LocalDateTime.class), ArgumentMatchers.eq(pageRequest))).thenReturn(page);

        Page<NucleusFunctionSpaceMarketSegment> closedRangePage = restController.getAllMarketSegmentsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
        assertPage(closedRangePage, 1);

        Page<NucleusFunctionSpaceMarketSegment> openRangePage = restController.getAllMarketSegmentsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, null, pageRequest);
        assertPage(openRangePage, 1);

        verify(functionSpaceMarketSegmentService, times(2)).findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), endDateCaptor.capture(), ArgumentMatchers.eq(pageRequest));
        assertSame(endDate, endDateCaptor.getAllValues().get(0));
        assertNotNull(endDateCaptor.getAllValues().get(1));
    }

    @Test
    public void getAllGuestRoomPacesByDateRange() {
        Page<NucleusFunctionSpaceGuestRoomPace> page = new PageImpl<>(Collections.singletonList(buildFunctionSpaceGuestRoomPace()));
        when(functionSpaceGuestRoomPaceService.findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), ArgumentMatchers.any(LocalDateTime.class), ArgumentMatchers.eq(pageRequest))).thenReturn(page);

        Page<NucleusFunctionSpaceGuestRoomPace> closedRangePage = restController.getAllGuestRoomPacesByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
        assertPage(closedRangePage, 1);

        Page<NucleusFunctionSpaceGuestRoomPace> openRangePage = restController.getAllGuestRoomPacesByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, null, pageRequest);
        assertPage(openRangePage, 1);

        verify(functionSpaceGuestRoomPaceService, times(2)).findAllByDateRange(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), endDateCaptor.capture(), ArgumentMatchers.eq(pageRequest));
        assertSame(endDate, endDateCaptor.getAllValues().get(0));
        assertNotNull(endDateCaptor.getAllValues().get(1));
    }

    @Test
    public void getLatestBookingPerDayByGroup() throws Exception {

        List<FunctionSpaceGuestRoomPaceWithBookingInfoDto> guestRoomBookingPaces = createGuestRoomBookingPaces(5);
        Pageable pageable = PageRequest.of(0, 5);
        Page<FunctionSpaceGuestRoomPaceWithBookingInfoDto> guestRoomBookingPacePage = new PageImpl<>(guestRoomBookingPaces, pageable, 10);
        when(functionSpaceGuestRoomPaceService.getLatestBookingPerDayByGroup(anyString(), anyString(), ArgumentMatchers.any(Date.class), ArgumentMatchers.any(Date.class),
                ArgumentMatchers.any(Pageable.class))).thenReturn(guestRoomBookingPacePage);
        mockMvc.perform(get(PAGED_OCCUPANCY_SUMMARY_MARKET_CODE_RATE_CODE_URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", Matchers.hasSize(5)))
                .andExpect(jsonPath("$.content[0].bookingPaceId", Matchers.is("00234-000")))
                .andExpect(jsonPath("$.content[1].bookingPaceId", Matchers.is("00234-001")))
                .andExpect(jsonPath("$.content[4].bookingPaceId", Matchers.is("00234-004")))
                .andExpect(jsonPath("$.links[2].rel", Matchers.is("next")));
    }

    @Test
    public void getMissingFunctionRoomIdsReceivedInEvents() throws Exception {
        List<String> roomIds = new ArrayList<>();
        roomIds.add(ROOM_ID_1);
        roomIds.add(ROOM_ID_2);
        roomIds.add("3");
        List<String> functionRoomIds = new ArrayList<>();
        functionRoomIds.add(ROOM_ID_1);
        functionRoomIds.add(ROOM_ID_2);
        when(functionRoomService.fetchAllFunctionRoomIds(anyString(), anyString())).thenReturn(functionRoomIds);
        when(eventService.getDistinctRoomIdsByClientCodeAndPropertyCode(anyString(), anyString())).thenReturn(roomIds);

        mockMvc.perform(get(FETCH_MISSING_FUNCTION_ROOM_IDS_RECEIVED_IN_EVENTS_URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0]", Matchers.is("3")));
    }

    private List<FunctionSpaceGuestRoomPaceWithBookingInfoDto> createGuestRoomBookingPaces(int count) {
        List<FunctionSpaceGuestRoomPaceWithBookingInfoDto> functionSpaceGuestRoomPaceWithBookingInfoDtos = new ArrayList<>();
        IntStream.range(0, count).forEach(i -> {
            NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace = new NucleusFunctionSpaceGuestRoomPace();
            nucleusFunctionSpaceGuestRoomPace.setRoomNights(i);
            nucleusFunctionSpaceGuestRoomPace.setBookingPaceId("00234-00" + i);
            functionSpaceGuestRoomPaceWithBookingInfoDtos.add(new FunctionSpaceGuestRoomPaceWithBookingInfoDto(nucleusFunctionSpaceGuestRoomPace, new NucleusFunctionSpaceBooking()));
        });
        return functionSpaceGuestRoomPaceWithBookingInfoDtos;
    }
}
