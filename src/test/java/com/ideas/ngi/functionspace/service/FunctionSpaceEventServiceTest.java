package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.common.test.categories.ComponentTest;
import com.ideas.ngi.functionspace.IntegrationFlowTest;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEvent;
import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceEventRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.List;

import static com.ideas.ngi.nucleus.test.NucleusTestConstants.CLIENT_CODE;
import static com.ideas.ngi.nucleus.test.NucleusTestConstants.PROPERTY_CODE;
import static org.junit.Assert.*;

@Category(ComponentTest.class)
public class FunctionSpaceEventServiceTest extends IntegrationFlowTest {
    private static final String NON_EXISTENT_CLIENT_CODE = "CLIENT_CODE";
    private static final String NON_EXISTENT_PROPERTY_CODE = "PROPERTY_CODE";
    private static final String ROOM_ID_1 = "RoomID1";
    private static final String ROOM_ID_2 = "RoomID2";

    @Autowired
    private NucleusFunctionSpaceEventRepository functionSpaceEventRepository;
    @Autowired
    private MongoOperations mongoOperations;

    private FunctionSpaceEventService functionSpaceEventService;

    @Before
    public void setup() {
        functionSpaceEventService = new FunctionSpaceEventService(functionSpaceEventRepository, mongoOperations);
        saveNucleusFunctionSpaceEventTestData();
    }

    @After
    public void tearDown() {
        deleteAllRecordsByClientCodeAndPropertyCode();
    }

    private void deleteAllRecordsByClientCodeAndPropertyCode() {
        Criteria criteria = Criteria.where("clientCode").is(CLIENT_CODE).and("propertyCode").is(PROPERTY_CODE);
        functionSpaceEventRepository.findAllAndRemove(criteria);
    }

    @Test
    public void returnsEmptyArrayWhenNoRecordsOfPropertyExists() {
        List<String> rateIds = functionSpaceEventService.getDistinctRoomIdsByClientCodeAndPropertyCode(NON_EXISTENT_CLIENT_CODE, NON_EXISTENT_PROPERTY_CODE);
        assertTrue(rateIds.isEmpty());
    }

    @Test
    public void returnsDistinctRoomIdsByClientCodeAndPropertyCode() {
        List<String> rateIds = functionSpaceEventService.getDistinctRoomIdsByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE);
        assertEquals(2, rateIds.size());
        assertTrue(rateIds.contains(ROOM_ID_1));
        assertTrue(rateIds.contains(ROOM_ID_2));
    }

    private void saveNucleusFunctionSpaceEventTestData() {
        deleteAllRecordsByClientCodeAndPropertyCode();
        functionSpaceEventRepository.save(buildFunctionSpaceEvent(ROOM_ID_1, "2"));
        functionSpaceEventRepository.save(buildFunctionSpaceEvent(ROOM_ID_1, "3"));
        functionSpaceEventRepository.save(buildFunctionSpaceEvent(ROOM_ID_2, "5"));
        functionSpaceEventRepository.save(buildFunctionSpaceEvent(ROOM_ID_2, "4"));
    }

    private NucleusFunctionSpaceEvent buildFunctionSpaceEvent(String roomID, String eventID) {
        NucleusFunctionSpaceEvent functionSpaceEvent = NucleusFunctionSpaceObjectMother.buildFunctionSpaceEvent();
        functionSpaceEvent.setRoomId(roomID);
        functionSpaceEvent.setEventId(eventID);
        functionSpaceEvent.setId(eventID);
        functionSpaceEvent.setClientCode(CLIENT_CODE);
        functionSpaceEvent.setPropertyCode(PROPERTY_CODE);

        return functionSpaceEvent;
    }
}
