package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEvent;
import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceEventRepository;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.CLIENT_CODE;
import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.PROPERTY_CODE;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class FunctionSpaceEventServiceMockTest {
    private static final String BOOKING_ID = "BookingId";
    private static final String EVENT_ID = "EventId";

    @Mock
    private NucleusFunctionSpaceEventRepository functionSpaceEventRepository;

    @InjectMocks
    private FunctionSpaceEventService functionSpaceEventService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findOne() {
        NucleusFunctionSpaceEvent event = NucleusFunctionSpaceObjectMother.buildFunctionSpaceEvent();

        Mockito.when(functionSpaceEventRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndEventId(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, EVENT_ID)).thenReturn(event);

        NucleusFunctionSpaceEvent retEvent = functionSpaceEventService.findOne(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, EVENT_ID);

        assertSame(event, retEvent);
        Mockito.verify(functionSpaceEventRepository).findFirstByClientCodeAndPropertyCodeAndBookingIdAndEventId(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, EVENT_ID);
    }

    @Test
    public void findAll() {
        List<NucleusFunctionSpaceEvent> events = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceEvent());

        Mockito.when(functionSpaceEventRepository.findByClientCodeAndPropertyCodeOrderByBookingIdAscEventIdAsc(CLIENT_CODE, PROPERTY_CODE)).thenReturn(events);

        List<NucleusFunctionSpaceEvent> retEvents = functionSpaceEventService.findAll(CLIENT_CODE, PROPERTY_CODE);

        assertTrue(CollectionUtils.isNotEmpty(retEvents));
        assertSame(events.get(0), retEvents.get(0));

        Mockito.verify(functionSpaceEventRepository).findByClientCodeAndPropertyCodeOrderByBookingIdAscEventIdAsc(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void findAll_ForPropertyAndEvent() {
        List<NucleusFunctionSpaceEvent> events = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceEvent());

        Mockito.when(functionSpaceEventRepository.findByClientCodeAndPropertyCodeAndEventId(CLIENT_CODE, PROPERTY_CODE, EVENT_ID)).thenReturn(events);

        List<NucleusFunctionSpaceEvent> retEvents = functionSpaceEventService.findAll(CLIENT_CODE, PROPERTY_CODE, EVENT_ID);

        assertTrue(CollectionUtils.isNotEmpty(retEvents));
        assertSame(events.get(0), retEvents.get(0));

        Mockito.verify(functionSpaceEventRepository).findByClientCodeAndPropertyCodeAndEventId(CLIENT_CODE, PROPERTY_CODE, EVENT_ID);
    }

    @Test
    public void delete() {
        Mockito.when(functionSpaceEventRepository.deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE)).thenReturn(1L);

        functionSpaceEventService.delete(CLIENT_CODE, PROPERTY_CODE);

        Mockito.verify(functionSpaceEventRepository).deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void findAllByDateRange() {
        LocalDateTime startDate = LocalDate.of(2017, 10, 1).atStartOfDay();
        LocalDateTime endDate = LocalDate.of(2017, 10, 31).atStartOfDay();
        PageRequest pageRequest = new PageRequest(1, 1);
        List<NucleusFunctionSpaceEvent> events = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceEvent());
        Page<NucleusFunctionSpaceEvent> page = new PageImpl<>(events);

        Mockito.when(functionSpaceEventRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest)).thenReturn(page);

        Page<NucleusFunctionSpaceEvent> eventPages = functionSpaceEventService.findAllByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);

        assertNotNull(eventPages);
        assertEquals(1, eventPages.getTotalElements());

        Mockito.verify(functionSpaceEventRepository).findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
    }

    @Test
    public void save() {
        List<NucleusFunctionSpaceEvent> events = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceEvent());

        Mockito.when(functionSpaceEventRepository.saveAll(events)).thenReturn(events);

        functionSpaceEventService.save(events);

        Mockito.verify(functionSpaceEventRepository).saveAll(events);
    }

    @Test
    public void countByClientCodeAndPropertyCodeAndBookingIdAndStatusIsNot() {
        when(functionSpaceEventRepository.countByClientCodeAndPropertyCodeAndBookingIdAndStatusIsNot(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, "EventCancelled")).thenReturn(0);

        functionSpaceEventService.findCount(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, "EventCancelled");

        verify(functionSpaceEventRepository).countByClientCodeAndPropertyCodeAndBookingIdAndStatusIsNot(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, "EventCancelled");
    }
}
