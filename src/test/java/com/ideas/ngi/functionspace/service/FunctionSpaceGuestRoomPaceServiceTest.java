package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.nucleus.data.correlation.entity.NucleusCorrelationMetadata;
import com.ideas.ngi.functionspace.dto.FunctionSpaceGuestRoomPaceWithBookingInfoDto;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBooking;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBookingPace;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceGuestRoomPace;
import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceGuestRoomPaceRepository;
import com.ideas.ngi.nucleus.data.occupancy.service.CountHolder;
import com.ideas.ngi.nucleus.util.DateTimeUtil;
import org.apache.commons.collections.CollectionUtils;
import org.bson.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.CLIENT_CODE;
import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.PROPERTY_CODE;
import static com.ideas.ngi.nucleus.util.DateTimeUtil.toDate;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.DEFAULT_CONTEXT;

public class FunctionSpaceGuestRoomPaceServiceTest {
    private static final String EXPECTED_AGGREGATION_FOR_BOOKING_PER_DAY_COUNT = "{\"aggregate\": \"__collection__\", \"pipeline\": [{\"$match\": {\"clientCode\": \"SANDBOX\", \"propertyCode\": \"AHWS01\", \"$and\": [{\"lastModifiedDate\": {\"$lt\": {\"$date\": 1516492800000}}}, {\"lastModifiedDate\": {\"$gte\": {\"$date\": 1514764800000}}}]}}, {\"$project\": {\"clientCode\": 1, \"propertyCode\": 1, \"bookingId\": 1, \"roomType\": 1, \"stayDate\": 1, \"bookingPaceId\": 1, \"roomNights\": 1, \"roomRevenue\": 1, \"roomNightsChanged\": 1, \"roomRevenueChanged\": 1, \"blockedRoomNightsChanged\": 1, \"blockedRoomRevenueChanged\": 1, \"contractedRoomNightsChanged\": 1, \"contractedRoomRevenueChanged\": 1, \"changeDate\": 1, \"guestRoomStatus\": 1, \"lastModifiedDate\": 1, \"OffsetAdjustedTimeInHour\": {\"$hour\": {\"date\": \"$changeDate\", \"timezone\": \"UTC\"}}, \"OffsetAdjustedTimeInMinute\": {\"$minute\": {\"date\": \"$changeDate\", \"timezone\": \"UTC\"}}}}, {\"$project\": {\"clientCode\": 1, \"propertyCode\": 1, \"bookingId\": 1, \"roomType\": 1, \"stayDate\": 1, \"bookingPaceId\": 1, \"roomNights\": 1, \"roomRevenue\": 1, \"roomNightsChanged\": 1, \"roomRevenueChanged\": 1, \"blockedRoomNightsChanged\": 1, \"blockedRoomRevenueChanged\": 1, \"contractedRoomNightsChanged\": 1, \"contractedRoomRevenueChanged\": 1, \"changeDate\": 1, \"guestRoomStatus\": 1, \"lastModifiedDate\": 1, \"changeDateWithoutTime\": {\"$subtract\": [\"$changeDate\", {\"$add\": [{\"$multiply\": [\"$OffsetAdjustedTimeInHour\", 60, 60, 1000]}, {\"$multiply\": [\"$OffsetAdjustedTimeInMinute\", 60, 1000]}, {\"$multiply\": [{\"$second\": \"$changeDate\"}, 1000]}, {\"$millisecond\": \"$changeDate\"}]}]}}}, {\"$group\": {\"_id\": {\"clientCode\": \"$clientCode\", \"propertyCode\": \"$propertyCode\", \"bookingId\": \"$bookingId\", \"stayDate\": \"$stayDate\", \"changeDateWithoutTime\": \"$changeDateWithoutTime\"}, \"bookingId\": {\"$first\": \"$bookingId\"}, \"bookingPaceId\": {\"$first\": \"$bookingPaceId\"}, \"roomType\": {\"$first\": \"$roomType\"}, \"roomNightsChanged\": {\"$sum\": \"$roomNightsChanged\"}, \"roomRevenueChanged\": {\"$sum\": \"$roomRevenueChanged\"}, \"blockedRoomNightsChanged\": {\"$sum\": \"$blockedRoomNightsChanged\"}, \"blockedRoomRevenueChanged\": {\"$sum\": \"$blockedRoomRevenueChanged\"}, \"contractedRoomNightsChanged\": {\"$sum\": \"$contractedRoomNightsChanged\"}, \"contractedRoomRevenueChanged\": {\"$sum\": \"$contractedRoomRevenueChanged\"}, \"guestRoomStatus\": {\"$first\": \"$guestRoomStatus\"}, \"stayDate\": {\"$first\": \"$stayDate\"}, \"changeDate\": {\"$first\": \"$changeDate\"}}}, {\"$count\": \"count\"}], \"allowDiskUse\": true}";
    private static final String EXPECTED_AGGREGATION_FOR_LATEST_BOOKING_PER_DAY = "{\"aggregate\": \"__collection__\", \"pipeline\": [{\"$match\": {\"clientCode\": \"SANDBOX\", \"propertyCode\": \"AHWS01\", \"$and\": [{\"lastModifiedDate\": {\"$lt\": {\"$date\": 1516492800000}}}, {\"lastModifiedDate\": {\"$gte\": {\"$date\": 1514764800000}}}]}}, {\"$project\": {\"clientCode\": 1, \"propertyCode\": 1, \"bookingId\": 1, \"roomType\": 1, \"stayDate\": 1, \"bookingPaceId\": 1, \"roomNights\": 1, \"roomRevenue\": 1, \"roomNightsChanged\": 1, \"roomRevenueChanged\": 1, \"blockedRoomNightsChanged\": 1, \"blockedRoomRevenueChanged\": 1, \"contractedRoomNightsChanged\": 1, \"contractedRoomRevenueChanged\": 1, \"changeDate\": 1, \"guestRoomStatus\": 1, \"lastModifiedDate\": 1, \"OffsetAdjustedTimeInHour\": {\"$hour\": {\"date\": \"$changeDate\", \"timezone\": \"UTC\"}}, \"OffsetAdjustedTimeInMinute\": {\"$minute\": {\"date\": \"$changeDate\", \"timezone\": \"UTC\"}}}}, {\"$project\": {\"clientCode\": 1, \"propertyCode\": 1, \"bookingId\": 1, \"roomType\": 1, \"stayDate\": 1, \"bookingPaceId\": 1, \"roomNights\": 1, \"roomRevenue\": 1, \"roomNightsChanged\": 1, \"roomRevenueChanged\": 1, \"blockedRoomNightsChanged\": 1, \"blockedRoomRevenueChanged\": 1, \"contractedRoomNightsChanged\": 1, \"contractedRoomRevenueChanged\": 1, \"changeDate\": 1, \"guestRoomStatus\": 1, \"lastModifiedDate\": 1, \"changeDateWithoutTime\": {\"$subtract\": [\"$changeDate\", {\"$add\": [{\"$multiply\": [\"$OffsetAdjustedTimeInHour\", 60, 60, 1000]}, {\"$multiply\": [\"$OffsetAdjustedTimeInMinute\", 60, 1000]}, {\"$multiply\": [{\"$second\": \"$changeDate\"}, 1000]}, {\"$millisecond\": \"$changeDate\"}]}]}}}, {\"$sort\": {\"clientCode\": 1, \"propertyCode\": 1, \"bookingId\": 1, \"stayDate\": 1, \"changeDateWithoutTime\": 1, \"changeDate\": -1}}, {\"$group\": {\"_id\": {\"clientCode\": \"$clientCode\", \"propertyCode\": \"$propertyCode\", \"bookingId\": \"$bookingId\", \"stayDate\": \"$stayDate\", \"changeDateWithoutTime\": \"$changeDateWithoutTime\"}, \"bookingId\": {\"$first\": \"$bookingId\"}, \"bookingPaceId\": {\"$first\": \"$bookingPaceId\"}, \"roomType\": {\"$first\": \"$roomType\"}, \"roomNightsChanged\": {\"$sum\": \"$roomNightsChanged\"}, \"roomRevenueChanged\": {\"$sum\": \"$roomRevenueChanged\"}, \"blockedRoomNightsChanged\": {\"$sum\": \"$blockedRoomNightsChanged\"}, \"blockedRoomRevenueChanged\": {\"$sum\": \"$blockedRoomRevenueChanged\"}, \"contractedRoomNightsChanged\": {\"$sum\": \"$contractedRoomNightsChanged\"}, \"contractedRoomRevenueChanged\": {\"$sum\": \"$contractedRoomRevenueChanged\"}, \"guestRoomStatus\": {\"$first\": \"$guestRoomStatus\"}, \"stayDate\": {\"$first\": \"$stayDate\"}, \"changeDate\": {\"$first\": \"$changeDate\"}}}, {\"$sort\": {\"_id.clientCode\": 1, \"_id.propertyCode\": 1, \"changeDate\": 1, \"bookingId\": 1, \"stayDate\": 1}}, {\"$skip\": {\"$numberLong\": \"1\"}}, {\"$limit\": {\"$numberLong\": \"1\"}}], \"allowDiskUse\": true}";
    private static final LocalDate LOCAL_DATE_1_JAN_2018 = LocalDate.parse("2018-01-01");
    private static final LocalDate LOCAL_DATE_2_JAN_2018 = LocalDate.parse("2018-01-02");
    private static final String BOOKING_ID = "BookingId";
    private static final String ROOM_TYPE = "RoomType";
    private static final String BOOKING_PACE_ID = "BookingPaceId";
    private static final String GUESTROOM_PACE_WITH_BOOKING_INFO = "[FunctionSpaceGuestRoomPaceWithBookingInfoDto{clientCode='BSTN', propertyCode='H1', bookingId='000100-765', bookingPaceId='000100-765[0001]', stayDate=Tue Jan 02 00:00:00 UTC 2018, changeDate=Tue Jan 02 00:00:00 UTC 2018, marketSegmentCode='null', blockName='BLOCK_NAME-000100-765', arrivalDate=Mon Jan 01 00:00:00 UTC 2018, departureDate=Tue Jan 02 00:00:00 UTC 2018, status='PROS', insertDate=null, bookingType='null', opportunityCreateDate=null, cancellationDate=null, cutoffDate=null, roomNightsChanged=7, roomRevenueChanged=12.5, blockedRoomRevenueChanged=12.5, blockedRoomNightsChanged=7, contractedRoomRevenueChanged=12.5, contractedRoomNightsChanged=7}, FunctionSpaceGuestRoomPaceWithBookingInfoDto{clientCode='BSTN', propertyCode='H1', bookingId='000100-765', bookingPaceId='000100-765[0001]', stayDate=Tue Jan 02 00:00:00 UTC 2018, changeDate=Tue Jan 02 00:00:00 UTC 2018, marketSegmentCode='null', blockName='BLOCK_NAME-000100-765', arrivalDate=Mon Jan 01 00:00:00 UTC 2018, departureDate=Tue Jan 02 00:00:00 UTC 2018, status='PROS', insertDate=null, bookingType='null', opportunityCreateDate=null, cancellationDate=null, cutoffDate=null, roomNightsChanged=7, roomRevenueChanged=12.5, blockedRoomRevenueChanged=12.5, blockedRoomNightsChanged=7, contractedRoomRevenueChanged=12.5, contractedRoomNightsChanged=7}, FunctionSpaceGuestRoomPaceWithBookingInfoDto{clientCode='BSTN', propertyCode='H1', bookingId='000100-765', bookingPaceId='000100-765[0001]', stayDate=Tue Jan 02 00:00:00 UTC 2018, changeDate=Tue Jan 02 00:00:00 UTC 2018, marketSegmentCode='null', blockName='BLOCK_NAME-000100-765', arrivalDate=Mon Jan 01 00:00:00 UTC 2018, departureDate=Tue Jan 02 00:00:00 UTC 2018, status='PROS', insertDate=null, bookingType='null', opportunityCreateDate=null, cancellationDate=null, cutoffDate=null, roomNightsChanged=7, roomRevenueChanged=12.5, blockedRoomRevenueChanged=12.5, blockedRoomNightsChanged=7, contractedRoomRevenueChanged=12.5, contractedRoomNightsChanged=7}, FunctionSpaceGuestRoomPaceWithBookingInfoDto{clientCode='BSTN', propertyCode='H1', bookingId='000100-765', bookingPaceId='000100-765[0001]', stayDate=Wed Jan 03 00:00:00 UTC 2018, changeDate=Wed Jan 03 00:00:00 UTC 2018, marketSegmentCode='null', blockName='BLOCK_NAME-000100-765', arrivalDate=Mon Jan 01 00:00:00 UTC 2018, departureDate=Tue Jan 02 00:00:00 UTC 2018, status='PROS', insertDate=null, bookingType='null', opportunityCreateDate=null, cancellationDate=null, cutoffDate=null, roomNightsChanged=7, roomRevenueChanged=12.5, blockedRoomRevenueChanged=12.5, blockedRoomNightsChanged=7, contractedRoomRevenueChanged=12.5, contractedRoomNightsChanged=7}, FunctionSpaceGuestRoomPaceWithBookingInfoDto{clientCode='BSTN', propertyCode='H1', bookingId='000100-765', bookingPaceId='000100-765[0001]', stayDate=Wed Jan 03 00:00:00 UTC 2018, changeDate=Wed Jan 03 00:00:00 UTC 2018, marketSegmentCode='null', blockName='BLOCK_NAME-000100-765', arrivalDate=Mon Jan 01 00:00:00 UTC 2018, departureDate=Tue Jan 02 00:00:00 UTC 2018, status='PROS', insertDate=null, bookingType='null', opportunityCreateDate=null, cancellationDate=null, cutoffDate=null, roomNightsChanged=7, roomRevenueChanged=12.5, blockedRoomRevenueChanged=12.5, blockedRoomNightsChanged=7, contractedRoomRevenueChanged=12.5, contractedRoomNightsChanged=7}, FunctionSpaceGuestRoomPaceWithBookingInfoDto{clientCode='BSTN', propertyCode='H1', bookingId='000100-765', bookingPaceId='000100-765[00043]', stayDate=Sat Jan 06 00:00:00 UTC 2018, changeDate=Sat Jan 06 00:00:00 UTC 2018, marketSegmentCode='null', blockName='BLOCK_NAME-000100-765', arrivalDate=Mon Jan 01 00:00:00 UTC 2018, departureDate=Tue Jan 02 00:00:00 UTC 2018, status='PROS', insertDate=null, bookingType='null', opportunityCreateDate=null, cancellationDate=null, cutoffDate=null, roomNightsChanged=7, roomRevenueChanged=12.5, blockedRoomRevenueChanged=12.5, blockedRoomNightsChanged=7, contractedRoomRevenueChanged=12.5, contractedRoomNightsChanged=7}, FunctionSpaceGuestRoomPaceWithBookingInfoDto{clientCode='BSTN', propertyCode='H1', bookingId='000100-999', bookingPaceId='000100-765[0056]', stayDate=Tue Jan 02 00:00:00 UTC 2018, changeDate=Tue Jan 02 00:00:00 UTC 2018, marketSegmentCode='null', blockName='BLOCK_NAME-000100-999', arrivalDate=Mon Jan 01 00:00:00 UTC 2018, departureDate=Tue Jan 02 00:00:00 UTC 2018, status='PROS', insertDate=null, bookingType='null', opportunityCreateDate=null, cancellationDate=null, cutoffDate=null, roomNightsChanged=7, roomRevenueChanged=12.5, blockedRoomRevenueChanged=12.5, blockedRoomNightsChanged=7, contractedRoomRevenueChanged=12.5, contractedRoomNightsChanged=7}, FunctionSpaceGuestRoomPaceWithBookingInfoDto{clientCode='BSTN', propertyCode='H1', bookingId='000100-999', bookingPaceId='000100-765[0056]', stayDate=Tue Jan 02 00:00:00 UTC 2018, changeDate=Tue Jan 02 00:00:00 UTC 2018, marketSegmentCode='null', blockName='BLOCK_NAME-000100-999', arrivalDate=Mon Jan 01 00:00:00 UTC 2018, departureDate=Tue Jan 02 00:00:00 UTC 2018, status='PROS', insertDate=null, bookingType='null', opportunityCreateDate=null, cancellationDate=null, cutoffDate=null, roomNightsChanged=7, roomRevenueChanged=12.5, blockedRoomRevenueChanged=12.5, blockedRoomNightsChanged=7, contractedRoomRevenueChanged=12.5, contractedRoomNightsChanged=7}]";
    public static final String GUEST_ROOM_STATUS = "PROS";

    @Mock
    private NucleusFunctionSpaceGuestRoomPaceRepository functionSpaceGuestRoomPaceRepository;

    @Mock
    private FunctionSpaceBookingService functionSpaceBookingService;

    @Mock
    private FunctionSpaceBookingPaceService spaceBookingPaceService;

    @Mock
    private MongoOperations mongoOperations;

    @InjectMocks
    private FunctionSpaceGuestRoomPaceService functionSpaceGuestRoomPaceService;
    private TimeZone actualTimeZone;

    @InjectMocks
    private NucleusCorrelationMetadata csvFileCorrelationMetadata;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        actualTimeZone = TimeZone.getDefault();
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
   }

    @After
    public void tearDown() {
        TimeZone.setDefault(actualTimeZone);
   }

    @Test
    public void findOne() {
        NucleusFunctionSpaceGuestRoomPace guestRoomPace = NucleusFunctionSpaceObjectMother.buildFunctionSpaceGuestRoomPace();

        when(functionSpaceGuestRoomPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateOrderByChangeDateDesc(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, ROOM_TYPE, null)).thenReturn(guestRoomPace);

        NucleusFunctionSpaceGuestRoomPace retGuestRoomPace = functionSpaceGuestRoomPaceService.findOne(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, ROOM_TYPE, null);

        assertSame(guestRoomPace, retGuestRoomPace);
        verify(functionSpaceGuestRoomPaceRepository).findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateOrderByChangeDateDesc(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, ROOM_TYPE, null);
   }

    @Test
    public void findOne_WithBookingPaceId() {
        NucleusFunctionSpaceGuestRoomPace guestRoomPace = NucleusFunctionSpaceObjectMother.buildFunctionSpaceGuestRoomPace();

        when(functionSpaceGuestRoomPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateAndBookingPaceId(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, ROOM_TYPE, null, BOOKING_PACE_ID)).thenReturn(guestRoomPace);

        NucleusFunctionSpaceGuestRoomPace retGuestRoomPace = functionSpaceGuestRoomPaceService.findOne(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, ROOM_TYPE, null, BOOKING_PACE_ID);

        assertSame(guestRoomPace, retGuestRoomPace);
        Mockito.verify(functionSpaceGuestRoomPaceRepository).findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateAndBookingPaceId(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, ROOM_TYPE, null, BOOKING_PACE_ID);
   }

    @Test
    public void findAll() {
        List<NucleusFunctionSpaceGuestRoomPace> guestRoomPaces = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceGuestRoomPace());

        when(functionSpaceGuestRoomPaceRepository.findByClientCodeAndPropertyCodeOrderByBookingIdAscBookingPaceIdAsc(CLIENT_CODE, PROPERTY_CODE)).thenReturn(guestRoomPaces);

        List<NucleusFunctionSpaceGuestRoomPace> retGuestRoomPaces = functionSpaceGuestRoomPaceService.findAll(CLIENT_CODE, PROPERTY_CODE);

        assertTrue(CollectionUtils.isNotEmpty(retGuestRoomPaces));
        assertSame(guestRoomPaces.get(0), retGuestRoomPaces.get(0));

        verify(functionSpaceGuestRoomPaceRepository).findByClientCodeAndPropertyCodeOrderByBookingIdAscBookingPaceIdAsc(CLIENT_CODE, PROPERTY_CODE);
   }

    @Test
    public void delete() {
        when(functionSpaceGuestRoomPaceRepository.deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE)).thenReturn(1L);

        functionSpaceGuestRoomPaceService.delete(CLIENT_CODE, PROPERTY_CODE);

        verify(functionSpaceGuestRoomPaceRepository).deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE);
   }

    @Test
    public void findAllByDateRange() {
        LocalDateTime startDate = LocalDate.of(2017, 10, 1).atStartOfDay();
        LocalDateTime endDate = LocalDate.of(2017, 10, 31).atStartOfDay();
        PageRequest pageRequest = new PageRequest(1, 1);
        List<NucleusFunctionSpaceGuestRoomPace> guestRoomPaces = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceGuestRoomPace());
        Page<NucleusFunctionSpaceGuestRoomPace> page = new PageImpl<>(guestRoomPaces);

        when(functionSpaceGuestRoomPaceRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest)).thenReturn(page);

        Page<NucleusFunctionSpaceGuestRoomPace> guestRoomPacePages = functionSpaceGuestRoomPaceService.findAllByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);

        assertNotNull(guestRoomPacePages);
        assertEquals(1, guestRoomPacePages.getTotalElements());

        verify(functionSpaceGuestRoomPaceRepository).findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
   }

    @Test
    public void savePaceCallsRepositoryMethod() {
        NucleusFunctionSpaceGuestRoomPace pace = new NucleusFunctionSpaceGuestRoomPace();
        when(functionSpaceGuestRoomPaceRepository.save(pace)).thenReturn(pace);

        assertSame(pace, functionSpaceGuestRoomPaceRepository.save(pace));

        verify(functionSpaceGuestRoomPaceRepository).save(pace);
   }

    @Test
    public void getLatestBookingPerDayByGroup() {
        String clientCode = "SANDBOX";
        String propertyCode = "AHWS01";
        LocalDate localDate = LOCAL_DATE_1_JAN_2018;
        Date startDate = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date endDate = Date.from(localDate.plusDays(20).atStartOfDay(ZoneId.systemDefault()).toInstant());
        ArgumentCaptor<Aggregation> aggregationArgumentCaptor = ArgumentCaptor.forClass(Aggregation.class);
        when(mongoOperations.aggregate(aggregationArgumentCaptor.capture(),
                eq(NucleusFunctionSpaceGuestRoomPace.class),
                eq(NucleusFunctionSpaceGuestRoomPace.class)))
                .thenReturn(new AggregationResults<>(Collections.emptyList(), new Document()));
        functionSpaceGuestRoomPaceService.getChangesPerBooking(clientCode, propertyCode, startDate, endDate, 1, 1);
        Aggregation aggregation = aggregationArgumentCaptor.getValue();
        String actual = aggregation.toDocument("__collection__", DEFAULT_CONTEXT).toJson();
        assertEquals(EXPECTED_AGGREGATION_FOR_LATEST_BOOKING_PER_DAY, actual);
   }

    @Test
    public void getChangesPerBookingCount() {
        String clientCode = "SANDBOX";
        String propertyCode = "AHWS01";
        LocalDate localDate = LOCAL_DATE_1_JAN_2018;
        Date startDate = DateTimeUtil.toDate(localDate);
        Date endDate = DateTimeUtil.toDate(localDate.plusDays(20));
        ArgumentCaptor<Aggregation> aggregationArgumentCaptor = ArgumentCaptor.forClass(Aggregation.class);
        when(mongoOperations.aggregate(aggregationArgumentCaptor.capture(), eq(NucleusFunctionSpaceGuestRoomPace.class), eq(CountHolder.class)))
                .thenReturn(new AggregationResults<>(Collections.emptyList(), new Document()));
        functionSpaceGuestRoomPaceService.getChangesPerBookingCount(clientCode, propertyCode, startDate, endDate);
        Aggregation aggregation = aggregationArgumentCaptor.getValue();
        assertEquals(EXPECTED_AGGREGATION_FOR_BOOKING_PER_DAY_COUNT, aggregation.toDocument("__collection__", DEFAULT_CONTEXT).toJson());
   }

    @Test
    public void getFunctionSpaceGuestRoomPaceWithBookingInfo() {
        String clientCode = "BSTN";
        String propertyCode = "H1";
        List<NucleusFunctionSpaceGuestRoomPace> guestRoomPaces = prepareDataGuestRoomPaces(LOCAL_DATE_1_JAN_2018);
        List<String> bookingIds = Arrays.asList("000100-765", "000100-999");
        List<NucleusFunctionSpaceBooking> bookings = new ArrayList<>();
        bookingIds.forEach(bookingId -> {
            NucleusFunctionSpaceBooking booking = new NucleusFunctionSpaceBooking();
            booking.setBookingId(bookingId);
            booking.setBlockName("BLOCK_NAME-" + bookingId);
            booking.setArrivalDate(toDate(LOCAL_DATE_1_JAN_2018));
            booking.setDepartureDate(toDate(LOCAL_DATE_2_JAN_2018));
            bookings.add(booking);
       });
        when(functionSpaceBookingService.findByClientCodeAndPropertyCodeAndBookingIdIn(clientCode, propertyCode, bookingIds)).thenReturn(bookings);
        List<FunctionSpaceGuestRoomPaceWithBookingInfoDto> functionSpaceGuestRoomPaceWithBookingInfo =
                functionSpaceGuestRoomPaceService.getFunctionSpaceGuestRoomPaceWithBookingInfo(clientCode, propertyCode, guestRoomPaces);
        functionSpaceGuestRoomPaceWithBookingInfo.sort(Comparator.comparing(b -> (b.getBookingId() + b.getBookingPaceId() + b.getStayDate())));
        assertEquals(GUESTROOM_PACE_WITH_BOOKING_INFO, functionSpaceGuestRoomPaceWithBookingInfo.toString());
   }

    @Test
    public void getFunctionSpaceGuestRoomPaceWithBookingInfoWithBookingPace() {
        String clientCode = "BSTN";
        String propertyCode = "H1";
        List<NucleusFunctionSpaceGuestRoomPace> guestRoomPaces = prepareDataGuestRoomPaces(LOCAL_DATE_1_JAN_2018);
        String bookingId1 = "000100-765";
        List<String> bookingIds = Arrays.asList(bookingId1, "000100-999");
        List<NucleusFunctionSpaceBooking> bookings = new ArrayList<>();
        bookingIds.forEach(bookingId -> {
            NucleusFunctionSpaceBooking booking = new NucleusFunctionSpaceBooking();
            booking.setBookingId(bookingId);
            booking.setBlockName("BLOCK_NAME-" + bookingId);
            booking.setArrivalDate(toDate(LOCAL_DATE_1_JAN_2018));
            booking.setDepartureDate(toDate(LOCAL_DATE_2_JAN_2018));
            bookings.add(booking);
       });
        NucleusFunctionSpaceBookingPace bookingPace = new NucleusFunctionSpaceBookingPace();
        bookingPace.setBookingId(bookingId1);
        Date arrivalDate = toDate(LocalDate.of(2019, 10, 1));
        Date departureDate = toDate(LocalDate.of(2019, 10, 3));
        bookingPace.setArrivalDate(arrivalDate);
        bookingPace.setDepartureDate(departureDate);
        when(spaceBookingPaceService.findOne(clientCode, propertyCode, bookingId1, toDate(LOCAL_DATE_2_JAN_2018))).thenReturn(bookingPace);
        when(functionSpaceBookingService.findByClientCodeAndPropertyCodeAndBookingIdIn(clientCode, propertyCode, bookingIds)).thenReturn(bookings);
        List<FunctionSpaceGuestRoomPaceWithBookingInfoDto> functionSpaceGuestRoomPaceWithBookingInfo =
                functionSpaceGuestRoomPaceService.getFunctionSpaceGuestRoomPaceWithBookingInfo(clientCode, propertyCode, guestRoomPaces);
        functionSpaceGuestRoomPaceWithBookingInfo.sort(Comparator.comparing(b -> (b.getBookingId() + b.getBookingPaceId() + b.getStayDate())));
        verify(spaceBookingPaceService, Mockito.times(1)).findOne(clientCode, propertyCode, bookingId1, toDate(LOCAL_DATE_2_JAN_2018));
        functionSpaceGuestRoomPaceWithBookingInfo.forEach(dto -> {
            if (dto.getBookingId().equals(bookingId1) && dto.getClientCode().equals(clientCode)
                    && dto.getPropertyCode().equals(propertyCode) && dto.getChangeDate().equals(toDate(LOCAL_DATE_2_JAN_2018))) {
                assertEquals(dto.getArrivalDate(), arrivalDate);
                assertEquals(dto.getDepartureDate(), departureDate);
           }
       });
   }

    private List<NucleusFunctionSpaceGuestRoomPace> prepareDataGuestRoomPaces(LocalDate localDate) {
        List<NucleusFunctionSpaceGuestRoomPace> guestRooms = new ArrayList<>();
        guestRooms.add(getNucleusFunctionSpaceGuestRoomPace("BSTN", "H1", "000100-765",
                "000100-765[0001]", toDate(localDate.plusDays(1)), 7,
                BigDecimal.valueOf(12.5), "SINGLE_ROOM"));
        guestRooms.add(getNucleusFunctionSpaceGuestRoomPace("BSTN", "H1", "000100-765",
                "000100-765[0001]", toDate(localDate.plusDays(1)), 7,
                BigDecimal.valueOf(12.5), "Double_ROOM"));
        guestRooms.add(getNucleusFunctionSpaceGuestRoomPace("BSTN", "H1", "000100-765",
                "000100-765[0001]", toDate(localDate.plusDays(2)), 7,
                BigDecimal.valueOf(12.5), "SINGLE_ROOM"));
        guestRooms.add(getNucleusFunctionSpaceGuestRoomPace("BSTN", "H1", "000100-765",
                "000100-765[0001]", toDate(localDate.plusDays(1)), 7,
                BigDecimal.valueOf(12.5), "Suite_ROOM"));
        guestRooms.add(getNucleusFunctionSpaceGuestRoomPace("BSTN", "H1", "000100-765",
                "000100-765[0001]", toDate(localDate.plusDays(2)), 7,
                BigDecimal.valueOf(12.5), "Double_ROOM"));
        guestRooms.add(getNucleusFunctionSpaceGuestRoomPace("BSTN", "H1", "000100-765",
                "000100-765[00043]", toDate(localDate.plusDays(5)), 7,
                BigDecimal.valueOf(12.5), "SINGLE_ROOM"));
        guestRooms.add(getNucleusFunctionSpaceGuestRoomPace("BSTN", "H1", "000100-999",
                "000100-765[0056]", toDate(localDate.plusDays(1)), 7,
                BigDecimal.valueOf(12.5), "SINGLE_ROOM"));
        guestRooms.add(getNucleusFunctionSpaceGuestRoomPace("BSTN", "H1", "000100-999",
                "000100-765[0056]", toDate(localDate.plusDays(1)), 7,
                BigDecimal.valueOf(12.5), "DOUBLE_ROOM"));
        return guestRooms;
   }

    private NucleusFunctionSpaceGuestRoomPace getNucleusFunctionSpaceGuestRoomPace(String clientCode, String propertyCode, String bookingId,
                                                                                   String bookingPaceId, Date stayDate,
                                                                                   int roomNightsChanged,
                                                                                   BigDecimal roomRevenueChanged, String roomType) {
        NucleusFunctionSpaceGuestRoomPace guestRoom = new NucleusFunctionSpaceGuestRoomPace();
        guestRoom.setClientCode(clientCode);
        guestRoom.setPropertyCode(propertyCode);
        guestRoom.setBookingId(bookingId);
        guestRoom.setBookingPaceId(bookingPaceId);
        guestRoom.setStayDate(stayDate);
        guestRoom.setChangeDate(stayDate);
        guestRoom.setRoomNightsChanged(roomNightsChanged);
        guestRoom.setRoomRevenueChanged(roomRevenueChanged);
        guestRoom.setBlockedRoomNightsChanged(roomNightsChanged);
        guestRoom.setBlockedRoomRevenueChanged(roomRevenueChanged);
        guestRoom.setContractedRoomNightsChanged(roomNightsChanged);
        guestRoom.setContractedRoomRevenueChanged(roomRevenueChanged);
        guestRoom.setRoomType(roomType);
        guestRoom.setGuestRoomStatus(GUEST_ROOM_STATUS);
        return guestRoom;
   }

    @Test
    public void saveAll() {
        List<NucleusFunctionSpaceGuestRoomPace> nucleusFunctionSpaceGuestRoomPaces = new ArrayList<>();
        NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace = new NucleusFunctionSpaceGuestRoomPace();
        nucleusFunctionSpaceGuestRoomPace.setRoomNights(10);
        nucleusFunctionSpaceGuestRoomPace.setBlockedRoomNights(10);
        nucleusFunctionSpaceGuestRoomPace.setContractedRoomNights(10);
        nucleusFunctionSpaceGuestRoomPace.setRoomRevenue(new BigDecimal(10));
        nucleusFunctionSpaceGuestRoomPace.setBlockedRoomRevenue(new BigDecimal(10));
        nucleusFunctionSpaceGuestRoomPace.setContractedRoomRevenue(new BigDecimal(10));
        nucleusFunctionSpaceGuestRoomPaces.add(nucleusFunctionSpaceGuestRoomPace);
        NucleusFunctionSpaceGuestRoomPace existingRecord = new NucleusFunctionSpaceGuestRoomPace();
        existingRecord.setRoomNights(20);
        Mockito.when(functionSpaceGuestRoomPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateAndChangeDateLessThanOrderByChangeDateDesc(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(existingRecord);
        Mockito.when(functionSpaceGuestRoomPaceRepository.saveAll(nucleusFunctionSpaceGuestRoomPaces)).thenReturn(nucleusFunctionSpaceGuestRoomPaces);
        functionSpaceGuestRoomPaceService.prepareAll(nucleusFunctionSpaceGuestRoomPaces);
        verify(functionSpaceGuestRoomPaceRepository, atLeastOnce()).findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateAndChangeDateLessThanOrderByChangeDateDesc(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
        verify(functionSpaceGuestRoomPaceRepository, atLeastOnce()).saveAll(anySet());
        assertEquals(new BigDecimal("10"), new BigDecimal(nucleusFunctionSpaceGuestRoomPace.getRoomNights()).multiply(new BigDecimal(1)));
        assertEquals(new BigDecimal("10"), new BigDecimal(nucleusFunctionSpaceGuestRoomPace.getContractedRoomNights()).multiply(new BigDecimal(1)));
   }

    @Test
    public void saveAllValidationTest() {
        NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace = getNucleusFunctionGuestRoomPace();
        nucleusFunctionSpaceGuestRoomPace.setRoomNights(null);
        nucleusFunctionSpaceGuestRoomPace.setContractedRoomNights(null);
        List<NucleusFunctionSpaceGuestRoomPace> nucleusFunctionSpaceGuestRoomPaces = new ArrayList<>();
        nucleusFunctionSpaceGuestRoomPaces.add(nucleusFunctionSpaceGuestRoomPace);

        ArgumentCaptor<Set> saveableNFSGuestRoomPaces = ArgumentCaptor.forClass(Set.class);

        functionSpaceGuestRoomPaceService.prepareAll(nucleusFunctionSpaceGuestRoomPaces);

        Mockito.verify(functionSpaceGuestRoomPaceRepository, Mockito.atLeastOnce()).saveAll(saveableNFSGuestRoomPaces.capture());
        assertWhenExistingRecordNull(saveableNFSGuestRoomPaces);


        NucleusFunctionSpaceGuestRoomPace nFSpaceGuestRoomPace = getNucleusFunctionGuestRoomPace();
        nFSpaceGuestRoomPace.setRoomNights(20);
        nFSpaceGuestRoomPace.setContractedRoomNights(null);

        List<NucleusFunctionSpaceGuestRoomPace> nFSpaceGuestRoomPaces = new ArrayList<>();
        nFSpaceGuestRoomPaces.add(nFSpaceGuestRoomPace);

        ArgumentCaptor<Set> saveableFSGuestRoomPaces = ArgumentCaptor.forClass(Set.class);

        functionSpaceGuestRoomPaceService.prepareAll(nFSpaceGuestRoomPaces);
        Mockito.verify(functionSpaceGuestRoomPaceRepository, Mockito.atLeastOnce()).saveAll(saveableFSGuestRoomPaces.capture());
        assertWhenContractedRoomNightsNull(saveableFSGuestRoomPaces);

        NucleusFunctionSpaceGuestRoomPace functionGuestRoomPace = getNucleusFunctionGuestRoomPace();
        functionGuestRoomPace.setRoomNights(null);

        List<NucleusFunctionSpaceGuestRoomPace> nucleusFunctionSpaceGuestRoomPaceList = new ArrayList<>();
        nucleusFunctionSpaceGuestRoomPaceList.add(functionGuestRoomPace);

        ArgumentCaptor<Set> setArgumentCaptor = ArgumentCaptor.forClass(Set.class);

        functionSpaceGuestRoomPaceService.prepareAll(nucleusFunctionSpaceGuestRoomPaceList);
        Mockito.verify(functionSpaceGuestRoomPaceRepository, Mockito.atLeastOnce()).saveAll(setArgumentCaptor.capture());
        assertWhenRoomNightsAndContractedRoomNightsNotNull(setArgumentCaptor);

   }

    @Test
    public void validateCalculationPaceDataWhenContractedRoomNightsAndRoomNightsNotNull() {
        NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace = getNucleusFunctionGuestRoomPace();
        nucleusFunctionSpaceGuestRoomPace.setClientCode("LOEWS");
        List<NucleusFunctionSpaceGuestRoomPace> nucleusFunctionSpaceGuestRoomPaces = new ArrayList<>();
        nucleusFunctionSpaceGuestRoomPaces.add(nucleusFunctionSpaceGuestRoomPace);

        NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPaceExisting = getNucleusFunctionGuestRoomPace();
        when(functionSpaceGuestRoomPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateAndChangeDateLessThanOrderByChangeDateDesc(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(nucleusFunctionSpaceGuestRoomPaceExisting);
        ArgumentCaptor<Set> saveableNFSGuestRoomPaces = ArgumentCaptor.forClass(Set.class);

        functionSpaceGuestRoomPaceService.prepareAll(nucleusFunctionSpaceGuestRoomPaces);

        Mockito.verify(functionSpaceGuestRoomPaceRepository, Mockito.atLeastOnce()).saveAll(saveableNFSGuestRoomPaces.capture());
        assertContractedRoomNightsAndRoomNightsNotNull(saveableNFSGuestRoomPaces);

   }

    private void assertContractedRoomNightsAndRoomNightsNotNull(ArgumentCaptor<Set> saveableNFSGuestRoomPaces) {
        Set<NucleusFunctionSpaceGuestRoomPace> nFSGuestRoomPaces = (Set<NucleusFunctionSpaceGuestRoomPace>) saveableNFSGuestRoomPaces.getValue();
        nFSGuestRoomPaces.forEach(nfs -> {
            assertEquals(Integer.valueOf("20"), nfs.getRoomNights());
            assertEquals(Integer.valueOf("30"), nfs.getContractedRoomNights());
            assertEquals(new BigDecimal("1000.00"), nfs.getContractedRoomRevenue());
            assertEquals(new BigDecimal("500.00"), nfs.getRoomRevenue());
            assertEquals(new BigDecimal("0.00"), nfs.getContractedRoomRevenueChanged());
            assertEquals(new BigDecimal("0.00"), nfs.getRoomRevenueChanged());
       });
   }

    @Test
    public void validatePaceDataCalculationWhenContractedRoomNightsNull() {
        NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace = getNucleusFunctionGuestRoomPace();
        nucleusFunctionSpaceGuestRoomPace.setClientCode("LOEWS");
        nucleusFunctionSpaceGuestRoomPace.setContractedRoomNights(null);
        List<NucleusFunctionSpaceGuestRoomPace> nucleusFunctionSpaceGuestRoomPaces = new ArrayList<>();
        nucleusFunctionSpaceGuestRoomPaces.add(nucleusFunctionSpaceGuestRoomPace);

        NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPaceExisting = getNucleusFunctionGuestRoomPace();
        nucleusFunctionSpaceGuestRoomPaceExisting.setContractedRoomNights(null);
        nucleusFunctionSpaceGuestRoomPaceExisting.setContractedRoomRevenue(null);
        nucleusFunctionSpaceGuestRoomPaceExisting.setContractedRoomRevenueChanged(null);
        when(functionSpaceGuestRoomPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateAndChangeDateLessThanOrderByChangeDateDesc(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(nucleusFunctionSpaceGuestRoomPaceExisting);
        ArgumentCaptor<Set> saveableNFSGuestRoomPaces = ArgumentCaptor.forClass(Set.class);

        functionSpaceGuestRoomPaceService.prepareAll(nucleusFunctionSpaceGuestRoomPaces);

        Mockito.verify(functionSpaceGuestRoomPaceRepository, Mockito.atLeastOnce()).saveAll(saveableNFSGuestRoomPaces.capture());
        assertContractedRoomNightsNull(saveableNFSGuestRoomPaces);

   }

    private void assertContractedRoomNightsNull(ArgumentCaptor<Set> saveableNFSGuestRoomPaces) {
        Set<NucleusFunctionSpaceGuestRoomPace> nFSGuestRoomPaces = (Set<NucleusFunctionSpaceGuestRoomPace>) saveableNFSGuestRoomPaces.getValue();
        nFSGuestRoomPaces.forEach(nfs -> {

            assertEquals(Integer.valueOf("10"), nfs.getBlockedRoomNights());
            assertEquals(new BigDecimal("499.00"), nfs.getBlockedRoomRevenue());
            assertEquals(Integer.valueOf("0"), nfs.getBlockedRoomNightsChanged());
            assertEquals(new BigDecimal("0.00"), nfs.getBlockedRoomRevenueChanged());

            assertEquals(Integer.valueOf("20"), nfs.getRoomNights());
            assertEquals(new BigDecimal("500.00"), nfs.getRoomRevenue());
            assertEquals(Integer.valueOf("0"), nfs.getRoomNightsChanged());
            assertEquals(new BigDecimal("0.00"), nfs.getRoomRevenueChanged());

            assertEquals(Integer.valueOf("10"), nfs.getContractedRoomNights());
            assertEquals(new BigDecimal("0"), nfs.getContractedRoomRevenue());
            assertEquals(Integer.valueOf("10"), nfs.getContractedRoomNightsChanged());
            assertEquals(new BigDecimal("0"), nfs.getContractedRoomRevenueChanged());
       });
   }

    @Test
    public void validatePaceDataCalculationWhenContractedRoomNightsAndRoomNightsNull() {
        NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace = getNucleusFunctionGuestRoomPace();
        nucleusFunctionSpaceGuestRoomPace.setClientCode("LOEWS");
        nucleusFunctionSpaceGuestRoomPace.setContractedRoomNights(null);
        nucleusFunctionSpaceGuestRoomPace.setRoomNights(null);
        List<NucleusFunctionSpaceGuestRoomPace> nucleusFunctionSpaceGuestRoomPaces = new ArrayList<>();
        nucleusFunctionSpaceGuestRoomPaces.add(nucleusFunctionSpaceGuestRoomPace);

        NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPaceExisting = getNucleusFunctionGuestRoomPace();
        when(functionSpaceGuestRoomPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateAndChangeDateLessThanOrderByChangeDateDesc(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(nucleusFunctionSpaceGuestRoomPaceExisting);
        ArgumentCaptor<Set> saveableNFSGuestRoomPaces = ArgumentCaptor.forClass(Set.class);

        functionSpaceGuestRoomPaceService.prepareAll(nucleusFunctionSpaceGuestRoomPaces);

        Mockito.verify(functionSpaceGuestRoomPaceRepository, Mockito.atLeastOnce()).saveAll(saveableNFSGuestRoomPaces.capture());
        assertWhenContractedRoomNightsAndRoomNightsNull(saveableNFSGuestRoomPaces);

   }

    private void assertWhenContractedRoomNightsAndRoomNightsNull(ArgumentCaptor<Set> saveableNFSGuestRoomPaces) {
        Set<NucleusFunctionSpaceGuestRoomPace> nFSGuestRoomPaces = (Set<NucleusFunctionSpaceGuestRoomPace>) saveableNFSGuestRoomPaces.getValue();
        nFSGuestRoomPaces.forEach(nfs -> {

            assertEquals(Integer.valueOf("10"), nfs.getBlockedRoomNights());
            assertEquals(new BigDecimal("499.00"), nfs.getBlockedRoomRevenue());
            assertEquals(Integer.valueOf("0"), nfs.getBlockedRoomNightsChanged());
            assertEquals(new BigDecimal("0.00"), nfs.getBlockedRoomRevenueChanged());

            assertEquals(Integer.valueOf("20"), nfs.getRoomNights());
            assertEquals(new BigDecimal("500.00"), nfs.getRoomRevenue());
            assertEquals(Integer.valueOf("0"), nfs.getRoomNightsChanged());
            assertEquals(new BigDecimal("0.00"), nfs.getRoomRevenueChanged());

            assertEquals(Integer.valueOf("30"), nfs.getContractedRoomNights());
            assertEquals(new BigDecimal("1000.00"), nfs.getContractedRoomRevenue());
            assertEquals(Integer.valueOf("0"), nfs.getContractedRoomNightsChanged());
            assertEquals(new BigDecimal("0.00"), nfs.getContractedRoomRevenueChanged());

       });
   }

    @Test
    public void validatePaceDataCalculationWhenRoomNightsNull() {
        NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace = getNucleusFunctionGuestRoomPace();
        nucleusFunctionSpaceGuestRoomPace.setClientCode("LOEWS");
        nucleusFunctionSpaceGuestRoomPace.setRoomNights(null);
        List<NucleusFunctionSpaceGuestRoomPace> nucleusFunctionSpaceGuestRoomPaces = new ArrayList<>();
        nucleusFunctionSpaceGuestRoomPaces.add(nucleusFunctionSpaceGuestRoomPace);

        NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPaceExisting = getNucleusFunctionGuestRoomPace();
        when(functionSpaceGuestRoomPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndRoomTypeAndStayDateAndChangeDateLessThanOrderByChangeDateDesc(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(nucleusFunctionSpaceGuestRoomPaceExisting);
        ArgumentCaptor<Set> saveableNFSGuestRoomPaces = ArgumentCaptor.forClass(Set.class);

        functionSpaceGuestRoomPaceService.prepareAll(nucleusFunctionSpaceGuestRoomPaces);

        Mockito.verify(functionSpaceGuestRoomPaceRepository, Mockito.atLeastOnce()).saveAll(saveableNFSGuestRoomPaces.capture());
        assertRoomNightsNull(saveableNFSGuestRoomPaces);

   }

    private void assertRoomNightsNull(ArgumentCaptor<Set> saveableNFSGuestRoomPaces) {
        Set<NucleusFunctionSpaceGuestRoomPace> nFSGuestRoomPaces = (Set<NucleusFunctionSpaceGuestRoomPace>) saveableNFSGuestRoomPaces.getValue();
        nFSGuestRoomPaces.forEach(nfs -> {

            assertEquals(Integer.valueOf("10"), nfs.getBlockedRoomNights());
            assertEquals(new BigDecimal("499.00"), nfs.getBlockedRoomRevenue());
            assertEquals(Integer.valueOf("0"), nfs.getBlockedRoomNightsChanged());
            assertEquals(new BigDecimal("0.00"), nfs.getBlockedRoomRevenueChanged());

            assertEquals(Integer.valueOf("20"), nfs.getRoomNights());
            assertEquals(new BigDecimal("500.00"), nfs.getRoomRevenue());
            assertEquals(Integer.valueOf("0"), nfs.getRoomNightsChanged());
            assertEquals(new BigDecimal("0.00"), nfs.getRoomRevenueChanged());

            assertEquals(Integer.valueOf("30"), nfs.getContractedRoomNights());
            assertEquals(new BigDecimal("1000.00"), nfs.getContractedRoomRevenue());
            assertEquals(Integer.valueOf("0"), nfs.getContractedRoomNightsChanged());
            assertEquals(new BigDecimal("0.00"), nfs.getContractedRoomRevenueChanged());


       });

   }

    private void assertWhenRoomNightsAndContractedRoomNightsNotNull(ArgumentCaptor<Set> saveableFSGuestRoomPaces) {
        Set<NucleusFunctionSpaceGuestRoomPace> nFSGuestRoomPaces = (Set<NucleusFunctionSpaceGuestRoomPace>) saveableFSGuestRoomPaces.getValue();
        nFSGuestRoomPaces.forEach(nfs -> {
            assertEquals(Integer.valueOf("10"), nfs.getRoomNights());
            assertEquals(Integer.valueOf("30"), nfs.getContractedRoomNights());
       });
   }

    private void assertWhenContractedRoomNightsNull(ArgumentCaptor<Set> saveableFSGuestRoomPaces) {
        Set<NucleusFunctionSpaceGuestRoomPace> nFSGuestRoomPaces = (Set<NucleusFunctionSpaceGuestRoomPace>) saveableFSGuestRoomPaces.getValue();
        nFSGuestRoomPaces.forEach(nfs -> {
            assertEquals(Integer.valueOf("20"), nfs.getRoomNights());
            assertEquals(Integer.valueOf("10"), nfs.getContractedRoomNights());
       });
   }

    private void assertWhenExistingRecordNull(ArgumentCaptor<Set> saveableNFSGuestRoomPaces) {
        Set<NucleusFunctionSpaceGuestRoomPace> nFSGuestRoomPaces = (Set<NucleusFunctionSpaceGuestRoomPace>) saveableNFSGuestRoomPaces.getValue();
        nFSGuestRoomPaces.forEach(nfs -> {
            assertEquals(Integer.valueOf("10"), nfs.getRoomNights());
            assertEquals(Integer.valueOf("10"), nfs.getContractedRoomNights());
       });
   }

    private NucleusFunctionSpaceGuestRoomPace getNucleusFunctionGuestRoomPace() {
        NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace = new NucleusFunctionSpaceGuestRoomPace();
        nucleusFunctionSpaceGuestRoomPace.setCorrelationId(csvFileCorrelationMetadata.getId());
        nucleusFunctionSpaceGuestRoomPace.setClientCode(csvFileCorrelationMetadata.getClientCode());
        nucleusFunctionSpaceGuestRoomPace.setPropertyCode(csvFileCorrelationMetadata.getPropertyCode());
        nucleusFunctionSpaceGuestRoomPace.setBookingPaceId("1");
        nucleusFunctionSpaceGuestRoomPace.setPropertyId("2");

        nucleusFunctionSpaceGuestRoomPace.setBlockedRoomNights(10);
        nucleusFunctionSpaceGuestRoomPace.setBlockedRoomRevenue(new BigDecimal("499.00"));
        nucleusFunctionSpaceGuestRoomPace.setBlockedRoomRevenueChanged(new BigDecimal("499.0"));

        nucleusFunctionSpaceGuestRoomPace.setRoomNights(20);
        nucleusFunctionSpaceGuestRoomPace.setRoomRevenue(new BigDecimal("500.00"));
        nucleusFunctionSpaceGuestRoomPace.setRoomRevenueChanged(new BigDecimal("500.00"));

        nucleusFunctionSpaceGuestRoomPace.setContractedRoomNights(30);
        nucleusFunctionSpaceGuestRoomPace.setContractedRoomRevenue(new BigDecimal("1000.00"));
        nucleusFunctionSpaceGuestRoomPace.setContractedRoomRevenueChanged(new BigDecimal("1000.00"));


        return nucleusFunctionSpaceGuestRoomPace;
   }

}