package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBookingPace;
import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceBookingPaceRepository;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.CLIENT_CODE;
import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.PROPERTY_CODE;
import static com.ideas.ngi.nucleus.util.DateTimeUtil.toDate;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class FunctionSpaceBookingPaceServiceTest {
    private static final String BOOKING_ID = "BookingId";
    private static final String BOOKING_PACE_ID = "BookingPaceId";

    @Mock
    private NucleusFunctionSpaceBookingPaceRepository functionSpaceBookingPaceRepository;

    @InjectMocks
    private FunctionSpaceBookingPaceService functionSpaceBookingPaceService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findOne() {
        NucleusFunctionSpaceBookingPace bookingPace = NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingPace();

        when(functionSpaceBookingPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndBookingPaceId(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, BOOKING_PACE_ID)).thenReturn(bookingPace);

        NucleusFunctionSpaceBookingPace retBookingPace = functionSpaceBookingPaceService.findOne(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, BOOKING_PACE_ID);

        assertSame(bookingPace, retBookingPace);
        verify(functionSpaceBookingPaceRepository).findFirstByClientCodeAndPropertyCodeAndBookingIdAndBookingPaceId(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, BOOKING_PACE_ID);
    }

    @Test
    public void findOne_ByClientCodeAndPropertyCodeAndBookingIdOrderByChangeDateDesc() {
        NucleusFunctionSpaceBookingPace bookingPace = NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingPace();

        when(functionSpaceBookingPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdOrderByChangeDateDesc(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID)).thenReturn(bookingPace);

        NucleusFunctionSpaceBookingPace retBookingPace = functionSpaceBookingPaceService.findOne(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID);

        assertSame(bookingPace, retBookingPace);
        verify(functionSpaceBookingPaceRepository).findFirstByClientCodeAndPropertyCodeAndBookingIdOrderByChangeDateDesc(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID);
    }

    @Test
    public void findOne_ByClientCodeAndPropertyCodeAndBookingIdAnChangeDateLessThanOrderByChangeDateDesc() {
        Date changeDate = toDate(LocalDate.of(2019, 01, 01));
        functionSpaceBookingPaceService.findOne(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, changeDate);
        verify(functionSpaceBookingPaceRepository).findFirstByClientCodeAndPropertyCodeAndBookingIdAndChangeDateLessThanEqualOrderByChangeDateDesc(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID, changeDate);
    }

    @Test
    public void findAll() {
        List<NucleusFunctionSpaceBookingPace> bookingPaces = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingPace());

        when(functionSpaceBookingPaceRepository.findByClientCodeAndPropertyCodeOrderByBookingIdAscBookingPaceIdAsc(CLIENT_CODE, PROPERTY_CODE)).thenReturn(bookingPaces);

        List<NucleusFunctionSpaceBookingPace> retBookingPaces = functionSpaceBookingPaceService.findAll(CLIENT_CODE, PROPERTY_CODE);

        assertTrue(CollectionUtils.isNotEmpty(retBookingPaces));
        assertSame(bookingPaces.get(0), retBookingPaces.get(0));

        verify(functionSpaceBookingPaceRepository).findByClientCodeAndPropertyCodeOrderByBookingIdAscBookingPaceIdAsc(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void findAll_ForBookingId() {
        List<NucleusFunctionSpaceBookingPace> bookingPaces = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingPace());

        when(functionSpaceBookingPaceRepository.findByClientCodeAndPropertyCodeAndBookingId(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID)).thenReturn(bookingPaces);

        List<NucleusFunctionSpaceBookingPace> retBookingPaces = functionSpaceBookingPaceService.findAll(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID);

        assertTrue(CollectionUtils.isNotEmpty(retBookingPaces));
        assertSame(bookingPaces.get(0), retBookingPaces.get(0));

        verify(functionSpaceBookingPaceRepository).findByClientCodeAndPropertyCodeAndBookingId(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID);
    }

    @Test
    public void delete() {
        when(functionSpaceBookingPaceRepository.deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE)).thenReturn(1L);

        functionSpaceBookingPaceService.delete(CLIENT_CODE, PROPERTY_CODE);

        verify(functionSpaceBookingPaceRepository).deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void findAllByDateRange() {
        LocalDateTime startDate = LocalDate.of(2017, 10, 1).atStartOfDay();
        LocalDateTime endDate = LocalDate.of(2017, 10, 31).atStartOfDay();
        PageRequest pageRequest = PageRequest.of(1, 1);
        List<NucleusFunctionSpaceBookingPace> bookingPaces = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingPace());
        Page<NucleusFunctionSpaceBookingPace> page = new PageImpl<>(bookingPaces);

        when(functionSpaceBookingPaceRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest)).thenReturn(page);

        Page<NucleusFunctionSpaceBookingPace> bookingPacePages = functionSpaceBookingPaceService.findAllByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);

        assertNotNull(bookingPacePages);
        assertEquals(1, bookingPacePages.getTotalElements());

        verify(functionSpaceBookingPaceRepository).findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
    }

    @Test
    public void savePaceCallsRepositoryMethod() {
        NucleusFunctionSpaceBookingPace pace = new NucleusFunctionSpaceBookingPace();
        when(functionSpaceBookingPaceRepository.save(pace)).thenReturn(pace);

        assertSame(pace, functionSpaceBookingPaceService.save(pace));

        verify(functionSpaceBookingPaceRepository).save(pace);
    }

    @Test
    public void saveAll() {
        List<NucleusFunctionSpaceBookingPace> nucleusFunctionSpaceBookings = new ArrayList<>();
        NucleusFunctionSpaceBookingPace nucleusFunctionSpaceBookingPace = new NucleusFunctionSpaceBookingPace();
        nucleusFunctionSpaceBookingPace.setBookingPaceId("BPID");
        nucleusFunctionSpaceBookingPace.setBookingId("133244");
        nucleusFunctionSpaceBookings.add(nucleusFunctionSpaceBookingPace);
        NucleusFunctionSpaceBookingPace existingRecord = new NucleusFunctionSpaceBookingPace();
        existingRecord.setBookingPaceId("BPID");
        Mockito.when(functionSpaceBookingPaceRepository.findFirstByClientCodeAndPropertyCodeAndBookingIdAndBookingPaceId(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(existingRecord);
        Mockito.when(functionSpaceBookingPaceRepository.saveAll(nucleusFunctionSpaceBookings)).thenReturn(nucleusFunctionSpaceBookings);
        functionSpaceBookingPaceService.saveAll(nucleusFunctionSpaceBookings);
        verify(functionSpaceBookingPaceRepository, atLeastOnce()).findFirstByClientCodeAndPropertyCodeAndBookingIdAndBookingPaceId(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
        verify(functionSpaceBookingPaceRepository, atLeastOnce()).save(nucleusFunctionSpaceBookingPace);
    }
}

