package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceMarketSegment;
import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceMarketSegmentRepository;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.Pair;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class FunctionSpaceMarketSegmentServiceTest {
    private static final String PROPERTY_ID = "propertyId";
    private static final String ABBREVIATION = "abbreviation";
    private static final String CLIENT_CODE = "SAMPLE";
    private static final String PROPERTY_CODE = "FS1";
    private static final String DEFAULT_MARKET_SEGMENT_CODE = "UNKNOWN";

    @Mock
    private NucleusFunctionSpaceMarketSegmentRepository functionSpaceMarketSegmentRepository;

    @InjectMocks
    private FunctionSpaceMarketSegmentService functionSpaceMarketSegmentService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findOrCreateFunctionSpaceMarketSegmentCreatesNew() {

        when(functionSpaceMarketSegmentRepository.findFirstByClientCodeAndPropertyCodeAndAbbreviation(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION)).thenReturn(null);
        when(functionSpaceMarketSegmentRepository.save(any(NucleusFunctionSpaceMarketSegment.class))).thenAnswer(invocation -> invocation.getArguments()[0]);

        NucleusFunctionSpaceMarketSegment functionSpaceMarketSegment = functionSpaceMarketSegmentService.findOrCreateFunctionSpaceMarketSegment(IntegrationType.FUNCTION_SPACE_CLIENT, PROPERTY_ID, CLIENT_CODE, PROPERTY_CODE, ABBREVIATION);
        assertEquals(PROPERTY_ID, functionSpaceMarketSegment.getPropertyId());
        assertEquals(ABBREVIATION, functionSpaceMarketSegment.getAbbreviation());
        assertEquals(ABBREVIATION, functionSpaceMarketSegment.getName());
        assertEquals(ABBREVIATION, functionSpaceMarketSegment.getDescription());
        assertEquals(ABBREVIATION, functionSpaceMarketSegment.getMarketSegmentId());
        assertEquals(CLIENT_CODE, functionSpaceMarketSegment.getClientCode());
        assertEquals(PROPERTY_CODE, functionSpaceMarketSegment.getPropertyCode());

        verify(functionSpaceMarketSegmentRepository).findFirstByClientCodeAndPropertyCodeAndAbbreviation(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION);
        verify(functionSpaceMarketSegmentRepository).save(any(NucleusFunctionSpaceMarketSegment.class));
    }

    @Test
    public void findOrCreateFunctionSpaceMarketSegmentReturnsExisting() {
        NucleusFunctionSpaceMarketSegment functionSpaceMarketSegment = new NucleusFunctionSpaceMarketSegment();
        when(functionSpaceMarketSegmentRepository.findFirstByClientCodeAndPropertyCodeAndAbbreviation(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION)).thenReturn(functionSpaceMarketSegment);

        NucleusFunctionSpaceMarketSegment foundFunctionSpaceMarketSegment = functionSpaceMarketSegmentService.findOrCreateFunctionSpaceMarketSegment(IntegrationType.FUNCTION_SPACE_CLIENT, PROPERTY_ID, CLIENT_CODE, PROPERTY_CODE, ABBREVIATION);
        assertSame(functionSpaceMarketSegment, foundFunctionSpaceMarketSegment);

        verify(functionSpaceMarketSegmentRepository).findFirstByClientCodeAndPropertyCodeAndAbbreviation(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION);
        verify(functionSpaceMarketSegmentRepository, never()).save(any(NucleusFunctionSpaceMarketSegment.class));
    }

    @Test
    public void findOne() {
        NucleusFunctionSpaceMarketSegment marketSegment = NucleusFunctionSpaceObjectMother.buildFunctionSpaceMarketSegment();

        when(functionSpaceMarketSegmentRepository.findFirstByClientCodeAndPropertyCodeAndMarketSegmentId(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION)).thenReturn(marketSegment);

        NucleusFunctionSpaceMarketSegment retMarketSegment = functionSpaceMarketSegmentService.findOne(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION);

        assertSame(marketSegment, retMarketSegment);
        verify(functionSpaceMarketSegmentRepository).findFirstByClientCodeAndPropertyCodeAndMarketSegmentId(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION);
    }

    @Test
    public void save_noExistingMarketSegment() {
        String marketSegmentId = "market-segment-id";

        when(functionSpaceMarketSegmentRepository.findFirstByClientCodeAndPropertyCodeAndMarketSegmentId(CLIENT_CODE, PROPERTY_CODE, marketSegmentId)).thenReturn(null);
        when(functionSpaceMarketSegmentRepository.save(Mockito.any(NucleusFunctionSpaceMarketSegment.class))).thenAnswer(invocation -> invocation.getArguments()[0]);

        NucleusFunctionSpaceMarketSegment marketSegment = NucleusFunctionSpaceObjectMother.buildFunctionSpaceMarketSegment();
        marketSegment.setMarketSegmentId(marketSegmentId);
        marketSegment.setId(null);
        marketSegment.setCreateDate(null);

        NucleusFunctionSpaceMarketSegment retMarketSegment = functionSpaceMarketSegmentService.save(marketSegment);
        assertSame(retMarketSegment, marketSegment);
        assertNull(retMarketSegment.getId());
        assertNull(retMarketSegment.getCreateDate());

        verify(functionSpaceMarketSegmentRepository).save(marketSegment);
    }

    @Test
    public void save_existingMarketSegment() {
        String marketSegmentId = "market-segment-id";
        NucleusFunctionSpaceMarketSegment existingMarketSegment = NucleusFunctionSpaceObjectMother.buildFunctionSpaceMarketSegment();
        existingMarketSegment.setMarketSegmentId(marketSegmentId);
        existingMarketSegment.setId("id-1");
        existingMarketSegment.setCreateDate(new Date());

        NucleusFunctionSpaceMarketSegment marketSegment = NucleusFunctionSpaceObjectMother.buildFunctionSpaceMarketSegment();
        marketSegment.setMarketSegmentId(marketSegmentId);

        when(functionSpaceMarketSegmentRepository.findFirstByClientCodeAndPropertyCodeAndMarketSegmentId(marketSegment.getClientCode(), marketSegment.getPropertyCode(), marketSegmentId)).thenReturn(existingMarketSegment);
        when(functionSpaceMarketSegmentRepository.save(Mockito.any(NucleusFunctionSpaceMarketSegment.class))).thenAnswer(invocation -> invocation.getArguments()[0]);

        NucleusFunctionSpaceMarketSegment retMarketSegment = functionSpaceMarketSegmentService.save(marketSegment);
        assertSame(retMarketSegment, marketSegment);
        assertEquals(existingMarketSegment.getId(), retMarketSegment.getId());
        assertEquals(existingMarketSegment.getCreateDate(), retMarketSegment.getCreateDate());

        verify(functionSpaceMarketSegmentRepository).save(marketSegment);
    }

    @Test
    public void findAll() {
        List<NucleusFunctionSpaceMarketSegment> marketSegments = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceMarketSegment());

        when(functionSpaceMarketSegmentRepository.findByClientCodeAndPropertyCodeOrderByAbbreviationAsc(CLIENT_CODE, PROPERTY_CODE)).thenReturn(marketSegments);

        List<NucleusFunctionSpaceMarketSegment> retMarketSegments = functionSpaceMarketSegmentService.findAll(CLIENT_CODE, PROPERTY_CODE);

        assertTrue(CollectionUtils.isNotEmpty(retMarketSegments));
        assertSame(marketSegments.get(0), retMarketSegments.get(0));

        verify(functionSpaceMarketSegmentRepository).findByClientCodeAndPropertyCodeOrderByAbbreviationAsc(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void saveMarketSegments() {
        List<NucleusFunctionSpaceMarketSegment> marketSegments = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceMarketSegment());

        when(functionSpaceMarketSegmentRepository.saveAll(marketSegments)).thenReturn(marketSegments);

        List<NucleusFunctionSpaceMarketSegment> retMarketSegments = functionSpaceMarketSegmentService.save(marketSegments);

        assertTrue(CollectionUtils.isNotEmpty(retMarketSegments));
        assertSame(marketSegments.get(0), retMarketSegments.get(0));

        verify(functionSpaceMarketSegmentRepository).saveAll(marketSegments);
    }

    @Test
    public void delete() {
        when(functionSpaceMarketSegmentRepository.deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE)).thenReturn(1L);

        functionSpaceMarketSegmentService.delete(CLIENT_CODE, PROPERTY_CODE);

        verify(functionSpaceMarketSegmentRepository).deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void findAllByDateRange() {
        LocalDateTime startDate = LocalDate.of(2017, 10, 1).atStartOfDay();
        LocalDateTime endDate = LocalDate.of(2017, 10, 31).atStartOfDay();
        PageRequest pageRequest = PageRequest.of(1, 1);
        List<NucleusFunctionSpaceMarketSegment> guestRoomTypes = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceMarketSegment());
        Page<NucleusFunctionSpaceMarketSegment> page = new PageImpl<>(guestRoomTypes);

        when(functionSpaceMarketSegmentRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest)).thenReturn(page);

        Page<NucleusFunctionSpaceMarketSegment> marketSegmentPages = functionSpaceMarketSegmentService.findAllByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);

        assertNotNull(marketSegmentPages);
        assertEquals(1, marketSegmentPages.getTotalElements());

        verify(functionSpaceMarketSegmentRepository).findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
    }

    @Test
    public void getMarketSegmentCode() {
        String marketSegmentId = "mktSegId";
        NucleusFunctionSpaceMarketSegment functionSpaceMarketSegment = NucleusFunctionSpaceObjectMother.buildFunctionSpaceMarketSegment();
        when(functionSpaceMarketSegmentRepository.findFirstByClientCodeAndPropertyCodeAndMarketSegmentId(CLIENT_CODE, PROPERTY_CODE, marketSegmentId)).thenReturn(functionSpaceMarketSegment);

        String retMktSegmentCode = functionSpaceMarketSegmentService.getMarketSegmentCode(CLIENT_CODE, PROPERTY_CODE, marketSegmentId, DEFAULT_MARKET_SEGMENT_CODE);

        assertEquals(functionSpaceMarketSegment.getAbbreviation(), retMktSegmentCode);
        verify(functionSpaceMarketSegmentRepository).findFirstByClientCodeAndPropertyCodeAndMarketSegmentId(CLIENT_CODE, PROPERTY_CODE, marketSegmentId);
    }

    @Test
    public void getMarketSegmentCode_noMarketSegmentId() {
        String retMktSegmentCode = functionSpaceMarketSegmentService.getMarketSegmentCode(CLIENT_CODE, PROPERTY_CODE, null, DEFAULT_MARKET_SEGMENT_CODE);
        assertEquals(DEFAULT_MARKET_SEGMENT_CODE, retMktSegmentCode);
    }

    @Test
    public void getMarketSegmentCode_noNucleusFunctionsSpaceMarketSegment() {
        String marketSegmentId = "mktSegId";

        when(functionSpaceMarketSegmentRepository.findFirstByClientCodeAndPropertyCodeAndMarketSegmentId(CLIENT_CODE, PROPERTY_CODE, marketSegmentId)).thenReturn(null);

        String retMktSegmentCode = functionSpaceMarketSegmentService.getMarketSegmentCode(CLIENT_CODE, PROPERTY_CODE, marketSegmentId, DEFAULT_MARKET_SEGMENT_CODE);

        assertEquals(DEFAULT_MARKET_SEGMENT_CODE, retMktSegmentCode);
        verify(functionSpaceMarketSegmentRepository).findFirstByClientCodeAndPropertyCodeAndMarketSegmentId(CLIENT_CODE, PROPERTY_CODE, marketSegmentId);
    }

    @Test
    public void upsertAll() {
        List<NucleusFunctionSpaceMarketSegment> nucleusFunctionSpaceMarketSegments = new ArrayList<>();
        nucleusFunctionSpaceMarketSegments.add(new NucleusFunctionSpaceMarketSegment());
        List<Pair<Query, Update>> updates = new ArrayList<>();
        Mockito.when(functionSpaceMarketSegmentRepository.bulkOpsUpsertMulti(updates)).thenReturn(null);
        functionSpaceMarketSegmentService.upsertAll(nucleusFunctionSpaceMarketSegments);
        verify(functionSpaceMarketSegmentRepository, atLeastOnce()).bulkOpsUpsertMulti(any());
    }


}