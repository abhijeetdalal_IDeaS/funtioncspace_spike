package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceGuestRoomType;
import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceGuestRoomTypeRepository;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.data.domain.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.CLIENT_CODE;
import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.PROPERTY_CODE;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class FunctionSpaceBookingGuestRoomTypeServiceTest {
    private static final String GUEST_ROOM_TYPE = "RoomType";

    @Mock
    private NucleusFunctionSpaceGuestRoomTypeRepository guestRoomTypeRepository;

    @InjectMocks
    private FunctionSpaceGuestRoomTypeService functionSpaceGuestRoomTypeService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findOne() {
        NucleusFunctionSpaceGuestRoomType guestRoomType = NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingGuestRoomType();

        when(guestRoomTypeRepository.findFirstByClientCodeAndPropertyCodeAndFunctionRoomType(CLIENT_CODE, PROPERTY_CODE, GUEST_ROOM_TYPE)).thenReturn(guestRoomType);

        NucleusFunctionSpaceGuestRoomType retGuestRoomType = functionSpaceGuestRoomTypeService.findOne(CLIENT_CODE, PROPERTY_CODE, GUEST_ROOM_TYPE);

        assertSame(guestRoomType, retGuestRoomType);
        verify(guestRoomTypeRepository).findFirstByClientCodeAndPropertyCodeAndFunctionRoomType(CLIENT_CODE, PROPERTY_CODE, GUEST_ROOM_TYPE);
    }

    @Test
    public void findAll() {
        List<NucleusFunctionSpaceGuestRoomType> guestRoomTypes = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingGuestRoomType());

        when(guestRoomTypeRepository.findByClientCodeAndPropertyCode(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.any(Sort.class))).thenReturn(guestRoomTypes);

        List<NucleusFunctionSpaceGuestRoomType> retGuestRoomTypes = functionSpaceGuestRoomTypeService.findAll(CLIENT_CODE, PROPERTY_CODE);

        assertTrue(CollectionUtils.isNotEmpty(retGuestRoomTypes));
        assertSame(guestRoomTypes.get(0), retGuestRoomTypes.get(0));

        ArgumentCaptor<Sort> sortCaptor = ArgumentCaptor.forClass(Sort.class);
        verify(guestRoomTypeRepository).findByClientCodeAndPropertyCode(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), sortCaptor.capture());

        Sort sort = sortCaptor.getValue();
        Sort.Order order = sort.getOrderFor("functionRoomType");
        assertNotNull(order);
        assertEquals(Sort.Direction.ASC, order.getDirection());
    }

    @Test
    public void delete() {
        when(guestRoomTypeRepository.deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE)).thenReturn(1L);

        functionSpaceGuestRoomTypeService.delete(CLIENT_CODE, PROPERTY_CODE);

        verify(guestRoomTypeRepository).deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void findAllByDateRange() {
        LocalDateTime startDate = LocalDate.of(2017, 10, 1).atStartOfDay();
        LocalDateTime endDate = LocalDate.of(2017, 10, 31).atStartOfDay();
        PageRequest pageRequest = PageRequest.of(1, 1);
        List<NucleusFunctionSpaceGuestRoomType> guestRoomTypes = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingGuestRoomType());
        Page<NucleusFunctionSpaceGuestRoomType> page = new PageImpl<>(guestRoomTypes);

        when(guestRoomTypeRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), ArgumentMatchers.eq(endDate), ArgumentMatchers.any(Pageable.class))).thenReturn(page);

        Page<NucleusFunctionSpaceGuestRoomType> guestRoomTypePages = functionSpaceGuestRoomTypeService.findAllByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);

        assertNotNull(guestRoomTypePages);
        assertEquals(1, guestRoomTypePages.getTotalElements());

        ArgumentCaptor<Pageable> pageCaptor = ArgumentCaptor.forClass(Pageable.class);
        verify(guestRoomTypeRepository).findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(ArgumentMatchers.eq(CLIENT_CODE), ArgumentMatchers.eq(PROPERTY_CODE), ArgumentMatchers.eq(startDate), ArgumentMatchers.eq(endDate), pageCaptor.capture());

        Pageable pageable = pageCaptor.getValue();
        assertNotNull(pageable);
        assertEquals(1, pageRequest.getPageSize());
        assertEquals(1, pageRequest.getPageNumber());
    }
}
