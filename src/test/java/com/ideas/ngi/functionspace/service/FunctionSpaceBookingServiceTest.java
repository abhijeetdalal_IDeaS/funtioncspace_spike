package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceBooking;
import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceBookingRepository;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.Pair;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.CLIENT_CODE;
import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.PROPERTY_CODE;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

public class FunctionSpaceBookingServiceTest {
    private static final String BOOKING_ID = "BookingId";

    @Mock
    private MongoOperations mongoOperations;

    @Mock
    private NucleusFunctionSpaceBookingRepository bookingRepository;

    @InjectMocks
    private FunctionSpaceBookingService functionSpaceBookingService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void updateEntireBookingForChange() {
        IntegrationType integrationType = IntegrationType.OXI_SC;
        String propertyId = "123";
        String bookingId = "1";
        Date lastModifiedDate = new Date();

        Query query = new Query(Criteria.where("integrationType").is(integrationType).and("propertyId").is(propertyId).and("bookingId").is(bookingId));
        Update update = Update.update("lastModifiedDate", lastModifiedDate);

        for (Class<?> clazz : FunctionSpaceBookingService.FUNCTION_SPACE_BOOKING_CLASS_HIERARCHY) {
            Mockito.when(mongoOperations.findAndModify(query, update, clazz)).thenReturn(null);
        }

        functionSpaceBookingService.updateEntireBookingForChange(IntegrationType.OXI_SC, "123", "1", lastModifiedDate);

        for (Class<?> clazz : FunctionSpaceBookingService.FUNCTION_SPACE_BOOKING_CLASS_HIERARCHY) {
            verify(mongoOperations).findAndModify(query, update, clazz);
        }
    }

    @Test
    public void findOne() {
        NucleusFunctionSpaceBooking booking = NucleusFunctionSpaceObjectMother.buildFunctionSpaceBooking();

        Mockito.when(bookingRepository.findFirstByClientCodeAndPropertyCodeAndBookingId(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID)).thenReturn(booking);

        NucleusFunctionSpaceBooking retBooking = functionSpaceBookingService.findOne(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID);

        assertSame(booking, retBooking);
        verify(bookingRepository).findFirstByClientCodeAndPropertyCodeAndBookingId(CLIENT_CODE, PROPERTY_CODE, BOOKING_ID);
    }

    @Test
    public void findAll() {
        List<NucleusFunctionSpaceBooking> bookings = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceBooking());

        Mockito.when(bookingRepository.findByClientCodeAndPropertyCodeOrderByBookingIdAsc(CLIENT_CODE, PROPERTY_CODE)).thenReturn(bookings);

        List<NucleusFunctionSpaceBooking> retBookings = functionSpaceBookingService.findAll(CLIENT_CODE, PROPERTY_CODE);

        assertTrue(CollectionUtils.isNotEmpty(retBookings));
        assertSame(bookings.get(0), retBookings.get(0));

        verify(bookingRepository).findByClientCodeAndPropertyCodeOrderByBookingIdAsc(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void delete() {
        Mockito.when(bookingRepository.deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE)).thenReturn(1L);

        functionSpaceBookingService.delete(CLIENT_CODE, PROPERTY_CODE);

        verify(bookingRepository).deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void findAllByDateRange() {
        LocalDateTime startDate = LocalDate.of(2017, 10, 1).atStartOfDay();
        LocalDateTime endDate = LocalDate.of(2017, 10, 31).atStartOfDay();
        PageRequest pageRequest = PageRequest.of(1, 1);
        List<NucleusFunctionSpaceBooking> bookings = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceBooking());
        Page<NucleusFunctionSpaceBooking> page = new PageImpl<>(bookings);

        Mockito.when(bookingRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest)).thenReturn(page);

        Page<NucleusFunctionSpaceBooking> bookingPages = functionSpaceBookingService.findAllByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);

        assertNotNull(bookingPages);
        assertEquals(1, bookingPages.getTotalElements());

        verify(bookingRepository).findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
    }

    @Test
    public void save() {
        NucleusFunctionSpaceBooking booking = NucleusFunctionSpaceObjectMother.buildFunctionSpaceBooking();

        Mockito.when(bookingRepository.save(booking)).thenReturn(booking);

        functionSpaceBookingService.save(booking);

        verify(bookingRepository).save(booking);
    }

    @Test
    public void findByClientCodeAndPropertyCodeAndBookingIdIn() {
        String clientCode = "BSTN";
        String propertyCode = "H1";
        List<String> bookingIds = Arrays.asList("2349a334[0001]", "2349a334[0221]");
        functionSpaceBookingService.findByClientCodeAndPropertyCodeAndBookingIdIn(clientCode, propertyCode, bookingIds);
        verify(bookingRepository).findByClientCodeAndPropertyCodeAndBookingIdIn(clientCode, propertyCode, bookingIds);
    }

    @Test
    public void upsertAll() {
        List<NucleusFunctionSpaceBooking> nucleusFunctionSpaceBookings = new ArrayList<>();
        nucleusFunctionSpaceBookings.add(new NucleusFunctionSpaceBooking());
        List<Pair<Query, Update>> updates = new ArrayList<>();
        Mockito.when(bookingRepository.bulkOpsUpsertMulti(updates)).thenReturn(null);
        functionSpaceBookingService.upsertAll(nucleusFunctionSpaceBookings);
        verify(bookingRepository, Mockito.atLeastOnce()).bulkOpsUpsertMulti(ArgumentMatchers.any());
    }

}
