package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.functionspace.IntegrationFlowTest;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceGuestRoomPace;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceGuestRoomPaceRepository;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FunctionSpaceGuestRoomPaceServiceComponentTest extends IntegrationFlowTest {

    public static final String CONFIRMED = "Confirmed";
    @Autowired
    FunctionSpaceGuestRoomPaceService functionSpaceGuestRoomPaceService;

    @Autowired
    NucleusFunctionSpaceGuestRoomPaceRepository guestRoomPaceRepository;
    public static final String BOOKING_PACE_ID_DAY_0 = "55555[BPID1]";
    public static final LocalDateTime LOCAL_DATE_TIME = LocalDateTime.parse("2019-10-06").withTime(0, 0, 0, 0);
    public static final LocalDateTime LAST_MODIFIED_START_DATE = LocalDateTime.now().withTime(0, 0, 0, 0);
    public static final LocalDateTime LAST_MODIFIED_END_DATE = LAST_MODIFIED_START_DATE.plusDays(1);
    public static final LocalDateTime END_DATE = LOCAL_DATE_TIME.plusDays(7);
    public static final String CLIENT_CODE = "SANDBOXNUCLEUS";
    public static final String PROPERTY_CODE = "H1";
    public static final String BOOKING_ID = "55556";
    public static final String BOOKING_PACE_ID_DAY_PLUS_1 = "6666[BPID1]";
    public static final String BOOKING_PACE_ID_PLUS_DAY_2 = "3333[BPID1]";

    @Before
    public void setup() {
        Criteria criteria = Criteria.where("clientCode").regex("NUCLEUS");
        guestRoomPaceRepository.findAllAndRemove(criteria);
        prepareGuestRoomPaceData(CLIENT_CODE, PROPERTY_CODE, LOCAL_DATE_TIME, END_DATE, BOOKING_ID, BOOKING_PACE_ID_DAY_0, 1, BigDecimal.valueOf(2.31), 2, BigDecimal.valueOf(3.31), 3, BigDecimal.valueOf(4.31));
        prepareGuestRoomPaceData(CLIENT_CODE, PROPERTY_CODE, LOCAL_DATE_TIME.plusDays(1), END_DATE, BOOKING_ID, BOOKING_PACE_ID_DAY_PLUS_1, 3, BigDecimal.valueOf(7.1), 4, BigDecimal.valueOf(8.1), 5, BigDecimal.valueOf(9.1));
        prepareGuestRoomPaceData(CLIENT_CODE, PROPERTY_CODE, LOCAL_DATE_TIME.minusDays(-2), END_DATE, BOOKING_ID, BOOKING_PACE_ID_PLUS_DAY_2, 6, BigDecimal.valueOf(2.3), 7, BigDecimal.valueOf(3.3), 8, BigDecimal.valueOf(4.3));
    }

    @Test
    public void getLatestBookingPerDay() {
        List<NucleusFunctionSpaceGuestRoomPace> latestBookingPerDay =
                functionSpaceGuestRoomPaceService.getChangesPerBooking(CLIENT_CODE, PROPERTY_CODE, LAST_MODIFIED_START_DATE.toDate(), LAST_MODIFIED_END_DATE.toDate(), 10, 0);
        assertEquals(3, latestBookingPerDay.size());
        NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace = latestBookingPerDay.get(0);
        assertEquals(1, nucleusFunctionSpaceGuestRoomPace.getRoomNightsChanged().intValue());
        assertEquals(2.31, nucleusFunctionSpaceGuestRoomPace.getRoomRevenueChanged().doubleValue(), 0.001);
        assertEquals(3, nucleusFunctionSpaceGuestRoomPace.getBlockedRoomNightsChanged().intValue());
        assertEquals(4.31, nucleusFunctionSpaceGuestRoomPace.getBlockedRoomRevenueChanged().doubleValue(), 0.001);
        assertEquals(2, nucleusFunctionSpaceGuestRoomPace.getContractedRoomNightsChanged().intValue());
        assertEquals(3.31, nucleusFunctionSpaceGuestRoomPace.getContractedRoomRevenueChanged().doubleValue(), 0.001);
        assertEquals(CONFIRMED, nucleusFunctionSpaceGuestRoomPace.getGuestRoomStatus());
        assertEquals(BOOKING_ID, nucleusFunctionSpaceGuestRoomPace.getBookingId());

        assertEquals(BOOKING_ID, nucleusFunctionSpaceGuestRoomPace.getBookingId());
        assertEquals(LOCAL_DATE_TIME.minusDays(3).plusHours(23).plusMinutes(12).toDate(), nucleusFunctionSpaceGuestRoomPace.getChangeDate());
        assertEquals(BOOKING_ID, latestBookingPerDay.get(1).getBookingId());
        assertEquals(LOCAL_DATE_TIME.minusDays(2).plusHours(23).plusMinutes(12).toDate(), latestBookingPerDay.get(1).getChangeDate());
        assertEquals(BOOKING_ID, latestBookingPerDay.get(2).getBookingId());
        assertEquals(LOCAL_DATE_TIME.minusDays(1).plusHours(23).plusMinutes(12).toDate(), latestBookingPerDay.get(2).getChangeDate());
    }

    @Test
    public void getLatestBookingPerDayTestSkipLimit() {
        List<NucleusFunctionSpaceGuestRoomPace> latestBookingPerDay =
                functionSpaceGuestRoomPaceService.getChangesPerBooking(CLIENT_CODE, PROPERTY_CODE, LAST_MODIFIED_START_DATE.toDate(), LAST_MODIFIED_END_DATE.toDate(),1, 1);
        assertEquals(1, latestBookingPerDay.size());
        NucleusFunctionSpaceGuestRoomPace nucleusFunctionSpaceGuestRoomPace = latestBookingPerDay.get(0);
        assertEquals(3, nucleusFunctionSpaceGuestRoomPace.getRoomNightsChanged().intValue());
        assertEquals(7.1, nucleusFunctionSpaceGuestRoomPace.getRoomRevenueChanged().doubleValue(), 0.01);
        assertEquals(5, nucleusFunctionSpaceGuestRoomPace.getBlockedRoomNightsChanged().intValue());
        assertEquals(9.1, nucleusFunctionSpaceGuestRoomPace.getBlockedRoomRevenueChanged().doubleValue(), 0.01);
        assertEquals(4, nucleusFunctionSpaceGuestRoomPace.getContractedRoomNightsChanged().intValue());
        assertEquals(8.1, nucleusFunctionSpaceGuestRoomPace.getContractedRoomRevenueChanged().doubleValue(), 0.01);
        assertEquals(CONFIRMED, nucleusFunctionSpaceGuestRoomPace.getGuestRoomStatus());
        assertEquals(BOOKING_ID, nucleusFunctionSpaceGuestRoomPace.getBookingId());
        assertEquals(LOCAL_DATE_TIME.minusDays(2).plusHours(23).plusMinutes(12).toDate(), nucleusFunctionSpaceGuestRoomPace.getChangeDate());
    }

    @Test
    public void getChangesPerBookingCount() {
        long changesPerBookingCount = functionSpaceGuestRoomPaceService.getChangesPerBookingCount(CLIENT_CODE, PROPERTY_CODE,
                LAST_MODIFIED_START_DATE.toDate(), LAST_MODIFIED_END_DATE.toDate());
        assertEquals(3, changesPerBookingCount);
    }

    private void prepareGuestRoomPaceData(String clientCode, String propertyCode, LocalDateTime startDate, LocalDateTime endDate, String bookingId, String bookingPaceId, int pickUpRoomNightsChanged, BigDecimal pickUpRoomRevenueChanged, int contRoomNights, BigDecimal contRoomRevenueChanged, int blockRoomNightChanged, BigDecimal blockRoomRevenueChanged) {
        List<NucleusFunctionSpaceGuestRoomPace> paceRecords = new ArrayList<>();
        LocalDateTime changeDate = startDate.minusDays(3);
        paceRecords.add(buildGuestRoomPaceRecord(clientCode, propertyCode, startDate, bookingId, bookingPaceId, "Tentative", "RT1", changeDate.toDate(), pickUpRoomNightsChanged, pickUpRoomRevenueChanged, contRoomNights, contRoomRevenueChanged, blockRoomNightChanged, blockRoomRevenueChanged));
        paceRecords.add(buildGuestRoomPaceRecord(clientCode, propertyCode, startDate, bookingId, bookingPaceId+"[001]", CONFIRMED, "RT1", changeDate.plusHours(23).plusMinutes(12).toDate(), -pickUpRoomNightsChanged, pickUpRoomRevenueChanged.negate(), -contRoomNights, contRoomRevenueChanged.negate(), -blockRoomNightChanged, blockRoomRevenueChanged.negate()));
        paceRecords.add(buildGuestRoomPaceRecord(clientCode, propertyCode, startDate, bookingId, bookingPaceId, "Tentative", "RT2", changeDate.toDate(), pickUpRoomNightsChanged, pickUpRoomRevenueChanged, contRoomNights, contRoomRevenueChanged, blockRoomNightChanged, blockRoomRevenueChanged));
        guestRoomPaceRepository.saveAll(paceRecords);
    }

    public NucleusFunctionSpaceGuestRoomPace buildGuestRoomPaceRecord(String clientCode, String propertyCode, LocalDateTime startDate, String bookingId, String bookingPaceId, String status, String roomType, Date changeDate, Integer pickUpRoomNightsChanged, BigDecimal pickUpRoomRevenueChanged, Integer contRoomNights, BigDecimal contRoomRevenueChanged, Integer blockRoomNightChanged, BigDecimal blockRoomRevenueChanged) {
            NucleusFunctionSpaceGuestRoomPace paceRecord = new NucleusFunctionSpaceGuestRoomPace();
            paceRecord.setId(Math.random() + "");
            paceRecord.setBookingId(bookingId);
            paceRecord.setBookingPaceId(bookingPaceId );
            paceRecord.setCreateDate(new Date());
            paceRecord.setLastModifiedDate(changeDate);
            paceRecord.setChangeDate(changeDate);
            paceRecord.setRoomNightsChanged(pickUpRoomNightsChanged);
            paceRecord.setRoomRevenueChanged(pickUpRoomRevenueChanged);
            paceRecord.setContractedRoomNightsChanged(contRoomNights);
            paceRecord.setContractedRoomRevenueChanged(contRoomRevenueChanged);
            paceRecord.setBlockedRoomNightsChanged(blockRoomNightChanged);
            paceRecord.setBlockedRoomRevenueChanged(blockRoomRevenueChanged);
            paceRecord.setGuestRoomStatus(status);
            paceRecord.setStayDate(startDate.plusDays(100).toDate());
            paceRecord.setRoomType(roomType);
            paceRecord.setIntegrationType(IntegrationType.AHWS_SC);
            paceRecord.setPropertyId("SANDBOX-AHWS");
            paceRecord.setClientCode(clientCode);
            paceRecord.setPropertyCode(propertyCode);
        return paceRecord;
    }
}
