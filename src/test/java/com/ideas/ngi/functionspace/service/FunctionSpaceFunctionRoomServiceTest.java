package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceFunctionRoom;
import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceFunctionRoomRepository;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.Pair;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.CLIENT_CODE;
import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.PROPERTY_CODE;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class FunctionSpaceFunctionRoomServiceTest {
    private static final String FUNCTION_ROOM_ID = "RoomId";

    @Mock
    private NucleusFunctionSpaceFunctionRoomRepository functionRoomRepository;

    @InjectMocks
    private FunctionSpaceFunctionRoomService functionSpaceFunctionRoomService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findOne() {
        NucleusFunctionSpaceFunctionRoom room = NucleusFunctionSpaceObjectMother.buildFunctionSpaceFunctionRoom();

        when(functionRoomRepository.findFirstByClientCodeAndPropertyCodeAndFunctionRoomId(CLIENT_CODE, PROPERTY_CODE, FUNCTION_ROOM_ID)).thenReturn(room);

        NucleusFunctionSpaceFunctionRoom retRoom = functionSpaceFunctionRoomService.findOne(CLIENT_CODE, PROPERTY_CODE, FUNCTION_ROOM_ID);

        assertSame(room, retRoom);
        verify(functionRoomRepository).findFirstByClientCodeAndPropertyCodeAndFunctionRoomId(CLIENT_CODE, PROPERTY_CODE, FUNCTION_ROOM_ID);
    }

    @Test
    public void findAll() {
        List<NucleusFunctionSpaceFunctionRoom> rooms = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceComboFunctionRoom());

        when(functionRoomRepository.findByClientCodeAndPropertyCodeOrderByFunctionRoomIdAsc(CLIENT_CODE, PROPERTY_CODE)).thenReturn(rooms);

        List<NucleusFunctionSpaceFunctionRoom> retRooms = functionSpaceFunctionRoomService.findAll(CLIENT_CODE, PROPERTY_CODE);

        assertTrue(CollectionUtils.isNotEmpty(retRooms));
        assertSame(rooms.get(0), retRooms.get(0));

        verify(functionRoomRepository).findByClientCodeAndPropertyCodeOrderByFunctionRoomIdAsc(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void delete() {
        when(functionRoomRepository.deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE)).thenReturn(1L);

        functionSpaceFunctionRoomService.delete(CLIENT_CODE, PROPERTY_CODE);

        verify(functionRoomRepository).deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void findAllByDateRange() {
        LocalDateTime startDate = LocalDate.of(2017, 10, 1).atStartOfDay();
        LocalDateTime endDate = LocalDate.of(2017, 10, 31).atStartOfDay();
        PageRequest pageRequest = PageRequest.of(1, 1);
        List<NucleusFunctionSpaceFunctionRoom> rooms = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceFunctionRoom());
        Page<NucleusFunctionSpaceFunctionRoom> page = new PageImpl<>(rooms);

        when(functionRoomRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest)).thenReturn(page);

        Page<NucleusFunctionSpaceFunctionRoom> roomPages = functionSpaceFunctionRoomService.findAllByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);

        assertNotNull(roomPages);
        assertEquals(1, roomPages.getTotalElements());

        verify(functionRoomRepository).findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
    }

    @Test
    public void findAllComboRoomsByDateRange() {
        LocalDateTime startDate = LocalDate.of(2017, 10, 1).atStartOfDay();
        LocalDateTime endDate = LocalDate.of(2017, 10, 31).atStartOfDay();
        PageRequest pageRequest = PageRequest.of(1, 1);
        List<NucleusFunctionSpaceFunctionRoom> rooms = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceFunctionRoom());
        Page<NucleusFunctionSpaceFunctionRoom> page = new PageImpl<>(rooms);

        when(functionRoomRepository.findByClientCodeAndPropertyCodeAndComboIsTrueAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest)).thenReturn(page);

        Page<NucleusFunctionSpaceFunctionRoom> roomPages = functionSpaceFunctionRoomService.findAllComboRoomsByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);

        assertNotNull(roomPages);
        assertEquals(1, roomPages.getTotalElements());

        verify(functionRoomRepository).findByClientCodeAndPropertyCodeAndComboIsTrueAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
    }

    @Test
    public void save() {
        List<NucleusFunctionSpaceFunctionRoom> rooms = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceFunctionRoom());

        when(functionRoomRepository.saveAll(rooms)).thenReturn(rooms);

        functionSpaceFunctionRoomService.save(rooms);

        verify(functionRoomRepository).saveAll(rooms);
    }

    @Test
    public void fetchAllFunctionRoomIds() {
        List<NucleusFunctionSpaceFunctionRoom> rooms = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceFunctionRoom());
        when(functionRoomRepository.findByClientCodeAndPropertyCodeOrderByFunctionRoomIdAsc(CLIENT_CODE, PROPERTY_CODE)).thenReturn(rooms);
        List<String> functionRoomIds = functionSpaceFunctionRoomService.fetchAllFunctionRoomIds(CLIENT_CODE, PROPERTY_CODE);
        assertTrue(CollectionUtils.isNotEmpty(functionRoomIds));
        assertSame(rooms.get(0).getFunctionRoomId(), functionRoomIds.get(0));
        verify(functionRoomRepository).findByClientCodeAndPropertyCodeOrderByFunctionRoomIdAsc(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void upsertAll() {
        List<NucleusFunctionSpaceFunctionRoom> functionSpaceFunctionRooms = new ArrayList<>();
        functionSpaceFunctionRooms.add(new NucleusFunctionSpaceFunctionRoom());
        List<Pair<Query, Update>> updates = new ArrayList<>();
        Mockito.when(functionRoomRepository.bulkOpsUpsertMulti(updates)).thenReturn(null);
        functionSpaceFunctionRoomService.upsertAll(functionSpaceFunctionRooms);
        verify(functionRoomRepository, atLeastOnce()).bulkOpsUpsertMulti(ArgumentMatchers.any());
    }
}