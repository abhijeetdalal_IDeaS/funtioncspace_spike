package com.ideas.ngi.functionspace.service;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEvent;
import com.ideas.ngi.nucleus.data.functionspace.entity.NucleusFunctionSpaceEventType;
import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.functionspace.repository.NucleusFunctionSpaceEventTypeRepository;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.Pair;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.CLIENT_CODE;
import static com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother.PROPERTY_CODE;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class FunctionSpaceEventTypeServiceTest {
    private static final String PROPERTY_ID = "propertyId";
    private static final String ABBREVIATION = "abbreviation";
    private NucleusFunctionSpaceEvent functionSpaceEvent;

    @Mock
    private NucleusFunctionSpaceEventTypeRepository functionSpaceEventTypeRepository;

    @InjectMocks
    private FunctionSpaceEventTypeService functionSpaceEventTypeService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        functionSpaceEvent = new NucleusFunctionSpaceEvent();
        functionSpaceEvent.setClientCode(CLIENT_CODE);
        functionSpaceEvent.setPropertyCode(PROPERTY_CODE);
        functionSpaceEvent.setPropertyId(PROPERTY_ID);
        functionSpaceEvent.setEventTypeCode(ABBREVIATION);
    }

    @Test
    public void findOrCreateFunctionSpaceMarketSegmentCreatesNew() {
        when(functionSpaceEventTypeRepository.findFirstByClientCodeAndPropertyCodeAndAbbreviation(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION)).thenReturn(null);
        when(functionSpaceEventTypeRepository.save(any(NucleusFunctionSpaceEventType.class))).thenAnswer(invocation -> invocation.getArguments()[0]);

        NucleusFunctionSpaceEventType functionSpaceEventType = functionSpaceEventTypeService.findOrCreateEventType(IntegrationType.FUNCTION_SPACE_CLIENT, functionSpaceEvent);
        assertEquals(PROPERTY_ID, functionSpaceEventType.getPropertyId());
        assertEquals(ABBREVIATION, functionSpaceEventType.getAbbreviation());
        assertEquals(ABBREVIATION, functionSpaceEventType.getName());

        verify(functionSpaceEventTypeRepository).findFirstByClientCodeAndPropertyCodeAndAbbreviation(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION);
        verify(functionSpaceEventTypeRepository).save(Mockito.any(NucleusFunctionSpaceEventType.class));
    }

    @Test
    public void findOrCreateFunctionSpaceMarketSegmentCreatesNewWithDuplicateKeyException() {
        NucleusFunctionSpaceEventType shadowEventType = new NucleusFunctionSpaceEventType();
        when(functionSpaceEventTypeRepository.findFirstByClientCodeAndPropertyCodeAndAbbreviation(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION))
                .thenReturn(null)
                .thenReturn(shadowEventType);
        when(functionSpaceEventTypeRepository.save(any(NucleusFunctionSpaceEventType.class))).thenThrow(new DuplicateKeyException("Duplicate key!"));

        NucleusFunctionSpaceEventType functionSpaceEventType = functionSpaceEventTypeService.findOrCreateEventType(IntegrationType.FUNCTION_SPACE_CLIENT, functionSpaceEvent);
        assertSame(shadowEventType, functionSpaceEventType);

        verify(functionSpaceEventTypeRepository, times(2)).findFirstByClientCodeAndPropertyCodeAndAbbreviation(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION);
        verify(functionSpaceEventTypeRepository).save(Mockito.any(NucleusFunctionSpaceEventType.class));
    }

    @Test
    public void findOrCreateFunctionSpaceMarketSegmentReturnsExisting() {
        NucleusFunctionSpaceEventType functionSpaceEventType = NucleusFunctionSpaceObjectMother.buildFunctionSpaceEventType();
        when(functionSpaceEventTypeRepository.findFirstByClientCodeAndPropertyCodeAndAbbreviation(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION)).thenReturn(functionSpaceEventType);

        NucleusFunctionSpaceEventType foundFunctionSpaceEventType = functionSpaceEventTypeService.findOrCreateEventType(IntegrationType.FUNCTION_SPACE_CLIENT, functionSpaceEvent);
        assertSame(functionSpaceEventType, foundFunctionSpaceEventType);

        verify(functionSpaceEventTypeRepository).findFirstByClientCodeAndPropertyCodeAndAbbreviation(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION);
        verify(functionSpaceEventTypeRepository, never()).save(Mockito.any(NucleusFunctionSpaceEventType.class));
    }

    @Test
    public void findOne() {
        NucleusFunctionSpaceEventType functionSpaceEventType = NucleusFunctionSpaceObjectMother.buildFunctionSpaceEventType();

        when(functionSpaceEventTypeRepository.findFirstByClientCodeAndPropertyCodeAndAbbreviation(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION)).thenReturn(functionSpaceEventType);

        NucleusFunctionSpaceEventType retEventType = functionSpaceEventTypeService.findOne(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION);

        assertSame(functionSpaceEventType, retEventType);
        verify(functionSpaceEventTypeRepository).findFirstByClientCodeAndPropertyCodeAndAbbreviation(CLIENT_CODE, PROPERTY_CODE, ABBREVIATION);
    }

    @Test
    public void findAll() {
        List<NucleusFunctionSpaceEventType> eventTypes = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceEventType());

        when(functionSpaceEventTypeRepository.findByClientCodeAndPropertyCodeOrderByAbbreviationAsc(CLIENT_CODE, PROPERTY_CODE)).thenReturn(eventTypes);

        List<NucleusFunctionSpaceEventType> retEventTypes = functionSpaceEventTypeService.findAll(CLIENT_CODE, PROPERTY_CODE);

        assertTrue(CollectionUtils.isNotEmpty(retEventTypes));
        assertSame(eventTypes.get(0), retEventTypes.get(0));

        verify(functionSpaceEventTypeRepository).findByClientCodeAndPropertyCodeOrderByAbbreviationAsc(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void delete() {
        when(functionSpaceEventTypeRepository.deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE)).thenReturn(1L);

        functionSpaceEventTypeService.delete(CLIENT_CODE, PROPERTY_CODE);

        verify(functionSpaceEventTypeRepository).deleteByClientCodeAndPropertyCode(CLIENT_CODE, PROPERTY_CODE);
    }

    @Test
    public void findAllByDateRange() {
        LocalDateTime startDate = LocalDate.of(2017, 10, 1).atStartOfDay();
        LocalDateTime endDate = LocalDate.of(2017, 10, 31).atStartOfDay();
        PageRequest pageRequest = PageRequest.of(1, 1);
        List<NucleusFunctionSpaceEventType> events = Collections.singletonList(NucleusFunctionSpaceObjectMother.buildFunctionSpaceEventType());
        Page<NucleusFunctionSpaceEventType> page = new PageImpl<>(events);

        when(functionSpaceEventTypeRepository.findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest)).thenReturn(page);

        Page<NucleusFunctionSpaceEventType> eventPages = functionSpaceEventTypeService.findAllByDateRange(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);

        assertNotNull(eventPages);
        assertEquals(1, eventPages.getTotalElements());

        verify(functionSpaceEventTypeRepository).findByClientCodeAndPropertyCodeAndLastModifiedDateBetweenOrderByLastModifiedDateAsc(CLIENT_CODE, PROPERTY_CODE, startDate, endDate, pageRequest);
    }

    @Test
    public void upsertAll() {
        List<NucleusFunctionSpaceEventType> nucleusFunctionSpaceBookings = new ArrayList<>();
        nucleusFunctionSpaceBookings.add(new NucleusFunctionSpaceEventType());
        List<Pair<Query, Update>> updates = new ArrayList<>();
        Mockito.when(functionSpaceEventTypeRepository.bulkOpsUpsertMulti(updates)).thenReturn(null);
        functionSpaceEventTypeService.upsertAll(nucleusFunctionSpaceBookings);
        verify(functionSpaceEventTypeRepository, Mockito.atLeastOnce()).bulkOpsUpsertMulti(any());
    }
}
