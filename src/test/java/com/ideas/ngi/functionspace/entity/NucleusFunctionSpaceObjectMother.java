package com.ideas.ngi.functionspace.entity;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.functionspace.entity.*;
import com.ideas.ngi.nucleus.util.DateTimeUtil;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType.*;
import static com.ideas.ngi.nucleus.data.functionspace.entity.FunctionSpaceBookingCategory.BOOKING;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;

public class NucleusFunctionSpaceObjectMother {
    public static final String CLIENT_CODE = "SAMPLE";
    public static final String PROPERTY_CODE = "FSC01";
    private static final String PROPERTY_ID = "SAMPLE-FSC01";
    public static final Date CURRENT_DATE = new Date();
    public static final Date CURRENT_DATE_PLUS_ONE = DateTimeUtil.toDate(LocalDate.now().plusDays(1));

    public static NucleusFunctionSpaceFunctionRoom buildFunctionSpaceComboFunctionRoom() {
        NucleusFunctionSpaceFunctionRoom comboRoom = buildFunctionSpaceFunctionRoom();
        comboRoom.setShareable(true);

        return comboRoom;
    }

    public static NucleusFunctionSpaceBooking buildFunctionSpaceBooking() {
        NucleusFunctionSpaceBooking functionSpaceBooking = new NucleusFunctionSpaceBooking();

        functionSpaceBooking.setClientCode(CLIENT_CODE);
        functionSpaceBooking.setPropertyCode(PROPERTY_CODE);
        functionSpaceBooking.setIntegrationType(AHWS_SC);
        functionSpaceBooking.setCorrelationMetadataId("1");
        functionSpaceBooking.setAccountNameId("1");
        functionSpaceBooking.setArrivalDate(CURRENT_DATE);
        functionSpaceBooking.setAttendees(1);
        functionSpaceBooking.setBlockCode("blockCode");
        functionSpaceBooking.setBlockName("blockName");
        functionSpaceBooking.setBookingId("bookingId");
        functionSpaceBooking.setBookingStatus("bookingStatus");
        functionSpaceBooking.setBookingType("bookingType");
        functionSpaceBooking.setCancellationDate(CURRENT_DATE);
        functionSpaceBooking.setCateringOnly(true);
        functionSpaceBooking.setCatStatus("catStatus");
        functionSpaceBooking.setCutoffDate(CURRENT_DATE);
        functionSpaceBooking.setCutoffDays(1);
        functionSpaceBooking.setDecisionDate(CURRENT_DATE);
        functionSpaceBooking.setDepartureDate(CURRENT_DATE_PLUS_ONE);
        functionSpaceBooking.setFunctionSpaceBookingCategory(BOOKING);
        functionSpaceBooking.setId("id");
        functionSpaceBooking.setInsertDate(CURRENT_DATE);
        functionSpaceBooking.setMarketSegmentCode("marketSegmentCode");
        functionSpaceBooking.setOpportunityCreateDate(CURRENT_DATE);
        functionSpaceBooking.setPropertyId("propertyId");
        functionSpaceBooking.setStatus("status");
        functionSpaceBooking.setUpdateDate(CURRENT_DATE);

        return functionSpaceBooking;
    }

    public static NucleusFunctionSpaceFunctionRoom buildFunctionSpaceFunctionRoom() {
        NucleusFunctionSpaceFunctionRoom functionSpaceFunctionRoom = new NucleusFunctionSpaceFunctionRoom();

        functionSpaceFunctionRoom.setClientCode(CLIENT_CODE);
        functionSpaceFunctionRoom.setPropertyCode(PROPERTY_CODE);
        functionSpaceFunctionRoom.setAlias("alias");
        functionSpaceFunctionRoom.setAreaSqFeet(ONE);
        functionSpaceFunctionRoom.setAreaSqMeters(ONE);
        functionSpaceFunctionRoom.setBuilding("building");
        functionSpaceFunctionRoom.setCombo(true);
        functionSpaceFunctionRoom.setDescription("description");
        functionSpaceFunctionRoom.setDiaryDisplay(true);
        functionSpaceFunctionRoom.setDiaryName("diaryName");
        functionSpaceFunctionRoom.setExcludedEventType("excludedEventType");
        functionSpaceFunctionRoom.setFloorName("floorName");
        functionSpaceFunctionRoom.setForceAlternate(true);
        functionSpaceFunctionRoom.setFunctionRoomAbbreviation("functionRoomAbbreviation");
        functionSpaceFunctionRoom.setFunctionRoomId("functionRoomId");
        functionSpaceFunctionRoom.setFunctionRoomType("functionRoomType");
        functionSpaceFunctionRoom.setId("id");
        functionSpaceFunctionRoom.setLengthInFeet(ONE);
        functionSpaceFunctionRoom.setLengthInMeters(ONE);
        functionSpaceFunctionRoom.setMaxOccupancy(1);
        functionSpaceFunctionRoom.setMinAdvance(1);
        functionSpaceFunctionRoom.setMinimumRevenue(ONE);
        functionSpaceFunctionRoom.setMinOccupancy(1);
        functionSpaceFunctionRoom.setName("name");
        functionSpaceFunctionRoom.setPropertyId("propertyId");
        functionSpaceFunctionRoom.setRoomCategory("roomCategory");
        functionSpaceFunctionRoom.setRoomClass("roomClass");
        functionSpaceFunctionRoom.setShareable(true);
        functionSpaceFunctionRoom.setSuiteType("suiteType");
        functionSpaceFunctionRoom.setWidthInFeet(ONE);
        functionSpaceFunctionRoom.setWidthInMeters(ONE);

        return functionSpaceFunctionRoom;
    }

    public static NucleusFunctionSpaceEvent buildFunctionSpaceEvent() {
        NucleusFunctionSpaceEvent functionSpaceEvent = new NucleusFunctionSpaceEvent();

        functionSpaceEvent.setIntegrationType(FUNCTION_SPACE_CLIENT);
        functionSpaceEvent.setClientCode(CLIENT_CODE);
        functionSpaceEvent.setPropertyCode(PROPERTY_CODE);
        functionSpaceEvent.setCorrelationMetadataId("1");
        functionSpaceEvent.setActualAttendees(1);
        functionSpaceEvent.setBlockEnd(CURRENT_DATE);
        functionSpaceEvent.setBlockStart(CURRENT_DATE);
        functionSpaceEvent.setBookingId("bookingId");
        functionSpaceEvent.setEndDate(CURRENT_DATE);
        functionSpaceEvent.setEventId("eventId");
        functionSpaceEvent.setEventTypeCode("eventTypeCode");
        functionSpaceEvent.setForecastAttendees(1);
        functionSpaceEvent.setFunctionRoomSetupType("functionRoomSetupType");
        functionSpaceEvent.setGroupId("groupId");
        functionSpaceEvent.setId("id");
        functionSpaceEvent.setMoveable(true);
        functionSpaceEvent.setPropertyId("propertyId");
        functionSpaceEvent.setRoomId("roomId");
        functionSpaceEvent.setSetUpTimeMinutes(1);
        functionSpaceEvent.setStartDate(CURRENT_DATE);
        functionSpaceEvent.setStatus("status");
        functionSpaceEvent.setTearDownTimeMinutes(1);

        return functionSpaceEvent;
    }

    public static NucleusFunctionSpaceEventPace buildNucleusFunctionSpaceEventPace() throws InvocationTargetException, IllegalAccessException {
        NucleusFunctionSpaceEvent nucleusFunctionSpaceEvent = buildFunctionSpaceEvent();
        List<FunctionSpaceEventRevenue> functionSpaceEventRevenueList = new ArrayList<>();
        functionSpaceEventRevenueList.add(buildFunctionSpaceEventRevenue("BEV"));
        nucleusFunctionSpaceEvent.setFunctionSpaceEventRevenues(functionSpaceEventRevenueList);
        NucleusFunctionSpaceEventPace nucleusFunctionSpaceEventPace = new NucleusFunctionSpaceEventPace();
        BeanUtils.copyProperties(nucleusFunctionSpaceEventPace, nucleusFunctionSpaceEvent);
        nucleusFunctionSpaceEventPace.setMoveable(nucleusFunctionSpaceEvent.isMoveable());
        nucleusFunctionSpaceEventPace.setChangeDate(DateTimeUtil.buildDate(2019, 07, 14));
        return nucleusFunctionSpaceEventPace;
    }

    public static NucleusFunctionSpaceBookingGuestRoom buildFunctionSpaceBookingGuestRoom() {
        return buildFunctionSpaceBookingGuestRoom(IntegrationType.FUNCTION_SPACE_CLIENT);
    }

    public static NucleusFunctionSpaceBookingGuestRoom buildFunctionSpaceBookingGuestRoom(IntegrationType integrationType) {

        NucleusFunctionSpaceBookingGuestRoom functionSpaceBookingGuestRoom = new NucleusFunctionSpaceBookingGuestRoom();

        functionSpaceBookingGuestRoom.setId("1");
        functionSpaceBookingGuestRoom.setIntegrationType(integrationType);
        functionSpaceBookingGuestRoom.setBlockedDoubleRooms(2);
        functionSpaceBookingGuestRoom.setBlockedQuadRooms(4);
        functionSpaceBookingGuestRoom.setBlockedSingleRooms(1);
        functionSpaceBookingGuestRoom.setBlockedTripleRooms(3);
        functionSpaceBookingGuestRoom.setContractedDoubleRooms(2);
        functionSpaceBookingGuestRoom.setContractedQuadRooms(4);
        functionSpaceBookingGuestRoom.setContractedRoomsTotal(10);
        functionSpaceBookingGuestRoom.setContractedSingleRooms(1);
        functionSpaceBookingGuestRoom.setContractedTripleRooms(3);
        functionSpaceBookingGuestRoom.setForecastedRoomsTotal(10);
        functionSpaceBookingGuestRoom.setGuestRoomId("guestRoomId");
        functionSpaceBookingGuestRoom.setGuestRoomRateDouble(ONE);
        functionSpaceBookingGuestRoom.setGuestRoomRateQuad(ONE);
        functionSpaceBookingGuestRoom.setGuestRoomRateSingle(ONE);
        functionSpaceBookingGuestRoom.setGuestRoomRateTriple(ONE);
        functionSpaceBookingGuestRoom.setOccupancyDate(CURRENT_DATE);
        functionSpaceBookingGuestRoom.setPickupDoubleRooms(2);
        functionSpaceBookingGuestRoom.setPickupQuadRooms(4);
        functionSpaceBookingGuestRoom.setPickupRoomsTotal(10);
        functionSpaceBookingGuestRoom.setPickupSingleRooms(1);
        functionSpaceBookingGuestRoom.setPickupTripleRooms(3);
        functionSpaceBookingGuestRoom.setRoomCategory("roomCategory");
        functionSpaceBookingGuestRoom.setRoomClass("roomClass");
        functionSpaceBookingGuestRoom.setScRoomCategory("scRoomCategory");
        functionSpaceBookingGuestRoom.setStatus("status");
        functionSpaceBookingGuestRoom.setCorrelationMetadataId("correlationMetadataId");

        return functionSpaceBookingGuestRoom;
    }

    public static NucleusFunctionSpaceGuestRoomType buildFunctionSpaceBookingGuestRoomType() {
        NucleusFunctionSpaceGuestRoomType functionSpaceGuestRoomType = new NucleusFunctionSpaceGuestRoomType();

        functionSpaceGuestRoomType.setId("ID");
        functionSpaceGuestRoomType.setIntegrationType(OXI_SC);
        functionSpaceGuestRoomType.setPropertyId("PROPERTY_ID");
        functionSpaceGuestRoomType.setFunctionRoomType("FUNCTION_ROOM_TYPE");
        functionSpaceGuestRoomType.setMaxOccupancy(2);
        functionSpaceGuestRoomType.setDescription("DESCRIPTION");
        functionSpaceGuestRoomType.setRoomCategory("ROOM_CATEGORY");
        functionSpaceGuestRoomType.setRoomClass("ROOM_CLASS");
        functionSpaceGuestRoomType.setNoOfRooms(3);
        functionSpaceGuestRoomType.setInactive(false);

        return functionSpaceGuestRoomType;
    }

    public static NucleusFunctionSpaceMarketSegment buildFunctionSpaceMarketSegment() {
        NucleusFunctionSpaceMarketSegment nucleusFunctionSpaceMarketSegment = new NucleusFunctionSpaceMarketSegment();

        nucleusFunctionSpaceMarketSegment.setId("1");
        nucleusFunctionSpaceMarketSegment.setIntegrationType(IntegrationType.FUNCTION_SPACE_CLIENT);
        nucleusFunctionSpaceMarketSegment.setCreateDate(CURRENT_DATE);
        nucleusFunctionSpaceMarketSegment.setPropertyId("PropertyId");
        nucleusFunctionSpaceMarketSegment.setClientCode(CLIENT_CODE);
        nucleusFunctionSpaceMarketSegment.setPropertyCode(PROPERTY_CODE);
        nucleusFunctionSpaceMarketSegment.setAbbreviation("ABC");
        nucleusFunctionSpaceMarketSegment.setName("Name");
        nucleusFunctionSpaceMarketSegment.setDescription("Desc");
        nucleusFunctionSpaceMarketSegment.setSalesAndCateringLastModified(CURRENT_DATE);
        nucleusFunctionSpaceMarketSegment.setParentMarketSegmentId("ParentMktSegId");

        return nucleusFunctionSpaceMarketSegment;
    }

    public static NucleusFunctionSpaceBookingPace buildFunctionSpaceBookingPace() {
        NucleusFunctionSpaceBookingPace functionSpaceBookingChange = new NucleusFunctionSpaceBookingPace();

        functionSpaceBookingChange.setIntegrationType(OXI_SC);
        functionSpaceBookingChange.setBookingPaceId("bookingPaceId");
        functionSpaceBookingChange.setBookingId("bookingId");
        functionSpaceBookingChange.setPropertyId("propertyId");
        functionSpaceBookingChange.setBookingStatus("bookingStatus");
        functionSpaceBookingChange.setChangeDate(CURRENT_DATE);
        functionSpaceBookingChange.setRoomNights(1);
        functionSpaceBookingChange.setRoomRevenue(ZERO);
        functionSpaceBookingChange.setStayDate(CURRENT_DATE);
        functionSpaceBookingChange.setUpdateDate(CURRENT_DATE);
        functionSpaceBookingChange.setCateringStatus("INQ");
        functionSpaceBookingChange.setGuestRoomStatus("DEF");
        functionSpaceBookingChange.setCorrelationMetadataId("correlationMetadataId");

        return functionSpaceBookingChange;
    }

    public static NucleusFunctionSpaceGuestRoomPace buildFunctionSpaceGuestRoomPace() {
        NucleusFunctionSpaceGuestRoomPace functionSpaceGuestRoomPace = new NucleusFunctionSpaceGuestRoomPace();

        functionSpaceGuestRoomPace.setId("id");
        functionSpaceGuestRoomPace.setBookingPaceId("bookingPaceId");
        functionSpaceGuestRoomPace.setBookingId("bookingId");
        functionSpaceGuestRoomPace.setPropertyId("propertyId");
        functionSpaceGuestRoomPace.setChangeDate(CURRENT_DATE);
        functionSpaceGuestRoomPace.setRoomNights(1);
        functionSpaceGuestRoomPace.setRoomRevenue(ZERO);
        functionSpaceGuestRoomPace.setStayDate(CURRENT_DATE);
        functionSpaceGuestRoomPace.setRoomType("roomType");
        functionSpaceGuestRoomPace.setRoomRevenueChanged(ZERO);
        functionSpaceGuestRoomPace.setRoomNightsChanged(1);
        functionSpaceGuestRoomPace.setIntegrationType(OXI_SC);
        functionSpaceGuestRoomPace.setGuestRoomStatus("DEF");

        return functionSpaceGuestRoomPace;
    }

    public static FunctionSpaceBookingRevenue buildFunctionSpaceBookingRevenue() {
        FunctionSpaceBookingRevenue functionSpaceBookingRevenue = new FunctionSpaceBookingRevenue();

        functionSpaceBookingRevenue.setActualRevenue(BigDecimal.valueOf(1000));
        functionSpaceBookingRevenue.setExpectedRevenue(BigDecimal.valueOf(900));
        functionSpaceBookingRevenue.setForecastRevenue(BigDecimal.valueOf(1100));
        functionSpaceBookingRevenue.setRevenueGroup("RevGroup");
        functionSpaceBookingRevenue.setRevenueType("Food");

        return functionSpaceBookingRevenue;
    }

    public static FunctionSpaceEventRevenue buildFunctionSpaceEventRevenue(String revenueGroup) {
        Date date = DateTimeUtil.buildDate(2017, 7, 31);
        FunctionSpaceEventRevenue functionSpaceEventRevenue = new FunctionSpaceEventRevenue();

        functionSpaceEventRevenue.setRevenueGroup(revenueGroup);
        functionSpaceEventRevenue.setRevenueType(revenueGroup);
        functionSpaceEventRevenue.setActualRevenue(BigDecimal.valueOf(200.00).setScale(5, RoundingMode.HALF_UP));
        functionSpaceEventRevenue.setExpectedRevenue(BigDecimal.valueOf(190.00).setScale(5, RoundingMode.HALF_UP));
        functionSpaceEventRevenue.setGuaranteedRevenue(BigDecimal.valueOf(195.00).setScale(5, RoundingMode.HALF_UP));
        functionSpaceEventRevenue.setForecastRevenue(BigDecimal.valueOf(197.00).setScale(5, RoundingMode.HALF_UP));
        functionSpaceEventRevenue.setInsertDate(date);
        functionSpaceEventRevenue.setUpdateDate(date);

        return functionSpaceEventRevenue;
    }

    public static NucleusFunctionSpaceEventType buildFunctionSpaceEventType() {
        NucleusFunctionSpaceEventType eventType = new NucleusFunctionSpaceEventType();
        eventType.setIntegrationType(IntegrationType.AHWS_SC);
        eventType.setPropertyId(PROPERTY_ID);
        eventType.setAbbreviation("EVT1");
        eventType.setName("Event 1");
        eventType.setId("1");
        return eventType;
    }

    public static FunctionSpaceBookingRevenue buildFunctionSpaceBookingRevenue(String revenueGroup) {
        FunctionSpaceBookingRevenue functionSpaceBookingRevenue = new FunctionSpaceBookingRevenue();

        functionSpaceBookingRevenue.setRevenueGroup(revenueGroup);
        functionSpaceBookingRevenue.setRevenueType(revenueGroup);
        functionSpaceBookingRevenue.setActualRevenue(BigDecimal.valueOf(200.00));
        functionSpaceBookingRevenue.setExpectedRevenue(BigDecimal.valueOf(50.00).setScale(5, RoundingMode.HALF_UP));
        functionSpaceBookingRevenue.setGuaranteedRevenue(BigDecimal.valueOf(195.00));
        functionSpaceBookingRevenue.setForecastRevenue(BigDecimal.valueOf(197.00));

        return functionSpaceBookingRevenue;
    }
}
