package com.ideas.ngi.functionspace;

import com.ideas.ngi.common.test.categories.ComponentTest;
import com.ideas.ngi.common.test.retry.RetryRule;
import com.ideas.ngi.nucleus.config.TestFunctionSpaceApplicationConfig;
import com.ideas.ngi.nucleus.config.internalclient.CompletedMonitorMessageHandler;
import com.ideas.ngi.nucleus.config.internalclient.ErrorMonitorMessageHandler;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.integration.repository.IntegrationConfigRepository;
import com.ideas.ngi.nucleus.data.vendor.repository.NucleusVendorConfigParamsRepository;
import com.ideas.ngi.nucleus.monitor.entity.NucleusCompletedMessage;
import com.ideas.ngi.nucleus.monitor.entity.NucleusErrorMessage;
import com.ideas.ngi.nucleus.monitor.repository.NucleusCompletedMessageRepository;
import com.ideas.ngi.nucleus.monitor.repository.NucleusErrorMessageRepository;
import com.ideas.ngi.nucleus.rest.client.RestClient;
import com.ideas.ngi.nucleus.rest.client.stateful.BasicAuthenticationRestTemplate;
import com.ideas.ngi.nucleus.test.IntegrationSettingsTestUtil;
import com.ideas.ngi.nucleus.test.TestFileUtil;
import com.ideas.ngi.nucleus.test.listener.MongoExtractOnFailureListener;
import org.apache.commons.collections.CollectionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.integration.context.IntegrationContextUtils;
import org.springframework.integration.support.management.AbstractMessageChannelMetrics;
import org.springframework.integration.support.management.ConfigurableMetricsAware;
import org.springframework.integration.support.management.DefaultMessageChannelMetrics;
import org.springframework.integration.support.management.MessageChannelMetrics;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static com.ideas.ngi.nucleus.util.ThreadUtil.waitUntil;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


@ContextConfiguration(classes = {TestFunctionSpaceApplicationConfig.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@TestPropertySource({"/application.properties", "/test-application.properties"})
@Category(ComponentTest.class)
@RunWith(SpringRunner.class)
@TestExecutionListeners(MongoExtractOnFailureListener.class)
public abstract class IntegrationFlowTest extends AbstractJUnit4SpringContextTests {

    @Captor
    protected ArgumentCaptor<String> monitorRestEndpoints;

    @Captor
    protected ArgumentCaptor<Object> monitorMessages;

    @Captor
    protected ArgumentCaptor<Class<Long>> responseTypes;

    @Captor
    protected ArgumentCaptor<Map<String, Object>> monitorParameters;

    @Autowired
    protected RestClient g3MonitorRestClient;

    @Autowired
    protected RestClient intfMonitorRestClient;

    @Autowired
    protected RestClient g3RestClient;

    @Autowired
    protected Environment environment;

    @Mock
    protected BasicAuthenticationRestTemplate g3RestTemplate;

    @Mock
    protected BasicAuthenticationRestTemplate monitorRestTemplate;

    @Autowired
    protected NucleusCompletedMessageRepository completedMessageRepository;

    @Autowired
    protected NucleusErrorMessageRepository errorMessageRepository;

    @Autowired
    protected NucleusVendorConfigParamsRepository nucleusVendorConfigParamsRepository;

    @Autowired
    protected IntegrationConfigRepository integrationConfigRepository;

    @Autowired
    protected IntegrationSettingsTestUtil integrationSettingsTestUtil;

    @Autowired
    protected CompletedMonitorMessageHandler completedMonitorMessageHandler;

    @Autowired
    protected ErrorMonitorMessageHandler errorMonitorMessageHandler;

    @Autowired
    @Qualifier(IntegrationContextUtils.NULL_CHANNEL_BEAN_NAME)
    protected MessageChannel nullChannel;

    @Rule
    public RetryRule retryRule = new RetryRule();

    // This method is final so that subclasses cannot override it and forget to call super()
    @Before
    public final void baseBefore() {
        MockitoAnnotations.initMocks(this);

        // Be sure to reset the mock RestClients
        Mockito.reset(monitorRestTemplate);
        g3MonitorRestClient.setStatefulRestTemplate(monitorRestTemplate);
        intfMonitorRestClient.setStatefulRestTemplate(monitorRestTemplate);

        Mockito.reset(g3RestTemplate);
        g3RestClient.setStatefulRestTemplate(g3RestTemplate);

        // Mock out the calls to the Monitor Endpoints
        Mockito.when(monitorRestTemplate.postForObject(monitorRestEndpoints.capture(), monitorMessages.capture(), responseTypes.capture(), monitorParameters.capture())).thenReturn(1L);

        configureMetrics(nullChannel);
    }

    @After
    public void after() {
        completedMessageRepository.deleteAll();
        errorMessageRepository.deleteAll();
    }

    protected List<NucleusCompletedMessage> waitForCompletedMessages(String jobName, int numberOfExpectedCompletedMessages) {
        return doWithRetry(() -> completedMessageRepository.findByJobNameAndJobInstanceIdIsNotNull(jobName), (results) -> CollectionUtils.size(results) == numberOfExpectedCompletedMessages);
    }

    protected List<NucleusErrorMessage> waitForErrorMessages(String jobName, int numberOfExpectedErrorMessages) {
        return doWithRetry(() -> errorMessageRepository.findByJobNameAndJobInstanceIdIsNotNull(jobName), (results) -> CollectionUtils.size(results) == numberOfExpectedErrorMessages);
    }

    protected <T> T doWithRetry(Supplier<T> doer) {
        return doWithRetry(doer, Objects::nonNull);
    }

    protected <T> T doWithRetry(Supplier<T> doer, Predicate<T> predicate) {
        return waitUntil(doer, predicate);
    }

    protected File getResourcesFile(String relativePath) {
        String filePath = getClass().getResource("/").getFile();
        if (filePath.endsWith("/out/test/classes/")) {
            return new File(getClass().getResource("/").getFile() + "../resources/" + relativePath);
        }

        return new File(getClass().getResource("/").getFile() + "../../../resources/test/" + relativePath);
    }

    protected File getClassPathResourcesFile(String resource) {
        try {
            return new ClassPathResource(resource).getFile();
        } catch (IOException e) {
            throw new RuntimeException("Could not get resource: " + resource, e);
        }
    }

    protected File getResourceAsFile(String relativePath) {
        try {
            URL uri = getClass().getResource(relativePath);
            if (uri == null) {
                throw new RuntimeException("Resource " + relativePath + " does not exist or could not be found");
            }
            return new File(uri.toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException("Could not get resource: " + relativePath, e);
        }
    }

    protected String getResourceAsString(String resourcePath) {
        return TestFileUtil.getResourceAsString(resourcePath, this.getClass());
    }

    @SuppressWarnings("unchecked")
    protected AbstractMessageChannelMetrics configureMetrics(MessageChannel channel) {
        AbstractMessageChannelMetrics metrics = new DefaultMessageChannelMetrics();
        if (channel instanceof ConfigurableMetricsAware) {
            ((ConfigurableMetricsAware) channel).configureMetrics(metrics);
        } else {
            throw new AssertionError("Cannot add metrics to this channel");
        }

        if (channel instanceof MessageChannelMetrics) {
            MessageChannelMetrics messageChannel = (MessageChannelMetrics) channel;
            messageChannel.setCountsEnabled(true);
            messageChannel.setStatsEnabled(true);
        } else {
            throw new AssertionError("Cannot add metrics to this channel");
        }

        metrics.reset();
        return metrics;
    }

    protected void assertMessageCount(int messageCount, MessageChannel messageChannel) {
        if (messageChannel instanceof MessageChannelMetrics) {
            assertEquals(messageCount, ((MessageChannelMetrics) messageChannel).getSendCount());
        } else {
            fail("Message channel does not support MessageChannelMetrics");
        }
    }

    protected void ensureConfigurationSettings(IntegrationType integrationType, String clientCode, String propertyCode) {
        ensureConfigurationSettings(integrationType, clientCode, propertyCode, null);
    }

    protected void ensureConfigurationSettings(IntegrationType integrationType, String clientCode, String propertyCode, Boolean nonPickedUpBlocks) {
        ensureConfigurationSettings(integrationType, clientCode, propertyCode, nonPickedUpBlocks, true, true, false);
    }

    protected void ensureConfigurationSettings(IntegrationType integrationType, String clientCode, String propertyCode, Boolean nonPickedUpBlocks,
                                               Boolean pseudoRTRevenue, Boolean generateRateCodeStats) {
        ensureConfigurationSettings(integrationType, clientCode, propertyCode, nonPickedUpBlocks, pseudoRTRevenue, generateRateCodeStats, false);
    }

    protected void ensureConfigurationSettings(
            IntegrationType integrationType,
            String clientCode,
            String propertyCode,
            Boolean nonPickedUpBlocks,
            Boolean pseudoRTRevenueEnabled,
            Boolean generateRateCodeStatsEnabled,
            Boolean generateHotelActivityFromRoomTypeActivity) {

        integrationSettingsTestUtil.ensureSettings(integrationType, v -> {
        });

        integrationSettingsTestUtil.ensureSettings(integrationType, clientCode, c -> {
            c.setClientEnvironmentName("g3");
        });

        integrationSettingsTestUtil.ensureSettings(integrationType, clientCode, propertyCode, p -> {
            p.setCalculateNonPickedUpBlocksUsingSummaryData(nonPickedUpBlocks);
            p.setPreventPseudoDataInActivity(pseudoRTRevenueEnabled);
        });

        integrationSettingsTestUtil.ensureSettings(integrationType, clientCode, propertyCode, p -> {
            p.setGenerateRateCodeStatsEnabled(generateRateCodeStatsEnabled);
        });

        integrationSettingsTestUtil.ensureSettings(integrationType, clientCode, propertyCode, p -> {
            p.setGenerateHotelActivityFromRoomTypeActivity(generateHotelActivityFromRoomTypeActivity);
        });
    }

    protected void clearConfigurations() {
        nucleusVendorConfigParamsRepository.deleteAll();
        integrationConfigRepository.deleteAll();
    }
}