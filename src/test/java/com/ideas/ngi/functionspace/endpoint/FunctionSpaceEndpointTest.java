package com.ideas.ngi.functionspace.endpoint;

import org.junit.Test;
import org.springframework.http.HttpMethod;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class FunctionSpaceEndpointTest {
    @Test
    public void testEnum() {
        assertEquals("ngiEventService/event/functionspace/?propertyId={defaultPropertyId}&sendingSystemPropertyId={sendingSystemPropertyId}&integrationType={integrationType}&clientCode={clientCode}&propertyCode={propertyCode}", FunctionSpaceEndpoint.POST_EVENT.getURL());
        assertEquals(HttpMethod.POST, FunctionSpaceEndpoint.POST_EVENT.getHttpMethod());
        assertEquals(List.class, FunctionSpaceEndpoint.POST_EVENT.getResponseType());
    }
}
