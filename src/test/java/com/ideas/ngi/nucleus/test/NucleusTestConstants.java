package com.ideas.ngi.nucleus.test;

/**
 * Miscellaneous field names and/or values commonly used in testing.
 */
public class NucleusTestConstants {

    public static final String CLIENT_CODE = "SANDBOXNUCLEUS";
    public static final String PROFILE_ID = "profileId";
    public static final String PROPERTY_CODE = "propertyCode";
}
