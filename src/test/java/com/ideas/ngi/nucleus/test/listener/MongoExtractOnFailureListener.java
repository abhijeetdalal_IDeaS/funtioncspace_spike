package com.ideas.ngi.nucleus.test.listener;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import org.bson.Document;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.support.AbstractTestExecutionListener;

import java.util.Objects;

public class MongoExtractOnFailureListener extends AbstractTestExecutionListener {

    @Override
    public void afterTestMethod(TestContext testContext) {
        if (testContext.getTestException() != null) {
            MongoTemplate mongoTemplate = testContext.getApplicationContext().getBean(MongoTemplate.class);
            System.out.println("Detected a test failure!  Exporting mongo data from db " + mongoTemplate.getDb().getName() + "...");

            mongoTemplate.getCollectionNames().stream()
                    // exclude change logs to reduce noise
                    .filter(this::excludesChangeLogs)
                    // dump data for collections with data
                    .forEach(collectionName -> {
                        MongoCollection<Document> collection = mongoTemplate.getCollection(collectionName);
                        FindIterable<Document> result = collection.find();
                        try (MongoCursor<Document>  cursor = result.iterator()) {
                            if (cursor.hasNext()) {
                                System.out.println("-----------------------------------------------");
                                System.out.println(collectionName);
                                System.out.println("-----------------------------------------------");

                                cursor.forEachRemaining(o -> System.out.println(Objects.toString(o)));
                            }
                        }
                    });
        }
    }

    private boolean excludesChangeLogs(String collectionName) {
        return !collectionName.endsWith("ChangeLog");
    }
}
