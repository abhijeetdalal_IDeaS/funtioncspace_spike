package com.ideas.ngi.nucleus.test;


import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.integration.entity.BaseIntegrationConfig;
import com.ideas.ngi.nucleus.data.integration.entity.IntegrationClientConfig;
import com.ideas.ngi.nucleus.data.integration.entity.IntegrationPropertyConfig;
import com.ideas.ngi.nucleus.data.integration.entity.IntegrationVendorConfig;
import com.ideas.ngi.nucleus.data.integration.service.IntegrationSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Component
public class IntegrationSettingsTestUtil {

    private IntegrationSettingsService settingsService;

    @Autowired
    public IntegrationSettingsTestUtil(IntegrationSettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void ensureSettings(IntegrationType integrationType, String clientCode, String propertyCode, Consumer<IntegrationPropertyConfig> configurer) {
        IntegrationPropertyConfig settings = settingsService.getPropertySettings(integrationType.name(), clientCode, propertyCode);
        if (settings == null) {
            settings = new IntegrationPropertyConfig(integrationType.name(), integrationType, clientCode, propertyCode);
        }
        configurer.accept(settings);
        saveSettings(settings);
    }

    public void ensureSettings(IntegrationType integrationType, String clientCode, Consumer<IntegrationClientConfig> configurer) {
        IntegrationClientConfig settings = settingsService.getClientSettings(integrationType.name(), clientCode);
        if (settings == null) {
            settings = new IntegrationClientConfig(integrationType.name(), integrationType, clientCode);
        }
        configurer.accept(settings);
        saveSettings(settings);
    }

    public void ensureSettings(IntegrationType integrationType, Consumer<IntegrationVendorConfig> configurer) {
        IntegrationVendorConfig settings = settingsService.getVendorSettings(integrationType.name());
        if (settings == null) {
            settings = new IntegrationVendorConfig(integrationType.name(), integrationType);
        }
        configurer.accept(settings);
        saveSettings(settings);
    }

    public void removeSettings(IntegrationType integrationType) {
        IntegrationVendorConfig settings = settingsService.getVendorSettings(integrationType.name());
        if (settings != null) {
            settingsService.deleteSettings(settings);
        }
    }

    public void removeSettings(IntegrationType integrationType, String clientCode) {
        IntegrationClientConfig settings = settingsService.getClientSettings(integrationType.name(), clientCode);
        if (settings != null) {
            settingsService.deleteSettings(settings);
        }
    }

    public void removeSettings(IntegrationType integrationType, String clientCode, String propertyCode) {
        IntegrationPropertyConfig settings = settingsService.getPropertySettings(integrationType.name(), clientCode, propertyCode);
        if (settings != null) {
            settingsService.deleteSettings(settings);
        }
    }

    private void saveSettings(BaseIntegrationConfig settings) {
        if (settings.getId() == null) {
            settingsService.createSettings(settings);
        } else {
            settingsService.updateSettings(settings.getId(), settings);
        }
    }
}
