package com.ideas.ngi.nucleus.test;

import com.ideas.ngi.nucleus.exception.NucleusException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

public class TestFileUtil {

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static File createTemporaryDirectory() {
        return createTemporaryDirectory(getBaseTestDirectory().getAbsolutePath() + "/../../tmp/test/data" + File.separator + System.currentTimeMillis());
    }

    public static File createTemporaryDirectory(String directoryPath) {
        File tempDirectory = new File(directoryPath);
        tempDirectory.mkdirs();
        return tempDirectory;
    }

    public static File getTestFile(String fileName) {
        return getTestFile(getBaseTestDirectory(), fileName);
    }

    public static File getTestFile(File parentDirectory, String fileName) {
        File testFile = new File(parentDirectory, fileName);
        try {
            FileUtils.touch(testFile);
        } catch (IOException e) {
            throw new NucleusException("there was an issue creating the file :" + testFile.getAbsolutePath(), e);
        }
        return testFile;
    }

    public static File getBaseTestDirectory() {
        return new File(TestFileUtil.class.getResource("/").getFile());
    }

    public static String getResourceAsString(String resource, Class<?> clazz) {
        try {
            InputStream stream = clazz.getResourceAsStream(resource);
            return IOUtils.toString(stream, Charset.defaultCharset());
        } catch (NullPointerException npe) {
            throw new IllegalArgumentException("Could not find resource: " + resource, npe);
        } catch (IOException e) {
            throw new RuntimeException("Could not read resource: " + resource, e);
        }
    }

    public static String getResourceAsString(String resource) {
        return getResourceAsString(resource, TestFileUtil.class);
    }
}
