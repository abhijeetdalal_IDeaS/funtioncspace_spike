package com.ideas.ngi.nucleus.util;

import com.ideas.ngi.nucleus.validator.NucleusValidationException;
import com.ideas.ngi.nucleus.validator.NucleusValidator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ValidationTestHelper {

    public static void validate(Object obj, String... expectedErrors) {
        try {
            getNucleusValidator().validate(obj);

            if (expectedErrors == null || expectedErrors.length > 0) {
                fail("Record should have thrown validation errors");
            }
        } catch (NucleusValidationException nve) {
            if (expectedErrors == null || expectedErrors.length== 0) {
                fail("Expected no validation errors but found: " + nve.getMessage());
            }

            for (String expectedError : expectedErrors) {
                assertTrue("Unable to find error: " + expectedError, nve.getMessage().contains(expectedError));
            }
        }
    }
    
    public static NucleusValidator getNucleusValidator() {
        LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
        localValidatorFactoryBean.afterPropertiesSet();
        return new NucleusValidator(localValidatorFactoryBean);
    }

}
