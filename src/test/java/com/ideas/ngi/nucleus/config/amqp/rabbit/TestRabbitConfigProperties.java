package com.ideas.ngi.nucleus.config.amqp.rabbit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Contains all System properties used in tests related to RabbitMQ.
 */

@Component
public class TestRabbitConfigProperties extends RabbitConfigProperties {
    @Value("${rabbit.virtualHost:test}")
    private String virtualHost;

    public String getVirtualHost() {
        return virtualHost;
    }

    public void setVirtualHost(String virtualHost) {
        this.virtualHost = virtualHost;
    }
}