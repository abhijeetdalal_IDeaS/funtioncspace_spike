package com.ideas.ngi.nucleus.config;

import com.ideas.ngi.functionspace.config.FunctionSpaceApplicationConfig;
import com.ideas.ngi.nucleus.config.amqp.rabbit.TestRabbitConfigProperties;
import com.ideas.ngi.nucleus.config.mongo.NucleusMongoDbFactory;
import com.ideas.ngi.nucleus.config.mongo.TestMongoConfigProperties;
import com.ideas.ngi.nucleus.config.mongo.migration.MigrationRunnerFactory;
import com.ideas.ngi.nucleus.config.quartz.config.AutowiringQuartzJobFactory;
import com.ideas.ngi.nucleus.integration.NucleusIntegrationComponent;
import com.ideas.ngi.nucleus.integration.util.MessageHeaderUtil;
import com.ideas.ngi.nucleus.monitor.gateway.ErrorMessageGateway;
import com.ideas.ngi.nucleus.rest.client.RestClient;
import com.ideas.ngi.nucleus.rest.endpoint.RestEndpoint;
import com.ideas.ngi.nucleus.rest.handler.RestHandler;
import com.mongodb.MongoClient;
import org.quartz.SimpleScheduleBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.router.RecipientListRouter;
import org.springframework.messaging.MessageChannel;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import static com.ideas.ngi.nucleus.config.mongo.GlobalMongoConfig.*;

@Import(FunctionSpaceApplicationConfig.class)
public class TestFunctionSpaceApplicationConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestFunctionSpaceApplicationConfig.class);

    public static final String RETRY_TEST_CHANNEL = "retryTestChannel";
    public static final String ERROR_INPUT_TEST_CHANNEL = "errorInputChannel_TEST";
    public static final String ERROR_INPUT_DIRECT_TEST_CHANNEL = "errorInputDirectChannel_TEST";
    public static final String ERROR_A_TEST_CHANNEL = "errorAChannel_TEST";
    public static final String INTERNAL_CLIENT_INPUT_TEST_CHANNEL = "internalClientInputChannel_TEST";
    public static final String UNIT_TEST_TARGET_CHANNEL = "unitTestTargetChannel";
    public static final String UNIT_TEST_TARGET_ENDPOINT = "unitTestTargetEndpoint";

    @Autowired
    private AutowiringQuartzJobFactory autowiringQuartzJobFactory;

    @Autowired
    private TestMongoConfigProperties testMongoConfigProperties;

    @Autowired
    private TestRabbitConfigProperties testRabbitConfigProperties;

    @Autowired
    private RecipientListRouter allInternalClientsRestClientRouter;

    @Bean
    @Primary
    public ConnectionFactory rabbitConnectionFactory() {
        TestConfigUtils.createVirtualHost(testRabbitConfigProperties);
        TestConfigUtils.givePermissionsToVirtualHost(testRabbitConfigProperties);

        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(testRabbitConfigProperties.getHost(), testRabbitConfigProperties.getPort());
        connectionFactory.setUsername(testRabbitConfigProperties.getRabbitUser());
        connectionFactory.setPassword(testRabbitConfigProperties.getRabbitPassword());
        connectionFactory.setVirtualHost(testRabbitConfigProperties.getVirtualHost());
        return connectionFactory;
    }

    @Bean
    @Primary
    public NucleusMongoDbFactory mongoDbFactory(MongoClient mongo) {
        LOGGER.info("Configuring test MongoDbFactory for database {}", testMongoConfigProperties.getNGIDatabaseName());
        return new NucleusMongoDbFactory(mongo, testMongoConfigProperties.getNGIDatabaseName());
    }

    @Bean
    @Primary
    public MongoTemplate mongoTemplate(NucleusMongoDbFactory mongoDbFactory, MongoConverter mongoConverter) {
        LOGGER.info("Configuring test MongoTemplate for database {}", testMongoConfigProperties.getNGIDatabaseName());
        return new MongoTemplate(mongoDbFactory, mongoConverter);
    }

    @Bean(GLOBAL_MONGO_FACTORY)
    public MongoDbFactory globalMongoFactory(@Qualifier(GLOBAL_MONGO_CLIENT) MongoClient mongo) {
        LOGGER.info("Configuring test MongoDbFactory for database {}", testMongoConfigProperties.getGlobalDatabaseName());
        return new NucleusMongoDbFactory(mongo, testMongoConfigProperties.getGlobalDatabaseName());
    }

    @Bean(GLOBAL_MONGO_TEMPLATE)
    public MongoTemplate globalMongoTemplate(@Qualifier(GLOBAL_MONGO_FACTORY) MongoDbFactory mongoDbFactory, MongoConverter mongoConverter) {
        LOGGER.info("Configuring test MongoTemplate for database {}", testMongoConfigProperties.getGlobalDatabaseName());
        return new MongoTemplate(mongoDbFactory, mongoConverter);
    }

    @Bean(name = "GridFsTemplate")
    @Primary
    public GridFsTemplate gridFsTemplate(NucleusMongoDbFactory mongoDbFactory, MongoConverter mongoConverter) {
        return new GridFsTemplate(mongoDbFactory, mongoConverter);
    }

    @Bean
    @Primary
    public MigrationRunnerFactory mongoRunnerBeanFactory(MongoClient mongo, @Qualifier(GLOBAL_MONGO_CLIENT) MongoClient globalMongo, ErrorMessageGateway errorMessageGateway) {
        MigrationRunnerFactory beanFactory = new MigrationRunnerFactory(mongo, globalMongo, testMongoConfigProperties, errorMessageGateway);
        beanFactory.setMongoDatabase(testMongoConfigProperties.getNGIDatabaseName());
        beanFactory.setMigrationEnabled(true);
        return beanFactory;
    }

    @Bean
    @Primary
    public SchedulerFactoryBean schedulerFactoryBean() {
        SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
        scheduler.setApplicationContextSchedulerContextKey("applicationContext");
        scheduler.setWaitForJobsToCompleteOnShutdown(false);
        scheduler.setAutoStartup(false);
        scheduler.setJobFactory(autowiringQuartzJobFactory);
        return scheduler;
    }

    @Bean
    @Primary
    public SimpleScheduleBuilder simpleScheduleBuilder() {
        return SimpleScheduleBuilder.repeatSecondlyForever();
    }

    @Bean(name = RETRY_TEST_CHANNEL)
    public MessageChannel retryTestChannel(NucleusIntegrationComponent nucleusIntegrationComponent) {
        FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(100);
        return nucleusIntegrationComponent.amqpChannel(RETRY_TEST_CHANNEL, 1, nucleusIntegrationComponent.retryAdvice(2, backOffPolicy));
    }

    @Bean
    public IntegrationFlow retryTestChannelFlow(NucleusIntegrationComponent nucleusIntegrationComponent, RestHandler restHandler, RestClient g3RestClient) {
        return nucleusIntegrationComponent
                .from(RETRY_TEST_CHANNEL)
                .handle((p, h) -> restHandler.handle(g3RestClient, (RestEndpoint) h.get(MessageHeaderUtil.REST_ENDPOINT), p))
                .get();

    }

    @Bean(name = ERROR_INPUT_DIRECT_TEST_CHANNEL)
    public MessageChannel errorInputDirectTestChannel(NucleusIntegrationComponent nucleusIntegrationComponent) {
        return nucleusIntegrationComponent.direct(ERROR_INPUT_DIRECT_TEST_CHANNEL);
    }

    @Bean(name = ERROR_A_TEST_CHANNEL)
    public MessageChannel errorATestChannel(NucleusIntegrationComponent nucleusIntegrationComponent) {
        return nucleusIntegrationComponent.amqpChannel(ERROR_A_TEST_CHANNEL);
    }

    @Bean(name = ERROR_INPUT_TEST_CHANNEL)
    public MessageChannel errorInputTestChannel(NucleusIntegrationComponent nucleusIntegrationComponent) {
        return nucleusIntegrationComponent.amqpChannel(ERROR_INPUT_TEST_CHANNEL);
    }

    @Bean
    public IntegrationFlow errorInputTestChannelFlow(NucleusIntegrationComponent nucleusIntegrationComponent) {
        return nucleusIntegrationComponent
                .from(ERROR_INPUT_TEST_CHANNEL)
                .handle((p, h) -> {
                    if (h.containsKey("unsafeExceptionMessage") && h.containsKey("innerMessage")) {
                        throw new NukeException(new NonSerializablePayload(2, (String) h.get("innerMessage")), (String) h.get("unsafeExceptionMessage"));
                    } else if (h.containsKey("safeExceptionMessage")) {
                        throw new RuntimeException((String) h.get("safeExceptionMessage"));
                    }
                    return p;
                })
                .channel(ERROR_A_TEST_CHANNEL)
                .get();
    }

    @Bean
    public IntegrationFlow errorInputDirectTestChannelFlow(NucleusIntegrationComponent nucleusIntegrationComponent) {
        return nucleusIntegrationComponent
                .from(ERROR_INPUT_DIRECT_TEST_CHANNEL)
                .handle((p, h) -> {
                    if (h.containsKey("unsafeExceptionMessage") && h.containsKey("innerMessage")) {
                        throw new NukeException(new NonSerializablePayload(2, (String) h.get("innerMessage")), (String) h.get("unsafeExceptionMessage"));
                    } else if (h.containsKey("safeExceptionMessage")) {
                        throw new RuntimeException((String) h.get("safeExceptionMessage"));
                    }
                    return p;
                })
                .channel(ERROR_A_TEST_CHANNEL)
                .get();
    }

    @Bean(name = INTERNAL_CLIENT_INPUT_TEST_CHANNEL)
    public MessageChannel internalClientInputTestChannel(NucleusIntegrationComponent nucleusIntegrationComponent) {
        return nucleusIntegrationComponent.amqpChannel(INTERNAL_CLIENT_INPUT_TEST_CHANNEL);
    }

    @Bean
    public IntegrationFlow internalClientChannelFlow(NucleusIntegrationComponent nucleusIntegrationComponent) {
        return nucleusIntegrationComponent
                .from(INTERNAL_CLIENT_INPUT_TEST_CHANNEL)
                .enrichHeaders(s -> s.header(MessageHeaderUtil.REST_ENDPOINT, InternalClientEndpoint.TEST_NGI_JOB))
                .route(allInternalClientsRestClientRouter)
                .get();
    }

    @Bean(name = UNIT_TEST_TARGET_CHANNEL)
    public MessageChannel unitTestTargetChannel(NucleusIntegrationComponent nucleusIntegrationComponent) {
        return nucleusIntegrationComponent.amqpChannel(UNIT_TEST_TARGET_CHANNEL);
    }

    @Bean
    public IntegrationFlow unitTestTargetChannelFlow(NucleusIntegrationComponent nucleusIntegrationComponent) {
        return nucleusIntegrationComponent
                .from(UNIT_TEST_TARGET_CHANNEL)
                .<Object>handle((p, h) -> {
                    return p;
                }, e -> e.id(UNIT_TEST_TARGET_ENDPOINT))
                .get();
    }

    public static class NukeException extends RuntimeException {
        private NonSerializablePayload payload;

        NukeException(NonSerializablePayload payload, String message) {
            super(message);
            this.payload = payload;
        }
    }

    public static class NonSerializablePayload {
        private Integer testNumber;
        private String testMessage;

        public NonSerializablePayload(Integer number, String message) {
            this.testNumber = number;
            this.testMessage = message;
        }

        public Integer getTestNumber() {
            return this.testNumber;
        }

        public String getTestMessage() {
            return this.testMessage;
        }
    }

    public enum InternalClientEndpoint implements RestEndpoint {
        TEST_NGI_JOB(HttpMethod.POST, "ngiEventService/testJob", Long.class);

        private final HttpMethod httpMethod;
        private final String url;
        private final Class<?> responseType;

        InternalClientEndpoint(HttpMethod httpMethod, String url, Class<?> responseType) {
            this.httpMethod = httpMethod;
            this.url = url;
            this.responseType = responseType;
        }

        @Override
        public HttpMethod getHttpMethod() {
            return httpMethod;
        }

        @Override
        public String getURL() {
            return url;
        }

        @Override
        public Class<?> getResponseType() {
            return responseType;
        }
    }
}