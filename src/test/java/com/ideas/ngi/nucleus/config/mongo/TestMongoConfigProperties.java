package com.ideas.ngi.nucleus.config.mongo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Contains all System properties used in tests related to MongoDB.
 */
@Component
public class TestMongoConfigProperties extends MongoConfigProperties {

    @Value("${ngi.database.name:test-ngi}")
    private String ngiDatabaseName;

    public String getNGIDatabaseName() {
        return ngiDatabaseName;
    }

    public void setNGIDatabaseName(String ngiDatabaseName) {
        this.ngiDatabaseName = ngiDatabaseName;
    }
}
