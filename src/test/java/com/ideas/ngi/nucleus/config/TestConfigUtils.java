package com.ideas.ngi.nucleus.config;

import com.ideas.ngi.nucleus.config.amqp.rabbit.TestRabbitConfigProperties;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class TestConfigUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestConfigUtils.class);

    static void givePermissionsToVirtualHost(TestRabbitConfigProperties testRabbitConfigProperties) {
        HttpHeaders headers = getHttpHeaders();

        // Give Permissions to Virtual Host for guest user
        RestTemplate givePermissionsRestTemplate = new RestTemplate();
        givePermissionsRestTemplate.getInterceptors().add(new
                BasicAuthorizationInterceptor(testRabbitConfigProperties.getRabbitUser(), testRabbitConfigProperties.getRabbitPassword()));
        final String givePermissionsToUser = String.format("http://%s:%d/api/permissions/%s/guest", testRabbitConfigProperties.getHost(), testRabbitConfigProperties.getAdminPort(), testRabbitConfigProperties.getVirtualHost());
        JSONObject request = new JSONObject();
        request.put("scope", "client");
        request.put("configure", ".*");
        request.put("write", ".*");
        request.put("read", ".*");

        HttpEntity<String> parameterEntity = new HttpEntity<>(request.toString(), headers);
        try {
            givePermissionsRestTemplate.exchange(givePermissionsToUser, HttpMethod.PUT, parameterEntity, String.class);
        } catch (HttpClientErrorException hcee) {
            LOGGER.debug(String.format("Unable to allow permissions for virtual Host %s:", testRabbitConfigProperties.getVirtualHost()));
        }
    }

    static void createVirtualHost(TestRabbitConfigProperties testRabbitConfigProperties) {
        HttpHeaders headers = getHttpHeaders();

        // Create Virtual Host
        RestTemplate createVirtualHostRestTemplate = new RestTemplate();
        createVirtualHostRestTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(testRabbitConfigProperties.getRabbitUser(), testRabbitConfigProperties.getRabbitPassword()));
        final String createVHostUri = String.format("http://%s:%d/api/vhosts/%s", testRabbitConfigProperties.getHost(), testRabbitConfigProperties.getAdminPort(), testRabbitConfigProperties.getVirtualHost());
        HttpEntity<String> headerEntity = new HttpEntity<>(headers);
        try {
            createVirtualHostRestTemplate.exchange(createVHostUri, HttpMethod.PUT, headerEntity, String.class);
        } catch (HttpClientErrorException hcee) {
            LOGGER.debug(String.format("Unable to create virtual Host %s:", testRabbitConfigProperties.getVirtualHost()));
        }
    }

    private static HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}
