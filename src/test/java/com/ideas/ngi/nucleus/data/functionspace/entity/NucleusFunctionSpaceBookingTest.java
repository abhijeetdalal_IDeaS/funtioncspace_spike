package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.util.DateTimeUtil;
import com.ideas.ngi.nucleus.util.ValidationTestHelper;
import org.junit.Test;

import static com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType.AHWS_SC;
import static com.ideas.ngi.nucleus.data.functionspace.entity.FunctionSpaceBookingCategory.*;
import static java.lang.Integer.valueOf;
import static org.junit.Assert.*;

public class NucleusFunctionSpaceBookingTest {

    @Test
    public void testGettersAndSetters() {
        NucleusFunctionSpaceBooking functionSpaceBooking = NucleusFunctionSpaceObjectMother.buildFunctionSpaceBooking();

        assertEquals(AHWS_SC, functionSpaceBooking.getIntegrationType());
        assertEquals("1", functionSpaceBooking.getCorrelationMetadataId());
        assertEquals("1", functionSpaceBooking.getAccountNameId());
        assertNotNull(functionSpaceBooking.getArrivalDate());
        assertEquals(valueOf(1), functionSpaceBooking.getAttendees());
        assertEquals("blockCode", functionSpaceBooking.getBlockCode());
        assertEquals("blockName", functionSpaceBooking.getBlockName());
        assertEquals("bookingId", functionSpaceBooking.getBookingId());
        assertEquals("bookingStatus", functionSpaceBooking.getBookingStatus());
        assertEquals("bookingType", functionSpaceBooking.getBookingType());
        assertNotNull(functionSpaceBooking.getCancellationDate());
        assertEquals("catStatus", functionSpaceBooking.getCatStatus());
        assertNotNull(functionSpaceBooking.getCutoffDate());
        assertEquals(valueOf(1), functionSpaceBooking.getCutoffDays());
        assertNotNull(functionSpaceBooking.getDecisionDate());
        assertNotNull(functionSpaceBooking.getDepartureDate());
        assertEquals(BOOKING, functionSpaceBooking.getFunctionSpaceBookingCategory());
        assertEquals("id", functionSpaceBooking.getId());
        assertNotNull(functionSpaceBooking.getInsertDate());
        assertEquals("marketSegmentCode", functionSpaceBooking.getMarketSegmentCode());
        assertNotNull(functionSpaceBooking.getOpportunityCreateDate());
        assertEquals("propertyId", functionSpaceBooking.getPropertyId());
        assertEquals("status", functionSpaceBooking.getStatus());
        assertNotNull(functionSpaceBooking.getUpdateDate());
        assertTrue(functionSpaceBooking.isCateringOnly());
    }

    @Test
    public void applyFunctionSpaceBookingCategory() {
        NucleusFunctionSpaceBooking functionSpaceBooking = new NucleusFunctionSpaceBooking();
        functionSpaceBooking.setFunctionSpaceBookingCategory(BOOKING);

        // Verify when a booking turns into a booking with events
        functionSpaceBooking.applyFunctionSpaceBookingCategory(BOOKING_WITH_EVENTS);
        assertEquals(BOOKING_WITH_EVENTS, functionSpaceBooking.getFunctionSpaceBookingCategory());

        // Verify when a guest room gets added to the event
        functionSpaceBooking.applyFunctionSpaceBookingCategory(BOOKING_WITH_GUEST_ROOMS);
        assertEquals(BOOKING_WITH_EVENTS_AND_GUESTROOMS, functionSpaceBooking.getFunctionSpaceBookingCategory());

        // Reset to a booking
        functionSpaceBooking.setFunctionSpaceBookingCategory(BOOKING);

        // Verify when a booking turns into a booking with events
        functionSpaceBooking.applyFunctionSpaceBookingCategory(BOOKING_WITH_GUEST_ROOMS);
        assertEquals(BOOKING_WITH_GUEST_ROOMS, functionSpaceBooking.getFunctionSpaceBookingCategory());

        // Verify when a guest room gets added to the event
        functionSpaceBooking.applyFunctionSpaceBookingCategory(BOOKING_WITH_EVENTS);
        assertEquals(BOOKING_WITH_EVENTS_AND_GUESTROOMS, functionSpaceBooking.getFunctionSpaceBookingCategory());
    }

    @Test
    public void validate() {
        NucleusFunctionSpaceBooking functionSpaceBooking = new NucleusFunctionSpaceBooking();

        ValidationTestHelper.validate(functionSpaceBooking, "'propertyId' must not be null, but was 'null'", "'bookingId' must not be null, but was 'null'", "'status' must not be null, but was 'null'", "'arrivalDate' must not be null, but was 'null'");

        functionSpaceBooking.setPropertyId("1");
        functionSpaceBooking.setIntegrationType(IntegrationType.FUNCTION_SPACE_CLIENT);
        functionSpaceBooking.setBookingId("1");
        functionSpaceBooking.setArrivalDate(DateTimeUtil.buildDate(2014, 0, 1));
        functionSpaceBooking.setDepartureDate(DateTimeUtil.buildDate(2014, 0, 1));
        functionSpaceBooking.setMarketSegmentCode("Something");
        functionSpaceBooking.setStatus("status");
        functionSpaceBooking.setBookingType("bookingType");

        ValidationTestHelper.validate(functionSpaceBooking);
    }
}
