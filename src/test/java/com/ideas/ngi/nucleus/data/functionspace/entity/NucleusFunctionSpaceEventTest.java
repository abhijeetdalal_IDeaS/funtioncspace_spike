package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.util.ValidationTestHelper;
import org.junit.Test;

import java.util.Date;

import static com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType.FUNCTION_SPACE_CLIENT;
import static java.math.BigDecimal.ONE;
import static org.junit.Assert.*;

public class NucleusFunctionSpaceEventTest {

    @Test
    public void testGettersAndSetters() {
        NucleusFunctionSpaceEvent functionSpaceEvent = NucleusFunctionSpaceObjectMother.buildFunctionSpaceEvent();

        assertEquals(FUNCTION_SPACE_CLIENT, functionSpaceEvent.getIntegrationType());
        assertEquals("1", functionSpaceEvent.getCorrelationMetadataId());
        assertEquals(Integer.valueOf(1), functionSpaceEvent.getActualAttendees());
        assertNotNull(functionSpaceEvent.getBlockEnd());
        assertNotNull(functionSpaceEvent.getBlockStart());
        assertEquals("bookingId", functionSpaceEvent.getBookingId());
        assertNotNull(functionSpaceEvent.getEndDate());
        assertEquals("eventId", functionSpaceEvent.getEventId());
        assertEquals("eventTypeCode", functionSpaceEvent.getEventTypeCode());
        assertEquals(Integer.valueOf(1), functionSpaceEvent.getForecastAttendees());
        assertEquals("functionRoomSetupType", functionSpaceEvent.getFunctionRoomSetupType());
        assertEquals("groupId", functionSpaceEvent.getGroupId());
        assertEquals("id", functionSpaceEvent.getId());
        assertEquals("propertyId", functionSpaceEvent.getPropertyId());
        assertEquals("roomId", functionSpaceEvent.getRoomId());
        assertEquals(Integer.valueOf(1), functionSpaceEvent.getSetUpTimeMinutes());
        assertNotNull(functionSpaceEvent.getStartDate());
        assertEquals("status", functionSpaceEvent.getStatus());
        assertEquals(Integer.valueOf(1), functionSpaceEvent.getTearDownTimeMinutes());
        assertTrue(functionSpaceEvent.isMoveable());

        assertEquals(0, functionSpaceEvent.getFunctionSpaceEventRevenues().size());

        FunctionSpaceEventRevenue functionSpaceEventRevenue = new FunctionSpaceEventRevenue();
        functionSpaceEventRevenue.setActualRevenue(ONE);
        functionSpaceEventRevenue.setExpectedRevenue(ONE);
        functionSpaceEventRevenue.setForecastRevenue(ONE);
        functionSpaceEventRevenue.setGuaranteedRevenue(ONE);
        functionSpaceEventRevenue.setInsertDate(new Date());
        functionSpaceEventRevenue.setRevenueGroup("revenueGroup");
        functionSpaceEventRevenue.setRevenueType("revenueType");
        functionSpaceEventRevenue.setUpdateDate(new Date());
        functionSpaceEvent.getFunctionSpaceEventRevenues().add(functionSpaceEventRevenue);

        assertEquals(ONE, functionSpaceEventRevenue.getActualRevenue());
        assertEquals(ONE, functionSpaceEventRevenue.getExpectedRevenue());
        assertEquals(ONE, functionSpaceEventRevenue.getForecastRevenue());
        assertEquals(ONE, functionSpaceEventRevenue.getGuaranteedRevenue());
        assertNotNull(functionSpaceEventRevenue.getInsertDate());
        assertEquals("revenueGroup", functionSpaceEventRevenue.getRevenueGroup());
        assertEquals("revenueType", functionSpaceEventRevenue.getRevenueType());
        assertNotNull(functionSpaceEventRevenue.getUpdateDate());
    }

    @Test
    public void validate() {
        NucleusFunctionSpaceEvent functionSpaceEvent = new NucleusFunctionSpaceEvent();

        ValidationTestHelper.validate(functionSpaceEvent, "'status' must not be null, but was 'null'", "'eventId' must not be null, but was 'null'", "'bookingId' must not be null, but was 'null'", "'propertyId' must not be null, but was 'null'", "'blockEnd' must not be null, but was 'null'", "'blockStart' must not be null, but was 'null'", "'integrationType' must not be null, but was 'null'");

        functionSpaceEvent.setPropertyId("1");
        functionSpaceEvent.setIntegrationType(IntegrationType.FUNCTION_SPACE_CLIENT);
        functionSpaceEvent.setRoomId("1");
        functionSpaceEvent.setBookingId("1");
        functionSpaceEvent.setEventId("1");
        functionSpaceEvent.setEventTypeCode("BALL");
        functionSpaceEvent.setStatus("DEF");
        functionSpaceEvent.setBlockStart(new Date());
        functionSpaceEvent.setBlockEnd(new Date());
        ValidationTestHelper.validate(functionSpaceEvent);
    }

    @Test
    public void findFunctionSpaceEventRevenue() {
        String revenueGroup = "revenueGroup";
        String revenueType = "revenueType";

        // Find without revenues
        NucleusFunctionSpaceEvent functionSpaceEvent = new NucleusFunctionSpaceEvent();
        assertNull(functionSpaceEvent.findFunctionSpaceEventRevenue(revenueGroup, revenueType));

        // Find event revenue
        FunctionSpaceEventRevenue functionSpaceEventRevenue = new FunctionSpaceEventRevenue();
        functionSpaceEventRevenue.setRevenueGroup(revenueGroup);
        functionSpaceEventRevenue.setRevenueType(revenueType);
        functionSpaceEvent.getFunctionSpaceEventRevenues().add(functionSpaceEventRevenue);
        assertEquals(functionSpaceEventRevenue, functionSpaceEvent.findFunctionSpaceEventRevenue(revenueGroup, revenueType));

        // Find doesn't find revenue
        assertNull(functionSpaceEvent.findFunctionSpaceEventRevenue("someRevenueGroup", "someRevenueType"));
    }
}
