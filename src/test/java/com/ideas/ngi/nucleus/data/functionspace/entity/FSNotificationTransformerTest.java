package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.functionspace.dto.FunctionSpaceData;
import com.ideas.ngi.functionspace.dto.FunctionSpaceDataNotification;
import com.ideas.ngi.nucleus.rest.hateoas.ProxyLinkBuilderTransformer;
import org.junit.Before;
import org.junit.Test;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.UriTemplate;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class FSNotificationTransformerTest {
    private static final Integer MAX_HREF_LENGTH = 250;
    private FSNotificationTransformer transformer;

    @Before
    public void setup() {
        transformer = new FSNotificationTransformer(new ProxyLinkBuilderTransformer("http://ngitestingcluster01.ideasprod.int:9090"));
    }

    @Test
    public void transform() {
        FunctionSpaceData data = new FunctionSpaceData();
        data.setClientCode("client");
        data.setSendingSystemPropertyId("1");
        data.setIntegrationType(IntegrationType.ACTIVITY_DATA);
        data.setPropertyCode("property");
        data.setFullSync(false);


        FunctionSpaceDataNotification result = transformer.transform(data);

        assertEquals("http://ngitestingcluster01.ideasprod.int:9090/correlationMetadataService/search?integrationType={integrationType}&sendingSystemPropertyId={propertyId}&correlationStatus=SUCCESSFUL&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}", result.getCorrelationLink().getHref());
        assertEquals("http://ngitestingcluster01.ideasprod.int:9090/functionSpace/eventTypes/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}", result.getEventTypeLink().getHref());
        assertEquals("http://ngitestingcluster01.ideasprod.int:9090/functionSpace/marketSegments/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}", result.getMarketSegmentLink().getHref());
        assertEquals("http://ngitestingcluster01.ideasprod.int:9090/functionSpace/rooms/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}", result.getRoomLink().getHref());
        assertEquals("http://ngitestingcluster01.ideasprod.int:9090/functionSpace/comboRooms/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}", result.getRoomComboLink().getHref());
        assertEquals("http://ngitestingcluster01.ideasprod.int:9090/functionSpace/bookings/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}", result.getBookingLink().getHref());
        assertEquals("http://ngitestingcluster01.ideasprod.int:9090/functionSpace/bookings/search/optimized?clientCode={clientCode}&propertyCode={propertyCode}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}", result.getBookingIndexedLink().getHref());
        assertEquals("http://ngitestingcluster01.ideasprod.int:9090/functionSpace/bookings/pace/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}", result.getBookingPaceLink().getHref());
        assertEquals("http://ngitestingcluster01.ideasprod.int:9090/functionSpace/bookedGuestRooms/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}", result.getBookingGuestLink().getHref());
        assertEquals("http://ngitestingcluster01.ideasprod.int:9090/functionSpace/events/search?integrationType={integrationType}&propertyId={propertyId}&startDate={startDate}&endDate={endDate}&format=haljson&page={page}&size={size}", result.getEventLink().getHref());
        assertEquals("http://ngitestingcluster01.ideasprod.int:9090/functionSpace/events/search/optimized?clientCode={clientCode}&propertyCode={propertyCode}&startDate={startDate}&format=haljson&page={page}&size={size}", result.getEventIndexedLink().getHref());
    }

    @Test
    public void simulateG3LinkTemplateExpansion() {
        Link link = new Link("http://dev.ngitesting.com:9090/correlationMetadataService?integrationType=ACTIVITY_DATA&sendingSystemPropertyId=1&correlationStatus=SUCCESSFUL&startDate={startDate}&endDate={endDate}&sort=lastModifiedDate,asc&{&page,size}");

        Map<String,String> params = new HashMap<>();
        params.put("startDate", "2020-04-01");
        params.put("endDate", "2020-04-03");
        params.put("page", "0");
        params.put("size", "15");
        String result = new UriTemplate(link.getHref()).expand(params).toString();

        assertNotNull(result);

        params = new HashMap<>();
        params.put("startDate", "2020-04-01");
        params.put("endDate", "2020-04-03");
        params.put("size", "15");
        result = new UriTemplate(link.getHref()).expand(params).toString();
        assertNotNull(result);

        params = new HashMap<>();
        params.put("startDate", "2020-04-01");
        params.put("endDate", "2020-04-03");
        result = new UriTemplate(link.getHref()).expand(params).toString();
        assertNotNull(result);

    }
}