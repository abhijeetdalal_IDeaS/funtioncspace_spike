package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NucleusFunctionSpaceEventTypeTest {

    @Test
    public void gettersAndSetters() {
        NucleusFunctionSpaceEventType nucleusFunctionSpaceEventType = NucleusFunctionSpaceObjectMother.buildFunctionSpaceEventType();

        assertEquals("SAMPLE-FSC01", nucleusFunctionSpaceEventType.getPropertyId());
        assertEquals("1", nucleusFunctionSpaceEventType.getId());
        assertEquals(IntegrationType.AHWS_SC, nucleusFunctionSpaceEventType.getIntegrationType());
        assertEquals("EVT1", nucleusFunctionSpaceEventType.getAbbreviation());
        assertEquals("Event 1", nucleusFunctionSpaceEventType.getName());
    }
}
