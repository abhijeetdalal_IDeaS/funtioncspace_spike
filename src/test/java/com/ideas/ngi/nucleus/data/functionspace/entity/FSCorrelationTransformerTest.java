package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.data.correlation.entity.NucleusCorrelationMetadata;
import com.ideas.ngi.functionspace.dto.FunctionSpaceData;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;

public class FSCorrelationTransformerTest {
    @InjectMocks
    FSCorrelationTransformer transformer;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void transform() {
        NucleusCorrelationMetadata metadata = new NucleusCorrelationMetadata();
        metadata.setClientCode("clientCode");
        metadata.setPropertyCode("propertyCode");
        metadata.setIntegrationType(IntegrationType.ACTIVITY_DATA);
        metadata.setSendingSystemPropertyId("sendingSystemPropertyId");

        FunctionSpaceData expected = new FunctionSpaceData(metadata);

        FunctionSpaceData result = transformer.transform(metadata);
        assertEquals(expected.getClientCode(), result.getClientCode());
        assertEquals(expected.getPropertyCode(), result.getPropertyCode());
        assertEquals(expected.getIntegrationType(), result.getIntegrationType());
        assertEquals(expected.getSendingSystemPropertyId(), result.getSendingSystemPropertyId());
    }
}