package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.util.ValidationTestHelper;
import org.junit.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.ONE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class NucleusFunctionSpaceFunctionRoomTest {

    @Test
    public void testGettersAndSetters() {
        NucleusFunctionSpaceFunctionRoom functionSpaceFunctionRoom = NucleusFunctionSpaceObjectMother.buildFunctionSpaceFunctionRoom();

        assertEquals("alias", functionSpaceFunctionRoom.getAlias());
        assertEquals(ONE, functionSpaceFunctionRoom.getAreaSqFeet());
        assertEquals(ONE, functionSpaceFunctionRoom.getAreaSqMeters());
        assertEquals("building", functionSpaceFunctionRoom.getBuilding());
        assertEquals("description", functionSpaceFunctionRoom.getDescription());
        assertEquals("diaryName", functionSpaceFunctionRoom.getDiaryName());
        assertEquals("excludedEventType", functionSpaceFunctionRoom.getExcludedEventType());
        assertEquals("floorName", functionSpaceFunctionRoom.getFloorName());
        assertEquals("functionRoomAbbreviation", functionSpaceFunctionRoom.getFunctionRoomAbbreviation());
        assertEquals("functionRoomId", functionSpaceFunctionRoom.getFunctionRoomId());
        assertEquals("functionRoomType", functionSpaceFunctionRoom.getFunctionRoomType());
        assertEquals("id", functionSpaceFunctionRoom.getId());
        assertEquals(ONE, functionSpaceFunctionRoom.getLengthInFeet());
        assertEquals(ONE, functionSpaceFunctionRoom.getLengthInMeters());
        assertEquals(Integer.valueOf(1), functionSpaceFunctionRoom.getMaxOccupancy());
        assertEquals(Integer.valueOf(1), functionSpaceFunctionRoom.getMinAdvance());
        assertEquals(ONE, functionSpaceFunctionRoom.getMinimumRevenue());
        assertEquals(Integer.valueOf(1), functionSpaceFunctionRoom.getMinOccupancy());
        assertEquals("name", functionSpaceFunctionRoom.getName());
        assertEquals("propertyId", functionSpaceFunctionRoom.getPropertyId());
        assertEquals("roomCategory", functionSpaceFunctionRoom.getRoomCategory());
        assertEquals("roomClass", functionSpaceFunctionRoom.getRoomClass());
        assertEquals("suiteType", functionSpaceFunctionRoom.getSuiteType());
        assertEquals(ONE, functionSpaceFunctionRoom.getWidthInFeet());
        assertEquals(ONE, functionSpaceFunctionRoom.getWidthInMeters());
        assertTrue(functionSpaceFunctionRoom.isCombo());
        assertTrue(functionSpaceFunctionRoom.isDiaryDisplay());
        assertTrue(functionSpaceFunctionRoom.isForceAlternate());
        assertTrue(functionSpaceFunctionRoom.isShareable());
        assertEquals(0, functionSpaceFunctionRoom.getFunctionRoomPartIds().size());
    }

    @Test
    public void validate() {
        NucleusFunctionSpaceFunctionRoom functionSpaceFunctionRoom = new NucleusFunctionSpaceFunctionRoom();

        ValidationTestHelper.validate(functionSpaceFunctionRoom, "'lengthInFeet' must not be null, but was 'null'", "'areaSqMeters' must not be null, but was 'null'", "'functionRoomId' must not be null, but was 'null'", "'widthInMeters' must not be null, but was 'null'", "'widthInFeet' must not be null, but was 'null'", "'lengthInMeters' must not be null, but was 'null'", "'propertyId' must not be null, but was 'null'", "'areaSqFeet' must not be null, but was 'null'", "'integrationType' must not be null, but was 'null'");

        functionSpaceFunctionRoom.setPropertyId("1");
        functionSpaceFunctionRoom.setFunctionRoomId("DEF");
        functionSpaceFunctionRoom.setIntegrationType(IntegrationType.FUNCTION_SPACE_CLIENT);
        functionSpaceFunctionRoom.setFunctionRoomId("1");
        functionSpaceFunctionRoom.setAreaSqFeet(new BigDecimal(100));
        functionSpaceFunctionRoom.setAreaSqMeters(new BigDecimal(100));
        functionSpaceFunctionRoom.setLengthInFeet(new BigDecimal(100));
        functionSpaceFunctionRoom.setWidthInFeet(new BigDecimal(100));
        functionSpaceFunctionRoom.setLengthInMeters(new BigDecimal(100));
        functionSpaceFunctionRoom.setWidthInMeters(new BigDecimal(100));

        ValidationTestHelper.validate(functionSpaceFunctionRoom);
    }
}
