package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import org.junit.Test;

import static com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType.OXI_SC;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class NucleusFunctionSpaceGuestRoomTest {

    @Test
    public void testGettersAndSetters() {
        NucleusFunctionSpaceGuestRoomPace functionSpaceGuestRoomPace = NucleusFunctionSpaceObjectMother.buildFunctionSpaceGuestRoomPace();

        assertEquals(functionSpaceGuestRoomPace, functionSpaceGuestRoomPace);
        assertEquals(functionSpaceGuestRoomPace.hashCode(), functionSpaceGuestRoomPace.hashCode());
        assertEquals(functionSpaceGuestRoomPace, functionSpaceGuestRoomPace);
        assertEquals("propertyId", functionSpaceGuestRoomPace.getPropertyId());
        assertEquals("bookingId", functionSpaceGuestRoomPace.getBookingId());
        assertEquals("bookingPaceId", functionSpaceGuestRoomPace.getBookingPaceId());
        assertEquals("roomType", functionSpaceGuestRoomPace.getRoomType());
        assertNotNull(functionSpaceGuestRoomPace.getChangeDate());
        assertEquals(Integer.valueOf(1), functionSpaceGuestRoomPace.getRoomNights());
        assertEquals(ZERO, functionSpaceGuestRoomPace.getRoomRevenue());
        assertEquals(Integer.valueOf(1), functionSpaceGuestRoomPace.getRoomNightsChanged());
        assertEquals(ZERO, functionSpaceGuestRoomPace.getRoomRevenueChanged());
        assertNotNull(functionSpaceGuestRoomPace.getStayDate());
        assertEquals(OXI_SC, functionSpaceGuestRoomPace.getIntegrationType());
        assertEquals("id", functionSpaceGuestRoomPace.getId());
        assertEquals("DEF", functionSpaceGuestRoomPace.getGuestRoomStatus());
    }
}
