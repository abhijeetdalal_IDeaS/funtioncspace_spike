package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class NucleusFunctionSpaceMarketSegmentTest {

    @Test
    public void gettersAndSetters() {
        NucleusFunctionSpaceMarketSegment nucleusFunctionSpaceMarketSegment = NucleusFunctionSpaceObjectMother.buildFunctionSpaceMarketSegment();

        assertEquals("1", nucleusFunctionSpaceMarketSegment.getId());
        assertEquals(IntegrationType.FUNCTION_SPACE_CLIENT, nucleusFunctionSpaceMarketSegment.getIntegrationType());
        assertEquals("PropertyId", nucleusFunctionSpaceMarketSegment.getPropertyId());
        assertEquals("ABC", nucleusFunctionSpaceMarketSegment.getAbbreviation());
        assertEquals("Name", nucleusFunctionSpaceMarketSegment.getName());
        assertEquals("Desc", nucleusFunctionSpaceMarketSegment.getDescription());
        assertEquals("ParentMktSegId", nucleusFunctionSpaceMarketSegment.getParentMarketSegmentId());
        assertNotNull(nucleusFunctionSpaceMarketSegment.getCreateDate());
        assertNotNull(nucleusFunctionSpaceMarketSegment.getSalesAndCateringLastModified());
    }
}
