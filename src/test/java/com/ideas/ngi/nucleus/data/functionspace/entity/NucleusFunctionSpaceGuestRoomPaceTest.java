package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.util.DateTimeUtil;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.*;

public class NucleusFunctionSpaceGuestRoomPaceTest {

    @Test
    public void testPaceRecordCreatedUsingConstructor(){
        NucleusFunctionSpaceGuestRoomPace record = buildGuestRoomPaceRecord("1", "BPID1", BigDecimal.TEN, 10, 25);
        NucleusFunctionSpaceGuestRoomPace paceRecord = new NucleusFunctionSpaceGuestRoomPace(record);
        assertPaceRecord(paceRecord);
        assertTrue(record.equals(paceRecord));
        int recordHashCode = record.hashCode();
        int paceRecordHashCode = paceRecord.hashCode();
        assertTrue(recordHashCode == paceRecordHashCode);
    }
    @Test
    public void testEquals(){
        NucleusFunctionSpaceGuestRoomPace existingRecord = buildGuestRoomPaceRecord("1", "BPID1", BigDecimal.TEN, 10, 25);
        NucleusFunctionSpaceGuestRoomPace receivedRecord = buildGuestRoomPaceRecord("2", "BPID2", null, null, 25);
        assertTrue(existingRecord.equals(receivedRecord));
    }

    @Test
    public void testNotEquals(){
        NucleusFunctionSpaceGuestRoomPace existingRecord = buildGuestRoomPaceRecord("1", "BPID1", BigDecimal.TEN, 10, 25);
        NucleusFunctionSpaceGuestRoomPace receivedRecord = buildGuestRoomPaceRecord("2", "BPID2", null, null, 10);
        assertFalse(existingRecord.equals(receivedRecord));
    }

    @Test
    public void testSetGet(){
        NucleusFunctionSpaceGuestRoomPace paceRecord = buildGuestRoomPaceRecord("1", "BPID1", BigDecimal.TEN, 10, 25);
        assertNotNull(paceRecord.getCreateDate());
        assertNotNull(paceRecord.getLastModifiedDate());
        assertPaceRecord(paceRecord);
    }

    public void assertPaceRecord(NucleusFunctionSpaceGuestRoomPace paceRecord) {
        assertEquals("1", paceRecord.getId());
        assertEquals("ClientCode", paceRecord.getClientCode());
        assertEquals("PropertyCode", paceRecord.getPropertyCode());
        assertNotNull(paceRecord.getChangeDate());
        assertEquals("BPID1", paceRecord.getBookingPaceId());
        assertEquals(BigDecimal.TEN, paceRecord.getRoomRevenueChanged());
        assertEquals(new Integer(10), paceRecord.getRoomNightsChanged());
        assertEquals(new Integer(25), paceRecord.getRoomNights());
        assertEquals(BigDecimal.valueOf(50), paceRecord.getRoomRevenue());
        assertEquals("Tentative", paceRecord.getGuestRoomStatus());
        assertEquals(DateTimeUtil.buildDate(2017,10,10), paceRecord.getStayDate());
        assertEquals("RT1", paceRecord.getRoomType());
        assertEquals("12345", paceRecord.getBookingId());
        assertEquals(IntegrationType.AHWS_SC, paceRecord.getIntegrationType());
        assertEquals("SANDBOX-AHWS", paceRecord.getPropertyId());
        assertEquals(new Integer(1), paceRecord.getBlockedRoomNights());
        assertEquals(new Integer(-1), paceRecord.getBlockedRoomNightsChanged());
        assertEquals(BigDecimal.TEN, paceRecord.getBlockedRoomRevenue());
        assertEquals(BigDecimal.valueOf(-10), paceRecord.getBlockedRoomRevenueChanged());
        assertEquals(new Integer(2), paceRecord.getContractedRoomNights());
        assertEquals(new Integer(0), paceRecord.getContractedRoomNightsChanged());
        assertEquals(BigDecimal.valueOf(20), paceRecord.getContractedRoomRevenue());
        assertEquals(BigDecimal.ZERO, paceRecord.getContractedRoomRevenueChanged());
        assertEquals("correlationId", paceRecord.getCorrelationId());


    }

    public NucleusFunctionSpaceGuestRoomPace buildGuestRoomPaceRecord(String id, String bookingPaceId, BigDecimal roomRevenueChanged, Integer roomNightsChanged, Integer roomNights) {
        NucleusFunctionSpaceGuestRoomPace paceRecord = new NucleusFunctionSpaceGuestRoomPace();
        paceRecord.setId(id);
        paceRecord.setCreateDate(new Date());
        paceRecord.setLastModifiedDate(new Date());
        paceRecord.setChangeDate(new Date());
        paceRecord.setBookingPaceId(bookingPaceId);
        paceRecord.setRoomRevenueChanged(roomRevenueChanged);
        paceRecord.setRoomNightsChanged(roomNightsChanged);
        paceRecord.setRoomNights(roomNights);
        paceRecord.setRoomRevenue(BigDecimal.valueOf(50));
        paceRecord.setGuestRoomStatus("Tentative");
        paceRecord.setStayDate(DateTimeUtil.buildDate(2017,10,10));
        paceRecord.setRoomType("RT1");
        paceRecord.setBookingId("12345");
        paceRecord.setIntegrationType(IntegrationType.AHWS_SC);
        paceRecord.setPropertyId("SANDBOX-AHWS");
        paceRecord.setCorrelationId("correlationId");
        paceRecord.setBlockedRoomNights(1);
        paceRecord.setBlockedRoomRevenue(BigDecimal.TEN);
        paceRecord.setContractedRoomNights(2);
        paceRecord.setContractedRoomRevenue(BigDecimal.valueOf(20));
        paceRecord.setBlockedRoomNightsChanged(-1);
        paceRecord.setBlockedRoomRevenueChanged(BigDecimal.valueOf(-10));
        paceRecord.setContractedRoomNightsChanged(0);
        paceRecord.setContractedRoomRevenueChanged(BigDecimal.ZERO);
        paceRecord.setPropertyCode("PropertyCode");
        paceRecord.setClientCode("ClientCode");
        return paceRecord;
    }
}