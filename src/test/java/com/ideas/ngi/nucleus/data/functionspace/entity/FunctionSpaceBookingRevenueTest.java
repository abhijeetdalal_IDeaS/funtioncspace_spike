package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import org.junit.Test;

import java.math.BigDecimal;

import static junit.framework.TestCase.assertEquals;

public class FunctionSpaceBookingRevenueTest {

    @Test
    public void getSet() throws Exception {
        FunctionSpaceBookingRevenue bookingRevenue = NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingRevenue();

        assertEquals(BigDecimal.valueOf(1000), bookingRevenue.getActualRevenue());
        assertEquals(BigDecimal.valueOf(900), bookingRevenue.getExpectedRevenue());
        assertEquals(BigDecimal.valueOf(1100), bookingRevenue.getForecastRevenue());
        assertEquals("RevGroup", bookingRevenue.getRevenueGroup());
        assertEquals("Food", bookingRevenue.getRevenueType());
    }
}
