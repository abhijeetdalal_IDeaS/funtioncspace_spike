package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.util.DateTimeUtil;
import com.ideas.ngi.nucleus.util.ValidationTestHelper;
import org.junit.Test;

import static com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType.FUNCTION_SPACE_CLIENT;
import static java.lang.Integer.valueOf;
import static java.math.BigDecimal.ONE;
import static org.junit.Assert.*;

public class NucleusFunctionSpaceBookingGuestRoomTest {

    @Test
    public void testGettersAndSetters() {
        NucleusFunctionSpaceBookingGuestRoom functionSpaceBookingGuestRoom = NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingGuestRoom();

        assertEquals("1", functionSpaceBookingGuestRoom.getId());
        assertEquals(FUNCTION_SPACE_CLIENT, functionSpaceBookingGuestRoom.getIntegrationType());
        assertEquals(functionSpaceBookingGuestRoom, functionSpaceBookingGuestRoom);
        assertEquals(functionSpaceBookingGuestRoom.hashCode(), functionSpaceBookingGuestRoom.hashCode());
        assertEquals(valueOf(2), functionSpaceBookingGuestRoom.getBlockedDoubleRooms());
        assertEquals(valueOf(4), functionSpaceBookingGuestRoom.getBlockedQuadRooms());
        assertEquals(valueOf(1), functionSpaceBookingGuestRoom.getBlockedSingleRooms());
        assertEquals(valueOf(3), functionSpaceBookingGuestRoom.getBlockedTripleRooms());
        assertEquals(valueOf(2), functionSpaceBookingGuestRoom.getContractedDoubleRooms());
        assertEquals(valueOf(4), functionSpaceBookingGuestRoom.getContractedQuadRooms());
        assertEquals(valueOf(10), functionSpaceBookingGuestRoom.getContractedRoomsTotal());
        assertEquals(valueOf(1), functionSpaceBookingGuestRoom.getContractedSingleRooms());
        assertEquals(valueOf(3), functionSpaceBookingGuestRoom.getContractedTripleRooms());
        assertEquals(valueOf(10), functionSpaceBookingGuestRoom.getForecastedRoomsTotal());
        assertEquals(valueOf(10), functionSpaceBookingGuestRoom.getBlockedRoomsTotal());
        assertEquals("guestRoomId", functionSpaceBookingGuestRoom.getGuestRoomId());
        assertEquals(ONE, functionSpaceBookingGuestRoom.getGuestRoomRateDouble());
        assertEquals(ONE, functionSpaceBookingGuestRoom.getGuestRoomRateQuad());
        assertEquals(ONE, functionSpaceBookingGuestRoom.getGuestRoomRateSingle());
        assertEquals(ONE, functionSpaceBookingGuestRoom.getGuestRoomRateTriple());
        assertNotNull(functionSpaceBookingGuestRoom.getOccupancyDate());
        assertEquals(valueOf(2), functionSpaceBookingGuestRoom.getPickupDoubleRooms());
        assertEquals(valueOf(4), functionSpaceBookingGuestRoom.getPickupQuadRooms());
        assertEquals(valueOf(10), functionSpaceBookingGuestRoom.getPickupRoomsTotal());
        assertEquals(valueOf(1), functionSpaceBookingGuestRoom.getPickupSingleRooms());
        assertEquals(valueOf(3), functionSpaceBookingGuestRoom.getPickupTripleRooms());
        assertEquals("roomCategory", functionSpaceBookingGuestRoom.getRoomCategory());
        assertEquals("roomClass", functionSpaceBookingGuestRoom.getRoomClass());
        assertEquals("scRoomCategory", functionSpaceBookingGuestRoom.getScRoomCategory());
        assertEquals("status", functionSpaceBookingGuestRoom.getStatus());
        assertEquals("correlationMetadataId", functionSpaceBookingGuestRoom.getCorrelationMetadataId());
        assertTrue(functionSpaceBookingGuestRoom.hasBookedRooms());

        functionSpaceBookingGuestRoom.setContractedSingleRooms(0);
        assertTrue(functionSpaceBookingGuestRoom.hasBookedRooms());
        functionSpaceBookingGuestRoom.setContractedDoubleRooms(0);
        assertTrue(functionSpaceBookingGuestRoom.hasBookedRooms());
        functionSpaceBookingGuestRoom.setContractedTripleRooms(0);
        assertTrue(functionSpaceBookingGuestRoom.hasBookedRooms());
        functionSpaceBookingGuestRoom.setContractedRoomsTotal(0);
        assertTrue(functionSpaceBookingGuestRoom.hasBookedRooms());
        functionSpaceBookingGuestRoom.setContractedQuadRooms(0);
        assertTrue(functionSpaceBookingGuestRoom.hasBookedRooms());
        functionSpaceBookingGuestRoom.setBlockedSingleRooms(0);
        assertTrue(functionSpaceBookingGuestRoom.hasBookedRooms());
        functionSpaceBookingGuestRoom.setBlockedDoubleRooms(0);
        assertTrue(functionSpaceBookingGuestRoom.hasBookedRooms());
        functionSpaceBookingGuestRoom.setBlockedTripleRooms(0);
        assertTrue(functionSpaceBookingGuestRoom.hasBookedRooms());
        functionSpaceBookingGuestRoom.setBlockedQuadRooms(0);
        assertEquals(valueOf(0), functionSpaceBookingGuestRoom.getBlockedRoomsTotal());
        assertTrue(functionSpaceBookingGuestRoom.hasBookedRooms());
        functionSpaceBookingGuestRoom.setForecastedRoomsTotal(0);
        assertTrue(functionSpaceBookingGuestRoom.hasBookedRooms());
        functionSpaceBookingGuestRoom.setPickupSingleRooms(0);
        assertTrue(functionSpaceBookingGuestRoom.hasBookedRooms());
        functionSpaceBookingGuestRoom.setPickupDoubleRooms(0);
        assertTrue(functionSpaceBookingGuestRoom.hasBookedRooms());
        functionSpaceBookingGuestRoom.setPickupTripleRooms(0);
        assertTrue(functionSpaceBookingGuestRoom.hasBookedRooms());
        functionSpaceBookingGuestRoom.setPickupQuadRooms(0);
        assertTrue(functionSpaceBookingGuestRoom.hasBookedRooms());
        functionSpaceBookingGuestRoom.setPickupRoomsTotal(0);
        assertFalse(functionSpaceBookingGuestRoom.hasBookedRooms());

        functionSpaceBookingGuestRoom.setBlockedSingleRooms(null);
        functionSpaceBookingGuestRoom.setBlockedDoubleRooms(null);
        functionSpaceBookingGuestRoom.setBlockedTripleRooms(null);
        functionSpaceBookingGuestRoom.setBlockedQuadRooms(null);
        assertNull(functionSpaceBookingGuestRoom.getBlockedSingleRooms());
        assertNull(functionSpaceBookingGuestRoom.getBlockedDoubleRooms());
        assertNull(functionSpaceBookingGuestRoom.getBlockedTripleRooms());
        assertNull(functionSpaceBookingGuestRoom.getBlockedQuadRooms());
        assertNull(functionSpaceBookingGuestRoom.getBlockedRoomsTotal());
    }

    @Test
    public void validate() {
        NucleusFunctionSpaceBookingGuestRoom functionSpaceBookingGuestRoom = new NucleusFunctionSpaceBookingGuestRoom();

        ValidationTestHelper.validate(functionSpaceBookingGuestRoom, "'propertyId' must not be null, but was 'null'", "'bookingId' must not be null, but was 'null'", "'occupancyDate' must not be null, but was 'null'", "'roomCategory' must not be null, but was 'null'");

        functionSpaceBookingGuestRoom.setPropertyId("1");
        functionSpaceBookingGuestRoom.setIntegrationType(IntegrationType.FUNCTION_SPACE_CLIENT);
        functionSpaceBookingGuestRoom.setBookingId("1");
        functionSpaceBookingGuestRoom.setOccupancyDate(DateTimeUtil.buildDate(2014, 0, 1));
        functionSpaceBookingGuestRoom.setRoomCategory("roomCategory");
        functionSpaceBookingGuestRoom.setGuestRoomId("Room 1");
        ValidationTestHelper.validate(functionSpaceBookingGuestRoom);
    }
}