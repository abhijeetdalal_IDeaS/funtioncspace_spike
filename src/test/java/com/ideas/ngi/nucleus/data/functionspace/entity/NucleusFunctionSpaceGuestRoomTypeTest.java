package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import org.junit.Test;

import static com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType.OXI_SC;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class NucleusFunctionSpaceGuestRoomTypeTest {

    @Test
    public void testGettersAndSetters() {
        NucleusFunctionSpaceGuestRoomType functionSpaceGuestRoomType = NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingGuestRoomType();

        assertEquals(functionSpaceGuestRoomType, functionSpaceGuestRoomType);
        assertEquals(functionSpaceGuestRoomType.hashCode(), functionSpaceGuestRoomType.hashCode());
        assertEquals("ID", functionSpaceGuestRoomType.getId());
        assertEquals(OXI_SC, functionSpaceGuestRoomType.getIntegrationType());
        assertEquals("PROPERTY_ID", functionSpaceGuestRoomType.getPropertyId());
        assertEquals("FUNCTION_ROOM_TYPE", functionSpaceGuestRoomType.getFunctionRoomType());
        assertEquals(Integer.valueOf(2), functionSpaceGuestRoomType.getMaxOccupancy());
        assertEquals("DESCRIPTION", functionSpaceGuestRoomType.getDescription());
        assertEquals("ROOM_CATEGORY", functionSpaceGuestRoomType.getRoomCategory());
        assertEquals("ROOM_CLASS", functionSpaceGuestRoomType.getRoomClass());
        assertEquals(Integer.valueOf(3), functionSpaceGuestRoomType.getNoOfRooms());
        assertFalse(functionSpaceGuestRoomType.getInactive());
    }
}