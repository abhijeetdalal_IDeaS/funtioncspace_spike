package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType;
import com.ideas.ngi.nucleus.util.DateTimeUtil;
import com.ideas.ngi.nucleus.util.ValidationTestHelper;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

import static com.ideas.ngi.nucleus.data.correlation.entity.IntegrationType.OXI_SC;
import static java.lang.Integer.valueOf;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.*;

public class NucleusFunctionSpaceBookingPaceTest {
    private static final String BOOKING_ID = "booking-1";
    private static final String PROPERTY_ID = "property-1";

    @Test
    public void testGettersAndSetters() {
        NucleusFunctionSpaceBookingPace functionSpaceBookingChange = NucleusFunctionSpaceObjectMother.buildFunctionSpaceBookingPace();

        assertEquals(functionSpaceBookingChange, functionSpaceBookingChange);
        assertEquals(functionSpaceBookingChange.hashCode(), functionSpaceBookingChange.hashCode());
        assertEquals(functionSpaceBookingChange, functionSpaceBookingChange);
        assertEquals(OXI_SC, functionSpaceBookingChange.getIntegrationType());
        assertEquals("propertyId", functionSpaceBookingChange.getPropertyId());
        assertEquals("bookingId", functionSpaceBookingChange.getBookingId());
        assertEquals("bookingPaceId", functionSpaceBookingChange.getBookingPaceId());
        assertEquals("bookingStatus", functionSpaceBookingChange.getBookingStatus());
        assertNotNull(functionSpaceBookingChange.getChangeDate());
        assertEquals(valueOf(1), functionSpaceBookingChange.getRoomNights());
        assertEquals(ZERO, functionSpaceBookingChange.getRoomRevenue());
        assertNotNull(functionSpaceBookingChange.getStayDate());
        assertNotNull(functionSpaceBookingChange.getUpdateDate());
        assertEquals("INQ", functionSpaceBookingChange.getCateringStatus());
        assertEquals("DEF", functionSpaceBookingChange.getGuestRoomStatus());
        assertEquals("correlationMetadataId", functionSpaceBookingChange.getCorrelationMetadataId());
    }

    @Test
    public void validate() {
        NucleusFunctionSpaceBookingPace nucleusFunctionSpaceBookingPace = new NucleusFunctionSpaceBookingPace();

        ValidationTestHelper.validate(nucleusFunctionSpaceBookingPace, "'propertyId' must not be null, but was 'null'", "'bookingId' must not be null, but was 'null'", "'changeDate' must not be null, but was 'null'");

        nucleusFunctionSpaceBookingPace.setPropertyId("1");
        nucleusFunctionSpaceBookingPace.setBookingId("1");
        nucleusFunctionSpaceBookingPace.setChangeDate(DateTimeUtil.buildDate(2014, 0, 1, 12, 0, 0));
        nucleusFunctionSpaceBookingPace.setBookingStatus("status");

        nucleusFunctionSpaceBookingPace.setIntegrationType(IntegrationType.FUNCTION_SPACE_CLIENT);
        nucleusFunctionSpaceBookingPace.setBookingPaceId("1");
        ValidationTestHelper.validate(nucleusFunctionSpaceBookingPace);
    }

    @Test
    public void generateBookingPaceId() {
        NucleusFunctionSpaceBookingPace pace = new NucleusFunctionSpaceBookingPace();
        pace.setBookingPaceId("pace-123");
        pace.generateBookingPaceId();
        assertEquals("pace-123", pace.getBookingPaceId());

        pace.setBookingPaceId(null);
        pace.setBookingId("booking123");
        pace.setChangeDate(DateTimeUtil.buildDate(2017, 10, 11));
        pace.generateBookingPaceId();
        assertEquals("booking123-1510380000000", pace.getBookingPaceId());

        pace.setBookingPaceId(null);
        pace.setStayDate(DateTimeUtil.buildDate(2017, 5, 9));
        pace.generateBookingPaceId();
        assertEquals("booking123-1510380000000-1496984400000", pace.getBookingPaceId());
    }

    @Test
    public void isEquals_OtherIsNull() {
        NucleusFunctionSpaceBookingPace pace = new NucleusFunctionSpaceBookingPace();
        assertFalse(pace.isEquals(null));
    }

    @Test
    public void isEquals_SelfReferental() {
        NucleusFunctionSpaceBookingPace pace = buildBookingPace();
        assertTrue(pace.isEquals(pace));
    }

    @Test
    public void isEquals_SameTransitive() {
        NucleusFunctionSpaceBookingPace pace1 = buildBookingPace();
        NucleusFunctionSpaceBookingPace pace2 = buildBookingPace();
        assertTrue(pace1.isEquals(pace2));
        assertTrue(pace2.isEquals(pace1));
    }

    @Test
    public void isEquals_AuditFieldsDifferent() {
        NucleusFunctionSpaceBookingPace pace1 = buildBookingPace();
        pace1.setId("pace-1");
        pace1.setCreateDate(DateTimeUtil.buildDate(2017, 10, 11));
        pace1.setLastModifiedDate(DateTimeUtil.buildDate(2017, 10, 12));

        NucleusFunctionSpaceBookingPace pace2 = buildBookingPace();
        assertTrue(pace1.isEquals(pace2));
        assertTrue(pace2.isEquals(pace1));
        pace2.setId("pace-2");
        pace2.setCreateDate(DateTimeUtil.buildDate(2016, 10, 11));
        pace2.setLastModifiedDate(DateTimeUtil.buildDate(2016, 10, 12));

        assertTrue(pace1.isEquals(pace2));
        assertTrue(pace2.isEquals(pace1));
    }

    @Test
    public void isEquals_PaceMetadataisDifferent() {
        NucleusFunctionSpaceBookingPace pace1 = buildBookingPace();
        pace1.setBookingPaceId("pace-1");

        NucleusFunctionSpaceBookingPace pace2 = buildBookingPace();
        pace2.setBookingPaceId("pace2");

        assertTrue(pace1.isEquals(pace2));
        assertTrue(pace2.isEquals(pace1));

        pace1.setBookingPaceId("pace-1");
        pace2.setBookingPaceId("pace-1");
        pace1.setChangeDate(DateTimeUtil.buildDate(2017, 10, 11));
        pace2.setChangeDate(DateTimeUtil.buildDate(2016, 10, 11));

        assertTrue(pace1.isEquals(pace2));
        assertTrue(pace2.isEquals(pace1));
    }

    @Test
    public void isEquals_KeyDataDifferent() {
        NucleusFunctionSpaceBookingPace pace1 = buildBookingPace();
        NucleusFunctionSpaceBookingPace pace2 = buildBookingPace();

        pace1.setRoomRevenue(pace2.getRoomRevenue().add(BigDecimal.valueOf(1)));

        assertFalse(pace1.isEquals(pace2));
        assertFalse(pace2.isEquals(pace1));
    }

    @Test
    public void isNotEquals_ArrivalDate() {
        NucleusFunctionSpaceBookingPace pace1 = buildBookingPace();
        NucleusFunctionSpaceBookingPace pace2 = buildBookingPace();

        pace1.setArrivalDate(new Date());

        assertFalse(pace1.isEquals(pace2));
        assertFalse(pace2.isEquals(pace1));
    }

    @Test
    public void isNotEquals_DepartureDate() {
        NucleusFunctionSpaceBookingPace pace1 = buildBookingPace();
        NucleusFunctionSpaceBookingPace pace2 = buildBookingPace();

        pace1.setDepartureDate(new Date());

        assertFalse(pace1.isEquals(pace2));
        assertFalse(pace2.isEquals(pace1));
    }

    private NucleusFunctionSpaceBookingPace buildBookingPace() {
        NucleusFunctionSpaceBookingPace bookingPace = new NucleusFunctionSpaceBookingPace();
        bookingPace.setIntegrationType(IntegrationType.AHWS_SC);
        bookingPace.setPropertyId(PROPERTY_ID);
        bookingPace.setBookingId(BOOKING_ID);
        bookingPace.setGuestRoomStatus("Prospect");
        bookingPace.setCateringStatus("Definite");
        bookingPace.setBookingStatus("Tentative");
        bookingPace.setStayDate(DateTimeUtil.buildDate(2017, 10, 1));
        bookingPace.setChangeDate(DateTimeUtil.buildDate(2017, 2, 12, 12, 23, 34));
        bookingPace.setUpdateDate(DateTimeUtil.buildDate(2017, 5, 6, 7, 8, 9));
        bookingPace.setRoomNights(10);
        bookingPace.setRoomRevenue(BigDecimal.valueOf(11));
        bookingPace.setArrivalDate(DateTimeUtil.buildDate(2017, 10, 1));
        bookingPace.setDepartureDate(DateTimeUtil.buildDate(2017, 10, 1));
        return bookingPace;
    }
}
