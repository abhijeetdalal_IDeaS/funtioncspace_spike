package com.ideas.ngi.nucleus.data.functionspace.entity;

import com.ideas.ngi.functionspace.entity.NucleusFunctionSpaceObjectMother;
import com.ideas.ngi.nucleus.util.DateTimeUtil;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class NucleusFunctionSpaceEventPaceTest {

    @Test
    public void testGettersAndSetters() throws InvocationTargetException, IllegalAccessException {
        NucleusFunctionSpaceEventPace functionSpaceEventPace = NucleusFunctionSpaceObjectMother.buildNucleusFunctionSpaceEventPace();

        assertEquals(DateTimeUtil.buildDate(2019, 07, 14), functionSpaceEventPace.getChangeDate());
    }
}