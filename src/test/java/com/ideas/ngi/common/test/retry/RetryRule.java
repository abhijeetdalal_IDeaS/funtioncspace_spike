package com.ideas.ngi.common.test.retry;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RetryRule implements TestRule {
    private static final Logger LOGGER = LoggerFactory.getLogger(RetryRule.class);

    @Override
    public Statement apply(Statement base, Description description) {
        final Retry retry = description.getAnnotation(Retry.class);
        if (retry == null) {
            return base;
        } else {
            return statement(base, description, retry);
        }
    }

    private Statement statement(final Statement base, Description description, final Retry retry) {

        final int retryCount = retry.count() < 1 ? 1 : retry.count();
        LOGGER.debug("Executing test with {} retries", retryCount);

        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                Throwable caughtThrowable = null;

                // implement retry logic here
                for (int i = 0; i <= retryCount; i++) {
                    try {
                        base.evaluate();
                        return;
                    } catch (Throwable t) {
                        caughtThrowable = t;
                        //  System.out.println(": run " + (i+1) + " failed");
                        LOGGER.error(description.getDisplayName() + ": run " + (i + 1) + " failed.");
                    }
                }

                LOGGER.error(description.getDisplayName() + ": giving up after " + retryCount + " failures.");
                throw caughtThrowable;
            }
        };
    }
}
