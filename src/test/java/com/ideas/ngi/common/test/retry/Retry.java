package com.ideas.ngi.common.test.retry;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Method level annotation to retry a test a given number of times.  Must be used inconjunction with the
 * RetryRule TestRule
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Retry {

    /**
     * Number of times to retry the test
     */
    int count() default 1;

}
